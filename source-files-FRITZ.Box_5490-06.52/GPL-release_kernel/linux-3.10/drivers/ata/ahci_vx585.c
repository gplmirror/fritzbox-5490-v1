
/*
 *  ahci.c - AHCI SATA support
 *
 *  Maintained by:  Jeff Garzik <jgarzik@pobox.com>
 *    		    Please ALWAYS copy linux-ide@vger.kernel.org
 *		    on emails.
 *
 *  Copyright 2004-2005 Red Hat, Inc.
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * libata documentation is available via 'make {ps|pdf}docs',
 * as Documentation/DocBook/libata.*
 *
 * AHCI hardware documentation:
 * http://www.intel.com/technology/serialata/pdf/rev1_0.pdf
 * http://www.intel.com/technology/serialata/pdf/rev1_1.pdf
 *
 */

/**
 * Some part of this file is modified by Ikanos Communications. 
 *
 * Copyright (C) 2013-2014 Ikanos Communications.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/blkdev.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/device.h>
#include <linux/dmi.h>
#include <scsi/scsi_host.h>
#include <scsi/scsi_cmnd.h>
#include <linux/libata.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/clk.h>

#include "ahci.h"
#if CONFIG_FUSIV_SATA_EXTERNAL_CLOCK
#include <asm/mach-fusiv/vx185_scu.h>
#endif
#include<asm/mach-fusiv/vx585_int.h>


#define DRV_NAME	"ahci_vx585"
#define DRV_VERSION	"IKF_S_1.1"

enum {
	board_ahci_vx185	= 0,
};

int ata_plat_init (void __iomem *base);
extern void ata_plat_remove(struct ata_host *host);
static int ahci_init_one(struct platform_device *pdev);

static struct scsi_host_template ahci_sht = {
	ATA_NCQ_SHT(DRV_NAME),
	.can_queue		= AHCI_MAX_CMDS - 1,
	.sg_tablesize		= AHCI_MAX_SG,
	.dma_boundary		= AHCI_DMA_BOUNDARY,
	.shost_attrs		= ahci_shost_attrs,
	.sdev_attrs		= ahci_sdev_attrs,
};

static const struct ata_port_info ahci_port_info[] = {
	[board_ahci_vx185] =
	{
		.flags		= AHCI_FLAG_COMMON,
		.pio_mask	= ATA_PIO4,
		.udma_mask	= ATA_UDMA6,
		.port_ops	= &ahci_ops,
	},
};


#define MAX_COUNT 10000
void wait_done_from_serdes(void)
{
	volatile unsigned int cnt = 0;
	volatile unsigned int reg_val;
	volatile unsigned int i = 0;
	while (((*(volatile unsigned int *)0xB90EA014) & 0x20000000) == 0) {
		cnt++;
		if ((cnt % 1000) == 0) {
			i++;
			printk("waiting for wait_done_from_serdes transaction to complete\n");
			reg_val = (*(volatile unsigned int *)0xB90EA014);
			reg_val = (*(volatile unsigned int *)0xB90EA000);
		}
	}
	(*(volatile unsigned int *)0xB90EA014) = 0x20000000;
}

void read_reg_sata(unsigned int Addr, volatile unsigned int *Value)
{
    *Value = *(volatile unsigned int *)Addr;
}

void write_reg_sata(unsigned int Addr, unsigned int Value)
{
    *(volatile unsigned int *)Addr = Value;
}

int config_sata_serdes(void)
{
	volatile unsigned int read_data;
	volatile unsigned int wr_dat;
	unsigned int cnt;
	printk("sata serdes init starts\n");
	read_reg_sata(0xB90EA000, &read_data);
	read_data = read_data | 0x00002000 ;
	write_reg_sata(0xB90EA000, read_data);

	read_data = 0x8080C400 ;           // Enabling mux not taking values from pins. 32'hB5FE_3FFF
	write_reg_sata(0xB90EA010, read_data);

	write_reg_sata(0xB9000074, 0x00);
	msleep(1); 

	write_reg_sata(0xB9000074, 0x01);
	msleep(1);

	//dd write_reg_sata(0xB9000074, 0x01); //new

	wr_dat = 0xA0C90065;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes(); ///indirect access response check

	wr_dat = 0x80070066;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80070067;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80180068;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80180069;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8001006A;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8022006B;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80CF006C; ///this is for 1.5 GBPS for 6 GBPS it is only C
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80AA806F;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80008070;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80348071;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80C98072;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80078073;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80078074;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80188075;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80188076;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80038077;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80148078;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80108079;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8000807A;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8010807B;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8000807C;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF807D;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80CF807E;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F7807F;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80E18080;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F58081;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD8082;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD8083;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8084;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8085;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8086;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8087;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80E38088;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80E78089;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80DB808A;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F5808B;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD808C;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD808D;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F5808E;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F5808F;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8090;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF8091;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80E38092;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80E78093;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80DB8094;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F58095;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD8096;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FD8097;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F58098;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F58099;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF809A;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF809B;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80FF809C;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80F5809D;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x803F809E;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8000809F;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x803280A0;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800080A1;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800280A2;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800580A3;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800480A4;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800080A5;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800080A6;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800880A7;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800480A8;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800080A9;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800080AA;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800480AB;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x800380AC;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80EA0152;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80C20153;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	msleep(1);

	// SSC Settings, are for 6Gbps. May not be required at 1.5 Gbps.
	wr_dat = 0x8000014C;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80C0014D;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80FA014E;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x809B014F;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80F90150;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80090151;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80EB0152;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80110156;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x805A0157;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x800080A6;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x801C80A7;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
//
// // ===================================================================================================================== 	
// // Here we deviate from AMS sequence, as internal overwrite enables, BIST, PSTATE control, RXEQ settings were not used	
// // ===================================================================================================================== 	
//
	// IDDQ State, Make IPD pins 0, in the SERDES wrapper register
	read_reg_sata(0xB90EA008, &wr_dat);
	wr_dat &= 0xFFF8FFFF;
	write_reg_sata(0xB90EA008, wr_dat);
	
	// SYNTH and LANE Reset removal for SATA SERDES, from SCU Register	
	read_reg_sata(0xB9000074, &wr_dat);
	wr_dat |=  0x00000007;
	write_reg_sata(0xB9000074, wr_dat);
	
	// RXEQ Sttings. Values taken from spreadsheet - 0x57-Vpp diff swing, 0-db de-emphasis
	wr_dat = 0x80020015;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80000016;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80080017;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80780018;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80020019;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x800A80A3;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x800000FD;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x806A013A;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	// TX DRiver settings. 0.57-Vpp, 0-dB de-emphassis. Values taken from spreadsheet
	wr_dat = 0x80DB0012;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();
	
	wr_dat = 0x80020013;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80000014;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x801b000b;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x8033000C;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	wr_dat = 0x80DA8019;
	write_reg_sata(0xB90EA014, wr_dat);
	wait_done_from_serdes();

	read_reg_sata(0xB90EA008, &read_data);
	read_data = read_data | 0x00070000 ;           ///IPD_PCS_RX_L0 bit 16, IPD_PCS_SYNTH bit 17 and IPD_PCS_TX_L0 bit 18
	write_reg_sata(0xB90EA008, read_data);

	read_reg_sata(0xB90EA000, &read_data);
	read_data = read_data | 0x00000002 ;           ///ICTL_PCS_EN_NT bit 1
	write_reg_sata(0xB90EA000, read_data);

	read_reg_sata(0xB90EA004, &read_data);
	read_data = read_data | 0x00800000 ;           ///TX_ENABLE [23]
	write_reg_sata(0xB90EA004, read_data);

	read_reg_sata(0xB90EA01C, &read_data);
	cnt = 0;
	while(1) {
		if(read_data & 0x1){
			break;
		}
		cnt++;
		if((cnt % 1000) == 0) {
			printk("waiting for synth ready status\n");
		}
		if (cnt > MAX_COUNT) {
			printk("synth initialization failed!!!!!\n");
			return 0;
		}
		read_reg_sata(0xB90EA01C, &read_data);
	}

	read_reg_sata(0xB90EA01C, &read_data);
	cnt = 0;
	while(1) {
		if(read_data & 0x1000000){
			break;
		}
		cnt++;
		if((cnt % 1000) == 0) {
			printk("waiting for txdata path status\n");
		}
		if (cnt > MAX_COUNT) {
			printk("txdata path init failed!!!!!\n");
			return 0;
		}
		read_reg_sata(0xB90EA01C, &read_data);
	}

	read_reg_sata(0xB90EA01C, &read_data);
	cnt = 0;
	while(1) {
		if(read_data & 0x80000) {
			break;
		}
		cnt++;
		if((cnt % 1000) == 0) {
			printk("waiting for rxdata path status\n");
		}
		if (cnt > MAX_COUNT) {
			printk("rxdata path init failed!!!!!\n");
			return 0;
		}
		read_reg_sata(0xB90EA01C, &read_data);
	}

	return 1;
}

void hba_init(void)
{
	volatile unsigned int read_data;
	read_data = (*(volatile unsigned int *)0xB91500BC);
	read_data = read_data | 0x80000000;
	(*(volatile unsigned int *)0xB91500BC) = read_data; 
	(*(volatile unsigned int *)0xB91500BC) = 0x870E182B;

	read_data = (*(volatile unsigned int *)0xB91500A4);
	read_data = read_data | 0x1000;
	(*(volatile unsigned int *)0xB91500A4) = read_data;

}

/* ata_plat_init - Do platform specific init of the AHCI SATA controller.
 * @base - Base address of the AHCI SATA controller
 */
int ata_plat_init (void __iomem *base)
{
	volatile unsigned int ret, read_data,regval;
	/* VCC reset 18th bit is for SATA*/
        *(volatile unsigned int *)0xb9000200 = 0xFFFBFFFF;
        regval = *(volatile unsigned int *)0xb9000204;
        regval = regval | 0x00040000 ;
        *(volatile unsigned int *)0xb9000204 = regval;
        *(volatile unsigned int *)0xb9000200 = 0xFFFFFFFF;
        mdelay(3);
	/* VCC reset end */
	ret = config_sata_serdes();
	if(ret == 1) {
		printk("sata serdes init success\n");
		msleep(100);
		hba_init();
		msleep(100);
		read_data = 0x00;
		(*(volatile unsigned int *)0xB9158000) = read_data;
		return 0;
	} else {
		printk("!!!!! sata serdes init failed !!!!!\n");
	}
	return -1;
}

static int ahci_init_one(struct platform_device *pdev)
{
	unsigned int board_id = *(int *)pdev->dev.platform_data;
	struct ata_port_info pi = ahci_port_info[board_id];
	struct resource *res;
	static int printed_version;
	const struct ata_port_info *ppi[] = { &pi, NULL };
	struct device *dev = &pdev->dev;
	struct ahci_host_priv *hpriv;
	struct ata_host *host;
	int n_ports, i, rc,ret;
        const char *scc_s;


	WARN_ON((int)ATA_MAX_QUEUE > AHCI_MAX_CMDS);
	
	if (!printed_version++)
		ata_print_version_once(&pdev->dev, DRV_VERSION);

	printk("version " DRV_VERSION "\n");

	hpriv = devm_kzalloc(dev, sizeof(*hpriv), GFP_KERNEL);
	
	if (!hpriv)
		return -ENOMEM;
	hpriv->flags |= (unsigned long)pi.private_data;

	/*
	 * Get the register base first
	 */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res)
		return -EINVAL;

	hpriv->mmio = devm_ioremap(&pdev->dev, res->start,
				   res->end - res->start + 1);

	res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (!res) {
		dev_printk(KERN_INFO, &pdev->dev, ": no irq\n");
		return -ENODEV;
	}

	hpriv->irq = res->start;

	/* Call platform specific init*/
	ret=ata_plat_init(hpriv->mmio);
	if(ret != 0)
	{
		printk("ata_plat_init failed\n");
		return -ENODEV;
	}

	/* save initial config */
	ahci_save_initial_config(&pdev->dev, hpriv,0,0);

	/* prepare host */
	if (hpriv->cap & HOST_CAP_NCQ)
		pi.flags |= ATA_FLAG_NCQ | ATA_FLAG_FPDMA_AA;

	if (hpriv->cap & HOST_CAP_PMP)
		pi.flags |= ATA_FLAG_PMP;


	/* CAP.NP sometimes indicate the index of the last enabled
	 * port, at other times, that of the last possible port, so
	 * determining the maximum port number requires looking at
	 * both CAP.NP and port_map.
	 */
	n_ports = max(ahci_nr_ports(hpriv->cap), fls(hpriv->port_map));

	host = ata_host_alloc_pinfo(&pdev->dev, ppi, n_ports);
	if (!host)
		return -ENOMEM;
	host->iomap = hpriv->base;
	host->private_data = hpriv;

	if (!(hpriv->cap & HOST_CAP_SSS) || ahci_ignore_sss)
		host->flags |= ATA_HOST_PARALLEL_SCAN;
	else
		printk(KERN_INFO "ahci: SSS flag set, parallel bus scan disabled\n");

	if (pi.flags & ATA_FLAG_EM)
		ahci_reset_em(host);

	for (i = 0; i < host->n_ports; i++) {
		struct ata_port *ap = host->ports[i];

		/* disabled/not-implemented port */
		if (!(hpriv->port_map & (1 << i)))
			ap->ops = &ata_dummy_port_ops;
	}

	rc = ahci_reset_controller(host);
	if (rc)
		return rc;

	ahci_init_controller(host);
	scc_s="SATA";
	ahci_print_info(host,scc_s);
	host->ops = pi.port_ops;

	


	ret = ata_host_activate(host, hpriv->irq, ahci_interrupt, IRQF_SHARED,
				&ahci_sht);
    if(!ret) {
        irq_set_affinity(hpriv->irq,cpumask_of(SATA_CPU_NUM));
    }
				
    VPRINTK("EXIT\n");
	return ret;
}

static struct platform_driver ahci_platform_driver = {
	.probe			= ahci_init_one,
	.driver			= {
					.name = DRV_NAME,
					.owner = THIS_MODULE,
				}
};

static int __init ahci_init(void)
{
	volatile uint regval = 0;
	/* Latch on Reset 1: 0xb9000224: 13th bit: 1-USB3 : 0-SATA */
	/* If the board is not configured to use serDES1 as SATA then don't register the SATA host controller driver */
	regval = *(volatile unsigned int *)0xb9000224;
	if((regval & 0x00002000) != 0x0)
	{
		return -EINVAL;
	}
	return platform_driver_register(&ahci_platform_driver);
}

static void __exit ahci_exit(void)
{
	platform_driver_unregister(&ahci_platform_driver);
}


MODULE_AUTHOR("Jeff Garzik;Modified by Ikanos for BSP Support");
MODULE_DESCRIPTION("AHCI SATA low-level driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);

module_init(ahci_init);
module_exit(ahci_exit);
