/******************************************************************************
**
** FILE NAME    : ifxmips_asc_ar9.h
** PROJECT      : IFX UEIP
** MODULES      : ASC (UART)
**
** DATE         : 27 May 2009
** AUTHOR       : Xu Liang
** DESCRIPTION  : Global IFX ASC (UART) driver header file for AR9
** COPYRIGHT    :       Copyright (c) 2009
**                      Infineon Technologies AG
**                      Am Campeon 1-12, 85579 Neubiberg, Germany
**
**    This program is free software; you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation; either version 2 of the License, or
**    (at your option) any later version.
**
** HISTORY
** $Date        $Author         $Comment
** 27 May 2009   Xu Liang        The first UEIP release
*******************************************************************************/



#ifndef IFX_ASC_AR9_H
#define IFX_ASC_AR9_H



#define UART_NR                         2

#if defined(CONFIG_SERIAL_IFX_ASC_CONSOLE)
#  if defined(CONFIG_IFX_ASC_CONSOLE_ASC0)
#    define IFX_ASC_CONSOLE_INDEX       0
#  else
#    define IFX_ASC_CONSOLE_INDEX       1
#  endif
#endif



static ifx_asc_port_priv_t ifx_asc_port_priv[UART_NR] = {
    {
        .base               = (ifx_asc_reg_t*)IFX_ASC0,  
        .portwidth          = 32,
        .tir                = IFX_ASC0_TIR,
        .tbir               = IFX_ASC0_TBIR,
        .rir                = IFX_ASC0_RIR,
        .eir                = IFX_ASC0_EIR,
        .tx_irq_name        = "asc0_tx",
        .rx_irq_name        = "asc0_rx",
        .err_irq_name       = "asc0_err",
        .tx_irq_on          = 0,
        .rx_bytes           = 0,                          
        .rx_parity_error    = 0,                          
        .rx_frame_error     = 0,                          
        .rx_overrun_error   = 0,                          
        .tx_bytes           = 0,                          
        .baudrate           = 0,
    },
    {
        .base               = (ifx_asc_reg_t*)IFX_ASC1,  
        .portwidth          = 8,
        .tir                = IFX_ASC1_TIR,
        .tbir               = IFX_ASC1_TBIR,
        .rir                = IFX_ASC1_RIR,
        .eir                = IFX_ASC1_EIR,
        .tx_irq_name        = "asc1_tx",
        .rx_irq_name        = "asc1_rx",
        .err_irq_name       = "asc1_err",
        .tx_irq_on          = 0,
        .rx_bytes           = 0,                          
        .rx_parity_error    = 0,                          
        .rx_frame_error     = 0,                          
        .rx_overrun_error   = 0,                          
        .tx_bytes           = 0,                          
        .baudrate           = 0,
    }
};



#endif  //  IFX_ASC_AR9_H
