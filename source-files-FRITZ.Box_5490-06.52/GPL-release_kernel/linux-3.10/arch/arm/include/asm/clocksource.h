/* IA64-specific clocksource additions */

#ifndef _ASM_ARM_CLOCKSOURCE_H
#define _ASM_ARM_CLOCKSOURCE_H

struct arch_clocksource_data {
#if defined(CONFIG_CLKSRC_QCOM)
	void __iomem *base;
	void __iomem *cpu0_base;
	void __iomem *cpu1_base;
#endif
};

#endif /* _ASM_ARM_CLOCKSOURCE_H */
