/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config für FRITZ!Box Fon 7390
 *
\*------------------------------------------------------------------------------------------*/

#undef AVM_HARDWARE_CONFIG
#define AVM_HARDWARE_CONFIG  avm_hardware_config_hw156


struct _avm_hw_config AVM_HARDWARE_CONFIG[] = {


    /****************************************************************************************\
     *
     * GPIO Config
     *
    \****************************************************************************************/


    /*------------------------------------------------------------------------------------------*\
     * LEDs / Taster
    \*------------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_led_power",
        .value  = 24,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_power_red",
        .value  = 25,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_internet",
        .value  = 28,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_festnetz",
        .value  = 26,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_wlan",
        .value  = 27,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_info",
        .value  = 22,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_led_info_red",
        .value  = 23,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },

    /*--- DECT button connected to EXTIN 1 ---*/
    {
        .name   = "gpio_avm_button_dect", // "gpio_avm_button_power" für 3370
        .value  = 29,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
        }
    },
    /*--- WLAN button connected to EXTIN 0 ---*/
    {
        .name   = "gpio_avm_button_wlan",
        .value  = 31,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
        }
    },


    /*--------------------------------------------------------------------------------------*\
     * DECT
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_dect_reset", 
        .value  = 6,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_dect_uart_rx",
        .value  = 4,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_reg  = VX180_MODE_CONTROL_BASE,
            .func_set  = 1 << 4,
            .func_mask = 1 << 4,
            /*--- .dir    = GPIO_INPUT_PIN, ---*/
        }
    },
    {
        .name   = "gpio_avm_dect_uart_tx",
        .value  = 3,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_reg  = VX180_MODE_CONTROL_BASE,
            .func_set  = 1 << 4,
            .func_mask = 1 << 4,
            /*--- .dir    = GPIO_OUTPUT_PIN ---*/
        }
    },


    /*--------------------------------------------------------------------------------------*\
     * FPGA
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_piglet_noemif_prog",
        .value  = 14,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_piglet_noemif_clk",
        .value  = 11,
        .param  = avm_hw_param_gpio_out_active_high,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_piglet_noemif_data",
        .value  = 12,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_piglet_noemif_done",
        .value  = 8,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
        }
    },
    {
        .name   = "gpio_avm_piglet_irq",
        .value  =  5,
        .param  = avm_hw_param_gpio_in_active_high,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
        }
    },


    /*--------------------------------------------------------------------------------------*\
     * SPI
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_spi_flash_wp",
        .value  = 0,
        .param  = avm_hw_param_gpio_out_active_high,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_spi_flash_cs",
        .value  = 1,
        .param  = avm_hw_param_gpio_out_active_high,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_reg  = VX180_SYSTEM_CONTROL_BASE,
            .func_set  = 2 << 25,
            .func_mask = 3 << 25,
            /*--- .dir    = GPIO_OUTPUT_PIN ---*/
        }
    },
    {
        .name   = "gpio_avm_spi_dect_cs",
        .value  = 2,
        .param  = avm_hw_param_gpio_out_active_high,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_reg  = VX180_SYSTEM_CONTROL_BASE,
            .func_set  = 2 << 24,
            .func_mask = 3 << 24,
            /*--- .dir    = GPIO_OUTPUT_PIN ---*/
        }
    },

    /*--------------------------------------------------------------------------------------*\
     * NAND
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_nand_ale",
        .value  = 19,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_nand_cle",
        .value  = 18,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_nand_rd",
        .value  = 21,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_nand_rd_by",
        .value  = 7,/*???*/
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
        }
    },
    {
        .name   = "gpio_avm_nand_wp",
        .value  = 20,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_nand_cs1",
        .value  = 16,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_nand_cs2",
        .value  = 17,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },


    /*--------------------------------------------------------------------------------------*\
     * ETHERNET
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_extphy1_reset",
        .value  = 15,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN
        }
    },
    {
        .name   = "gpio_avm_extphy_int",
        .value  = 9,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
           .dir    = GPIO_INPUT_PIN,
        }
    },


    {   .name   = NULL }
};
EXPORT_SYMBOL(AVM_HARDWARE_CONFIG);


