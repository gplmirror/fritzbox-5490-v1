/*
 *  Atheros AR71XX/AR724X/AR913X specific prom routines
 *
 *  Copyright (C) 2008-2010 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/string.h>

#include <linux/mm.h>
#include <linux/bootmem.h>
#include <linux/env.h>

#include <linux/of_fdt.h>

#include <asm/bootinfo.h>
#include <asm/addrspace.h>
#include <asm/traps.h>
#include <asm/cacheflush.h>
#include <asm/prom.h>

#include "common.h"

#ifdef CONFIG_EARLY_PRINTK
extern void serial_print(char *fmt, ...) ;
#else
#define serial_print(fmt, ...)
#endif

/*--- #define DEBUG_ATH_PROM ---*/

#if defined(DEBUG_ATH_PROM)
#define ath_prom_print(arg...)      serial_print(arg)
#else
#define ath_prom_print(arg...)   
#endif

void prom_printf (const char *fmt, ...) {
    va_list args;

    va_start (args, fmt);
    ath_prom_print (fmt, args);
    va_end (args);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static __init void ath79_prom_init_cmdline(int argc, char **argv) {

	int i;

    serial_print("\n");     /*--- damit es schön aussieht ---*/
    ath_prom_print("[%s]:\n", __func__);
    ath_prom_print("[%s]: argv[0] = '%s'\n", __func__, argv[0]);
    ath_prom_print("[%s]: argv[1] = '%s'\n", __func__, argv[1]);
    ath_prom_print("[%s]: argv[2] = '%s'\n", __func__, argv[2]);

#if 0
	if (!is_valid_ram_addr(argv))
		return;
#endif

	for (i = 0; i < argc; i++) {
#if 0
		if (is_valid_ram_addr(argv[i])) {
#endif
			strlcat(arcs_cmdline, " ", sizeof(arcs_cmdline));
			strlcat(arcs_cmdline, argv[i], sizeof(arcs_cmdline));
#if 0
		}
#endif
    }

    ath_prom_print("[%s]:commandline = '%s'\n", __FUNCTION__, arcs_cmdline);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __init mips_nmi_setup(void) {

	void *base;
	extern char except_vec_nmi;

	base = cpu_has_veic ?
		(void *)(CAC_BASE + 0xa80) :
		(void *)(CAC_BASE + 0x380);
	memcpy(base, &except_vec_nmi, 0x80);
    printk(KERN_ERR "[%s] setup NMI vector to base 0x%p\n", __FUNCTION__, base);
	flush_icache_range((unsigned long)base, (unsigned long)base + 0x80);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init device_tree_init(void)
{
	unsigned long base, size;

	if (!initial_boot_params)
		return;

	base = virt_to_phys((void *)initial_boot_params);
	size = be32_to_cpu(initial_boot_params->totalsize);

	/* Before we do anything, lets reserve the dt blob */
	reserve_bootmem(base, size, BOOTMEM_DEFAULT);

	unflatten_device_tree();
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init prom_init(void) {

    ath_prom_print("[%s]:\n", __func__);
	ath79_prom_init_cmdline(fw_arg0, (char **)fw_arg1);
    ath_prom_print("[%s]: call env_init\n", __func__);
    env_init((int *)fw_arg2, ENV_LOCATION_FLASH);

    ath_prom_print("[%s]: call set_wlan_dect_config_address\n", __func__);
    set_wlan_dect_config_address((unsigned int *)fw_arg3);
    mips_machtype  = MACH_GROUP_ATHEROS;

	board_nmi_handler_setup = mips_nmi_setup;

    ath_prom_print("[%s]: done\n", __func__);
}

void __init prom_free_prom_memory(void)
{
	/* We do not have to prom memory to free */
}

