/*
 *  Atheros AR71XX/AR724X/AR913X common devices
 *
 *  Copyright (C) 2008-2011 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  Parts of this file are based on Atheros' 2.6.15 BSP
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/serial_8250.h>
#include <linux/clk.h>
#include <linux/err.h>

#include <asm/time.h>		/* for mips_hpt_frequency */
#include <asm/mach-ath79/ath79.h>
#include <atheros.h>
#include <asm/mach-ath79/ar933x_uart_platform.h>
#include "common.h"
#include "dev-common.h"

static struct resource ath79_uart_resources[] = {
	{
		.start	= ATH_UART_BASE,
		.end	= ATH_UART_BASE + ATH_UART_SIZE - 1,
		.flags	= IORESOURCE_MEM,
	},
};

#define AR71XX_UART_FLAGS (UPF_BOOT_AUTOCONF | UPF_SKIP_TEST | UPF_IOREMAP)
static struct plat_serial8250_port ath79_uart_data[] = {
	{
		.mapbase	= ATH_UART_BASE,
		.irq		= ATH79_MISC_IRQ(3),
		.flags		= AR71XX_UART_FLAGS,
		.iotype		= UPIO_MEM32,
		.regshift	= 2,
	}, {
		/* terminating entry */
	}
};

static struct platform_device ath79_uart_device = {
	.name		= "serial8250",
	.id		= PLAT8250_DEV_PLATFORM,
	.resource	= ath79_uart_resources,
	.num_resources	= ARRAY_SIZE(ath79_uart_resources),
	.dev = {
		.platform_data	= ath79_uart_data
	},
};

#if (defined CONFIG_SOC_AR933X)
static struct resource ar933x_uart_resources[] = {
	{
		.start	= AR933X_UART_BASE,
		.end	= AR933X_UART_BASE + AR71XX_UART_SIZE - 1,
		.flags	= IORESOURCE_MEM,
	},
	{
		.start	= ATH79_MISC_IRQ(3),
		.end	= ATH79_MISC_IRQ(3),
		.flags	= IORESOURCE_IRQ,
	},
};

static struct ar933x_uart_platform_data ar933x_uart_data;
static struct platform_device ar933x_uart_device = {
	.name		= "ar933x-uart",
	.id		= -1,
	.resource	= ar933x_uart_resources,
	.num_resources	= ARRAY_SIZE(ar933x_uart_resources),
	.dev = {
		.platform_data	= &ar933x_uart_data,
	},
};
#endif

void __init ath79_register_uart(void)
{
	struct clk *clk;

	clk = clk_get(NULL, "uart");
	if (IS_ERR(clk)) {
		panic("unable to get UART clock, err=%ld", PTR_ERR(clk));
    }

	if (soc_is_ar71xx() ||
	    soc_is_ar724x() ||
	    soc_is_ar913x() ||
	    soc_is_ar934x() ||
	    soc_is_qca953x() ||
	    soc_is_qca955x() ||
        soc_is_qca956x()) 
    {
		ath79_uart_data[0].uartclk = clk_get_rate(clk);
		platform_device_register(&ath79_uart_device);
    }
#if (defined CONFIG_SOC_AR933X)
    else
    if (soc_is_ar933x ()) {
		ar933x_uart_data.uartclk = clk_get_rate(clk);
		platform_device_register(&ar933x_uart_device);
    }
#endif
    else {
        BUG ();
    }
}

void __init ath79_register_wdt(void)
{
	struct resource res;

	memset(&res, 0, sizeof(res));

	res.flags = IORESOURCE_MEM;
	res.start = ATH_RESET_BASE + ATH_RESET_REG_WDOG_CTRL;
	res.end = res.start + 0x8 - 1;

	platform_device_register_simple("ath79-wdt", -1, &res, 1);
}


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/uaccess.h>
#define M_PERFCTL_EVENT(event)          ((event) << 5)
unsigned int clocks_at_start;

void start_cntrs(unsigned int event0, unsigned int event1)
{
	write_c0_perfcntr0(0x00000000);
	write_c0_perfcntr1(0x00000000);
	/*
	 * go...
	 */
	write_c0_perfctrl0(0x80000000 | M_PERFCTL_EVENT(event0) | 0xf);
	write_c0_perfctrl1(0x00000000 | M_PERFCTL_EVENT(event1) | 0xf);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void stop_cntrs(void)
{
	write_c0_perfctrl0(0);
	write_c0_perfctrl1(0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void read_cntrs(unsigned int *c0, unsigned int *c1)
{
	*c0 = read_c0_perfcntr0();
	*c1 = read_c0_perfcntr1();
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ath_ioc_open(struct inode *inode, struct file *file)
{
	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t ath_ioc_read(struct file *file, char *buf, size_t count, loff_t * ppos)
{
#ifdef CONFIG_ATH_HS_UART
	extern void ath_hs_uart_init(void);
#endif /* CONFIG_ATH_HS_UART */

	unsigned int c0, c1, ticks = (read_c0_count() - clocks_at_start);
	char str[256];
	unsigned int secs = ticks / mips_hpt_frequency;

	read_cntrs(&c0, &c1);
	stop_cntrs();
	sprintf(str, "%d secs (%#x) event0:%#x event1:%#x", secs, ticks, c0, c1);
	copy_to_user(buf, str, strlen(str));

	return (strlen(str));
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t ath_ioc_write(struct file *file, const char *buf, size_t count,
		 loff_t * ppos)
{
	int event0, event1;

	sscanf(buf, "%d:%d", &event0, &event1);
	printk("\nevent0 %d event1 %d\n", event0, event1);

	clocks_at_start = read_c0_count();
	start_cntrs(event0, event1);

	return (count);
}

struct file_operations ath_ioc_fops = {
      open:ath_ioc_open,
      read:ath_ioc_read,
      write:ath_ioc_write,
};

/*------------------------------------------------------------------------------------------*\
 * General purpose ioctl i/f
\*------------------------------------------------------------------------------------------*/
static int __init ath_init_ioc(void) {

	static int _mymajor;

	_mymajor = register_chrdev(77, "ATH_GPIOC", &ath_ioc_fops);

	if (_mymajor < 0) {
		printk("Failed to register GPIOC\n");
		return _mymajor;
	}

	printk("ATH GPIOC major %d\n", _mymajor);
	return 0;
}

device_initcall(ath_init_ioc);


