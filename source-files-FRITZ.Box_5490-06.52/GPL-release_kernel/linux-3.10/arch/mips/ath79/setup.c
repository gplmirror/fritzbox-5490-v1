/*
 *  Atheros AR71XX/AR724X/AR913X specific setup
 *
 *  Copyright (C) 2010-2011 Jaiganesh Narayanan <jnarayanan@atheros.com>
 *  Copyright (C) 2008-2011 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  Parts of this file are based on Atheros' 2.6.15/2.6.31 BSP
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/bootmem.h>
#include <linux/err.h>
#include <linux/clk.h>

#include <asm/bootinfo.h>
#include <asm/idle.h>
#include <asm/time.h>		/* for mips_hpt_frequency */
#include <asm/reboot.h>		/* for _machine_{restart,halt} */
#include <asm/mips_machine.h>
#include <asm/traps.h>
#include <asm/prom.h>

#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/serial_reg.h>
#include <linux/serial_8250.h>
#include <linux/console.h>
#include <linux/types.h>
#include <linux/string.h>

#include <linux/avm_kernel_config.h>

#include <asm/mach-ath79/atheros.h>
#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include "common.h"
#include "dev-common.h"
#include "machtypes.h"

#define ATH79_SYS_TYPE_LEN	64

#ifdef CONFIG_EARLY_PRINTK
void serial_print(const char *fmt, ...);
#else
#define serial_print(fmt, ...)
#endif

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
extern void ath_savelog_to_ram(void);
extern void set_reboot_status_to_NMI(void);
extern void set_reboot_status_to_NMI_WA(void);
extern void set_reboot_status_to_BusError(void);
extern void set_reboot_status_to_SoftReboot(void);

extern void qca955x_setup(void);
extern void ar934x_setup(void);
extern void ar724x_setup(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static char ath79_sys_type[ATH79_SYS_TYPE_LEN];

void ath_restart(char *command) {
    unsigned int val = (1<<0);

    if (command) {
        if ( ! strncmp(command, "watchdog", sizeof("watchdog") - 1)) {
            val = (1<<1);
            set_reboot_status_to_NMI();
            ath_savelog_to_ram();
        } else if ( ! strncmp(command, "nmi_workaround", sizeof("nmi_workaround") - 1)) {
            val = (1<<2);
            set_reboot_status_to_NMI_WA();
            ath_savelog_to_ram();
        } else if ( ! strncmp(command, "bus_error", sizeof("bus_error") - 1)) {
            set_reboot_status_to_BusError();
            ath_savelog_to_ram();
        }
    } else {
        set_reboot_status_to_SoftReboot();
        if(oops_in_progress) {
            /*--- wir kommen aus panic() ---*/
            ath_savelog_to_ram();
        }
    }
#if !defined(CONFIG_SOC_AR724X)
    ath_reg_wr(ATH_RESET_BASE + ATH_RESET_REG_STICKY, val);   /*--- save-Rebootstatus ---*/
#endif/*--- #if !defined(CONFIG_SOC_AR724X) ---*/

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_NONE);
    /*--- SPI-Mode ausschalten ---*/
    while(ath_reg_rd(ATH_SPI_FS) == 1) {
        printk("%s:spi-gpio-mode %x %x\n", __func__, ath_reg_rd(ATH_SPI_FS), ath_reg_rd(ATH_SPI_CLOCK));
        ath_reg_wr(ATH_SPI_FS, 0);
    }
    printk("%s:reset %x %x\n", __func__, ath_reg_rd(ATH_SPI_FS), ath_reg_rd(ATH_SPI_CLOCK));
    mdelay(500);
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

	ath79_device_reset_set(ATH_RESET_FULL_CHIP);
   	for (;;) {
		if (cpu_wait)
			cpu_wait();
    }
}
EXPORT_SYMBOL(ath_restart);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void ath79_halt(void)
{
	while (1)
		cpu_wait();
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __init ath79_detect_sys_type(void)
{
	char *chip = "????";
	u32 id;
	u32 major;
	u32 minor;
	u32 rev = 0;
    
    (void) minor;

	id = ath79_reset_rr (ATH_RESET_REG_REVISION_ID);
	major = id & ATH_RESET_REVISION_ID_MAJOR_MASK;

	switch (major) {
#if defined CONFIG_SOC_AR71XX
	case REV_ID_MAJOR_AR71XX:
		minor = id & AR71XX_REV_ID_MINOR_MASK;
		rev = id >> AR71XX_REV_ID_REVISION_SHIFT;
		rev &= AR71XX_REV_ID_REVISION_MASK;
		switch (minor) {
		case AR71XX_REV_ID_MINOR_AR7130:
			ath79_soc = ATH79_SOC_AR7130;
			chip = "7130";
			break;

		case AR71XX_REV_ID_MINOR_AR7141:
			ath79_soc = ATH79_SOC_AR7141;
			chip = "7141";
			break;

		case AR71XX_REV_ID_MINOR_AR7161:
			ath79_soc = ATH79_SOC_AR7161;
			chip = "7161";
			break;
		}
		break;
#endif

#if defined CONFIG_SOC_AR724X
	case REV_ID_MAJOR_AR7240:
		ath79_soc = ATH79_SOC_AR7240;
		chip = "7240";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR7241:
		ath79_soc = ATH79_SOC_AR7241;
		chip = "7241";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR7242:
		ath79_soc = ATH79_SOC_AR7242;
		chip = "7242";
		rev = id & AR724X_REV_ID_REVISION_MASK;
		break;
#endif

#if defined CONFIG_SOC_AR913X
	case REV_ID_MAJOR_AR913X:
		minor = id & AR913X_REV_ID_MINOR_MASK;
		rev = id >> AR913X_REV_ID_REVISION_SHIFT;
		rev &= AR913X_REV_ID_REVISION_MASK;
		switch (minor) {
		case AR913X_REV_ID_MINOR_AR9130:
			ath79_soc = ATH79_SOC_AR9130;
			chip = "9130";
			break;

		case AR913X_REV_ID_MINOR_AR9132:
			ath79_soc = ATH79_SOC_AR9132;
			chip = "9132";
			break;
		}
		break;
#endif

#if defined CONFIG_SOC_AR933X
	case REV_ID_MAJOR_AR9330:
		ath79_soc = ATH79_SOC_AR9330;
		chip = "9330";
		rev = id & AR933X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9331:
		ath79_soc = ATH79_SOC_AR9331;
		chip = "9331";
		rev = id & AR933X_REV_ID_REVISION_MASK;
		break;
#endif

#if defined CONFIG_SOC_AR934X
	case REV_ID_MAJOR_AR9341:
		ath79_soc = ATH79_SOC_AR9341;
		chip = "9341";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9342:
		ath79_soc = ATH79_SOC_AR9342;
		chip = "9342";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_AR9344:
		ath79_soc = ATH79_SOC_AR9344;
		chip = "9344";
		rev = id & AR934X_REV_ID_REVISION_MASK;
		break;
#endif

#if defined CONFIG_SOC_QCA953X
    case QCA9531_REV_ID_MAJOR:  // GJu: Honey Bee
		ath79_soc = ATH79_SOC_QCA9531;
		chip = "9531";
		rev = id & QCA953X_MASK__RESET__RST_REVISION_ID__VALUE;
		break;
#endif

#if defined CONFIG_SOC_QCA955X
	case REV_ID_MAJOR_QCA9556:
		ath79_soc = ATH79_SOC_QCA9556;
		chip = "9556";
		rev = id & QCA955X_REV_ID_REVISION_MASK;
		break;

	case REV_ID_MAJOR_QCA9558:
		ath79_soc = ATH79_SOC_QCA9558;
		chip = "9558";
		rev = id & QCA955X_REV_ID_REVISION_MASK;
		break;
#endif

#if defined CONFIG_SOC_QCA956X
    case QCA956X_REV_ID_MAJOR:  // GJu: Dragonfly
		ath79_soc = ATH79_SOC_QCA956X;
		chip = "956x";
		rev = id & QCA956X_MASK__RESET__RST_REVISION_ID__VALUE;
		break;
#endif

	default:
		panic("ath79: unknown SoC, id:0x%08x", id);

        break;
	}

	ath79_soc_rev = rev;

	if (soc_is_qca953x() || soc_is_qca955x() || soc_is_qca956x())
		sprintf(ath79_sys_type, "Qualcomm Atheros QCA%s rev %u", chip, rev);
	else
		sprintf(ath79_sys_type, "Atheros AR%s rev %u", chip, rev);
	pr_info("SoC: %s\n", ath79_sys_type);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
const char *get_system_type(void)
{
	return ath79_sys_type;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int __cpuinit get_c0_compare_int(void)
{
	return CP0_LEGACY_COMPARE_IRQ;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int ath79_be_handler(struct pt_regs *regs, int is_fixup)
{
#ifdef CONFIG_SOC_AR934X
	printk("ath data bus error: cause %#x epc %#lx\nrebooting...", read_c0_cause(), read_c0_epc());
	ath_restart("bus_error");
#else
	printk("ath data bus error: cause %#x\n", read_c0_cause());
#endif
	return (is_fixup ? MIPS_BE_FIXUP : MIPS_BE_FATAL);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init plat_device_tree_setup(void) {

#if defined CONFIG_AVM_ENHANCED

    struct boot_param_header *dtb;

		char *subrev_str;
		int subrev = 0;
    
		subrev_str = prom_getenv("HWSubRevision");
		if (subrev_str) {
			if (sscanf(subrev_str, "%u", &subrev) != 1)
				subrev_str = NULL;
		}
		if (!subrev_str) {
			prom_printf("%s: Unable to read AVM hardware "
				 "subrevision! Identity crisis... who am I?",
				 __func__);
		}

		prom_printf("%s: AVM hardware subrevision %d\n", __func__,
			 subrev);

		if (subrev > avm_subrev_max) {
			prom_printf("%s: Too many hardware subrevisions!\n", __func__);
			panic("%s: Too many hardware subrevisions!\n", __func__);
		}

		dtb = (struct boot_param_header *)avm_kernel_config_device_tree[subrev];

		if (!dtb) {  /* fallback auf subrev == 0 */
		    dtb = (struct boot_param_header *)avm_kernel_config_device_tree[0];
			prom_printf("%s: Fallback device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
        }

		if (!dtb) {
			prom_printf("%s: Missing device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
			panic("%s: Missing device-tree for AVM hardware "
				 "subrevision %d\n", __func__, subrev);
		} else {
            extern struct boot_param_header *initial_boot_params;
            initial_boot_params = dtb;
            prom_printf("DT: %02x %02x %02x %02x %02x %02x %02x %02x\n",
                    ((unsigned char *)dtb)[0],
                    ((unsigned char *)dtb)[1],
                    ((unsigned char *)dtb)[2],
                    ((unsigned char *)dtb)[3],
                    ((unsigned char *)dtb)[4],
                    ((unsigned char *)dtb)[5],
                    ((unsigned char *)dtb)[6],
                    ((unsigned char *)dtb)[7]);
        }

	__dt_setup_arch(dtb);

#endif
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init plat_mem_setup(void) {

    char *s, *p;
    unsigned int memsize = 0;

	board_be_handler = ath79_be_handler;
	_machine_restart = ath_restart;
	_machine_halt = ath79_halt;
	pm_power_off = ath79_halt;

	set_io_port_base(KSEG1);

	ath79_reset_base = ioremap_nocache(ATH_RESET_BASE   , ATH_RESET_SIZE   );
	ath79_pll_base   = ioremap_nocache(ATH_PLL_BASE     , ATH_PLL_SIZE     );
	ath79_ddr_base   = ioremap_nocache(ATH_DDR_CTRL_BASE, ATH_DDR_CTRL_SIZE);

	ath79_detect_sys_type();

#if 0
	detect_memory_region(0, ATH79_MEM_SIZE_MIN, ATH79_MEM_SIZE_MAX);
#else
    s = prom_getenv("memsize");
    if (s) {
        memsize = simple_strtoul(s, &p, 16);
    } else {
        serial_print("[%s] memsize konnte nicht aus env ermittelt werden\n", __func__);
        BUG_ON(1);
    }

    printk("[%s] memsize 0x%x\n", __func__, memsize);

    add_memory_region(PHYS_OFFSET, memsize, BOOT_MEM_RAM);
#endif

	ath79_clocks_init();

}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init plat_time_init(void)
{
	struct clk *clk;

	clk = clk_get(NULL, "cpu");
	if (IS_ERR(clk))
		panic("unable to get CPU clock, err=%ld", PTR_ERR(clk));

	mips_hpt_frequency = clk_get_rate(clk) / 2;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init ath79_setup(void)
{
	/*--- ath79_gpio_init(); ---*/
	ath79_register_uart();
	ath79_register_wdt();

	mips_machine_setup();

#if defined(CONFIG_SOC_QCA955X)
    qca955x_setup();
#elif defined(CONFIG_SOC_AR934X)
    ar934x_setup();
#elif defined(CONFIG_SOC_AR724X)
    ar724x_setup();
#endif

	return 0;
}

arch_initcall(ath79_setup);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MIPS_MACHINE)
static void __init ath79_generic_init(void)
{
	/* Nothing to do */
}

MIPS_MACHINE(ATH79_MACH_GENERIC,
	     "Generic",
	     "Generic AR71XX/AR724X/AR913X based board",
	     ath79_generic_init);
#endif
