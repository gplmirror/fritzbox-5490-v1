/*
 * Qualcomm Atheros AP136 reference board support
 *
 * Copyright (c) 2012 Qualcomm Atheros
 * Copyright (c) 2012-2013 Gabor Juhos <juhosg@openwrt.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <linux/pci.h>
#include <linux/delay.h>

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include <atheros.h>
#include <atheros_gpio.h>
#include <linux/avm_hw_config.h>

#include "dev-spi.h"
#include "dev-usb.h"
#include "dev-wmac.h"
#include "pci.h"

static struct spi_board_info ar934x_spi_info[] = {
	{
		.bus_num	    = 0,
		.chip_select	= 0,
		.max_speed_hz	= 25000000,
		.modalias	    = "mx25l6405d",
	}
};

static struct ath79_spi_platform_data ar934x_spi_data = {
	.bus_num	= 0,
	.num_chipselect	= 1,
};

#ifdef CONFIG_PCI
static __init void ar934x_pci_pll_init(void) {

	/*----------------------------------------------------------------------*\
	 * Initialize PCIE PLL and get it out of RESET
	 * reg = 0x18050010
	\*----------------------------------------------------------------------*/
	// common for rc1 and rc2
	ath_reg_wr_nf(AR71XX_PLL_BASE + QCA955X_PLL_PCIE_PLL_DITHER_DIV_MAX,
		PCIE_PLL_DITHER_DIV_MAX_EN_DITHER_SET(0x1) |
		PCIE_PLL_DITHER_DIV_MAX_USE_MAX_SET(0x1) |
		PCIE_PLL_DITHER_DIV_MAX_DIV_MAX_INT_SET(0x14) |
		PCIE_PLL_DITHER_DIV_MAX_DIV_MAX_FRAC_SET(0x3ff));

	ath_reg_wr_nf(AR71XX_PLL_BASE + QCA955X_PLL_PCIE_PLL_DITHER_DIV_MIN, PCIE_PLL_DITHER_DIV_MIN_DIV_MIN_INT_SET(0x14));

	ath_reg_wr_nf(AR71XX_PLL_BASE + QCA955X_PLL_PCIE_PLL_CONFIG,
		PCIE_PLL_CONFIG_REFDIV_SET(1) |
		PCIE_PLL_CONFIG_BYPASS_SET(1) |
		PCIE_PLL_CONFIG_PLLPWD_SET(1));
	mdelay(10);

	ath_reg_rmw_clear(AR71XX_PLL_BASE + QCA955X_PLL_PCIE_PLL_CONFIG, PCIE_PLL_CONFIG_PLLPWD_SET(1));
	mdelay(1);
	ath_reg_rmw_clear(AR71XX_PLL_BASE + QCA955X_PLL_PCIE_PLL_CONFIG, PCIE_PLL_CONFIG_BYPASS_SET(1));

}
#endif /* CONFIG_PCI */

void __init ar934x_setup(void) {

    unsigned int HWRevision = 0;
    char *s;

    s = prom_getenv("HWRevision");
    if (s) {
        HWRevision = simple_strtoul(s, NULL, 10);
    }

    switch (HWRevision) {
        case 190:
        case 201:
        case 194:
        case 207:
            /*--- printk(KERN_ERR "{%s} HWRevision %d\n", __func__, HWRevision); ---*/
            ath_reg_wr(0x18116c40, 0x633c8166);
            ath_reg_wr(0x18116c44, 0x10280000); /* AR_PHY_PMU2 */ /*--- nun doch mit 1.8V 30.09.14 ---*/

            break;
        default:
            break;
    }

	ath79_register_spi(&ar934x_spi_data, ar934x_spi_info, ARRAY_SIZE(ar934x_spi_info));
	ath79_register_usb();
	/*--- ath79_register_wmac(art + AP136_WMAC_CALDATA_OFFSET); ---*/
#ifdef CONFIG_PCI
	ar934x_pci_pll_init();
	ath79_register_pci();
#else
    printk("[%s] disable PCIe\n", __func__);
    ath_reg_wr(ATH_PCIE_PHY_REG1,    0x1061060e);     /*--- change PCIE to reduce power about 18mA ---*/
    ath_reg_wr(ATH_PCIE_EP_PHY_REG1, 0x1061060e);     /*--- change PCIE to reduce power about 18mA ---*/

	ath_reg_wr_nf(ATH_PLL_BASE + PCIE_PLL_CONFIG_OFFSET,
		PCIE_PLL_CONFIG_REFDIV_SET(1) |
		PCIE_PLL_CONFIG_BYPASS_SET(1) |
		PCIE_PLL_CONFIG_PLLPWD_SET(1));
#endif
}


