/******************************************************************************
**
** FILE NAME    : ifxmips_vr9_clk.c
** PROJECT      : UEIP
** MODULES     	: CGU
**
** DATE         : 19 JUL 2005
** AUTHOR       : Xu Liang
** DESCRIPTION  : Clock Generation Unit (CGU) Driver
** COPYRIGHT    : 	Copyright (c) 2006
**			Infineon Technologies AG
**			Am Campeon 1-12, 85579 Neubiberg, Germany
**
**    This program is free software; you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation; either version 2 of the License, or
**    (at your option) any later version.
**
** HISTORY
** $Date        $Author         $Comment
** 19 JUL 2005  Xu Liang        Initiate Version
** 21 AUG 2006  Xu Liang        Work around to fix calculation error for 36M
**                              crystal.
** 23 OCT 2006  Xu Liang        Add GPL header.
** 28 Sept 2007 Teh Kok How	Use kernel interface for 64-bit arithmetics
** 28 May 2009  Huang Xiaogang  The first UEIP release
*******************************************************************************/
/*! 
  \file ifxmips_vr9_clk.c
  \ingroup IFX_CGU
  \brief This file contains Clock Generation Unit driver
*/
	
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/irq.h>
#include <asm/div64.h>
#include <asm/time.h>
#include <asm/r4kcache.h>
#include <asm/cevt-r4k.h>
#include <ifx_regs.h>
#include <ifx_clk.h>
#include <common_routines.h>
#ifdef CONFIG_USE_EMULATOR
#include <ar10/emulation.h>
#endif

#define ENABLE_ICACHE_LOCK
#define ENABLE_SET_CLOCK_PROFILING

#define BARRIER __asm__ __volatile__(".set noreorder\n\t" \
                                     "nop; nop; nop; nop; nop; nop; nop; nop; nop;\n\t" \
                                     "nop; nop; nop; nop; nop; nop; nop; nop; sync;\n\t" \
                                     ".set reorder\n\t")
/*
 * MPS SRAM Base Address
 */
#define MBX_BASEADDRESS			0xBF200000
#define MPS_MEM_SEG_DATASIZE 		512

/*
 *  Frequency of Clock Direct Feed from The Analog Line Driver Chip
 */
#define BASIC_INPUT_CLOCK_FREQUENCY     36000000

/*
 *  CGU PLL0 Configure Register
 */
#define CGU_PLL0_PLL_BP                 (*IFX_CGU_PLL0_CFG & (1 << 30))
#define CGU_PLL1_FMOD_S                 GET_BITS(*IFX_CGU_PLL0_CFG, 23, 22)
#define CGU_PLL0_PS_2_EN                (*IFX_CGU_PLL0_CFG & (1 << 21))
#define CGU_PLL0_PS_1_EN                (*IFX_CGU_PLL0_CFG & (1 << 20))
#define CGU_PLL0_CFG_PLLN               GET_BITS(*IFX_CGU_PLL0_CFG, 13, 6)
#define CGU_PLL0_CFG_PLLM               GET_BITS(*IFX_CGU_PLL0_CFG, 5, 2)
#define CGU_PLL0_CFG_PLLL               (*IFX_CGU_PLL0_CFG & (1 << 1))
#define CGU_PLL0_CFG_PLL_EN             (*IFX_CGU_PLL0_CFG & (1 << 0))

/*
 *  CGU PLL1 Configure Register
 */
#define CGU_PLL1_PHDIV_EN               (*IFX_CGU_PLL1_CFG & (1 << 31))
#define CGU_PLL1_PLL_BP                 (*IFX_CGU_PLL1_CFG & (1 << 30))
#define CGU_PLL1_CFG_CTEN               (*IFX_CGU_PLL1_CFG & (1 << 29))
#define CGU_PLL1_CFG_DSMSEL             (*IFX_CGU_PLL1_CFG & (1 << 28))
#define CGU_PLL1_CFG_FRAC_EN            (*IFX_CGU_PLL1_CFG & (1 << 27))
#define CGU_PLL1_CFG_PLLK_HI            GET_BITS(*IFX_CGU_PLL1_CFG, 26, 17)
#define CGU_PLL2_CFG_PLL1_K_LO          GET_BITS(*IFX_CGU_PLL2_CFG, 29, 20)
#define CGU_PLL1_CFG_PLLD               GET_BITS(*IFX_CGU_PLL1_CFG, 16, 13)
#define CGU_PLL1_CFG_PLLN               GET_BITS(*IFX_CGU_PLL1_CFG, 12, 6)
#define CGU_PLL1_CFG_PLLM               GET_BITS(*IFX_CGU_PLL1_CFG, 5, 2)
#define CGU_PLL1_CFG_PLLL               (*IFX_CGU_PLL1_CFG & (1 << 1))
#define CGU_PLL1_CFG_PLL_EN             (*IFX_CGU_PLL1_CFG & (1 << 0))

/*
 *  CGU PLL2 Configure/Status Register
 */
#define CGU_PLL2_CFG_PLL_BP               (*IFX_CGU_PLL2_CFG & (1 << 30))
#define CGU_PLL2_CFG_PLL1_K               GET_BITS(*IFX_CGU_PLL2_CFG, 29, 20)
#define CGU_PLL2_CFG_PLLN                 GET_BITS(*IFX_CGU_PLL2_CFG, 13, 6)
#define CGU_PLL2_CFG_PLLM                 GET_BITS(*IFX_CGU_PLL2_CFG, 5, 2)
#define CGU_PLL2_CFG_PLLL                 (*IFX_CGU_PLL2_CFG & (1 << 1))
#define CGU_PLL2_CFG_PLL_EN               (*IFX_CGU_PLL2_CFG & (1 << 0))

/*
 *  CGU Clock Sys Mux Register
 */
#define CGU_SYS_CPU			GET_BITS(*IFX_CGU_SYS, 7, 4)
#define CGU_SYS_OCP			GET_BITS(*IFX_CGU_SYS, 2, 0)

/*
 * CGU CGU Clock Frequency Select Register
 */
#define CGU_CLKFSR_FPIM			(*IFX_CGU_CLKFSR & (1 << 6))
#define CGU_CLKFSR_FPIS			(*IFX_CGU_CLKFSR & (1 << 5))
#define CGU_CLKFSR_AHBM			(*IFX_CGU_CLKFSR & (1 << 4))
#define CGU_CLKFSR_AHBS			(*IFX_CGU_CLKFSR & (1 << 3))
#define CGU_CLKFSR_PPE			GET_BITS(*IFX_CGU_CLKFSR, 18, 16)
/*
 *  CGU Update Register
 */
#define CGU_UPDATE_FSC_INT_TYPE   	(*IFX_CGU_UPDATE & (1 << 1))
#define CGU_UPDATE_UPD               	(*IFX_CGU_UPDATE & (1 << 0))

/*
 *  CGU Interface Clock Register
 */
#define CGU_IF_CLK_FPI_CLK		GET_BITS(*IFX_CGU_IF_CLK, 28, 25)
#define CGU_IF_CLK_PCI_CLK              GET_BITS(*IFX_CGU_IF_CLK, 24, 20)
#define CGU_IF_CLK_PDA                  (*IFX_CGU_IF_CLK & (1 << 19))
#define CGU_IF_CLK_PCI_B                (*IFX_CGU_IF_CLK & (1 << 18))
#define CGU_IF_CLK_PCIBM                (*IFX_CGU_IF_CLK & (1 << 17))
#define CGU_IF_CLK_PCIS                 (*IFX_CGU_IF_CLK & (1 << 16))
#define CGU_IF_CLK_CLKOD0               GET_BITS(*IFX_CGU_IF_CLK, 15, 14)
#define CGU_IF_CLK_CLKOD1               GET_BITS(*IFX_CGU_IF_CLK, 13, 12)
#define CGU_IF_CLK_CLKOD2               GET_BITS(*IFX_CGU_IF_CLK, 11, 10)
#define CGU_IF_CLK_CLKOD3               GET_BITS(*IFX_CGU_IF_CLK, 9, 8)
#define CGU_IF_CLK_SI_CLK_EN		(*IFX_CGU_IF_CLK & (1 << 7))
#define CGU_IF_CLK_GPHY_SEL		GET_BITS(*IFX_CGU_IF_CLK, 4, 2)
#define CGU_IF_CLK_USB_SEL              GET_BITS(*IFX_CGU_IF_CLK, 1, 0)

/*
 *  CGU DDR Memory Control Register
 */

/*
 *  CGU CT Status Register 1
 */
#define CGU_CT1SR_PDOUT                 GET_BITS(*IFX_CGU_CT1SR, 13, 0)

/*
 *  CGU CT Kval Register
 */
#define CGU_CT_KVAL_PLL1K               GET_BITS(*IFX_CGU_CT_KVAL, 19, 0)

/*
 *  CGU PCM Control Register
 */
#define CGU_PCMCR_INT_SEL               GET_BITS(*IFX_CGU_PCMCR, 29, 28)
#define CGU_PCMCR_DCL_SEL               GET_BITS(*IFX_CGU_PCMCR, 27, 25)
#define CGU_PCMCR_MUXDCL                (*IFX_CGU_MUXDCL & (1 << 22))
#define CGU_PCMCR_MUXFSC                (*IFX_CGU_MUXDCL & (1 << 18))
#define CGU_PCMCR_PCM_SL                (*IFX_CGU_MUXDCL & (1 << 13))
#define CGU_PCMCR_DNTR                  GET_BITS(*IFX_CGU_PCMCR, 12, 11)
#define CGU_PCMCR_NTRS                  (*IFX_CGU_MUXDCL & (1 << 10))
#define CGU_PCMCR_AC97_EN               (*IFX_CGU_MUXDCL & (1 << 9))
#define CGU_PCMCR_CTTMUX                (*IFX_CGU_MUXDCL & (1 << 8))
#define CGU_PCMCR_CT_MUX_SEL            GET_BITS(*IFX_CGU_PCMCR, 7, 6)
#define CGU_PCMCR_FSC_DUTY              (*IFX_CGU_MUXDCL & (1 << 5))
#define CGU_PCMCR_CTM_SEL               (*IFX_CGU_MUXDCL & (1 << 4))

/*
 *  PCI Clock Control Register
 */
#define CGU_PCI_CR_PADSEL               (*IFX_CGU_PCI_CR & (1 << 31))
#define CGU_PCI_CR_RESSEL               (*IFX_CGU_PCI_CR & (1 << 30))
#define CGU_PCI_CR_PCID_H               GET_BITS(*IFX_CGU_PCI_CR, 23, 21)
#define CGU_PCI_CR_PCID_L               GET_BITS(*IFX_CGU_PCI_CR, 20, 18)

/*
 *  Pre-declaration of File Operations
 */
static long cgu_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static int cgu_open(struct inode *, struct file *);
static int cgu_release(struct inode *, struct file *);

/*
 *  Calculate PLL Frequency
 */
static inline u32 get_input_clock(void);
static inline u32 cal_dsm(int, u32, u32);
static inline u32 mash_dsm(int, u32, u32, u32);
static inline u32 ssff_dsm_1(int, u32, u32, u32);
static inline u32 ssff_dsm_2(int, u32, u32, u32, u32);
static inline u32 dsm(int, u32 M, u32, u32, u32, u32, u32);
static inline u32 cgu_get_pll0_fosc(void);
static inline u32 cgu_get_pll1_fosc(void);
static inline u32 cgu_get_pll2_fosc(void);

/*
 * MPS SRAM backup mem pointer
 */
static  char    *argv;

/*
 *  Proc Filesystem
 */
#ifdef CONFIG_PROC_FS
static struct proc_dir_entry* g_gpio_dir = NULL;
static inline void proc_file_create(void);
static inline void proc_file_delete(void);
#endif

/*
 *  Init Help Functions
 */
static inline int ifx_cgu_version(char *, size_t);

int cpu_freq_old_g;   	/* old(current)cpu frequency in MHz */

static struct file_operations cgu_fops = {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
    unlocked_ioctl:  cgu_ioctl,
#else
    ioctl:      cgu_ioctl,
#endif
    open:       cgu_open,
    release:    cgu_release
};

static long cgu_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int ret = 0;
    struct cgu_clock_rates rates;

    if ( _IOC_TYPE(cmd) != IFX_CGU_IOC_MAGIC
        || _IOC_NR(cmd) >= CGU_IOC_MAXNR )
        return -ENOTTY;

    if ( _IOC_DIR(cmd) & _IOC_READ )
        ret = !access_ok(VERIFY_WRITE, arg, _IOC_SIZE(cmd));
    else if ( _IOC_DIR(cmd) & _IOC_WRITE )
        ret = !access_ok(VERIFY_READ, arg, _IOC_SIZE(cmd));
    if ( ret )
        return -EFAULT;

    switch ( cmd )
    {
    case IFX_CGU_GET_CLOCK_RATES:
        /*  Calculate Clock Rates   */
        rates.cpu       = ifx_get_cpu_hz();
        rates.fpi_bus1  = cgu_get_fpi_bus_clock(1);
        rates.fpi_bus2  = cgu_get_fpi_bus_clock(2);
        rates.pp32      = cgu_get_pp32_clock();
        rates.pci       = cgu_get_pci_clock();
        rates.mii0      = cgu_get_ethernet_clock();
        rates.mii1      = cgu_get_ethernet_clock();
        rates.usb       = cgu_get_usb_clock();
        rates.clockout0 = cgu_get_clockout(0);
        rates.clockout1 = cgu_get_clockout(1);
        rates.clockout2 = cgu_get_clockout(2);
        rates.clockout3 = cgu_get_clockout(3);
        /*  Copy to User Space      */
        copy_to_user((char*)arg, (char*)&rates, sizeof(rates));

        ret = 0;
        break;
    case IFX_CGU_IOC_VERSION:
        {
            struct ifx_cgu_ioctl_version version = {
                .major = IFX_CGU_VER_MAJOR,
                .mid   = IFX_CGU_VER_MID,
                .minor = IFX_CGU_VER_MINOR
            };
            ret = copy_to_user((void *)arg, (void *)&version, sizeof(version));
        }
        break;
    default:
        ret = -ENOTTY;
    }

    return ret;
}

static int cgu_open(struct inode *inode, struct file *file)
{
    return IFX_SUCCESS;
}

static int cgu_release(struct inode *inode, struct file *file)
{
    return IFX_SUCCESS;
}

/*
 *  Description:
 *    get input clock frequency according to GPIO config
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of input clock
 */
static inline u32 get_input_clock(void)
{
    return BASIC_INPUT_CLOCK_FREQUENCY;
}

/*
 *  Description:
 *    common routine to calculate PLL frequency
 *  Input:
 *    num --- u32, numerator
 *    den --- u32, denominator
 *  Output:
 *    u32 --- frequency the PLL output
 */
static inline u32 cal_dsm(int pll, u32 num, u32 den)
{
    u64 res;

    res = get_input_clock();
    res *= num;
    do_div(res, den);

    return res;
}

/*
 *  Description:
 *    calculate PLL frequency following MASH-DSM
 *  Input:
 *    M   --- u32, denominator coefficient
 *    N   --- u32, numerator integer coefficient
 *    K   --- u32, numerator fraction coefficient
 *  Output:
 *    u32 --- frequency the PLL output
 */
static inline u32 mash_dsm(int pll, u32 M, u32 N, u32 K)
{
    u32 num = ((N + 1) << 10) + K;
    u32 den = (M + 1) << 10;

    return cal_dsm(pll, num, den);
}

/*
 *  Description:
 *    calculate PLL frequency following SSFF-DSM (0.25 < fraction < 0.75)
 *  Input:
 *    M   --- u32, denominator coefficient
 *    N   --- u32, numerator integer coefficient
 *    K   --- u32, numerator fraction coefficient
 *  Output:
 *    u32 --- frequency the PLL output
 */
static inline u32 ssff_dsm_1(int pll, u32 M, u32 N, u32 K)
{
    u32 num = ((N + 1) << 11) + K + 512;
    u32 den = (M + 1) << 11;

    return cal_dsm(pll, num, den);
}

/*
 *  Description:
 *    calculate PLL frequency following SSFF-DSM
 *    (fraction < 0.125 || fraction > 0.875)
 *  Input:
 *    M   --- u32, denominator coefficient
 *    N   --- u32, numerator integer coefficient
 *    K   --- u32, numerator fraction coefficient
 *  Output:
 *    u32 --- frequency the PLL output
 */
static inline u32 ssff_dsm_2(int pll, u32 M, u32 N, u32 K, u32 modulo)
{
    u32 offset[4] = {512, 2560, 1536, 3584}; //  FIX the order
    u32 num = ((N + 1) << 12) + K + offset[modulo];
    u32 den = (M + 1) << 12;

    return cal_dsm(pll, num, den);
}

/*
 *  Description:
 *    calculate PLL frequency
 *  Input:
 *    M            --- u32, denominator coefficient
 *    N            --- u32, numerator integer coefficient
 *    K            --- u32, numerator fraction coefficient
 *    dsmsel       --- int, 0: MASH-DSM, 1: SSFF-DSM
 *    phase_div_en --- int, 0: 0.25 < fraction < 0.75
 *                          1: fraction < 0.125 || fraction > 0.875
 *  Output:
 *    u32          --- frequency the PLL output
 */
static inline u32 dsm(int pll, u32 M, u32 N, u32 K, u32 dsmsel, u32 phase_div_en, u32 modulo)
{
    if ( !dsmsel )
        return mash_dsm(pll, M, N, K);
    else
    {
        if ( !phase_div_en )
            return ssff_dsm_1(pll, M, N, K);
        else
            return ssff_dsm_2(pll, M, N, K, modulo);
    }
}

/*
 *  Description:
 *    get oscillate frequency of PLL0
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of PLL0 Fosc
 */
static inline u32 cgu_get_pll0_fosc(void)
{
    if (!CGU_PLL0_CFG_PLL_EN)
        return 0;
    else if (CGU_PLL0_PLL_BP)
        return get_input_clock();
    else
    	return cal_dsm(0, 2*(CGU_PLL0_CFG_PLLN + 1), CGU_PLL0_CFG_PLLM + 1);
}

/*
 *  Description:
 *    get oscillate frequency of PLL2
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of PLL2 Fosc
 */
static inline u32 cgu_get_pll2_fosc(void)
{
    if (!CGU_PLL2_CFG_PLL_EN)
        return 0;
    else if (CGU_PLL2_CFG_PLL_BP)
        return get_input_clock();
    else
	return cal_dsm(0, 2*(CGU_PLL2_CFG_PLLN + 1), CGU_PLL2_CFG_PLLM + 1);
}

/*
 *  Description:
 *    get oscillate frequency of PLL1
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of PLL1 Fosc
 */
static inline u32 cgu_get_pll1_fosc(void)
{
    if ( !CGU_PLL1_CFG_PLL_EN )
        return 0;
    else if ( CGU_PLL1_PLL_BP )
        return get_input_clock();
    else
    {
        if ( !CGU_PLL1_CFG_FRAC_EN )
            return dsm(1, CGU_PLL1_CFG_PLLM, CGU_PLL1_CFG_PLLN, 0, 0, 0, 0);
        else
            return dsm(1, CGU_PLL1_CFG_PLLM, CGU_PLL1_CFG_PLLN, CGU_PLL1_CFG_PLLK_HI, CGU_PLL1_CFG_DSMSEL, CGU_PLL1_PHDIV_EN, CGU_PLL1_FMOD_S);
    }
}

/*
 *  Description:
 *    get frequency of DDR
 *  Output:
 *    u32 --- frequency of DDR
 */
inline u32 ifx_get_ddr_hz(void)
{
    unsigned int sys_clk;

    switch ( *IFX_CGU_SYS & (0x01 << 8) )
    {
        case (0 << 8): sys_clk = CLOCK_500M; break;
        case (1 << 8): sys_clk = CLOCK_600M; break;
        default:       sys_clk = CLOCK_500M; break;
    }

    switch ( *IFX_CGU_SYS & (0x07) )
    {
        case (1): return sys_clk >> 1;
        case (2): return sys_clk >> 2;
        default:  return sys_clk >> 1;
    }
}
EXPORT_SYMBOL(ifx_get_ddr_hz);

/*
 *  Description:
 *    get frequency of FPI bus,
 *    On VR9 fpi has the same speed as DDR.
 *  Input:
 *    fpi --- int, 1: FPI bus 1 (Slave), 2: FPI bus 2 (Master)
 *  Output:
 *    u32 --- frequency of FPI bus
 */
u32 cgu_get_fpi_bus_clock(int fpi)
{
    if(fpi == 2)
    	return (CGU_CLKFSR_FPIM >> 6) ? (ifx_get_ddr_hz() >> 1) : ifx_get_ddr_hz();
    else if(fpi == 1)
	return (CGU_CLKFSR_FPIS >> 5) ? (ifx_get_ddr_hz() >> 1) : ifx_get_ddr_hz();
    else {
	printk("Invalid fpi number\n");
	return IFX_ERROR;
    }
}

/*
 *  Description:
 *    get frequency of PP32 processor
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of PP32 processor
 */
u32 cgu_get_pp32_clock(void)
{
    switch (CGU_CLKFSR_PPE){
    case 4: return CLOCK_400M;
    case 1:
    default: return CLOCK_250M;
    }
}

/*
 *  Description:
 *    get frequency of PCI bus
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of PCI bus
 */
u32 cgu_get_pci_clock(void)
{
    switch(CGU_IF_CLK_PCI_CLK){
    case (0x7): return (cgu_get_pll0_fosc() >> 1) / (7 + 1);
    case (0xe): return (cgu_get_pll0_fosc() >> 1) / (14 + 1);
    default: return (cgu_get_pll0_fosc() >> 1) / (7 + 1);
    }
}

/*
 *  Description:
 *    get frequency of ethernet module (MII)
 *  Input:
 *    mii --- int, 0: mii0, 1: mii1
 *  Output:
 *    u32 --- frequency of ethernet module
 */
u32 cgu_get_ethernet_clock(void)
{
    return 50000000;
}

/*
 *  Description:
 *    get frequency of USB
 *  Input:
 *    none
 *  Output:
 *    u32 --- frequency of USB
 */
u32 cgu_get_usb_clock(void)
{
    switch ( CGU_IF_CLK_USB_SEL )
    {
    case 0:
    case 3:
        return (get_input_clock() + 1) / 3;
    case 1:
        return get_input_clock();
    }
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int clockouttable[3][4] =  {
    { 0              ,    0,  8192 *    1000, 0 }, /*--- CLKOUT0 ---*/
    { 0              ,    0,               0, 0 }, /*--- CLKOUT1 ---*/
    { 25    * 1000000,    0,               0, 0 }, /*--- CLKOUT2 ---*/
};
static DEFINE_SPINLOCK(clklock);
/*--------------------------------------------------------------------------------*\
 *  Description:
 *    set frequency of CLK_OUT pin
 *  Input:
 *    clkout --- int, clock out pin number
 *    clk        frequency
 *  Output:
 *    u32    --- frequency of CLK_OUT pin
\*--------------------------------------------------------------------------------*/
u32 cgu_set_clockout(int clkout, u32 clk) {
    unsigned int i;
    if ( clk == 0 || clkout > 2 || clkout < 0 ) {
        return 0;
    }
    for(i = 0; i < sizeof(clockouttable) / sizeof(clockouttable[0]); i++) {
        if(clk == clockouttable[clkout][i]) {
            unsigned long flags;
            spin_lock_irqsave(&clklock, flags);
            *IFX_CGU_IF_CLK = SET_BITS(*IFX_CGU_IF_CLK, 15 - clkout * 2, 14 - clkout * 2, i);
            spin_unlock_irqrestore(&clklock, flags);
            return clk;
        }
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
 *  Description:
 *    get frequency of CLK_OUT pin
 *  Input:
 *    clkout --- int, clock out pin number
 *  Output:
 *    u32    --- frequency of CLK_OUT pin
\*--------------------------------------------------------------------------------*/
u32 cgu_get_clockout(int clkout) {
    unsigned int idx;

    if ( clkout > 3 || clkout < 0 ) {
        return 0;
    }
    idx =  GET_BITS(*IFX_CGU_IF_CLK, 15 - clkout * 2, 14 - clkout * 2);
    return clockouttable[clkout][idx];
}

#ifdef CONFIG_PROC_FS
static ssize_t proc_version_show(struct seq_file *seq, void *offset)
{
	char tmp[256];

	ifx_cgu_version(tmp, sizeof(tmp));
	seq_puts(seq, tmp);

	return 0;
}

static int proc_version_open(struct inode *inode, struct file *file)
{
	return single_open(file, proc_version_show, NULL);
}

static struct file_operations fops_version = {
	.open    = proc_version_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = single_release,
};

static ssize_t proc_clk_setting_show(struct seq_file *seq, void *offset)
{
	seq_printf(seq, "pll0 N = %d, M = %d, PLL_BP = %d, PLL_EN = %d\n",
		   CGU_PLL0_CFG_PLLN, CGU_PLL0_CFG_PLLM, CGU_PLL0_PLL_BP >> 30,
		   CGU_PLL0_CFG_PLL_EN);
	seq_printf(seq, "pll1 N = %d, M = %d, K_hi = %d, K_lo = %d, "
		   "phdiven = %d, dsmsel = %d, modulo = %d\n",
		   CGU_PLL1_CFG_PLLN, CGU_PLL1_CFG_PLLM, CGU_PLL1_CFG_PLLK_HI,
		   CGU_PLL2_CFG_PLL1_K_LO, CGU_PLL1_PHDIV_EN >> 31,
		   CGU_PLL1_CFG_DSMSEL >> 28, CGU_PLL1_FMOD_S);
	seq_printf(seq, "   PLL_BP = %d PLL_EN = %d\n",
		   CGU_PLL1_PLL_BP >> 30, CGU_PLL1_CFG_PLL_EN);
	seq_printf(seq, "pll2 N = %d, M = %d, PLL_BP = %d, PLL_EN = %d\n",
		   CGU_PLL2_CFG_PLLN, CGU_PLL2_CFG_PLLM,
		   CGU_PLL2_CFG_PLL_BP >> 30, CGU_PLL2_CFG_PLL_EN);
	seq_printf(seq, "pll0_fosc    = %d\n", cgu_get_pll0_fosc());
	seq_printf(seq, "pll1_fosc    = %d\n", cgu_get_pll1_fosc());
	seq_printf(seq, "pll2_fosc    = %d\n", cgu_get_pll2_fosc());
	seq_printf(seq, "cpu clock    = %d\n", ifx_get_cpu_hz());
	seq_printf(seq, "DDR clock    = %d\n", ifx_get_ddr_hz());
	seq_printf(seq, "FPI bus 1    = %d\n", ifx_get_fpi_hz());
	seq_printf(seq, "FPI bus 2    = %d\n", ifx_get_fpi_hz());
	seq_printf(seq, "PP32 clock   = %d\n", cgu_get_pp32_clock());
	seq_printf(seq, "PCI clock    = %d\n", cgu_get_pci_clock());
	seq_printf(seq, "Ethernet MII0= %d\n", cgu_get_ethernet_clock());
	seq_printf(seq, "Ethernet MII1= %d\n", cgu_get_ethernet_clock());
	seq_printf(seq, "USB clock    = %d\n", cgu_get_usb_clock());
	seq_printf(seq, "Clockout0    = %d\n", cgu_get_clockout(0));
	seq_printf(seq, "Clockout1    = %d\n", cgu_get_clockout(1));
	seq_printf(seq, "Clockout2    = %d\n", cgu_get_clockout(2));
	seq_printf(seq, "Clockout3    = %d\n", cgu_get_clockout(3));
	return 0;
}

static int proc_clk_setting_open(struct inode *inode, struct file *file)
{
	return single_open(file, proc_clk_setting_show, NULL);
}

static struct file_operations fops_clk_setting = {
	.open    = proc_clk_setting_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = single_release,
};

static inline void proc_file_create(void)
{
	g_gpio_dir = proc_mkdir("driver/ifx_cgu", NULL);

	proc_create("version", S_IRUGO, g_gpio_dir, &fops_version);
	proc_create("clk_setting", S_IRUGO | S_IWUSR, g_gpio_dir,
		    &fops_clk_setting);
}

static inline void proc_file_delete(void)
{
	remove_proc_entry("clk_setting", g_gpio_dir);
	remove_proc_entry("version", g_gpio_dir);
	remove_proc_entry("driver/ifx_cgu", NULL);
}
#endif /* CONFIG_PROC_FS */

#ifdef CONFIG_IFX_CLOCK_CHANGE

static void dump_tc(int t)
{
        unsigned long val;

        settc(t);
        printk(KERN_DEBUG "VPE loader: TC index %d targtc %ld "
               "TCStatus 0x%lx halt 0x%lx\n",
               t, read_c0_vpecontrol() & VPECONTROL_TARGTC,
               read_tc_c0_tcstatus(), read_tc_c0_tchalt());

        printk(KERN_DEBUG " tcrestart 0x%lx\n", read_tc_c0_tcrestart());
        printk(KERN_DEBUG " tcbind 0x%lx\n", read_tc_c0_tcbind());

        val = read_c0_vpeconf0();
        printk(KERN_DEBUG " VPEConf0 0x%lx MVP %ld\n", val,
               (val & VPECONF0_MVP) >> VPECONF0_MVP_SHIFT);

        printk(KERN_DEBUG " c0 status 0x%lx\n", read_vpe_c0_status());
        printk(KERN_DEBUG " c0 cause 0x%lx\n", read_vpe_c0_cause());

        printk(KERN_DEBUG " c0 badvaddr 0x%lx\n", read_vpe_c0_badvaddr());
        printk(KERN_DEBUG " c0 epc 0x%lx\n", read_vpe_c0_epc());
}
void cgu_dump_vpe(int t)
{
        settc(t);

        printk(KERN_DEBUG "VPEControl 0x%lx\n", read_vpe_c0_vpecontrol());
        printk(KERN_DEBUG "VPEConf0 0x%lx\n", read_vpe_c0_vpeconf0());

        dump_tc(t);
}

#endif //CONFIG_IFX_CLOCK_CHANGE

static inline int ifx_cgu_version(char *buf, size_t n)
{
	return ifx_driver_version(buf, n, "CGU", IFX_CGU_VER_MAJOR,
				  IFX_CGU_VER_MID, IFX_CGU_VER_MINOR);
}

static int __init cgu_init(void)
{
    int ret;
    char ver_str[128] = {0};

    ret = register_chrdev(IFX_CGU_MAJOR, "ifx_cgu", &cgu_fops);
    if ( ret != 0 ) {
        printk(KERN_ERR "Can not register CGU device - %d\n", ret);
        return ret;
    }

#ifdef CONFIG_PROC_FS
    proc_file_create();
#endif

    /* malloc MPS backup mem */
    argv = kmalloc(MPS_MEM_SEG_DATASIZE, GFP_KERNEL);

	ifx_cgu_version(ver_str, sizeof(ver_str));
	pr_info("%s\n", ver_str);

    //enable
    return IFX_SUCCESS;
}

static void __exit cgu_exit(void)
{

#ifdef CONFIG_PROC_FS
    proc_file_delete();
#endif

    /* free MPS backup mem */
    kfree(argv);

    unregister_chrdev(IFX_CGU_MAJOR, "ifx_cgu");
    return;

}

/*!
  \fn       unsigned int ifx_get_cpu_hz(void)
  \brief    Get CPU speed
  \return   CPU clock hz
  \ingroup  IFX_CGU_API
 */
unsigned int ifx_get_cpu_hz(void)
{
    unsigned int sys_clk;

    switch ( *IFX_CGU_SYS & (0x01 << 8) )
    {
        case (0 << 8): sys_clk = CLOCK_500M; break;
        case (1 << 8): sys_clk = CLOCK_600M; break;
        default:       sys_clk = CLOCK_500M; break;
    }

#ifdef CONFIG_USE_EMULATOR
    return EMULATOR_CPU_SPEED;
#else
    switch ( *IFX_CGU_SYS & (0x07 << 4) )
    {
        case (0 << 4): return sys_clk;
        case (1 << 4): return sys_clk >> 1;
        case (2 << 4): return sys_clk >> 2;
        default:       return sys_clk;
    }
#endif
}
EXPORT_SYMBOL(ifx_get_cpu_hz);

/*!
  \fn       unsigned int ifx_get_fpi_hz(void)
  \brief    Get FPI bus speed
  \return   FPI bus clock hz
  \ingroup  IFX_CGU_API
 */
unsigned int ifx_get_fpi_hz(void)
{
#ifdef CONFIG_USE_EMULATOR
    return EMULATOR_CPU_SPEED >> 1;
#else
    switch (CGU_IF_CLK_FPI_CLK)
    {
        case (6):  return CLOCK_125M;
        case (5):  return CLOCK_250M;
        case (1):  return CLOCK_300M;
        case (2):  return CLOCK_150M;
        default:   return CLOCK_125M;
    }
#endif
}
EXPORT_SYMBOL(ifx_get_fpi_hz);

/*!
  \fn       unsigned int ifx_get_usif_hz(void)
  \brief    Get USIF module clock hz
  \return   USIF module clock hz
  \ingroup  IFX_CGU_API
 */
unsigned int ifx_get_usif_hz(void)
{
#ifdef CONFIG_USE_EMULATOR
    return (PLL0_CLK_SPEED / 10);
#else
    return (cgu_get_pll0_fosc() / 10);
#endif
}
EXPORT_SYMBOL(ifx_get_usif_hz);

EXPORT_SYMBOL(cgu_get_fpi_bus_clock);
EXPORT_SYMBOL(cgu_get_pp32_clock);
EXPORT_SYMBOL(cgu_get_pci_clock);
EXPORT_SYMBOL(cgu_get_ethernet_clock);
EXPORT_SYMBOL(cgu_get_usb_clock);
EXPORT_SYMBOL(cgu_get_clockout);
EXPORT_SYMBOL(cgu_set_clockout);

module_init(cgu_init);
module_exit(cgu_exit);
