#if defined(CONFIG_AVM_WP)

#define SET_WATCHPOINT_FAIL_WRONG_TYPE 	-1
#define SET_WATCHPOINT_FAIL_NO_REG	 	-2
#define SET_WATCHPOINT_SUCCESS 			 0
#define NR_WATCHPOINTS 4

#define WP_TYPE_W (1 << 0)
#define WP_TYPE_R (1 << 1)
#define WP_TYPE_I (1 << 2)


struct avm_wp {
	int addr;
	int mask;
	int type;
	void (*handler)( int, int , int );
};

void default_wp_handler( int status, int deferred, int epc );
int avm_wp_dispatcher( void );

int watchpoint_busy( int watch_nr );
void del_watchpoint( int watch_nr );

int set_watchpoint(int watch_nr, int addr, int mask, int type);
int set_watchpoint_handler(int watch_nr, int addr, int mask, int type, void (*wp_handler)(int,int,int));
#endif

