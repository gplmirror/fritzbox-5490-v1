/*
 * vx185_snor.h -- Ikanox VX185 SPI NOR flash controller driver
 *
 * Author: Tido Klaassen <tklaassen@avm.de>
 * Copyright (C) 2012 AVM GmbH.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __VX185_SPI0_SNOR_H
#define __VX185_SPI0_SNOR_H

struct vx185_snor_platform {
    /* SNOR MTD partition information */
    int                     nr_partitions;
    struct mtd_partition    *partitions;
};

struct vx185_snor_ctx {
    struct mtd_info *mtd;
    unsigned int panic_mode;
};

#endif // __VX185_SPI0_SNOR
