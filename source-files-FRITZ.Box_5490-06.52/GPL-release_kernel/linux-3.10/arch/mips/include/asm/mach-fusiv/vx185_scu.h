/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _SCU_DEF
#define _SCU_DEF


#define cmips_map0  				bme_map0
#define cmips_mask0 				bme_mask0
#define cmips_map1  				bme_map1
#define cmips_mask1 				bme_mask1
#define h2c_mb_int_set 				h2b_mb_int_set
#define h2c_mb_int_clr				h2b_mb_int_clr
#define h2c_mb_int_en				h2b_mb_int_en
#define h2c_mb					h2b_mb
#define c2h_mb_int_set				b2h_mb_int_set
#define c2h_mb_int_clr				b2h_mb_int_clr
#define c2h_mb_int_en				b2h_mb_int_en				
#define c2h_mb					b2h_mb

typedef struct scu_reg_s {
  
	volatile unsigned int rst_mask;	  	//0x000
	volatile unsigned int rst_vec;		//0x004
	volatile unsigned int cpu_ctl_mask; 	//0x008
	volatile unsigned int cpu_ctl;		//0x00c
	volatile unsigned int rst_cause;	//0x010
	volatile unsigned int rst_strap; 	//0x014
	volatile unsigned int rev_num;		//0x018
	volatile unsigned int clk_gate_ctl;	//0x01c
	volatile unsigned int pll_pwr_rst;	//0x020
	volatile unsigned int pll_vcxo_sd;	//0x024
	volatile unsigned int pll_dsp_sd;	//0x028
	volatile unsigned int pll_pd1_ctl;	//0x02c
	volatile unsigned int pll_pd2_ctl;	//0x030
	volatile unsigned int pll_pd3_ctl;	//0x034
	#define  USB_CLK_PLL_PDIV3	1

	volatile unsigned int pll_actl0;	//0x038
	volatile unsigned int pll_actl1;	//0x03c
	volatile unsigned int pll_actl2;	//0x040
	volatile unsigned int pll_actl3;	//0x044
	volatile unsigned int pll_actl4;	//0x048
	volatile unsigned int pll_test1;	//0x04c
	volatile unsigned int pll_test2;	//0x050
	volatile unsigned int bme_map0; 	//0x054
	volatile unsigned int bme_mask0;	//0x058
	volatile unsigned int bme_map1; 	//0x05c
	volatile unsigned int bme_mask1;	//0x060
	volatile unsigned int h2b_mb_int_set;	//0x064
	volatile unsigned int h2b_mb_int_clr;	//0x068
	volatile unsigned int h2b_mb_int_en;	//0x06c
	volatile unsigned int h2b_mb[8]; 	//0x070-0x08c
	volatile unsigned int b2h_mb_int_set;	//0x090
	volatile unsigned int b2h_mb_int_clr;	//0x094
	volatile unsigned int b2h_mb_int_en;	//0x098
	volatile unsigned int b2h_mb[8]; 	//0x09c-0xB8
	volatile unsigned int nand_status; 	//0x0BC
#define NAND_CHIP_BUSY		(1 << 0)
#define NAND_PECC_BUSY		(1 << 1)
	volatile unsigned int por_ctl_status;	//0x0C0
	volatile unsigned int pvt_ctl_status;	//0x0C4
	volatile unsigned int fixed_pll_status;	//0x0C8
	volatile unsigned int vxc0_pll_status;	//0x0CC
	volatile unsigned int gw_pll_status;	//0x0D0
	volatile unsigned int bme_pll_status;	//0x0D4
	volatile unsigned int dsp_pll_status;	//0x0D8
	volatile unsigned int gw_pll_sd;	//0x0DC
	volatile unsigned int bme_pll_sd;	//0x0E0
	volatile unsigned int rsvd[2];		//0xE4-0xE8
	volatile unsigned int spare0;		//0x0EC
	volatile unsigned int spare1;		//0x0F0
	volatile unsigned int spare2;		//0x0F4
	volatile unsigned int spare3;		//0x0F8
	volatile unsigned int spare4;		//0x0FC

} scu_reg_t;

#define scu_regs ((scu_reg_t *) 0xb9000000)

#define  SYS_RESET		0
#define  HOST_RESET		1
#define  GIGE0_AP_RESET		2
#define  GIGE1_AP_RESET		3
#define  GIGE2_AP_RESET		4
#define  SPA_AP_RESET		5
#define  BMU_AP_RESET		6
#define  CLASS_AP_RESET		7
#define  HAP_RESET		9
#define  DSP_RESET		10
#define  PCIE_RESET		12
#define  SATA_RESET		13
#define  USB_RESET		14
#define  GIGE0_RESET		16
#define  GIGE1_RESET		17
#define  GIGE2_RESET		18
#define  VPHY_AFE_RESET		24
#define  VPHY_FD_RESET		25
#define  VPHY_BMIPS_RESET	26
#define  DDRC_RESET		27
#define  EXT_RESET		31

#define  GIGE0_AP_CLK_GATE	2
#define  GIGE1_AP_CLK_GATE	3
#define  GIGE2_AP_CLK_GATE	4
#define  SPA_AP_CLK_GATE	5
#define  BMU_AP_CLK_GATE	6
#define  CLS_AP_CLK_GATE	7
#define  HAP_CLK_GATE		9
#define  DSP_CLK_GATE           10
#define  GMII_CLK_GATE          11
#define  PCIE_CLK_GATE          12
#define  SATA_CLK_GATE          13
#define  USB_CLK_GATE           14
#define  SPI_SLOW_CLK_GATE      15
#define  SPI_CLK_GATE           16
#define  EMAC_SYS_CLK_GATE      18
#define  RMII_2_5MHZ_CLK_GATE   19
#define  RMII_25MHZ_CLK_GATE    20
#define  RMII_50MHZ_CLK_GATE    21

#define PCIE_REF_CLK_SEL        22  
#define SATA_REF_CLK_SEL		31
#define SATA_MPLL_PRESCALE		23


#define NAND_RESET_1GB_4KB_MASK		0x17
#define NAND_RESET_1GB_4KB		0x17

#define NAND_RESET_128MB_2KB_MASK	0x0F
#define NAND_RESET_128MB_2KB		0xC

/* CPU Control registers */
#define SCU_SYS_CTRL_XMII_CONFIG_GIGE0       12 // 12 and 13 are bits for configuration and mask for GIGE0
#define SCU_SYS_CTRL_XMII_CONFIG_GIGE1       14 // 14 and 15 are bits for configuration and mask for GIGE1
#define SCU_SYS_CTRL_XMII_CONFIG_GIGE2       16 // 16 and 17 are bits for configuration and mask for GIGE2

#endif
