/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_SPI_REG_H__
#define __IKN_SPI_REG_H__
#include "ikn_types.h"
/* SPI Registers */

#define  SPI_BASE_ADDR              0xB9028000
/*#define  SPI_BAUD_REG               0xB9028014
#define  SPI_CTRL_REG               0xB9028000
#define  SPI_FLG_REG                0xB9028004
#define  SPI_ST_REG                 0xB9028008
#define  SPI_TDBR_REG               0xB902800C
#define  SPI_RDBR_REG               0xB9028010*/


/* SPI Control Register */
enum 
{
    /* TIMOD - Defines initiation mode and interrupt generation.
     * 00 - initiates transfer by read of receive buffer.
     *      Interrupt is active when receive buffer is full.
     * 01 - initiates transfer by write to transmit buffer.
     *      Interrupt is active when transmit buffer is empty.
     * 10 - Reserved
     * 11 - Reserved
     */
    eSPI_CTRL_TIMOD     = 0x0003,   //bits 0 and 1
    eSPI_CTRL_SZ        = 0x0004,   /*bit 2   send 0 or last word when TDBR is empty.
                                        0 - sends last word
                                        1 - sends zeros */

    eSPI_CTRL_GM        = 0x0008,   /*bit 3    When RDBR is full, fetch data or discard incoming data
                                          0 - Discards incoming data
                                          1 - obtains additional data, overwrites previous data
                                     */

    eSPI_CTRL_PSSE      = 0x0010,   /*bit 4   Enables SPI_SLV_CS pin as error input to SPI-1, when SPI1
                                          is configured as Master.
                                          When not used, SPI_SLV_CS can be discarded, which releases
                                          a chip pin as general purpose I/O.
                                          0 - Disable
                                          1 - Enable */

    eSPI_CTRL_EMISO     = 0x0020,   /* bit 5    Enables MISO (SPI1_SDATA_B) pin as an output, 
                                          when SPI1 Controller is slave.
                                          0 - Disables MISO
                                          1 - Enables MISO */

    eSPI_CTRL_SIZE      = 0x0100,   /* bit 8    Word length
                                          0 - 8 bits
                                          1 - 16 bits */

    eSPI_CTRL_LSBF      = 0x0200,   /* bit 9    Data Format
                                          0 - MSB transmitted/received first
                                          1 - LSB transmitted/received first
                                          */

    eSPI_CTRL_CPHA      = 0x0400,   /* bit 10    Clock Phase of SPI1_CLK_B (Transfer format)
                                          0 - SPI1_CLK_B starts toggling at the middle of 1st data bit
                                          1 - SPI1_CLK_B starts toggling at the start of 1st data bit 
                                        */
    eSPI_CTRL_CPOL      = 0x0800,   /* bit 11    Clock Polarity of SPI1_CLK_B
                                          0 - Active high (low is the idle state)
                                          1 - Active low  (high is idle state)
                                        */
    eSPI_CTRL_MSTR      = 0x1000,   /* bit 12    Configures SPI-1 module as Master or Slave
                                          0 - Device is a slave device
                                          1 - Device is as master device
                                        */
    eSPI_CTRL_WOM       = 0x2000,   /* bit 13    Enables open-drain data output (for MOSI and MISO)
                                          0 - Normal
                                          1 - Open Drain
                                        */
    eSPI_CTRL_SPE       = 0x4000,   /* bit 14    Enables SPI-1 Module
                                          0 - Disables SPI-1 Module
                                          1 - Enables SPI-1 Module
                                        */
};


/* SPI Status Register */
enum 
{
    eSPI_ST_SPIF  = 0x0001,      /* bit 0    Set when an SPI Single-word transfer is complete */
    eSPI_ST_MODF  = 0x0002,      /* bit 1    Mode-fault error.  W1C bit 
                                               Set in a Master device when another device attempts to 
                                               become the master. */
    eSPI_ST_TXE   = 0x0004,      /* bit 2    Transmission Error.
                                              Set when transmission occurred when no new data in TDBR Reg. */
    eSPI_ST_TXS   = 0x0008,      /* bit 3    TDBR data buffer status.
                                             0 - Empty
                                              1 - Full */
    eSPI_ST_RBSY  = 0x0010,      /* bit 4    Receive error. Set when data is received with receive buffer full */
    eSPI_ST_RXS   = 0x0020,      /* bit 5    Rx Data buffer status.
                                              0 - Empty
                                              1 - Full */
    eSPI_ST_TXCOL = 0x0040       /* bit 6    Transmit Collision error.
                                              Set when corrupt data was transmitted. */
};

/* SPI Flag Register */
enum 
{
    eSPI_FLG_FLS = 0x00FE,       /* bits 7:1 SPI Flag Select bits.
                                             If High, configures corresponding SPI_FLOUT as
                                             flag output to be used for SPI Slave-select. */
    eSPI_FLG_FLG = 0xFE00,       /* bits 15:9 SPI Flags
                                             If low, activates the selected correspondig 
                                             SPI_FLOUT as a flag output to be used for SPI
                                             Slave-select */
};

#if 1
typedef struct spiRegs_s
{
    reg16Bit  regSPICtrl;            //0xb9028000
    UINT16    usUnused1;
    reg16Bit  regSPIFlg;             //0xb9028004
    UINT16    usUnused2;
    reg16Bit  regSPISt;              //0xb9028008
    UINT16    usUnused3;
    reg16Bit  regSPITdbr;            //0xb902800c
    UINT16    usUnused4;
    reg16Bit  regSPIRdbr;            //0xb9028010
    UINT16    usUnused5;
    reg16Bit  regSPIBaud;            //0xb9028014
}spiRegs_t;
#else
typedef struct spiRegs_s
{
    reg32Bit  regSPICtrl;            //0xb9028000
    reg32Bit  regSPIFlg;             //0xb9028004
    reg32Bit  regSPISt;              //0xb9028008
    reg32Bit  regSPITdbr;            //0xb902800c
    reg32Bit  regSPIRdbr;            //0xb9028010
    reg32Bit  regSPIBaud;            //0xb9028014
}spiRegs_t;

#endif

#define  SPI_HW_MAX_BUFF_SIZE           1000 //can be changed

#endif  //__IKN_SPI_REG_H__
