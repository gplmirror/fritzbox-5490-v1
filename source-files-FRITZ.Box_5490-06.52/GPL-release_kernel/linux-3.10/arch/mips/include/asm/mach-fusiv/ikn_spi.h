/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_SPI_H__

#define __IKN_SPI_H__
#include "ikn_types.h"
#include "ikn_gpio.h"


typedef enum
{
    eSPI_MODE_SLAVE  = 0,
    eSPI_MODE_MASTER = 0x1000
}eSPIMode_t;

typedef enum
{
    eSPI_INIT_MODE_BY_READ =  0,  /* Initiate transfer by read of receive buffer 
                                    Interrupt is active when receive buffer is full
                                  */
    eSPI_INIT_MODE_BY_WRITE = 1   /* Initiate transfer by write to transmit buffer 
                                    Interrupt is active when transmit buffer is empty
                                  */
}eSPIInitMode_t; 

typedef enum
{
    eSPI_SZ_SEND_LAST_WORD = 0,
    eSPI_SZ_SEND_ZEROS     = 0x0004
}eSPISZState_t;

typedef enum
{
    eSPI_GM_DISCARD_INCOMING_DATA = 0,
    eSPI_GM_OVERWRITE_PREVIOUS_DATA = 0x0008
}eSPIGMState_t;

typedef enum
{
    eSPI_PSSE_DISABLE  = 0,
    eSPI_PSSE_ENABLE   = 0x0010
}eSPIErrorInput_t;

typedef enum
{
    eSPI_EMISO_DISABLE = 0,
    eSPI_EMISO_ENABLE  = 0x0020
}eSPIEnableMISO_t;

typedef enum
{
    eSPI_WORD_SIZE_8  = 0,
    eSPI_WORD_SIZE_16 = 0x0100
}eSPIWordSize_t;

typedef enum
{
    eSPI_MSB_FIRST  = 0,
    eSPI_LSB_FIRST  = 0x0200
}eSPIDataFormat_t;

typedef enum
{
    eSPI_CPHA_DISABLE = 0,
    eSPI_CPHA_ENABLE  = 0x0400
}eSPIEnableCPHA_t;

typedef enum
{
    eSPI_CPOL_DISABLE = 0,
    eSPI_CPOL_ENABLE  = 0x0800
}eSPIEnableCPOL_t;

typedef enum
{
    eSPI_WOM_DISABLE  = 0,
    eSPI_WOM_ENABLE   = 0x2000
}eSPIOpenDrain_t;

typedef enum 
{
    eSPI_MODULE_DISABLE = 0,
    eSPI_MODULE_ENABLE  = 0x4000
}eSPIModuleState_t;

#define  SPI_GPIO_LINE_START         3  // SPI Slave select start from GPIO 4 to GPIO 10
#define  SPI_SLAVE_MODE_SS_LINE      eGPIO_B_3

#if 0
typedef enum
{
    eSPI_SLAVE_SELECT_0         = eGPIO_B_4,
    eSPI_SLAVE_SELECT_1         = eGPIO_B_5,
    eSPI_SLAVE_SELECT_2         = eGPIO_B_6,
    eSPI_SLAVE_SELECT_3         = eGPIO_B_7,
    eSPI_SLAVE_SELECT_4         = eGPIO_B_8,
    eSPI_SLAVE_SELECT_5         = eGPIO_B_9,
    eSPI_SLAVE_SELECT_6         = eGPIO_B_10
}eSPISlaveSelect_t;
#else
typedef enum
{
    eSPI_SLAVE_SELECT_0         = 1,
    eSPI_SLAVE_SELECT_1         = 2,
    eSPI_SLAVE_SELECT_2         = 3,
    eSPI_SLAVE_SELECT_3         = 4,
    eSPI_SLAVE_SELECT_4         = 5,
    eSPI_SLAVE_SELECT_5         = 6,
    eSPI_SLAVE_SELECT_6         = 7
}eSPISlaveSelect_t;
#endif


typedef struct spiConfig_s
{
    eSPIMode_t          eSPIMode;
    eSPIInitMode_t      eSPIInitMode;
    eSPISZState_t       eSPISZState;
    eSPIGMState_t       eSPIGMState;
    eSPIErrorInput_t    eSPIErrorInput;
    eSPIEnableMISO_t    eSPIEnableMISO;
    eSPIWordSize_t      eSPIWordSize;
    eSPIDataFormat_t    eSPIDataFormat;
    eSPIEnableCPHA_t    eSPIEnableCPHA;
    eSPIEnableCPOL_t    eSPIEnableCPOL;
    eSPIOpenDrain_t     eSPIOpenDrain;
    eSPIModuleState_t   eSPIModuleState;
    UINT32              spiSlaveSelectBitMap;  //number of slaves to enable
    UINT32              uiSPIBaudRate; // applicable only in Master Mode
    UINT32              u32SPIMaxBufSize;
    UINT32              u32SPIRxBusErrCnt;
    UINT32              u32SPITxBusErrCnt;
    UINT32              u32SPIRxBusCnt;
    UINT32              u32SPITxBusCnt;
    UINT32              u32SPIProtoRxCnt;
    UINT32              u32SPIProtoTxCnt;
    UINT32              u32SPIProtoRxErrCnt;
}spiConfig_t;

#define SPI_CONFIG_SIZE           sizeof(spiConfig_t)




eIKNReturn_t iknSPIEnabled(VOID);
eIKNReturn_t iknSPIConfigureBaud(spiConfig_t *pstSPIConfig);
eIKNReturn_t iknSPIModuleInit(spiConfig_t *pstSPIConfig);
VOID iknSPISetMAXBufSize(spiConfig_t *pstSPIConfig);

eIKNReturn_t iknSPISSEnable(spiConfig_t *pstSPIConfig,eSPISlaveSelect_t eSPISlaveSelect);
eIKNReturn_t iknSPITdbr16Bits(spiConfig_t *pstSPIConfig,eSPISlaveSelect_t eSPISlaveSelect,UINT16 u16Data);
eIKNReturn_t iknSPITdbr8Bits(spiConfig_t *pstSPIConfig,eSPISlaveSelect_t eSPISlaveSelect,UINT8 u8Data);
eIKNReturn_t iknSPIRdbr16Bits(spiConfig_t *pstSPIConfig,eSPISlaveSelect_t eSPISlaveSelect,UINT16 *pu16Data);
eIKNReturn_t iknSPIRdbr8Bits(spiConfig_t *pstSPIConfig,eSPISlaveSelect_t eSPISlaveSelect,UINT8 *pu8Data);
eIKNReturn_t iknSPITdbrRdbr(spiConfig_t *pstSPIConfig, eSPISlaveSelect_t eSPISlaveSelect,UINT16 *pu16RxData,
                            UINT16 u16TxData);
eIKNReturn_t iknSPITdbr(spiConfig_t *pstSPIConfig, eSPISlaveSelect_t eSPISlaveSelect, UINT16 u16TxData);
eIKNReturn_t iknSPIRdbr(spiConfig_t *pstSPIConfig, eSPISlaveSelect_t eSPISlaveSelect,UINT16 *pu16RxData);
eIKNReturn_t iknSPITdbr8bit(spiConfig_t *pstSPIConfig, eSPISlaveSelect_t eSPISlaveSelect, UINT8 u8TxData);
eIKNReturn_t iknSPIRdbr8bit(spiConfig_t *pstSPIConfig, eSPISlaveSelect_t eSPISlaveSelect,UINT8 *pu8RxData);
#endif //__IKN_SPI_H__
