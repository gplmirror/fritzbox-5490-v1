/**
 * Copyright (C) 2010-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _VX585_INT_H_
#define _VX585_INT_H_

/* Interrupts assigned */
#define FUSIV_UART0_INT         34      /* 26 + 8 */
#define FUSIV_SATA_INT          54      /* 46 + 8 */
#define FUSIV_PCIE0_INT         55      /* 47 + 8 */
#define FUSIV_PCIE1_INT         56      /* 48 + 8 */
#define FUSIV_USB_INT           57      /* 49 + 8 */
#define FUSIV_CL0_INT           62      /* 54 + 8 */
#define FUSIV_CL1_INT           63      /* 55 + 8 */
#define FUSIV_BMU_INT           64      /* 56 + 8 */
#define FUSIV_SPA_INT           65      /* 57 + 8 */
#define FUSIV_MAILBOX_B2H_INT   78      /* 70 + 8 */
#define FUSIV_CL3_INT           84      /* 76 + 8 */
#define FUSIV_HOSTAP_INT        85      /* 77 + 8 */

/* CPU Map Definition */
#define UART0_CPU_NUM           0
#define SATA_CPU_NUM            0
#define PCIE0_CPU_NUM           2
#define PCIE1_CPU_NUM           3
#define USB_CPU_NUM             1
#define CL0_CPU_NUM             0
#define CL1_CPU_NUM             3
#define BMU_CPU_NUM             0
#define SPA_CPU_NUM             2
#define MAILBOX_B2H_CPU_NUM     0
#define CL2_CPU_NUM             1
#define HOSTAP_CPU_NUM          2

#endif /* _VX585_INT_H_ */
