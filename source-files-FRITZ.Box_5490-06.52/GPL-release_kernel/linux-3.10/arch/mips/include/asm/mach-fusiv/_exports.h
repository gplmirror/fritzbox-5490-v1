/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */


EXPORT_FUNC(get_version0)
EXPORT_FUNC(getc0)
EXPORT_FUNC(tstc0)
EXPORT_FUNC(putc0)
EXPORT_FUNC(puts0)
EXPORT_FUNC(printf0)
EXPORT_FUNC(install_hdlr0)
EXPORT_FUNC(free_hdlr0)
EXPORT_FUNC(malloc0)
EXPORT_FUNC(free0)
EXPORT_FUNC(udelay0)
EXPORT_FUNC(get_timer0)
EXPORT_FUNC(vprintf0)
EXPORT_FUNC(do_reset0)
