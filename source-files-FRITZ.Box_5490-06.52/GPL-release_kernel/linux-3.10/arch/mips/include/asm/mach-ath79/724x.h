/*
 * vim: tabstop=8 : noexpandtab
 */
#ifndef _724x_H
#define _724x_H

/*
 * PCI block
 */
#define ATH_PCI_WINDOW			0x8000000 /* 128MB */
#define ATH_PCI_WINDOW0_OFFSET		ATH_DDR_CTL_BASE+0x7c
#define ATH_PCI_WINDOW1_OFFSET		ATH_DDR_CTL_BASE+0x80
#define ATH_PCI_WINDOW2_OFFSET		ATH_DDR_CTL_BASE+0x84
#define ATH_PCI_WINDOW3_OFFSET		ATH_DDR_CTL_BASE+0x88
#define ATH_PCI_WINDOW4_OFFSET		ATH_DDR_CTL_BASE+0x8c
#define ATH_PCI_WINDOW5_OFFSET		ATH_DDR_CTL_BASE+0x90
#define ATH_PCI_WINDOW6_OFFSET		ATH_DDR_CTL_BASE+0x94
#define ATH_PCI_WINDOW7_OFFSET		ATH_DDR_CTL_BASE+0x98

#define ATH_PCI_WINDOW0_VAL		0x10000000
#define ATH_PCI_WINDOW1_VAL		0x11000000
#define ATH_PCI_WINDOW2_VAL		0x12000000
#define ATH_PCI_WINDOW3_VAL		0x13000000
#define ATH_PCI_WINDOW4_VAL		0x14000000
#define ATH_PCI_WINDOW5_VAL		0x15000000
#define ATH_PCI_WINDOW6_VAL		0x16000000
#define ATH_PCI_WINDOW7_VAL		0x07000000

#define ath_write_pci_window(_no)	\
	ath_reg_wr(ATH_PCI_WINDOW##_no##_OFFSET, ATH_PCI_WINDOW##_no##_VAL);

/*
 * CRP. To access the host controller config and status registers
 */
#define ATH_PCI_CRP			0x180c0000
#define ATH_PCI_DEV_CFGBASE		0x14000000
#define ATH_PCI_CRP_AD_CBE		ATH_PCI_CRP
#define ATH_PCI_CRP_WRDATA		ATH_PCI_CRP+0x4
#define ATH_PCI_CRP_RDDATA		ATH_PCI_CRP+0x8
#define ATH_PCI_ERROR			ATH_PCI_CRP+0x1c
#define ATH_PCI_ERROR_ADDRESS		ATH_PCI_CRP+0x20
#define ATH_PCI_AHB_ERROR		ATH_PCI_CRP+0x24
#define ATH_PCI_AHB_ERROR_ADDRESS	ATH_PCI_CRP+0x28

#define ATH_CRP_CMD_WRITE		0x00010000
#define ATH_CRP_CMD_READ		0x00000000

/*
 * PCI CFG. To generate config cycles
 */
#define ATH_PCI_CFG_AD			ATH_PCI_CRP+0xc
#define ATH_PCI_CFG_CBE			ATH_PCI_CRP+0x10
#define ATH_PCI_CFG_WRDATA		ATH_PCI_CRP+0x14
#define ATH_PCI_CFG_RDDATA		ATH_PCI_CRP+0x18
#define ATH_CFG_CMD_READ		0x0000000a
#define ATH_CFG_CMD_WRITE		0x0000000b

#define ATH_PCI_IDSEL_ADLINE_START	17


/* Interrupts connected to CPU->PCI */
#ifdef CONFIG_PERICOM
#	define ATH_PRI_BUS_NO		0u
#	define ATH_PORT0_BUS_NO		1u
#	define ATH_PORT1_BUS_NO		2u
#	define ATH_PCI_IRQ_DEV0		(ATH_PCI_IRQ_BASE + 0)
#	define ATH_PCI_IRQ_DEV1		(ATH_PCI_IRQ_BASE + 1)
#	define ATH_PCI_IRQ_COUNT	2
#else
#	define ATH_PCI_IRQ_DEV0		ATH_PCI_IRQ_BASE+0
#	define ATH_PCI_IRQ_COUNT	1
#endif /* CONFIG_PERICOM */

/*
 * PCI interrupt mask and status
 */
#define PIMR_DEV0			0x01
#define PIMR_DEV1			0x02
#define PIMR_DEV2			0x04
#define PIMR_CORE			0x10

#define PISR_DEV0			PIMR_DEV0
#define PISR_DEV1			PIMR_DEV1
#define PISR_DEV2			PIMR_DEV2
#define PISR_CORE			PIMR_CORE

#define ATH_GPIO_OE_EN(x)				(x)
#define ATH_GPIO_IN_ENABLE4_SLIC_PCM_FS_IN(x)		((0xff & x)<< 8)
#define ATH_GPIO_IN_ENABLE4_SLIC_DATA_IN(x)		     (0xff & x)
#define ATH_GPIO_OUT_FUNCTION3_ENABLE_GPIO_15(x)	((0xff & x)<<24)
#define ATH_GPIO_OUT_FUNCTION3_ENABLE_GPIO_14(x)	((0xff & x)<<16)
#define ATH_GPIO_OUT_FUNCTION3_ENABLE_GPIO_13(x)	((0xff & x)<< 8)
#define ATH_GPIO_OUT_FUNCTION3_ENABLE_GPIO_12(x)	 (0xff & x)
#define ATH_GPIO_OUT_FUNCTION2_ENABLE_GPIO_11(x)	((0xff & x)<<24)
#define ATH_GPIO_OUT_FUNCTION2_ENABLE_GPIO_10(x)	((0xff & x)<<16)
#define ATH_GPIO_OUT_FUNCTION2_ENABLE_GPIO_9(x)		((0xff & x)<< 8)
#define ATH_GPIO_OUT_FUNCTION2_ENABLE_GPIO_8(x)		 (0xff & x)
#define ATH_GPIO_OUT_FUNCTION1_ENABLE_GPIO_7(x)		((0xff & x)<<24)
#define ATH_GPIO_OUT_FUNCTION1_ENABLE_GPIO_6(x)		((0xff & x)<<16)
#define ATH_GPIO_OUT_FUNCTION1_ENABLE_GPIO_5(x)		((0xff & x)<< 8)
#define ATH_GPIO_OUT_FUNCTION1_ENABLE_GPIO_4(x)		 (0xff & x)
#define ATH_GPIO_OUT_FUNCTION0_ENABLE_GPIO_3(x)		((0xff & x)<<24)
#define ATH_GPIO_OUT_FUNCTION0_ENABLE_GPIO_2(x)		((0xff & x)<<16)
#define ATH_GPIO_IN_ENABLE1_I2SEXT_MCLK(x)		    ((0xff & x)<<24)
#define ATH_GPIO_IN_ENABLE0_UART_SIN(x)			    ((0xff & x)<< 8)
#define ATH_GPIO_IN_ENABLE0_SPI_DATA_IN(x)		     (0xff & x)

/*
 * Reset block
 */
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_MSB                    12
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_LSB                    12
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_MASK                   0x00001000
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_GET(x)                 (((x) & RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_SET(x)                 (((x) << RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_LSB) & RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_S26_MAC_INT_RESET                  0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_MSB                       7
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_LSB                       7
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_MASK                      0x00000080
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_GET(x)                    (((x) & RST_MISC_INTERRUPT_STATUS_MBOX_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_MBOX_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_SET(x)                    (((x) << RST_MISC_INTERRUPT_STATUS_MBOX_INT_LSB) & RST_MISC_INTERRUPT_STATUS_MBOX_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_MBOX_INT_RESET                     0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_MSB                   6
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_LSB                   6
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_MASK                  0x00000040
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_GET(x)                (((x) & RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_SET(x)                (((x) << RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_LSB) & RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_USB_OHCI_INT_RESET                 0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_PC_INT_MSB                         5
#define RST_MISC_INTERRUPT_STATUS_PC_INT_LSB                         5
#define RST_MISC_INTERRUPT_STATUS_PC_INT_MASK                        0x00000020
#define RST_MISC_INTERRUPT_STATUS_PC_INT_GET(x)                      (((x) & RST_MISC_INTERRUPT_STATUS_PC_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_PC_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_PC_INT_SET(x)                      (((x) << RST_MISC_INTERRUPT_STATUS_PC_INT_LSB) & RST_MISC_INTERRUPT_STATUS_PC_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_PC_INT_RESET                       0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_MSB                   4
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_LSB                   4
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_MASK                  0x00000010
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_GET(x)                (((x) & RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_SET(x)                (((x) << RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_LSB) & RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_WATCHDOG_INT_RESET                 0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_UART_INT_MSB                       3
#define RST_MISC_INTERRUPT_STATUS_UART_INT_LSB                       3
#define RST_MISC_INTERRUPT_STATUS_UART_INT_MASK                      0x00000008
#define RST_MISC_INTERRUPT_STATUS_UART_INT_GET(x)                    (((x) & RST_MISC_INTERRUPT_STATUS_UART_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_UART_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_UART_INT_SET(x)                    (((x) << RST_MISC_INTERRUPT_STATUS_UART_INT_LSB) & RST_MISC_INTERRUPT_STATUS_UART_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_UART_INT_RESET                     0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_MSB                       2
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_LSB                       2
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_MASK                      0x00000004
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_GET(x)                    (((x) & RST_MISC_INTERRUPT_STATUS_GPIO_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_GPIO_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_SET(x)                    (((x) << RST_MISC_INTERRUPT_STATUS_GPIO_INT_LSB) & RST_MISC_INTERRUPT_STATUS_GPIO_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_GPIO_INT_RESET                     0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_MSB                      1
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_LSB                      1
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_MASK                     0x00000002
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_GET(x)                   (((x) & RST_MISC_INTERRUPT_STATUS_ERROR_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_ERROR_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_SET(x)                   (((x) << RST_MISC_INTERRUPT_STATUS_ERROR_INT_LSB) & RST_MISC_INTERRUPT_STATUS_ERROR_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_ERROR_INT_RESET                    0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_MSB                      0
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_LSB                      0
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_MASK                     0x00000001
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_GET(x)                   (((x) & RST_MISC_INTERRUPT_STATUS_TIMER_INT_MASK) >> RST_MISC_INTERRUPT_STATUS_TIMER_INT_LSB)
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_SET(x)                   (((x) << RST_MISC_INTERRUPT_STATUS_TIMER_INT_LSB) & RST_MISC_INTERRUPT_STATUS_TIMER_INT_MASK)
#define RST_MISC_INTERRUPT_STATUS_TIMER_INT_RESET                    0x0 // 0
#define RST_MISC_INTERRUPT_STATUS_OFFSET                             0x0010

#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_MSB                 12
#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_LSB                 12
#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_MASK                0x00001000
#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_GET(x)              (((x) & RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_SET(x)              (((x) << RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_LSB) & RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_S26_MAC_INT_MASK_RESET               0x0 // 0
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_MSB                        7
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_LSB                        7
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_MASK                       0x00000080
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_GET(x)                     (((x) & RST_MISC_INTERRUPT_MASK_MBOX_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_MBOX_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_SET(x)                     (((x) << RST_MISC_INTERRUPT_MASK_MBOX_MASK_LSB) & RST_MISC_INTERRUPT_MASK_MBOX_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_MBOX_MASK_RESET                      0x0 // 0
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_MSB                    6
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_LSB                    6
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_MASK                   0x00000040
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_GET(x)                 (((x) & RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_SET(x)                 (((x) << RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_LSB) & RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_USB_OHCI_MASK_RESET                  0x0 // 0
#define RST_MISC_INTERRUPT_MASK_PC_MASK_MSB                          5
#define RST_MISC_INTERRUPT_MASK_PC_MASK_LSB                          5
#define RST_MISC_INTERRUPT_MASK_PC_MASK_MASK                         0x00000020
#define RST_MISC_INTERRUPT_MASK_PC_MASK_GET(x)                       (((x) & RST_MISC_INTERRUPT_MASK_PC_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_PC_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_PC_MASK_SET(x)                       (((x) << RST_MISC_INTERRUPT_MASK_PC_MASK_LSB) & RST_MISC_INTERRUPT_MASK_PC_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_PC_MASK_RESET                        0x0 // 0
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_MSB                    4
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_LSB                    4
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_MASK                   0x00000010
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_GET(x)                 (((x) & RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_SET(x)                 (((x) << RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_LSB) & RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_WATCHDOG_MASK_RESET                  0x0 // 0
#define RST_MISC_INTERRUPT_MASK_UART_MASK_MSB                        3
#define RST_MISC_INTERRUPT_MASK_UART_MASK_LSB                        3
#define RST_MISC_INTERRUPT_MASK_UART_MASK_MASK                       0x00000008
#define RST_MISC_INTERRUPT_MASK_UART_MASK_GET(x)                     (((x) & RST_MISC_INTERRUPT_MASK_UART_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_UART_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_UART_MASK_SET(x)                     (((x) << RST_MISC_INTERRUPT_MASK_UART_MASK_LSB) & RST_MISC_INTERRUPT_MASK_UART_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_UART_MASK_RESET                      0x0 // 0
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_MSB                        2
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_LSB                        2
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_MASK                       0x00000004
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_GET(x)                     (((x) & RST_MISC_INTERRUPT_MASK_GPIO_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_GPIO_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_SET(x)                     (((x) << RST_MISC_INTERRUPT_MASK_GPIO_MASK_LSB) & RST_MISC_INTERRUPT_MASK_GPIO_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_GPIO_MASK_RESET                      0x0 // 0
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_MSB                       1
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_LSB                       1
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_MASK                      0x00000002
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_GET(x)                    (((x) & RST_MISC_INTERRUPT_MASK_ERROR_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_ERROR_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_SET(x)                    (((x) << RST_MISC_INTERRUPT_MASK_ERROR_MASK_LSB) & RST_MISC_INTERRUPT_MASK_ERROR_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_ERROR_MASK_RESET                     0x0 // 0
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_MSB                       0
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_LSB                       0
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_MASK                      0x00000001
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_GET(x)                    (((x) & RST_MISC_INTERRUPT_MASK_TIMER_MASK_MASK) >> RST_MISC_INTERRUPT_MASK_TIMER_MASK_LSB)
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_SET(x)                    (((x) << RST_MISC_INTERRUPT_MASK_TIMER_MASK_LSB) & RST_MISC_INTERRUPT_MASK_TIMER_MASK_MASK)
#define RST_MISC_INTERRUPT_MASK_TIMER_MASK_RESET                     0x0 // 0
#define RST_MISC_INTERRUPT_MASK_OFFSET                               0x0014

#define RST_RESET_EXTERNAL_RESET_MSB                                 28
#define RST_RESET_EXTERNAL_RESET_LSB                                 28
#define RST_RESET_EXTERNAL_RESET_MASK                                0x10000000
#define RST_RESET_EXTERNAL_RESET_GET(x)                              (((x) & RST_RESET_EXTERNAL_RESET_MASK) >> RST_RESET_EXTERNAL_RESET_LSB)
#define RST_RESET_EXTERNAL_RESET_SET(x)                              (((x) << RST_RESET_EXTERNAL_RESET_LSB) & RST_RESET_EXTERNAL_RESET_MASK)
#define RST_RESET_EXTERNAL_RESET_RESET                               0x0 // 0
#define RST_RESET_CHKSUM_ACC_RESET_MSB                               25
#define RST_RESET_CHKSUM_ACC_RESET_LSB                               25
#define RST_RESET_CHKSUM_ACC_RESET_MASK                              0x02000000
#define RST_RESET_CHKSUM_ACC_RESET_GET(x)                            (((x) & RST_RESET_CHKSUM_ACC_RESET_MASK) >> RST_RESET_CHKSUM_ACC_RESET_LSB)
#define RST_RESET_CHKSUM_ACC_RESET_SET(x)                            (((x) << RST_RESET_CHKSUM_ACC_RESET_LSB) & RST_RESET_CHKSUM_ACC_RESET_MASK)
#define RST_RESET_CHKSUM_ACC_RESET_RESET                             0x0 // 0
#define RST_RESET_FULL_CHIP_RESET_MSB                                24
#define RST_RESET_FULL_CHIP_RESET_LSB                                24
#define RST_RESET_FULL_CHIP_RESET_MASK                               0x01000000
#define RST_RESET_FULL_CHIP_RESET_GET(x)                             (((x) & RST_RESET_FULL_CHIP_RESET_MASK) >> RST_RESET_FULL_CHIP_RESET_LSB)
#define RST_RESET_FULL_CHIP_RESET_SET(x)                             (((x) << RST_RESET_FULL_CHIP_RESET_LSB) & RST_RESET_FULL_CHIP_RESET_MASK)
#define RST_RESET_FULL_CHIP_RESET_RESET                              0x0 // 0
#define RST_RESET_CPU_NMI_MSB                                        21
#define RST_RESET_CPU_NMI_LSB                                        21
#define RST_RESET_CPU_NMI_MASK                                       0x00200000
#define RST_RESET_CPU_NMI_GET(x)                                     (((x) & RST_RESET_CPU_NMI_MASK) >> RST_RESET_CPU_NMI_LSB)
#define RST_RESET_CPU_NMI_SET(x)                                     (((x) << RST_RESET_CPU_NMI_LSB) & RST_RESET_CPU_NMI_MASK)
#define RST_RESET_CPU_NMI_RESET                                      0x0 // 0
#define RST_RESET_CPU_COLD_RESET_MSB                                 20
#define RST_RESET_CPU_COLD_RESET_LSB                                 20
#define RST_RESET_CPU_COLD_RESET_MASK                                0x00100000
#define RST_RESET_CPU_COLD_RESET_GET(x)                              (((x) & RST_RESET_CPU_COLD_RESET_MASK) >> RST_RESET_CPU_COLD_RESET_LSB)
#define RST_RESET_CPU_COLD_RESET_SET(x)                              (((x) << RST_RESET_CPU_COLD_RESET_LSB) & RST_RESET_CPU_COLD_RESET_MASK)
#define RST_RESET_CPU_COLD_RESET_RESET                               0x0 // 0
#define RST_RESET_GE1_MAC_RESET_MSB                                  13
#define RST_RESET_GE1_MAC_RESET_LSB                                  13
#define RST_RESET_GE1_MAC_RESET_MASK                                 0x00002000
#define RST_RESET_GE1_MAC_RESET_GET(x)                               (((x) & RST_RESET_GE1_MAC_RESET_MASK) >> RST_RESET_GE1_MAC_RESET_LSB)
#define RST_RESET_GE1_MAC_RESET_SET(x)                               (((x) << RST_RESET_GE1_MAC_RESET_LSB) & RST_RESET_GE1_MAC_RESET_MASK)
#define RST_RESET_GE1_MAC_RESET_RESET                                0x1 // 1
#define RST_RESET_PCIE_PHY_SERIAL                                    10
#define RST_RESET_PCIE_PHY_SERIAL_LSB                                10
#define RST_RESET_PCIE_PHY_SERIAL_MASK                               0x00000400
#define RST_RESET_PCIE_PHY_SERIAL_GET(x)                             (((x) & RST_RESET_PCIE_PHY_SERIAL_MASK) >> RST_RESET_PCIE_PHY_SERIAL_LSB)
#define RST_RESET_PCIE_PHY_SERIAL_SET(x)                             (((x) << RST_RESET_PCIE_PHY_SERIAL_LSB) & RST_RESET_PCIE_PHY_SERIAL_MASK)
#define RST_RESET_PCIE_PHY_SERIAL_RESET                              0x0 // 0
#define RST_RESET_GE0_MAC_RESET_MSB                                  9
#define RST_RESET_GE0_MAC_RESET_LSB                                  9
#define RST_RESET_GE0_MAC_RESET_MASK                                 0x00000200
#define RST_RESET_GE0_MAC_RESET_GET(x)                               (((x) & RST_RESET_GE0_MAC_RESET_MASK) >> RST_RESET_GE0_MAC_RESET_LSB)
#define RST_RESET_GE0_MAC_RESET_SET(x)                               (((x) << RST_RESET_GE0_MAC_RESET_LSB) & RST_RESET_GE0_MAC_RESET_MASK)
#define RST_RESET_GE0_MAC_RESET_RESET                                0x1 // 1
#define RST_RESET_ETH_SWITCH_RESET_MSB                               8
#define RST_RESET_ETH_SWITCH_RESET_LSB                               8
#define RST_RESET_ETH_SWITCH_RESET_MASK                              0x00000100
#define RST_RESET_ETH_SWITCH_RESET_GET(x)                            (((x) & RST_RESET_ETH_SWITCH_RESET_MASK) >> RST_RESET_ETH_SWITCH_RESET_LSB)
#define RST_RESET_ETH_SWITCH_RESET_SET(x)                            (((x) << RST_RESET_ETH_SWITCH_RESET_LSB) & RST_RESET_ETH_SWITCH_RESET_MASK)
#define RST_RESET_ETH_SWITCH_RESET_RESET                             0x1 // 1
#define RST_RESET_PCIE_PHY_RESET_MSB                                 7
#define RST_RESET_PCIE_PHY_RESET_LSB                                 7
#define RST_RESET_PCIE_PHY_RESET_MASK                                0x00000080
#define RST_RESET_PCIE_PHY_RESET_GET(x)                              (((x) & RST_RESET_PCIE_PHY_RESET_MASK) >> RST_RESET_PCIE_PHY_RESET_LSB)
#define RST_RESET_PCIE_PHY_RESET_SET(x)                              (((x) << RST_RESET_PCIE_PHY_RESET_LSB) & RST_RESET_PCIE_PHY_RESET_MASK)
#define RST_RESET_PCIE_PHY_RESET_RESET                               0x1 // 1
#define RST_RESET_PCIE_RESET_MSB                                     6
#define RST_RESET_PCIE_RESET_LSB                                     6
#define RST_RESET_PCIE_RESET_MASK                                    0x00000040
#define RST_RESET_PCIE_RESET_GET(x)                                  (((x) & RST_RESET_PCIE_RESET_MASK) >> RST_RESET_PCIE_RESET_LSB)
#define RST_RESET_PCIE_RESET_SET(x)                                  (((x) << RST_RESET_PCIE_RESET_LSB) & RST_RESET_PCIE_RESET_MASK)
#define RST_RESET_PCIE_RESET_RESET                                   0x1 // 1
#define RST_RESET_USB_HOST_RESET_MSB                                 5
#define RST_RESET_USB_HOST_RESET_LSB                                 5
#define RST_RESET_USB_HOST_RESET_MASK                                0x00000020
#define RST_RESET_USB_HOST_RESET_GET(x)                              (((x) & RST_RESET_USB_HOST_RESET_MASK) >> RST_RESET_USB_HOST_RESET_LSB)
#define RST_RESET_USB_HOST_RESET_SET(x)                              (((x) << RST_RESET_USB_HOST_RESET_LSB) & RST_RESET_USB_HOST_RESET_MASK)
#define RST_RESET_USB_HOST_RESET_RESET                               0x1 // 1

/*
 * Performace counters
 */
#define ATH_PERF0_COUNTER		ATH_GE0_BASE+0xa0
#define ATH_PERF1_COUNTER		ATH_GE1_BASE+0xa0

/*
 * SLIC/STEREO DMA Size Configurations
 */
#define ATH_DMA_BUF_SIZE_4X2		0x00
#define ATH_DMA_BUF_SIZE_8X2		0x01
#define ATH_DMA_BUF_SIZE_16X2		0x02
#define ATH_DMA_BUF_SIZE_32X2		0x03
#define ATH_DMA_BUF_SIZE_64X2		0x04
#define ATH_DMA_BUF_SIZE_128X2		0x05
#define ATH_DMA_BUF_SIZE_256X2		0x06
#define ATH_DMA_BUF_SIZE_512X2		0x07

/*
 * SLIC/STEREO DMA Assignments
 */
#define ATH_DMA_CHAN_SLIC0_RX		0
#define ATH_DMA_CHAN_SLIC1_RX		1
#define ATH_DMA_CHAN_STEREO_RX		2
#define ATH_DMA_CHAN_SLIC0_TX		3
#define ATH_DMA_CHAN_SLIC1_TX		4
#define ATH_DMA_CHAN_STEREO_TX		5

/*
 * MBOX register definitions
 */
#define ATH_MBOX_FIFO				(ATH_DMA_BASE+0x00)
#define ATH_MBOX_FIFO_STATUS			(ATH_DMA_BASE+0x08)
#define ATH_MBOX_SLIC_FIFO_STATUS		(ATH_DMA_BASE+0x0c)
#define ATH_MBOX_DMA_POLICY			(ATH_DMA_BASE+0x10)
#define ATH_MBOX_SLIC_DMA_POLICY		(ATH_DMA_BASE+0x14)
#define ATH_MBOX_DMA_RX_DESCRIPTOR_BASE0	(ATH_DMA_BASE+0x18)
#define ATH_MBOX_DMA_RX_CONTROL0		(ATH_DMA_BASE+0x1c)
#define ATH_MBOX_DMA_TX_DESCRIPTOR_BASE0	(ATH_DMA_BASE+0x20)
#define ATH_MBOX_DMA_TX_CONTROL0		(ATH_DMA_BASE+0x24)
#define ATH_MBOX_DMA_RX_DESCRIPTOR_BASE1	(ATH_DMA_BASE+0x28)
#define ATH_MBOX_DMA_RX_CONTROL1		(ATH_DMA_BASE+0x2c)
#define ATH_MBOX_DMA_TX_DESCRIPTOR_BASE1	(ATH_DMA_BASE+0x30)
#define ATH_MBOX_DMA_TX_CONTROL1		(ATH_DMA_BASE+0x34)
#define ATH_MBOX_FRAME				(ATH_DMA_BASE+0x34)
#define ATH_MBOX_SLIC_FRAME			(ATH_DMA_BASE+0x3c)
#define ATH_MBOX_FIFO_TIMEOUT			(ATH_DMA_BASE+0x40)
#define ATH_MBOX_INT_STATUS			(ATH_DMA_BASE+0x44)
#define ATH_MBOX_SLIC_INT_STATUS		(ATH_DMA_BASE+0x48)
#define ATH_MBOX_INT_ENABLE			(ATH_DMA_BASE+0x4c)
#define ATH_MBOX_SLIC_INT_ENABLE		(ATH_DMA_BASE+0x50)
#define ATH_MBOX_FIFO_RESET			(ATH_DMA_BASE+0x58)
#define ATH_MBOX_SLIC_FIFO_RESET		(ATH_DMA_BASE+0x5c)

#define ATH_MBOX_DMA_POLICY_RX_QUANTUM		(1<< 1)
#define ATH_MBOX_DMA_POLICY_TX_QUANTUM		(1<< 3)
#define ATH_MBOX_DMA_POLICY_TX_FIFO_THRESH(x)	((0xff&x)<< 4)

/*
 * MBOX Enables
 */
#define ATH_MBOX_DMA_POLICY_RX_QUANTUM		(1<< 1)
#define ATH_MBOX_DMA_POLICY_TX_QUANTUM		(1<< 3)
#define ATH_MBOX_DMA_POLICY_TX_FIFO_THRESH(x)	((0xff&x)<< 4)

/*
 * SLIC register definitions
 */
#define ATH_SLIC_STATUS				(ATH_SLIC_BASE+0x00)
#define ATH_SLIC_CNTRL				(ATH_SLIC_BASE+0x04)
#define ATH_SLIC_SLOT0_NUM			(ATH_SLIC_BASE+0x08)
#define ATH_SLIC_SLOT1_NUM			(ATH_SLIC_BASE+0x0c)
#define ATH_SLIC_SAM_POS			(ATH_SLIC_BASE+0x2c)
#define ATH_SLIC_FREQ_DIV			(ATH_SLIC_BASE+0x30)

/*
 * SLIC Control bits
 */
#define ATH_SLIC_CNTRL_ENABLE			(1<<0)
#define ATH_SLIC_CNTRL_SLOT0_ENABLE		(1<<1)
#define ATH_SLIC_CNTRL_SLOT1_ENABLE		(1<<2)
#define ATH_SLIC_CNTRL_IRQ_ENABLE		(1<<3)

/*
 * STEREO register definitions
 */
#define ATH_STEREO_CONFIG			(ATH_STEREO_BASE+0x00)
#define ATH_STEREO_VOLUME			(ATH_STEREO_BASE+0x04)
#define ATH_STEREO_MCLK				(ATH_STEREO_BASE+0x08)

/*
 * Stereo Configuration Bits
 */
#define ATH_STEREO_CONFIG_SPDIF_ENABLE		(1<<23)
#define ATH_STEREO_CONFIG_ENABLE		(1<<21)
#define ATH_STEREO_CONFIG_RESET			(1<<19)
#define ATH_STEREO_CONFIG_DELAY			(1<<18)
#define ATH_STEREO_CONFIG_PCM_SWAP		(1<<17)
#define ATH_STEREO_CONFIG_MIC_WORD_SIZE		(1<<16)
#define ATH_STEREO_CONFIG_MODE(x)		((3&x)<<14)
#define ATH_STEREO_MODE_STEREO			0
#define ATH_STEREO_MODE_LEFT			1
#define ATH_STEREO_MODE_RIGHT			2
#define ATH_STEREO_CONFIG_DATA_WORD_SIZE(x)	((3&x)<<12)
#define ATH_STEREO_CONFIG_I2S_32B_WORD		(1<<11)
#define ATH_STEREO_CONFIG_I2S_MCLK_SEL		(1<<10)
#define ATH_STEREO_CONFIG_SAMPLE_CNT_CLEAR_TYPE	(1<<9)
#define ATH_STEREO_CONFIG_MASTER		(1<<8)
#define ATH_STEREO_CONFIG_PSEDGE(x)		(0xff&x)

/*
 * Word sizes to use with common configurations:
 */
#define ATH_STEREO_WS_8B		0
#define ATH_STEREO_WS_16B		1
#define ATH_STEREO_WS_24B		2
#define ATH_STEREO_WS_32B		3

/*
 * Slic Configuration Bits
 */
#define ATH_SLIC_SLOT_SEL(x)				(0x7f&x)
#define ATH_SLIC_CLOCK_CTRL_DIV(x)			(0x3f&x)
#define ATH_SLIC_CTRL_CLK_EN				(1<<3)
#define ATH_SLIC_CTRL_MASTER				(1<<2)
#define ATH_SLIC_CTRL_EN				(1<<1)
#define ATH_SLIC_TX_SLOTS1_EN(x)			(x)
#define ATH_SLIC_TX_SLOTS2_EN(x)			(x)
#define ATH_SLIC_RX_SLOTS1_EN(x)			(x)
#define ATH_SLIC_RX_SLOTS2_EN(x)			(x)
#define ATH_SLIC_TIMING_CTRL_RXDATA_SAMPLE_POS_EXTEND	(1<<11)
#define ATH_SLIC_TIMING_CTRL_DATAOEN_ALWAYS		(1<<9)
#define ATH_SLIC_TIMING_CTRL_RXDATA_SAMPLE_POS(x)	((0x3&x)<<7)
#define ATH_SLIC_TIMING_CTRL_TXDATA_FS_SYNC(x)		((0x3&x)<<5)
#define ATH_SLIC_TIMING_CTRL_LONG_FSCLKS(x)		((0x7&x)<<2)
#define ATH_SLIC_TIMING_CTRL_FS_POS			(1<<1)
#define ATH_SLIC_TIMING_CTRL_LONG_FS			(1<<0)
#define ATH_SLIC_INTR_MASK(x)				(0x1f&x)
#define ATH_SLIC_SWAP_RX_DATA				(1<<1)
#define ATH_SLIC_SWAP_TX_DATA				(1<<0)

#define ATH_SLIC_TIMING_CTRL_RXDATA_SAMPLE_POS_2_NGEDGE	2
#define ATH_SLIC_TIMING_CTRL_RXDATA_SAMPLE_POS_1_NGEDGE	1
#define ATH_SLIC_TIMING_CTRL_TXDATA_FS_SYNC_NXT_PSEDGE	2
#define ATH_SLIC_TIMING_CTRL_TXDATA_FS_SYNC_NXT_NGEDGE	3
#define ATH_SLIC_TIMING_CTRL_LONG_FSCLKS_1BIT		0
#define ATH_SLIC_TIMING_CTRL_LONG_FSCLKS_8BIT		7
#define ATH_SLIC_INTR_STATUS_NO_INTR			0
#define ATH_SLIC_INTR_STATUS_UNEXP_FRAME		1
#define ATH_SLIC_INTR_MASK_RESET			0x1f
#define ATH_SLIC_INTR_MASK_0				1
#define ATH_SLIC_INTR_MASK_1				2
#define ATH_SLIC_INTR_MASK_2				4
#define ATH_SLIC_INTR_MASK_3				8
#define ATH_SLIC_INTR_MASK_4				16

/*
 * Common configurations for stereo block
 */
#define ATH_STEREO_CFG_MASTER_STEREO_FS32_48KHZ(ws) ( \
	ATH_STEREO_CONFIG_DELAY | \
	ATH_STEREO_CONFIG_RESET | \
	ATH_STEREO_CONFIG_DATA_WORD_SIZE(ws) | \
	ATH_STEREO_CONFIG_MODE(ATH_STEREO_MODE_LEFT) | \
	ATH_STEREO_CONFIG_MASTER | \
	ATH_STEREO_CONFIG_PSEDGE(26))

#define ATH_STEREO_CFG_MASTER_STEREO_FS64_48KHZ(ws) ( \
	ATH_STEREO_CONFIG_DELAY | \
	ATH_STEREO_CONFIG_RESET | \
	ATH_STEREO_CONFIG_DATA_WORD_SIZE(ws) | \
	ATH_STEREO_CONFIG_MODE(ATH_STEREO_MODE_STEREO) | \
	ATH_STEREO_CONFIG_I2S_32B_WORD | \
	ATH_STEREO_CONFIG_MASTER | \
	ATH_STEREO_CONFIG_PSEDGE(13))

#define ATH_STEREO_CFG_SLAVE_STEREO_FS32_48KHZ(ws) ( \
	ATH_STEREO_CONFIG_RESET | \
	ATH_STEREO_CONFIG_DATA_WORD_SIZE(ws) | \
	ATH_STEREO_CONFIG_MODE(ATH_STEREO_MODE_STEREO) | \
	ATH_STEREO_CONFIG_PSEDGE(26))

#define ATH_STEREO_CFG_SLAVE_STEREO_FS64_48KHZ(ws) ( \
	ATH_STEREO_CONFIG_RESET | \
	ATH_STEREO_CONFIG_I2S_32B_WORD | \
	ATH_STEREO_CONFIG_DATA_WORD_SIZE(ws) | \
	ATH_STEREO_CONFIG_MODE(ATH_STEREO_MODE_STEREO) | \
	ATH_STEREO_CONFIG_PSEDGE(13))

/*
 * PERF CTL bits
 */
#define PERF_CTL_PCI_AHB_0		( 0)
#define PERF_CTL_PCI_AHB_1		( 1)
#define PERF_CTL_USB_0			( 2)
#define PERF_CTL_USB_1			( 3)
#define PERF_CTL_GE0_PKT_CNT		( 4)
#define PERF_CTL_GEO_AHB_1		( 5)
#define PERF_CTL_GE1_PKT_CNT		( 6)
#define PERF_CTL_GE1_AHB_1		( 7)
#define PERF_CTL_PCI_DEV_0_BUSY		( 8)
#define PERF_CTL_PCI_DEV_1_BUSY		( 9)
#define PERF_CTL_PCI_DEV_2_BUSY		(10)
#define PERF_CTL_PCI_HOST_BUSY		(11)
#define PERF_CTL_PCI_DEV_0_ARB		(12)
#define PERF_CTL_PCI_DEV_1_ARB		(13)
#define PERF_CTL_PCI_DEV_2_ARB		(14)
#define PERF_CTL_PCI_HOST_ARB		(15)
#define PERF_CTL_PCI_DEV_0_ACTIVE	(16)
#define PERF_CTL_PCI_DEV_1_ACTIVE	(17)
#define PERF_CTL_PCI_DEV_2_ACTIVE	(18)
#define PERF_CTL_HOST_ACTIVE		(19)

/*
 * Mii block
 */
#define ATH_MII0_CTRL		0x18070000
#define ATH_MII1_CTRL		0x18070004

#define ATH_DDR_GE0_FLUSH   (AR71XX_DDR_CTRL_BASE + AR724X_DDR_REG_FLUSH_GE0)
#define ATH_DDR_GE1_FLUSH   (AR71XX_DDR_CTRL_BASE + AR724X_DDR_REG_FLUSH_GE1)
#define ATH_DDR_PCIE_FLUSH  (AR71XX_DDR_CTRL_BASE + AR724X_DDR_REG_FLUSH_PCIE)
#define ATH_DDR_USB_FLUSH   (AR71XX_DDR_CTRL_BASE + AR724X_DDR_REG_FLUSH_USB)

#define ath_flush_ge(_unit) do { \
	u32 reg = (_unit) ? ATH_DDR_GE1_FLUSH : ATH_DDR_GE0_FLUSH; \
	ath_reg_wr(reg, 1); \
	while((ath_reg_rd(reg) & 0x1)); \
	ath_reg_wr(reg, 1); \
	while((ath_reg_rd(reg) & 0x1)); \
} while(0)

#define ath_flush_pcie() do { \
	ath_reg_wr(ATH_DDR_PCIE_FLUSH, 1); \
	while((ath_reg_rd(ATH_DDR_PCIE_FLUSH) & 0x1)); \
	ath_reg_wr(ATH_DDR_PCIE_FLUSH, 1); \
	while((ath_reg_rd(ATH_DDR_PCIE_FLUSH) & 0x1)); \
} while(0)

#define ath_flush_USB() do { \
	ath_reg_wr(ATH_DDR_USB_FLUSH, 1); \
	while((ath_reg_rd(ATH_DDR_USB_FLUSH) & 0x1)); \
	ath_reg_wr(ATH_DDR_USB_FLUSH, 1); \
	while((ath_reg_rd(ATH_DDR_USB_FLUSH) & 0x1)); \
} while(0)

/*------------------------------------------------------------------------------------------*\
 * Register Definitions
\*------------------------------------------------------------------------------------------*/
// 32'h002c (ETH_XMII)
#define ETH_XMII_TX_INVERT_MSB                                       31
#define ETH_XMII_TX_INVERT_LSB                                       31
#define ETH_XMII_TX_INVERT_MASK                                      0x80000000
#define ETH_XMII_TX_INVERT_GET(x)                                    (((x) & ETH_XMII_TX_INVERT_MASK) >> ETH_XMII_TX_INVERT_LSB)
#define ETH_XMII_TX_INVERT_SET(x)                                    (((x) << ETH_XMII_TX_INVERT_LSB) & ETH_XMII_TX_INVERT_MASK)
#define ETH_XMII_TX_INVERT_RESET                                     0
#define ETH_XMII_GIGE_QUAD_MSB                                       30
#define ETH_XMII_GIGE_QUAD_LSB                                       30
#define ETH_XMII_GIGE_QUAD_MASK                                      0x40000000
#define ETH_XMII_GIGE_QUAD_GET(x)                                    (((x) & ETH_XMII_GIGE_QUAD_MASK) >> ETH_XMII_GIGE_QUAD_LSB)
#define ETH_XMII_GIGE_QUAD_SET(x)                                    (((x) << ETH_XMII_GIGE_QUAD_LSB) & ETH_XMII_GIGE_QUAD_MASK)
#define ETH_XMII_GIGE_QUAD_RESET                                     0
#define ETH_XMII_RX_DELAY_MSB                                        29
#define ETH_XMII_RX_DELAY_LSB                                        28
#define ETH_XMII_RX_DELAY_MASK                                       0x30000000
#define ETH_XMII_RX_DELAY_GET(x)                                     (((x) & ETH_XMII_RX_DELAY_MASK) >> ETH_XMII_RX_DELAY_LSB)
#define ETH_XMII_RX_DELAY_SET(x)                                     (((x) << ETH_XMII_RX_DELAY_LSB) & ETH_XMII_RX_DELAY_MASK)
#define ETH_XMII_RX_DELAY_RESET                                      0
#define ETH_XMII_TX_DELAY_MSB                                        27
#define ETH_XMII_TX_DELAY_LSB                                        26
#define ETH_XMII_TX_DELAY_MASK                                       0x0c000000
#define ETH_XMII_TX_DELAY_GET(x)                                     (((x) & ETH_XMII_TX_DELAY_MASK) >> ETH_XMII_TX_DELAY_LSB)
#define ETH_XMII_TX_DELAY_SET(x)                                     (((x) << ETH_XMII_TX_DELAY_LSB) & ETH_XMII_TX_DELAY_MASK)
#define ETH_XMII_TX_DELAY_RESET                                      0
#define ETH_XMII_GIGE_MSB                                            25
#define ETH_XMII_GIGE_LSB                                            25
#define ETH_XMII_GIGE_MASK                                           0x02000000
#define ETH_XMII_GIGE_GET(x)                                         (((x) & ETH_XMII_GIGE_MASK) >> ETH_XMII_GIGE_LSB)
#define ETH_XMII_GIGE_SET(x)                                         (((x) << ETH_XMII_GIGE_LSB) & ETH_XMII_GIGE_MASK)
#define ETH_XMII_GIGE_RESET                                          0
#define ETH_XMII_OFFSET_PHASE_MSB                                    24
#define ETH_XMII_OFFSET_PHASE_LSB                                    24
#define ETH_XMII_OFFSET_PHASE_MASK                                   0x01000000
#define ETH_XMII_OFFSET_PHASE_GET(x)                                 (((x) & ETH_XMII_OFFSET_PHASE_MASK) >> ETH_XMII_OFFSET_PHASE_LSB)
#define ETH_XMII_OFFSET_PHASE_SET(x)                                 (((x) << ETH_XMII_OFFSET_PHASE_LSB) & ETH_XMII_OFFSET_PHASE_MASK)
#define ETH_XMII_OFFSET_PHASE_RESET                                  0
#define ETH_XMII_OFFSET_COUNT_MSB                                    23
#define ETH_XMII_OFFSET_COUNT_LSB                                    16
#define ETH_XMII_OFFSET_COUNT_MASK                                   0x00ff0000
#define ETH_XMII_OFFSET_COUNT_GET(x)                                 (((x) & ETH_XMII_OFFSET_COUNT_MASK) >> ETH_XMII_OFFSET_COUNT_LSB)
#define ETH_XMII_OFFSET_COUNT_SET(x)                                 (((x) << ETH_XMII_OFFSET_COUNT_LSB) & ETH_XMII_OFFSET_COUNT_MASK)
#define ETH_XMII_OFFSET_COUNT_RESET                                  0
#define ETH_XMII_PHASE1_COUNT_MSB                                    15
#define ETH_XMII_PHASE1_COUNT_LSB                                    8
#define ETH_XMII_PHASE1_COUNT_MASK                                   0x0000ff00
#define ETH_XMII_PHASE1_COUNT_GET(x)                                 (((x) & ETH_XMII_PHASE1_COUNT_MASK) >> ETH_XMII_PHASE1_COUNT_LSB)
#define ETH_XMII_PHASE1_COUNT_SET(x)                                 (((x) << ETH_XMII_PHASE1_COUNT_LSB) & ETH_XMII_PHASE1_COUNT_MASK)
#define ETH_XMII_PHASE1_COUNT_RESET                                  1
#define ETH_XMII_PHASE0_COUNT_MSB                                    7
#define ETH_XMII_PHASE0_COUNT_LSB                                    0
#define ETH_XMII_PHASE0_COUNT_MASK                                   0x000000ff
#define ETH_XMII_PHASE0_COUNT_GET(x)                                 (((x) & ETH_XMII_PHASE0_COUNT_MASK) >> ETH_XMII_PHASE0_COUNT_LSB)
#define ETH_XMII_PHASE0_COUNT_SET(x)                                 (((x) << ETH_XMII_PHASE0_COUNT_LSB) & ETH_XMII_PHASE0_COUNT_MASK)
#define ETH_XMII_PHASE0_COUNT_RESET                                  1
#define ETH_XMII_ADDRESS                                             0x002c
#define ETH_XMII_OFFSET                                              0x002c
// SW modifiable bits
#define ETH_XMII_SW_MASK                                             0xffffffff
// bits defined at reset
#define ETH_XMII_RSTMASK                                             0xffffffff
// reset value (ignore bits undefined at reset)
#define ETH_XMII_RESET                                               0x00000101


#define PCIE_RC_REG_ADDRESS                                          0x180c0000

#define PCIE_APP_CFG_TYPE_MSB                                        21
#define PCIE_APP_CFG_TYPE_LSB                                        20
#define PCIE_APP_CFG_TYPE_MASK                                       0x00300000
#define PCIE_APP_CFG_TYPE_GET(x)                                     (((x) & PCIE_APP_CFG_TYPE_MASK) >> PCIE_APP_CFG_TYPE_LSB)
#define PCIE_APP_CFG_TYPE_SET(x)                                     (((x) << PCIE_APP_CFG_TYPE_LSB) & PCIE_APP_CFG_TYPE_MASK)
#define PCIE_APP_CFG_TYPE_RESET                                      0x0 // 0
#define PCIE_APP_PCIE_BAR_MSN_MSB                                    19
#define PCIE_APP_PCIE_BAR_MSN_LSB                                    16
#define PCIE_APP_PCIE_BAR_MSN_MASK                                   0x000f0000
#define PCIE_APP_PCIE_BAR_MSN_GET(x)                                 (((x) & PCIE_APP_PCIE_BAR_MSN_MASK) >> PCIE_APP_PCIE_BAR_MSN_LSB)
#define PCIE_APP_PCIE_BAR_MSN_SET(x)                                 (((x) << PCIE_APP_PCIE_BAR_MSN_LSB) & PCIE_APP_PCIE_BAR_MSN_MASK)
#define PCIE_APP_PCIE_BAR_MSN_RESET                                  0x1 // 1
#define PCIE_APP_CFG_BE_MSB                                          15
#define PCIE_APP_CFG_BE_LSB                                          12
#define PCIE_APP_CFG_BE_MASK                                         0x0000f000
#define PCIE_APP_CFG_BE_GET(x)                                       (((x) & PCIE_APP_CFG_BE_MASK) >> PCIE_APP_CFG_BE_LSB)
#define PCIE_APP_CFG_BE_SET(x)                                       (((x) << PCIE_APP_CFG_BE_LSB) & PCIE_APP_CFG_BE_MASK)
#define PCIE_APP_CFG_BE_RESET                                        0xf // 15
#define PCIE_APP_SLV_RESP_ERR_MAP_MSB                                11
#define PCIE_APP_SLV_RESP_ERR_MAP_LSB                                6
#define PCIE_APP_SLV_RESP_ERR_MAP_MASK                               0x00000fc0
#define PCIE_APP_SLV_RESP_ERR_MAP_GET(x)                             (((x) & PCIE_APP_SLV_RESP_ERR_MAP_MASK) >> PCIE_APP_SLV_RESP_ERR_MAP_LSB)
#define PCIE_APP_SLV_RESP_ERR_MAP_SET(x)                             (((x) << PCIE_APP_SLV_RESP_ERR_MAP_LSB) & PCIE_APP_SLV_RESP_ERR_MAP_MASK)
#define PCIE_APP_SLV_RESP_ERR_MAP_RESET                              0x3f // 63
#define PCIE_APP_MSTR_RESP_ERR_MAP_MSB                               5
#define PCIE_APP_MSTR_RESP_ERR_MAP_LSB                               4
#define PCIE_APP_MSTR_RESP_ERR_MAP_MASK                              0x00000030
#define PCIE_APP_MSTR_RESP_ERR_MAP_GET(x)                            (((x) & PCIE_APP_MSTR_RESP_ERR_MAP_MASK) >> PCIE_APP_MSTR_RESP_ERR_MAP_LSB)
#define PCIE_APP_MSTR_RESP_ERR_MAP_SET(x)                            (((x) << PCIE_APP_MSTR_RESP_ERR_MAP_LSB) & PCIE_APP_MSTR_RESP_ERR_MAP_MASK)
#define PCIE_APP_MSTR_RESP_ERR_MAP_RESET                             0x0 // 0
#define PCIE_APP_INIT_RST_MSB                                        3
#define PCIE_APP_INIT_RST_LSB                                        3
#define PCIE_APP_INIT_RST_MASK                                       0x00000008
#define PCIE_APP_INIT_RST_GET(x)                                     (((x) & PCIE_APP_INIT_RST_MASK) >> PCIE_APP_INIT_RST_LSB)
#define PCIE_APP_INIT_RST_SET(x)                                     (((x) << PCIE_APP_INIT_RST_LSB) & PCIE_APP_INIT_RST_MASK)
#define PCIE_APP_INIT_RST_RESET                                      0x0 // 0
#define PCIE_APP_PM_XMT_TURNOFF_MSB                                  2
#define PCIE_APP_PM_XMT_TURNOFF_LSB                                  2
#define PCIE_APP_PM_XMT_TURNOFF_MASK                                 0x00000004
#define PCIE_APP_PM_XMT_TURNOFF_GET(x)                               (((x) & PCIE_APP_PM_XMT_TURNOFF_MASK) >> PCIE_APP_PM_XMT_TURNOFF_LSB)
#define PCIE_APP_PM_XMT_TURNOFF_SET(x)                               (((x) << PCIE_APP_PM_XMT_TURNOFF_LSB) & PCIE_APP_PM_XMT_TURNOFF_MASK)
#define PCIE_APP_PM_XMT_TURNOFF_RESET                                0x0 // 0
#define PCIE_APP_UNLOCK_MSG_MSB                                      1
#define PCIE_APP_UNLOCK_MSG_LSB                                      1
#define PCIE_APP_UNLOCK_MSG_MASK                                     0x00000002
#define PCIE_APP_UNLOCK_MSG_GET(x)                                   (((x) & PCIE_APP_UNLOCK_MSG_MASK) >> PCIE_APP_UNLOCK_MSG_LSB)
#define PCIE_APP_UNLOCK_MSG_SET(x)                                   (((x) << PCIE_APP_UNLOCK_MSG_LSB) & PCIE_APP_UNLOCK_MSG_MASK)
#define PCIE_APP_UNLOCK_MSG_RESET                                    0x0 // 0
#define PCIE_APP_LTSSM_ENABLE_MSB                                    0
#define PCIE_APP_LTSSM_ENABLE_LSB                                    0
#define PCIE_APP_LTSSM_ENABLE_MASK                                   0x00000001
#define PCIE_APP_LTSSM_ENABLE_GET(x)                                 (((x) & PCIE_APP_LTSSM_ENABLE_MASK) >> PCIE_APP_LTSSM_ENABLE_LSB)
#define PCIE_APP_LTSSM_ENABLE_SET(x)                                 (((x) << PCIE_APP_LTSSM_ENABLE_LSB) & PCIE_APP_LTSSM_ENABLE_MASK)
#define PCIE_APP_LTSSM_ENABLE_RESET                                  0x0 // 0
#define PCIE_APP_ADDRESS                                             0x180f0000

#define PCIE_RESET_EP_RESET_L_MSB                                    2
#define PCIE_RESET_EP_RESET_L_LSB                                    2
#define PCIE_RESET_EP_RESET_L_MASK                                   0x00000004
#define PCIE_RESET_EP_RESET_L_GET(x)                                 (((x) & PCIE_RESET_EP_RESET_L_MASK) >> PCIE_RESET_EP_RESET_L_LSB)
#define PCIE_RESET_EP_RESET_L_SET(x)                                 (((x) << PCIE_RESET_EP_RESET_L_LSB) & PCIE_RESET_EP_RESET_L_MASK)
#define PCIE_RESET_EP_RESET_L_RESET                                  0x0 // 0
#define PCIE_RESET_LINK_REQ_RESET_MSB                                1
#define PCIE_RESET_LINK_REQ_RESET_LSB                                1
#define PCIE_RESET_LINK_REQ_RESET_MASK                               0x00000002
#define PCIE_RESET_LINK_REQ_RESET_GET(x)                             (((x) & PCIE_RESET_LINK_REQ_RESET_MASK) >> PCIE_RESET_LINK_REQ_RESET_LSB)
#define PCIE_RESET_LINK_REQ_RESET_SET(x)                             (((x) << PCIE_RESET_LINK_REQ_RESET_LSB) & PCIE_RESET_LINK_REQ_RESET_MASK)
#define PCIE_RESET_LINK_REQ_RESET_RESET                              0x0 // 0
#define PCIE_RESET_LINK_UP_MSB                                       0
#define PCIE_RESET_LINK_UP_LSB                                       0
#define PCIE_RESET_LINK_UP_MASK                                      0x00000001
#define PCIE_RESET_LINK_UP_GET(x)                                    (((x) & PCIE_RESET_LINK_UP_MASK) >> PCIE_RESET_LINK_UP_LSB)
#define PCIE_RESET_LINK_UP_SET(x)                                    (((x) << PCIE_RESET_LINK_UP_LSB) & PCIE_RESET_LINK_UP_MASK)
#define PCIE_RESET_LINK_UP_RESET                                     0x0 // 0
#define PCIE_RESET_ADDRESS                                           0x180f0018
#define PCIE_INT_STATUS_LINK_DOWN_MSB                                27
#define PCIE_INT_STATUS_LINK_DOWN_LSB                                27
#define PCIE_INT_STATUS_LINK_DOWN_MASK                               0x08000000
#define PCIE_INT_STATUS_LINK_DOWN_GET(x)                             (((x) & PCIE_INT_STATUS_LINK_DOWN_MASK) >> PCIE_INT_STATUS_LINK_DOWN_LSB)
#define PCIE_INT_STATUS_LINK_DOWN_SET(x)                             (((x) << PCIE_INT_STATUS_LINK_DOWN_LSB) & PCIE_INT_STATUS_LINK_DOWN_MASK)
#define PCIE_INT_STATUS_LINK_DOWN_RESET                              0x0 // 0
#define PCIE_INT_STATUS_LINK_REQ_RST_MSB                             26
#define PCIE_INT_STATUS_LINK_REQ_RST_LSB                             26
#define PCIE_INT_STATUS_LINK_REQ_RST_MASK                            0x04000000
#define PCIE_INT_STATUS_LINK_REQ_RST_GET(x)                          (((x) & PCIE_INT_STATUS_LINK_REQ_RST_MASK) >> PCIE_INT_STATUS_LINK_REQ_RST_LSB)
#define PCIE_INT_STATUS_LINK_REQ_RST_SET(x)                          (((x) << PCIE_INT_STATUS_LINK_REQ_RST_LSB) & PCIE_INT_STATUS_LINK_REQ_RST_MASK)
#define PCIE_INT_STATUS_LINK_REQ_RST_RESET                           0x0 // 0
#define PCIE_INT_STATUS_MSI_VEC_MSB                                  25
#define PCIE_INT_STATUS_MSI_VEC_LSB                                  22
#define PCIE_INT_STATUS_MSI_VEC_MASK                                 0x03c00000
#define PCIE_INT_STATUS_MSI_VEC_GET(x)                               (((x) & PCIE_INT_STATUS_MSI_VEC_MASK) >> PCIE_INT_STATUS_MSI_VEC_LSB)
#define PCIE_INT_STATUS_MSI_VEC_SET(x)                               (((x) << PCIE_INT_STATUS_MSI_VEC_LSB) & PCIE_INT_STATUS_MSI_VEC_MASK)
#define PCIE_INT_STATUS_MSI_VEC_RESET                                0x0 // 0
#define PCIE_INT_STATUS_CPU_INTD_MSB                                 21
#define PCIE_INT_STATUS_CPU_INTD_LSB                                 21
#define PCIE_INT_STATUS_CPU_INTD_MASK                                0x00200000
#define PCIE_INT_STATUS_CPU_INTD_GET(x)                              (((x) & PCIE_INT_STATUS_CPU_INTD_MASK) >> PCIE_INT_STATUS_CPU_INTD_LSB)
#define PCIE_INT_STATUS_CPU_INTD_SET(x)                              (((x) << PCIE_INT_STATUS_CPU_INTD_LSB) & PCIE_INT_STATUS_CPU_INTD_MASK)
#define PCIE_INT_STATUS_CPU_INTD_RESET                               0x0 // 0
#define PCIE_INT_STATUS_CPU_INTC_MSB                                 20
#define PCIE_INT_STATUS_CPU_INTC_LSB                                 20
#define PCIE_INT_STATUS_CPU_INTC_MASK                                0x00100000
#define PCIE_INT_STATUS_CPU_INTC_GET(x)                              (((x) & PCIE_INT_STATUS_CPU_INTC_MASK) >> PCIE_INT_STATUS_CPU_INTC_LSB)
#define PCIE_INT_STATUS_CPU_INTC_SET(x)                              (((x) << PCIE_INT_STATUS_CPU_INTC_LSB) & PCIE_INT_STATUS_CPU_INTC_MASK)
#define PCIE_INT_STATUS_CPU_INTC_RESET                               0x0 // 0
#define PCIE_INT_STATUS_CPU_INTB_MSB                                 19
#define PCIE_INT_STATUS_CPU_INTB_LSB                                 19
#define PCIE_INT_STATUS_CPU_INTB_MASK                                0x00080000
#define PCIE_INT_STATUS_CPU_INTB_GET(x)                              (((x) & PCIE_INT_STATUS_CPU_INTB_MASK) >> PCIE_INT_STATUS_CPU_INTB_LSB)
#define PCIE_INT_STATUS_CPU_INTB_SET(x)                              (((x) << PCIE_INT_STATUS_CPU_INTB_LSB) & PCIE_INT_STATUS_CPU_INTB_MASK)
#define PCIE_INT_STATUS_CPU_INTB_RESET                               0x0 // 0
#define PCIE_INT_STATUS_CPU_INTA_MSB                                 18
#define PCIE_INT_STATUS_CPU_INTA_LSB                                 18
#define PCIE_INT_STATUS_CPU_INTA_MASK                                0x00040000
#define PCIE_INT_STATUS_CPU_INTA_GET(x)                              (((x) & PCIE_INT_STATUS_CPU_INTA_MASK) >> PCIE_INT_STATUS_CPU_INTA_LSB)
#define PCIE_INT_STATUS_CPU_INTA_SET(x)                              (((x) << PCIE_INT_STATUS_CPU_INTA_LSB) & PCIE_INT_STATUS_CPU_INTA_MASK)
#define PCIE_INT_STATUS_CPU_INTA_RESET                               0x0 // 0
#define PCIE_INT_STATUS_INTDL_MSB                                    17
#define PCIE_INT_STATUS_INTDL_LSB                                    17
#define PCIE_INT_STATUS_INTDL_MASK                                   0x00020000
#define PCIE_INT_STATUS_INTDL_GET(x)                                 (((x) & PCIE_INT_STATUS_INTDL_MASK) >> PCIE_INT_STATUS_INTDL_LSB)
#define PCIE_INT_STATUS_INTDL_SET(x)                                 (((x) << PCIE_INT_STATUS_INTDL_LSB) & PCIE_INT_STATUS_INTDL_MASK)
#define PCIE_INT_STATUS_INTDL_RESET                                  0x0 // 0
#define PCIE_INT_STATUS_INTCL_MSB                                    16
#define PCIE_INT_STATUS_INTCL_LSB                                    16
#define PCIE_INT_STATUS_INTCL_MASK                                   0x00010000
#define PCIE_INT_STATUS_INTCL_GET(x)                                 (((x) & PCIE_INT_STATUS_INTCL_MASK) >> PCIE_INT_STATUS_INTCL_LSB)
#define PCIE_INT_STATUS_INTCL_SET(x)                                 (((x) << PCIE_INT_STATUS_INTCL_LSB) & PCIE_INT_STATUS_INTCL_MASK)
#define PCIE_INT_STATUS_INTCL_RESET                                  0x0 // 0
#define PCIE_INT_STATUS_INTBL_MSB                                    15
#define PCIE_INT_STATUS_INTBL_LSB                                    15
#define PCIE_INT_STATUS_INTBL_MASK                                   0x00008000
#define PCIE_INT_STATUS_INTBL_GET(x)                                 (((x) & PCIE_INT_STATUS_INTBL_MASK) >> PCIE_INT_STATUS_INTBL_LSB)
#define PCIE_INT_STATUS_INTBL_SET(x)                                 (((x) << PCIE_INT_STATUS_INTBL_LSB) & PCIE_INT_STATUS_INTBL_MASK)
#define PCIE_INT_STATUS_INTBL_RESET                                  0x0 // 0
#define PCIE_INT_STATUS_INTAL_MSB                                    14
#define PCIE_INT_STATUS_INTAL_LSB                                    14
#define PCIE_INT_STATUS_INTAL_MASK                                   0x00004000
#define PCIE_INT_STATUS_INTAL_GET(x)                                 (((x) & PCIE_INT_STATUS_INTAL_MASK) >> PCIE_INT_STATUS_INTAL_LSB)
#define PCIE_INT_STATUS_INTAL_SET(x)                                 (((x) << PCIE_INT_STATUS_INTAL_LSB) & PCIE_INT_STATUS_INTAL_MASK)
#define PCIE_INT_STATUS_INTAL_RESET                                  0x0 // 0
#define PCIE_INT_STATUS_SYS_ERR_MSB                                  13
#define PCIE_INT_STATUS_SYS_ERR_LSB                                  13
#define PCIE_INT_STATUS_SYS_ERR_MASK                                 0x00002000
#define PCIE_INT_STATUS_SYS_ERR_GET(x)                               (((x) & PCIE_INT_STATUS_SYS_ERR_MASK) >> PCIE_INT_STATUS_SYS_ERR_LSB)
#define PCIE_INT_STATUS_SYS_ERR_SET(x)                               (((x) << PCIE_INT_STATUS_SYS_ERR_LSB) & PCIE_INT_STATUS_SYS_ERR_MASK)
#define PCIE_INT_STATUS_SYS_ERR_RESET                                0x0 // 0
#define PCIE_INT_STATUS_AER_MSI_MSB                                  12
#define PCIE_INT_STATUS_AER_MSI_LSB                                  12
#define PCIE_INT_STATUS_AER_MSI_MASK                                 0x00001000
#define PCIE_INT_STATUS_AER_MSI_GET(x)                               (((x) & PCIE_INT_STATUS_AER_MSI_MASK) >> PCIE_INT_STATUS_AER_MSI_LSB)
#define PCIE_INT_STATUS_AER_MSI_SET(x)                               (((x) << PCIE_INT_STATUS_AER_MSI_LSB) & PCIE_INT_STATUS_AER_MSI_MASK)
#define PCIE_INT_STATUS_AER_MSI_RESET                                0x0 // 0
#define PCIE_INT_STATUS_AER_INT_MSB                                  11
#define PCIE_INT_STATUS_AER_INT_LSB                                  11
#define PCIE_INT_STATUS_AER_INT_MASK                                 0x00000800
#define PCIE_INT_STATUS_AER_INT_GET(x)                               (((x) & PCIE_INT_STATUS_AER_INT_MASK) >> PCIE_INT_STATUS_AER_INT_LSB)
#define PCIE_INT_STATUS_AER_INT_SET(x)                               (((x) << PCIE_INT_STATUS_AER_INT_LSB) & PCIE_INT_STATUS_AER_INT_MASK)
#define PCIE_INT_STATUS_AER_INT_RESET                                0x0 // 0
#define PCIE_INT_STATUS_MSI_ERR_MSB                                  10
#define PCIE_INT_STATUS_MSI_ERR_LSB                                  10
#define PCIE_INT_STATUS_MSI_ERR_MASK                                 0x00000400
#define PCIE_INT_STATUS_MSI_ERR_GET(x)                               (((x) & PCIE_INT_STATUS_MSI_ERR_MASK) >> PCIE_INT_STATUS_MSI_ERR_LSB)
#define PCIE_INT_STATUS_MSI_ERR_SET(x)                               (((x) << PCIE_INT_STATUS_MSI_ERR_LSB) & PCIE_INT_STATUS_MSI_ERR_MASK)
#define PCIE_INT_STATUS_MSI_ERR_RESET                                0x0 // 0
#define PCIE_INT_STATUS_MSI_MSB                                      9
#define PCIE_INT_STATUS_MSI_LSB                                      9
#define PCIE_INT_STATUS_MSI_MASK                                     0x00000200
#define PCIE_INT_STATUS_MSI_GET(x)                                   (((x) & PCIE_INT_STATUS_MSI_MASK) >> PCIE_INT_STATUS_MSI_LSB)
#define PCIE_INT_STATUS_MSI_SET(x)                                   (((x) << PCIE_INT_STATUS_MSI_LSB) & PCIE_INT_STATUS_MSI_MASK)
#define PCIE_INT_STATUS_MSI_RESET                                    0x0 // 0
#define PCIE_INT_STATUS_INTD_MSB                                     8
#define PCIE_INT_STATUS_INTD_LSB                                     8
#define PCIE_INT_STATUS_INTD_MASK                                    0x00000100
#define PCIE_INT_STATUS_INTD_GET(x)                                  (((x) & PCIE_INT_STATUS_INTD_MASK) >> PCIE_INT_STATUS_INTD_LSB)
#define PCIE_INT_STATUS_INTD_SET(x)                                  (((x) << PCIE_INT_STATUS_INTD_LSB) & PCIE_INT_STATUS_INTD_MASK)
#define PCIE_INT_STATUS_INTD_RESET                                   0x0 // 0
#define PCIE_INT_STATUS_INTC_MSB                                     7
#define PCIE_INT_STATUS_INTC_LSB                                     7
#define PCIE_INT_STATUS_INTC_MASK                                    0x00000080
#define PCIE_INT_STATUS_INTC_GET(x)                                  (((x) & PCIE_INT_STATUS_INTC_MASK) >> PCIE_INT_STATUS_INTC_LSB)
#define PCIE_INT_STATUS_INTC_SET(x)                                  (((x) << PCIE_INT_STATUS_INTC_LSB) & PCIE_INT_STATUS_INTC_MASK)
#define PCIE_INT_STATUS_INTC_RESET                                   0x0 // 0
#define PCIE_INT_STATUS_INTB_MSB                                     6
#define PCIE_INT_STATUS_INTB_LSB                                     6
#define PCIE_INT_STATUS_INTB_MASK                                    0x00000040
#define PCIE_INT_STATUS_INTB_GET(x)                                  (((x) & PCIE_INT_STATUS_INTB_MASK) >> PCIE_INT_STATUS_INTB_LSB)
#define PCIE_INT_STATUS_INTB_SET(x)                                  (((x) << PCIE_INT_STATUS_INTB_LSB) & PCIE_INT_STATUS_INTB_MASK)
#define PCIE_INT_STATUS_INTB_RESET                                   0x0 // 0
#define PCIE_INT_STATUS_INTA_MSB                                     5
#define PCIE_INT_STATUS_INTA_LSB                                     5
#define PCIE_INT_STATUS_INTA_MASK                                    0x00000020
#define PCIE_INT_STATUS_INTA_GET(x)                                  (((x) & PCIE_INT_STATUS_INTA_MASK) >> PCIE_INT_STATUS_INTA_LSB)
#define PCIE_INT_STATUS_INTA_SET(x)                                  (((x) << PCIE_INT_STATUS_INTA_LSB) & PCIE_INT_STATUS_INTA_MASK)
#define PCIE_INT_STATUS_INTA_RESET                                   0x0 // 0
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MSB                    4
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_LSB                    4
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MASK                   0x00000010
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_GET(x)                 (((x) & PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MASK) >> PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_LSB)
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_SET(x)                 (((x) << PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_LSB) & PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_MASK)
#define PCIE_INT_STATUS_RADMX_COMP_LOOKUP_ERR_RESET                  0x0 // 0
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MSB                       3
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_LSB                       3
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MASK                      0x00000008
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_GET(x)                    (((x) & PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MASK) >> PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_LSB)
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_SET(x)                    (((x) << PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_LSB) & PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_MASK)
#define PCIE_INT_STATUS_GM_COMP_LOOKUP_ERR_RESET                     0x0 // 0
#define PCIE_INT_STATUS_FATAL_ERR_MSB                                2
#define PCIE_INT_STATUS_FATAL_ERR_LSB                                2
#define PCIE_INT_STATUS_FATAL_ERR_MASK                               0x00000004
#define PCIE_INT_STATUS_FATAL_ERR_GET(x)                             (((x) & PCIE_INT_STATUS_FATAL_ERR_MASK) >> PCIE_INT_STATUS_FATAL_ERR_LSB)
#define PCIE_INT_STATUS_FATAL_ERR_SET(x)                             (((x) << PCIE_INT_STATUS_FATAL_ERR_LSB) & PCIE_INT_STATUS_FATAL_ERR_MASK)
#define PCIE_INT_STATUS_FATAL_ERR_RESET                              0x0 // 0
#define PCIE_INT_STATUS_NONFATAL_ERR_MSB                             1
#define PCIE_INT_STATUS_NONFATAL_ERR_LSB                             1
#define PCIE_INT_STATUS_NONFATAL_ERR_MASK                            0x00000002
#define PCIE_INT_STATUS_NONFATAL_ERR_GET(x)                          (((x) & PCIE_INT_STATUS_NONFATAL_ERR_MASK) >> PCIE_INT_STATUS_NONFATAL_ERR_LSB)
#define PCIE_INT_STATUS_NONFATAL_ERR_SET(x)                          (((x) << PCIE_INT_STATUS_NONFATAL_ERR_LSB) & PCIE_INT_STATUS_NONFATAL_ERR_MASK)
#define PCIE_INT_STATUS_NONFATAL_ERR_RESET                           0x0 // 0
#define PCIE_INT_STATUS_CORR_ERR_MSB                                 0
#define PCIE_INT_STATUS_CORR_ERR_LSB                                 0
#define PCIE_INT_STATUS_CORR_ERR_MASK                                0x00000001
#define PCIE_INT_STATUS_CORR_ERR_GET(x)                              (((x) & PCIE_INT_STATUS_CORR_ERR_MASK) >> PCIE_INT_STATUS_CORR_ERR_LSB)
#define PCIE_INT_STATUS_CORR_ERR_SET(x)                              (((x) << PCIE_INT_STATUS_CORR_ERR_LSB) & PCIE_INT_STATUS_CORR_ERR_MASK)
#define PCIE_INT_STATUS_CORR_ERR_RESET                               0x0 // 0
#define PCIE_INT_STATUS_ADDRESS                                      0x180f004c

#define PCIE_INT_MASK_LINK_DOWN_MSB                                  27
#define PCIE_INT_MASK_LINK_DOWN_LSB                                  27
#define PCIE_INT_MASK_LINK_DOWN_MASK                                 0x08000000
#define PCIE_INT_MASK_LINK_DOWN_GET(x)                               (((x) & PCIE_INT_MASK_LINK_DOWN_MASK) >> PCIE_INT_MASK_LINK_DOWN_LSB)
#define PCIE_INT_MASK_LINK_DOWN_SET(x)                               (((x) << PCIE_INT_MASK_LINK_DOWN_LSB) & PCIE_INT_MASK_LINK_DOWN_MASK)
#define PCIE_INT_MASK_LINK_DOWN_RESET                                0x0 // 0
#define PCIE_INT_MASK_LINK_REQ_RST_MSB                               26
#define PCIE_INT_MASK_LINK_REQ_RST_LSB                               26
#define PCIE_INT_MASK_LINK_REQ_RST_MASK                              0x04000000
#define PCIE_INT_MASK_LINK_REQ_RST_GET(x)                            (((x) & PCIE_INT_MASK_LINK_REQ_RST_MASK) >> PCIE_INT_MASK_LINK_REQ_RST_LSB)
#define PCIE_INT_MASK_LINK_REQ_RST_SET(x)                            (((x) << PCIE_INT_MASK_LINK_REQ_RST_LSB) & PCIE_INT_MASK_LINK_REQ_RST_MASK)
#define PCIE_INT_MASK_LINK_REQ_RST_RESET                             0x0 // 0
#define PCIE_INT_MASK_INTDL_MSB                                      17
#define PCIE_INT_MASK_INTDL_LSB                                      17
#define PCIE_INT_MASK_INTDL_MASK                                     0x00020000
#define PCIE_INT_MASK_INTDL_GET(x)                                   (((x) & PCIE_INT_MASK_INTDL_MASK) >> PCIE_INT_MASK_INTDL_LSB)
#define PCIE_INT_MASK_INTDL_SET(x)                                   (((x) << PCIE_INT_MASK_INTDL_LSB) & PCIE_INT_MASK_INTDL_MASK)
#define PCIE_INT_MASK_INTDL_RESET                                    0x0 // 0
#define PCIE_INT_MASK_INTCL_MSB                                      16
#define PCIE_INT_MASK_INTCL_LSB                                      16
#define PCIE_INT_MASK_INTCL_MASK                                     0x00010000
#define PCIE_INT_MASK_INTCL_GET(x)                                   (((x) & PCIE_INT_MASK_INTCL_MASK) >> PCIE_INT_MASK_INTCL_LSB)
#define PCIE_INT_MASK_INTCL_SET(x)                                   (((x) << PCIE_INT_MASK_INTCL_LSB) & PCIE_INT_MASK_INTCL_MASK)
#define PCIE_INT_MASK_INTCL_RESET                                    0x0 // 0
#define PCIE_INT_MASK_INTBL_MSB                                      15
#define PCIE_INT_MASK_INTBL_LSB                                      15
#define PCIE_INT_MASK_INTBL_MASK                                     0x00008000
#define PCIE_INT_MASK_INTBL_GET(x)                                   (((x) & PCIE_INT_MASK_INTBL_MASK) >> PCIE_INT_MASK_INTBL_LSB)
#define PCIE_INT_MASK_INTBL_SET(x)                                   (((x) << PCIE_INT_MASK_INTBL_LSB) & PCIE_INT_MASK_INTBL_MASK)
#define PCIE_INT_MASK_INTBL_RESET                                    0x0 // 0
#define PCIE_INT_MASK_INTAL_MSB                                      14
#define PCIE_INT_MASK_INTAL_LSB                                      14
#define PCIE_INT_MASK_INTAL_MASK                                     0x00004000
#define PCIE_INT_MASK_INTAL_GET(x)                                   (((x) & PCIE_INT_MASK_INTAL_MASK) >> PCIE_INT_MASK_INTAL_LSB)
#define PCIE_INT_MASK_INTAL_SET(x)                                   (((x) << PCIE_INT_MASK_INTAL_LSB) & PCIE_INT_MASK_INTAL_MASK)
#define PCIE_INT_MASK_INTAL_RESET                                    0x0 // 0
#define PCIE_INT_MASK_SYS_ERR_MSB                                    13
#define PCIE_INT_MASK_SYS_ERR_LSB                                    13
#define PCIE_INT_MASK_SYS_ERR_MASK                                   0x00002000
#define PCIE_INT_MASK_SYS_ERR_GET(x)                                 (((x) & PCIE_INT_MASK_SYS_ERR_MASK) >> PCIE_INT_MASK_SYS_ERR_LSB)
#define PCIE_INT_MASK_SYS_ERR_SET(x)                                 (((x) << PCIE_INT_MASK_SYS_ERR_LSB) & PCIE_INT_MASK_SYS_ERR_MASK)
#define PCIE_INT_MASK_SYS_ERR_RESET                                  0x0 // 0
#define PCIE_INT_MASK_AER_MSI_MSB                                    12
#define PCIE_INT_MASK_AER_MSI_LSB                                    12
#define PCIE_INT_MASK_AER_MSI_MASK                                   0x00001000
#define PCIE_INT_MASK_AER_MSI_GET(x)                                 (((x) & PCIE_INT_MASK_AER_MSI_MASK) >> PCIE_INT_MASK_AER_MSI_LSB)
#define PCIE_INT_MASK_AER_MSI_SET(x)                                 (((x) << PCIE_INT_MASK_AER_MSI_LSB) & PCIE_INT_MASK_AER_MSI_MASK)
#define PCIE_INT_MASK_AER_MSI_RESET                                  0x0 // 0
#define PCIE_INT_MASK_AER_INT_MSB                                    11
#define PCIE_INT_MASK_AER_INT_LSB                                    11
#define PCIE_INT_MASK_AER_INT_MASK                                   0x00000800
#define PCIE_INT_MASK_AER_INT_GET(x)                                 (((x) & PCIE_INT_MASK_AER_INT_MASK) >> PCIE_INT_MASK_AER_INT_LSB)
#define PCIE_INT_MASK_AER_INT_SET(x)                                 (((x) << PCIE_INT_MASK_AER_INT_LSB) & PCIE_INT_MASK_AER_INT_MASK)
#define PCIE_INT_MASK_AER_INT_RESET                                  0x0 // 0
#define PCIE_INT_MASK_MSI_ERR_MSB                                    10
#define PCIE_INT_MASK_MSI_ERR_LSB                                    10
#define PCIE_INT_MASK_MSI_ERR_MASK                                   0x00000400
#define PCIE_INT_MASK_MSI_ERR_GET(x)                                 (((x) & PCIE_INT_MASK_MSI_ERR_MASK) >> PCIE_INT_MASK_MSI_ERR_LSB)
#define PCIE_INT_MASK_MSI_ERR_SET(x)                                 (((x) << PCIE_INT_MASK_MSI_ERR_LSB) & PCIE_INT_MASK_MSI_ERR_MASK)
#define PCIE_INT_MASK_MSI_ERR_RESET                                  0x0 // 0
#define PCIE_INT_MASK_MSI_MSB                                        9
#define PCIE_INT_MASK_MSI_LSB                                        9
#define PCIE_INT_MASK_MSI_MASK                                       0x00000200
#define PCIE_INT_MASK_MSI_GET(x)                                     (((x) & PCIE_INT_MASK_MSI_MASK) >> PCIE_INT_MASK_MSI_LSB)
#define PCIE_INT_MASK_MSI_SET(x)                                     (((x) << PCIE_INT_MASK_MSI_LSB) & PCIE_INT_MASK_MSI_MASK)
#define PCIE_INT_MASK_MSI_RESET                                      0x0 // 0
#define PCIE_INT_MASK_INTD_MSB                                       8
#define PCIE_INT_MASK_INTD_LSB                                       8
#define PCIE_INT_MASK_INTD_MASK                                      0x00000100
#define PCIE_INT_MASK_INTD_GET(x)                                    (((x) & PCIE_INT_MASK_INTD_MASK) >> PCIE_INT_MASK_INTD_LSB)
#define PCIE_INT_MASK_INTD_SET(x)                                    (((x) << PCIE_INT_MASK_INTD_LSB) & PCIE_INT_MASK_INTD_MASK)
#define PCIE_INT_MASK_INTD_RESET                                     0x0 // 0
#define PCIE_INT_MASK_INTC_MSB                                       7
#define PCIE_INT_MASK_INTC_LSB                                       7
#define PCIE_INT_MASK_INTC_MASK                                      0x00000080
#define PCIE_INT_MASK_INTC_GET(x)                                    (((x) & PCIE_INT_MASK_INTC_MASK) >> PCIE_INT_MASK_INTC_LSB)
#define PCIE_INT_MASK_INTC_SET(x)                                    (((x) << PCIE_INT_MASK_INTC_LSB) & PCIE_INT_MASK_INTC_MASK)
#define PCIE_INT_MASK_INTC_RESET                                     0x0 // 0
#define PCIE_INT_MASK_INTB_MSB                                       6
#define PCIE_INT_MASK_INTB_LSB                                       6
#define PCIE_INT_MASK_INTB_MASK                                      0x00000040
#define PCIE_INT_MASK_INTB_GET(x)                                    (((x) & PCIE_INT_MASK_INTB_MASK) >> PCIE_INT_MASK_INTB_LSB)
#define PCIE_INT_MASK_INTB_SET(x)                                    (((x) << PCIE_INT_MASK_INTB_LSB) & PCIE_INT_MASK_INTB_MASK)
#define PCIE_INT_MASK_INTB_RESET                                     0x0 // 0
#define PCIE_INT_MASK_INTA_MSB                                       5
#define PCIE_INT_MASK_INTA_LSB                                       5
#define PCIE_INT_MASK_INTA_MASK                                      0x00000020
#define PCIE_INT_MASK_INTA_GET(x)                                    (((x) & PCIE_INT_MASK_INTA_MASK) >> PCIE_INT_MASK_INTA_LSB)
#define PCIE_INT_MASK_INTA_SET(x)                                    (((x) << PCIE_INT_MASK_INTA_LSB) & PCIE_INT_MASK_INTA_MASK)
#define PCIE_INT_MASK_INTA_RESET                                     0x0 // 0
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_MSB                      4
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_LSB                      4
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_MASK                     0x00000010
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_GET(x)                   (((x) & PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_MASK) >> PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_LSB)
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_SET(x)                   (((x) << PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_LSB) & PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_MASK)
#define PCIE_INT_MASK_RADMX_COMP_LOOKUP_ERR_RESET                    0x0 // 0
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_MSB                         3
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_LSB                         3
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_MASK                        0x00000008
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_GET(x)                      (((x) & PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_MASK) >> PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_LSB)
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_SET(x)                      (((x) << PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_LSB) & PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_MASK)
#define PCIE_INT_MASK_GM_COMP_LOOKUP_ERR_RESET                       0x0 // 0
#define PCIE_INT_MASK_FATAL_ERR_MSB                                  2
#define PCIE_INT_MASK_FATAL_ERR_LSB                                  2
#define PCIE_INT_MASK_FATAL_ERR_MASK                                 0x00000004
#define PCIE_INT_MASK_FATAL_ERR_GET(x)                               (((x) & PCIE_INT_MASK_FATAL_ERR_MASK) >> PCIE_INT_MASK_FATAL_ERR_LSB)
#define PCIE_INT_MASK_FATAL_ERR_SET(x)                               (((x) << PCIE_INT_MASK_FATAL_ERR_LSB) & PCIE_INT_MASK_FATAL_ERR_MASK)
#define PCIE_INT_MASK_FATAL_ERR_RESET                                0x0 // 0
#define PCIE_INT_MASK_NONFATAL_ERR_MSB                               1
#define PCIE_INT_MASK_NONFATAL_ERR_LSB                               1
#define PCIE_INT_MASK_NONFATAL_ERR_MASK                              0x00000002
#define PCIE_INT_MASK_NONFATAL_ERR_GET(x)                            (((x) & PCIE_INT_MASK_NONFATAL_ERR_MASK) >> PCIE_INT_MASK_NONFATAL_ERR_LSB)
#define PCIE_INT_MASK_NONFATAL_ERR_SET(x)                            (((x) << PCIE_INT_MASK_NONFATAL_ERR_LSB) & PCIE_INT_MASK_NONFATAL_ERR_MASK)
#define PCIE_INT_MASK_NONFATAL_ERR_RESET                             0x0 // 0
#define PCIE_INT_MASK_CORR_ERR_MSB                                   0
#define PCIE_INT_MASK_CORR_ERR_LSB                                   0
#define PCIE_INT_MASK_CORR_ERR_MASK                                  0x00000001
#define PCIE_INT_MASK_CORR_ERR_GET(x)                                (((x) & PCIE_INT_MASK_CORR_ERR_MASK) >> PCIE_INT_MASK_CORR_ERR_LSB)
#define PCIE_INT_MASK_CORR_ERR_SET(x)                                (((x) << PCIE_INT_MASK_CORR_ERR_LSB) & PCIE_INT_MASK_CORR_ERR_MASK)
#define PCIE_INT_MASK_CORR_ERR_RESET                                 0x0 // 0
#define PCIE_INT_MASK_ADDRESS                                        0x180f0050
/*
 * CRP. To access the host controller config and status registers
 */
#define ATH_PCI_CRP			0x180c0000
#define ATH_PCI_DEV_CFGBASE		0x14000000
#define ATH_PCI_CRP_AD_CBE		ATH_PCI_CRP
#define ATH_PCI_CRP_WRDATA		ATH_PCI_CRP+0x4
#define ATH_PCI_CRP_RDDATA		ATH_PCI_CRP+0x8
#define ATH_PCI_ERROR			ATH_PCI_CRP+0x1c
#define ATH_PCI_ERROR_ADDRESS		ATH_PCI_CRP+0x20
#define ATH_PCI_AHB_ERROR		ATH_PCI_CRP+0x24
#define ATH_PCI_AHB_ERROR_ADDRESS	ATH_PCI_CRP+0x28

#define ATH_CRP_CMD_WRITE		0x00010000
#define ATH_CRP_CMD_READ		0x00000000

/*
 * PCI CFG. To generate config cycles
 */
#define ATH_PCI_CFG_AD			ATH_PCI_CRP+0xc
#define ATH_PCI_CFG_CBE			ATH_PCI_CRP+0x10
#define ATH_PCI_CFG_WRDATA		ATH_PCI_CRP+0x14
#define ATH_PCI_CFG_RDDATA		ATH_PCI_CRP+0x18
#define ATH_CFG_CMD_READ		0x0000000a
#define ATH_CFG_CMD_WRITE		0x0000000b

#define ATH_PCI_IDSEL_ADLINE_START	17




#define ATH_DECL_PCI_IM_ARR(x)		\
	uint32_t x[] = { PCIE_INT_MASK_ADDRESS }
#define ATH_DECL_PCI_IS_ARR(x)		\
	uint32_t x[] = { PCIE_INT_STATUS_ADDRESS }
#define ATH_DECL_PCI_RST_ARR(x)		\
	uint32_t x[] = { PCIE_RESET_ADDRESS }
#define ATH_DECL_PCI_CFG_BASE_ARR(x)	\
	uint32_t x[] = { ATH_PCI_DEV_CFGBASE }
#define ATH_DECL_PCI_CRP_ARR(x)		\
	uint32_t x[] = { PCIE_RC_REG_ADDRESS }
#	define ATH_DECL_PCI_IO_RES		\
		ATH_PCI_RES_IO(0, 0x0000, 0x0000)
#	define ATH_DECL_PCI_MEM_RES		\
		ATH_PCI_RES_MEM(0)
#	define ATH_DECL_PCI_CTRLR		\
		ATH_PCI_CTRL_DESCRIPTOR(0)
#	undef ATH_PCI_RC2_IRQ






#endif
