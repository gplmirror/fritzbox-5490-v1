#if ! defined(_HW_NAND_H_)
#define _HW_NAND_H_

#define ATH_NF_COMMAND		    (ATH_NAND_FLASH_BASE + 0x200u)
#define ATH_NF_CTRL		        (ATH_NAND_FLASH_BASE + 0x204u)
#define ATH_NF_STATUS		    (ATH_NAND_FLASH_BASE + 0x208u)
#define ATH_NF_INT_MASK		    (ATH_NAND_FLASH_BASE + 0x20cu)
#define ATH_NF_INT_STATUS	    (ATH_NAND_FLASH_BASE + 0x210u)
#define ATH_NF_ECC_CTRL		    (ATH_NAND_FLASH_BASE + 0x214u)
#define ATH_NF_ECC_OFFSET	    (ATH_NAND_FLASH_BASE + 0x218u)
#define ATH_NF_ADDR0_0		    (ATH_NAND_FLASH_BASE + 0x21cu)
#define ATH_NF_ADDR1_0		    (ATH_NAND_FLASH_BASE + 0x220u)
#define ATH_NF_ADDR0_1		    (ATH_NAND_FLASH_BASE + 0x224u)
#define ATH_NF_ADDR1_1		    (ATH_NAND_FLASH_BASE + 0x228u)
#define ATH_NF_SPARE_SIZE	    (ATH_NAND_FLASH_BASE + 0x230u)
#define ATH_NF_PROTECT		    (ATH_NAND_FLASH_BASE + 0x238u)
#define ATH_NF_LOOKUP_EN	    (ATH_NAND_FLASH_BASE + 0x240u)
#define ATH_NF_LOOKUP0		    (ATH_NAND_FLASH_BASE + 0x244u)
#define ATH_NF_LOOKUP1		    (ATH_NAND_FLASH_BASE + 0x248u)
#define ATH_NF_LOOKUP2		    (ATH_NAND_FLASH_BASE + 0x24cu)
#define ATH_NF_LOOKUP3		    (ATH_NAND_FLASH_BASE + 0x250u)
#define ATH_NF_LOOKUP4		    (ATH_NAND_FLASH_BASE + 0x254u)
#define ATH_NF_LOOKUP5		    (ATH_NAND_FLASH_BASE + 0x258u)
#define ATH_NF_LOOKUP6		    (ATH_NAND_FLASH_BASE + 0x25cu)
#define ATH_NF_LOOKUP7		    (ATH_NAND_FLASH_BASE + 0x260u)
#define ATH_NF_DMA_ADDR		    (ATH_NAND_FLASH_BASE + 0x264u)
#define ATH_NF_DMA_COUNT	    (ATH_NAND_FLASH_BASE + 0x268u)
#define ATH_NF_DMA_CTRL		    (ATH_NAND_FLASH_BASE + 0x26cu)
#define ATH_NF_MEM_CTRL		    (ATH_NAND_FLASH_BASE + 0x280u)
#define ATH_NF_PG_SIZE		    (ATH_NAND_FLASH_BASE + 0x284u)
#define ATH_NF_RD_STATUS	    (ATH_NAND_FLASH_BASE + 0x288u)
#define ATH_NF_TIME_SEQ		    (ATH_NAND_FLASH_BASE + 0x28cu)
#define ATH_NF_TIMINGS_ASYN	    (ATH_NAND_FLASH_BASE + 0x290u)
#define ATH_NF_TIMINGS_SYN	    (ATH_NAND_FLASH_BASE + 0x294u)
#define ATH_NF_FIFO_DATA	    (ATH_NAND_FLASH_BASE + 0x298u)
#define ATH_NF_TIME_MODE	    (ATH_NAND_FLASH_BASE + 0x29cu)
#define ATH_NF_DMA_ADDR_OFFSET	(ATH_NAND_FLASH_BASE + 0x2a0u)
#define ATH_NF_FIFO_INIT	    (ATH_NAND_FLASH_BASE + 0x2b0u)
#define ATH_NF_GENERIC_SEQ_CTRL	(ATH_NAND_FLASH_BASE + 0x2b4u)

#define ATH_NF_COMMAND_CMD_SEQ_0	0x00
#define ATH_NF_COMMAND_CMD_SEQ_1	0x21
#define ATH_NF_COMMAND_CMD_SEQ_2	0x22
#define ATH_NF_COMMAND_CMD_SEQ_3	0x03
#define ATH_NF_COMMAND_CMD_SEQ_4	0x24
#define ATH_NF_COMMAND_CMD_SEQ_5	0x25
#define ATH_NF_COMMAND_CMD_SEQ_6	0x26
#define ATH_NF_COMMAND_CMD_SEQ_7	0x27
#define ATH_NF_COMMAND_CMD_SEQ_8	0x08
#define ATH_NF_COMMAND_CMD_SEQ_9	0x29
#define ATH_NF_COMMAND_CMD_SEQ_10	0x2A
#define ATH_NF_COMMAND_CMD_SEQ_11	0x2B
#define ATH_NF_COMMAND_CMD_SEQ_12	0x0C
#define ATH_NF_COMMAND_CMD_SEQ_13	0x0D
#define ATH_NF_COMMAND_CMD_SEQ_14	0x0E
#define ATH_NF_COMMAND_CMD_SEQ_15	0x2F
#define ATH_NF_COMMAND_CMD_SEQ_16	0x30
#define ATH_NF_COMMAND_CMD_SEQ_17	0x11
#define ATH_NF_COMMAND_CMD_SEQ_18	0x32
#define ATH_NF_COMMAND_CMD_SEQ_19	0x13

union _cmd {
    volatile unsigned int Register;
    struct __cmd {
        volatile unsigned int cmd_2      : 8;
        volatile unsigned int cmd_1      : 8;
        volatile unsigned int cmd_0      : 8;
        volatile unsigned int addr_sel   : 1;
        volatile unsigned int input_sel  : 1;
        volatile unsigned int cmd_seq    : 6;
    } Bits;
};

#define ATH_NAND_INPUT_DMA (1<<6)

enum ath_nand_cmd {
    ATH_NAND_CMD_READID,            /*---  0 ---*/
    ATH_NAND_CMD_READ,              /*---  1 ---*/
    ATH_NAND_CMD_READ_INIT,         /*---  2 ---*/
    ATH_NAND_CMD_READ_START,        /*---  3 ---*/
    ATH_NAND_CMD_READ_COLUMN,       /*---  4 ---*/
    ATH_NAND_CMD_PROGRAM,           /*---  5 ---*/
    ATH_NAND_CMD_PROGRAM_INIT,      /*---  6 ---*/
    ATH_NAND_CMD_PROGRAM_COLUMN,    /*---  7 ---*/
    ATH_NAND_CMD_PROGRAM_START,     /*---  8 ---*/
    ATH_NAND_CMD_ERASE,             /*---  9 ---*/
    ATH_NAND_CMD_READOOB,           /*--- 10 ---*/
    ATH_NAND_CMD_PROGRAMOOB,        /*--- 11 ---*/
    ATH_NAND_CMD_READONFI           /*--- 12 ---*/
};

union _ctrl {
    volatile unsigned int Register;
    struct __ctrl {
        volatile unsigned int reserved  : 10;
        volatile unsigned int small_block  : 1;
        volatile unsigned int addr_cycle1  : 3;
        volatile unsigned int addr1_auto_incr   : 1;
        volatile unsigned int addr0_auto_incr   : 1;
        volatile unsigned int work_mode   : 1;
        volatile unsigned int prot_en   : 1;
        volatile unsigned int lookup_en   : 1;
        volatile unsigned int io_with   : 1;
        volatile unsigned int customsize   : 1;
        volatile unsigned int pagesize      : 3;
        volatile unsigned int blocksize      : 2;
        volatile unsigned int ecc_en      : 1;
        volatile unsigned int int_en   : 1;
        volatile unsigned int spare_en    : 1;
        volatile unsigned int addr_cycle0  : 3;
    } Bits;
};

#define ATH_NF_CTRL_SMALL_BLOCK_EN		(1 << 21)

#define ATH_NF_CTRL_PAGE_SIZE_256		(0 <<  8)	/* bytes */
#define ATH_NF_CTRL_PAGE_SIZE_512		(1 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_1024		(2 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_2048		(3 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_4096		(4 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_8192		(5 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_16384		(6 <<  8)
#define ATH_NF_CTRL_PAGE_SIZE_0		    (7 <<  8)

#define ATH_NF_CTRL_BLOCK_SIZE_32		(0 <<  6)	/* pages */
#define ATH_NF_CTRL_BLOCK_SIZE_64		(1 <<  6)
#define ATH_NF_CTRL_BLOCK_SIZE_128		(2 <<  6)
#define ATH_NF_CTRL_BLOCK_SIZE_256		(3 <<  6)

#define ATH_NF_CTRL_ADDR1_AUTO_INC_EN	(1 << 17)
#define ATH_NF_CTRL_ADDR0_AUTO_INC_EN	(1 << 16)
#define ATH_NF_CTRL_WORK_MODE_SYNC		(1 << 15)
#define ATH_NF_CTRL_PROT_EN			    (1 << 14)
#define ATH_NF_CTRL_LOOKUP_EN		    (1 << 13)
#define ATH_NF_CTRL_IO_WIDTH_16BIT		(1 << 12)
#define ATH_NF_CTRL_CUSTOM_SIZE_EN		(1 << 11)

#define ATH_NF_CTRL_ECC_EN			    (1 <<  5)
#define ATH_NF_CTRL_INT_EN			    (1 <<  4)
#define ATH_NF_CTRL_SPARE_EN			(1 <<  3)

#define ATH_NF_CTRL_ADDR_CYCLE_0		(0 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE_1		(1 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE_2		(2 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE_3		(3 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE_4		(4 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE_5		(5 <<  0)
#define ATH_NF_CTRL_ADDR_CYCLE0(c)	((c) << 0)
#define ATH_NF_CTRL_ADDR_CYCLE1(c)	((c) << 18)

union _status {
    volatile unsigned int Register;
    struct __status {
        volatile unsigned int reserved  : 22;
        volatile unsigned int syn   : 1;
        volatile unsigned int ctrl  : 1;
        volatile unsigned int mem7  : 1;
        volatile unsigned int mem6  : 1;
        volatile unsigned int mem5  : 1;
        volatile unsigned int mem4  : 1;
        volatile unsigned int mem3  : 1;
        volatile unsigned int mem2  : 1;
        volatile unsigned int mem1  : 1;
        volatile unsigned int mem0  : 1;
    } Bits;
};

union _irq {
    volatile unsigned int Register;
    struct __irq {
        volatile unsigned int reserved  : 19;
        volatile unsigned int fifo_err   : 1;
        volatile unsigned int mem7_rdy  : 1;
        volatile unsigned int mem6_rdy  : 1;
        volatile unsigned int mem5_rdy  : 1;
        volatile unsigned int mem4_rdy  : 1;
        volatile unsigned int mem3_rdy  : 1;
        volatile unsigned int mem2_rdy  : 1;
        volatile unsigned int mem1_rdy  : 1;
        volatile unsigned int mem0_rdy  : 1;
        volatile unsigned int ecc_thrsh  : 1;
        volatile unsigned int ecc_fatal  : 1;
        volatile unsigned int cmd_end  : 1;
        volatile unsigned int prot  : 1;
    } Bits;
};

#define ATH_NF_MEMX_RDY     ( ATH_NF_MEM7_RDY | \
                              ATH_NF_MEM6_RDY | \
                              ATH_NF_MEM5_RDY | \
                              ATH_NF_MEM4_RDY | \
                              ATH_NF_MEM3_RDY | \
                              ATH_NF_MEM2_RDY | \
                              ATH_NF_MEM1_RDY | \
                              ATH_NF_MEM0_RDY )

#define ATH_NF_FIFO_ERR 	    (1 << 12)
#define ATH_NF_MEM7_RDY 	    (1 << 11)
#define ATH_NF_MEM6_RDY 	    (1 << 10)
#define ATH_NF_MEM5_RDY 	    (1 << 9)
#define ATH_NF_MEM4_RDY 	    (1 << 8)
#define ATH_NF_MEM3_RDY 	    (1 << 7)
#define ATH_NF_MEM2_RDY 	    (1 << 6)
#define ATH_NF_MEM1_RDY 	    (1 << 5)
#define ATH_NF_MEM0_RDY 	    (1 << 4)
#define ATH_NF_ECC_TRSH_ERR	    (1 << 3)
#define ATH_NF_ECC_FATAL_ERR	(1 << 2)
#define ATH_NF_CMD_END_INT		(1 << 1)
#define ATH_NF_PROT_INT		    (1 << 0)

union _ecc_ctrl {
    volatile unsigned int Register;
    struct __ecc_ctrl {
        volatile unsigned int res1 : 19;
        volatile unsigned int err_thrsh  : 5;
        volatile unsigned int cap  : 3;
        volatile unsigned int res0  : 2;
        volatile unsigned int over  : 1;
        volatile unsigned int uncorrect  : 1;
        volatile unsigned int correct  : 1;
    } Bits;
};

#define ATH_NAND_ECC_BITS   4
#define ATH_NAND_ECC_OFFSET (4+16)      /*--- 4Byte BBT, 16Byte frei ---*/

#define ATH_NF_ECC_CTRL_ERR_THRESH(x)	(((x) << 8) & (0x1fu << 8))
#define ATH_NF_ECC_CTRL_ECC_CAP(x)	    (((x) << 5) & (0x07u << 5))
								// ECC per
								// 512 bytes
#define ATH_NF_ECC_CTRL_ECC_2_BITS	ATH_NF_ECC_CTRL_ECC_CAP(0)	// 4
#define ATH_NF_ECC_CTRL_ECC_4_BITS	ATH_NF_ECC_CTRL_ECC_CAP(1)	// 7
#define ATH_NF_ECC_CTRL_ECC_6_BITS	ATH_NF_ECC_CTRL_ECC_CAP(2)	// 10
#define ATH_NF_ECC_CTRL_ECC_8_BITS	ATH_NF_ECC_CTRL_ECC_CAP(3)	// 13
#define ATH_NF_ECC_CTRL_ECC_10_BITS	ATH_NF_ECC_CTRL_ECC_CAP(4)	// 17
#define ATH_NF_ECC_CTRL_ECC_12_BITS	ATH_NF_ECC_CTRL_ECC_CAP(5)	// 20
#define ATH_NF_ECC_CTRL_ECC_14_BITS	ATH_NF_ECC_CTRL_ECC_CAP(6)	// 23
#define ATH_NF_ECC_CTRL_ECC_16_BITS	ATH_NF_ECC_CTRL_ECC_CAP(7)	// 26

#define ATH_NF_ECC_CORR_BITS(x)		ATH_NF_ECC_CTRL_ERR_THRESH(x) | ATH_NF_ECC_CTRL_ECC_CAP(((x) / 2) - 1)

#define ATH_NF_ECC_CTRL_ERR_OVER	(1 << 2)
#define ATH_NF_ECC_CTRL_ERR_UNCORR	(1 << 1)
#define ATH_NF_ECC_CTRL_ERR_CORR	(1 << 0)
#define ATH_NF_ECC_ERROR		(ATH_NF_ECC_CTRL_ERR_UNCORR |  ATH_NF_ECC_CTRL_ERR_OVER) 

union _ecc_offset {
    volatile unsigned int Register;
    struct __ecc_offset {
        volatile unsigned int res : 16;
        volatile unsigned int offset  : 16;
    } Bits;
};

union _addr0 {
    volatile unsigned int Register;
    struct __addr0 {
        volatile unsigned int addr3  : 8;
        volatile unsigned int addr2  : 8;
        volatile unsigned int addr1  : 8;
        volatile unsigned int addr0  : 8;
    } Bits;
};

union _addr1 {
    volatile unsigned int Register;
    struct __addr1 {
        volatile unsigned int res  : 16;
        volatile unsigned int addr1  : 8;
        volatile unsigned int addr0  : 8;
    } Bits;
};

union _spare {
    volatile unsigned int Register;
    struct __spare_size {
        volatile unsigned int res  : 23;
        volatile unsigned int size  : 9;
    } Bits;
};

union _protect {
    volatile unsigned int Register;
    struct __protect {
        volatile unsigned int up    : 16;
        volatile unsigned int down  : 16;
    } Bits;
};

union _lookup_en {
    volatile unsigned int Register;
    struct __lookup_en {
        volatile unsigned int res    : 16;
        volatile unsigned int lut_en  : 16;
    } Bits;
};

union _lookup {
    volatile unsigned int Register;
    struct __lookup {
        volatile unsigned int dest_addr    : 16;
        volatile unsigned int src_addr  : 16;
    } Bits;
};

union _dma_cnt {
    volatile unsigned int Register;
    struct __dma_cnt {
        volatile unsigned int res    : 16;
        volatile unsigned int init   : 16;
    } Bits;
};

union _dma_ctrl {
    volatile unsigned int Register;
    struct __dma_ctrl {
        volatile unsigned int res  : 24;
        volatile unsigned int start  : 1;
        volatile unsigned int dir  : 1;
        volatile unsigned int mode  : 1;
        volatile unsigned int burst  : 3;
        volatile unsigned int err_flag  : 1;
        volatile unsigned int ready  : 1;
    } Bits;
};

#define ATH_NF_DMA_CTRL_DMA_START	    (1 << 7)
#define ATH_NF_DMA_CTRL_DMA_DIR_WRITE	(0 << 6)
#define ATH_NF_DMA_CTRL_DMA_DIR_READ	(1 << 6)
#define ATH_NF_DMA_CTRL_DMA_MODE_SG	    (1 << 5)

#define ATH_NF_DMA_CTRL_DMA_BURST_0	(0 << 2)
#define ATH_NF_DMA_CTRL_DMA_BURST_1	(1 << 2)
#define ATH_NF_DMA_CTRL_DMA_BURST_2	(2 << 2)
#define ATH_NF_DMA_CTRL_DMA_BURST_3	(3 << 2)
#define ATH_NF_DMA_CTRL_DMA_BURST_4	(4 << 2)
#define ATH_NF_DMA_CTRL_DMA_BURST_5	(5 << 2)
#define ATH_NF_DMA_CTRL_ERR_FLAG	(1 << 1)
#define ATH_NF_DMA_CTRL_DMA_READY	(1 << 0)

union _mem_ctrl {
    volatile unsigned int Register;
    struct __mem_ctrl {
        volatile unsigned int res  : 16;
        volatile unsigned int mem7_wp  : 1;
        volatile unsigned int mem6_wp  : 1;
        volatile unsigned int mem5_wp  : 1;
        volatile unsigned int mem4_wp  : 1;
        volatile unsigned int mem3_wp  : 1;
        volatile unsigned int mem2_wp  : 1;
        volatile unsigned int mem1_wp  : 1;
        volatile unsigned int mem0_wp  : 1;
        volatile unsigned int res0  : 5;
        volatile unsigned int mem0_ce  : 3;
    } Bits;
};

union _data_size {
    volatile unsigned int Register;
    struct __data_size {
        volatile unsigned int res    : 16;
        volatile unsigned int data_size   : 16;
    } Bits;
};

union _read_status {
    volatile unsigned int Register;
    struct __read_status {
        volatile unsigned int res    : 16;
        volatile unsigned int state_msk : 8;
        volatile unsigned int status : 8;
    } Bits;
};

union _time_seq {
    volatile unsigned int Register;
    struct __time_seq {
        volatile unsigned int res  : 17;
        volatile unsigned int twb  : 3;
        volatile unsigned int twhr : 3;
        volatile unsigned int trhw : 3;
        volatile unsigned int tadl : 3;
        volatile unsigned int tccs : 3;
    } Bits;
};

union _time_asyn {
    volatile unsigned int Register;
    struct __time_asyn {
        volatile unsigned int res  : 24;
        volatile unsigned int trhw : 4;
        volatile unsigned int trwp : 4;
    } Bits;
};

union _time_syn {
    volatile unsigned int Register;
    struct __time_syn {
        volatile unsigned int res  : 28;
        volatile unsigned int tcad : 4;
    } Bits;
};

union _time_mode {
    volatile unsigned int Register;
    struct __time_mode {
        volatile unsigned int dqs_delay7 : 4;
        volatile unsigned int dqs_delay6 : 4;
        volatile unsigned int dqs_delay5 : 4;
        volatile unsigned int dqs_delay4 : 4;
        volatile unsigned int dqs_delay3 : 4;
        volatile unsigned int dqs_delay2 : 4;
        volatile unsigned int dqs_delay1 : 4;
        volatile unsigned int dqs_delay0 : 4;
    } Bits;
};

union _fifo_init {
    volatile unsigned int Register;
    struct __fifo_init {
        volatile unsigned int res : 31;
        volatile unsigned int fifo_init : 1;
    } Bits;
};

union _seq_ctrl {
    volatile unsigned int Register;
    struct __seq_ctrl {
        volatile unsigned int res : 14;
        volatile unsigned int col_addr : 1;
        volatile unsigned int data_en : 1;
        volatile unsigned int cmd3_code : 8;
        volatile unsigned int del_en : 2;
        volatile unsigned int cmd3_en : 1;
        volatile unsigned int cmd2_en : 1;
        volatile unsigned int addr1_en : 1;
        volatile unsigned int cmd1_en : 1;
        volatile unsigned int addr0_en : 1;
        volatile unsigned int cmd0_en : 1;
    } Bits;
};

#define ATH_NF_GENERIC_SEQ_CTRL_COL_ADDR	(1 << 17)
#define ATH_NF_GENERIC_SEQ_CTRL_DATA_EN		(1 << 16)
#define ATH_NF_GENERIC_SEQ_CTRL_CMD3_CODE(x)	(((x) & 0xff) << 8)
#define ATH_NF_GENERIC_SEQ_CTRL_DEL_EN(x)	(((x) & 3) << 6)
#define ATH_NF_GENERIC_SEQ_CTRL_CMD3_EN		(1 << 5)
#define ATH_NF_GENERIC_SEQ_CTRL_CMD2_EN		(1 << 4)
#define ATH_NF_GENERIC_SEQ_CTRL_ADDR1_EN	(1 << 3)
#define ATH_NF_GENERIC_SEQ_CTRL_CMD1_EN		(1 << 2)
#define ATH_NF_GENERIC_SEQ_CTRL_ADDR0_EN	(1 << 1)
#define ATH_NF_GENERIC_SEQ_CTRL_CMD0_EN		(1 << 0)



struct ath_nand {
    union _cmd          cmd;
    union _ctrl         ctrl;
    union _status       status;

    union _irq          irq_mask;
    union _irq          irq_status;

    union _ecc_ctrl     ecc_ctrl;
    union _ecc_offset   ecc_offset;
    union _addr0        addr0_0;

    union _addr1        addr1_0;
    union _addr0        addr0_1;
    union _addr1        addr1_1;
    volatile unsigned int res22c;

    union _spare        spare_size;
    volatile unsigned int res234;
    union _protect      protect;
    volatile unsigned int res23c;

    union _lookup_en    lookup_en;
    union _lookup       lookup[8];
    volatile unsigned int dma_addr;
    union _dma_cnt      dma_cnt;
    union _dma_ctrl     dma_ctrl;

    volatile unsigned int res270[4];
    union _mem_ctrl     mem_ctrl;
    union _data_size    pagesize;
    union _read_status  read_status;

    union _time_seq     time_seq;
    union _time_asyn    time_asyn;
    union _time_syn     time_syn;
    volatile unsigned int fifo_data;
    union _time_mode    time_mode;
    unsigned int        dma_addr_offset;
    unsigned int        res2b4[3];
    union _fifo_init    fifo_init;
    union _seq_ctrl     seq_ctrl;
};

#endif /*--- #if ! defined(_HW_NAND_H_) ---*/
