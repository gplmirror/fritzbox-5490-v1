/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2014 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 *
 *   mips-helper-functions for stackdump on smp etc.
\*------------------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/mm.h>
#include <linux/rmap.h>
#include <linux/proc_fs.h>
#include <asm/avm_enh/avm_enh.h>
#include <asm/mipsregs.h>
#include <asm/mipsmtregs.h>
#include <asm/mmu_context.h>
#include <asm/uaccess.h>
#include <asm/pgtable.h>
#include <asm/branch.h>
#include <asm/mmu_context.h>
#include <asm/types.h>
#include <asm/stacktrace.h>
#include <linux/simple_proc.h>

extern void show_backtrace(struct task_struct *task, const struct pt_regs *regs);

/*--------------------------------------------------------------------------------*\
 * ret: -1 invalid tc
 *       1 tc is halted
\*--------------------------------------------------------------------------------*/
static int mips_mt_prepare_frametrace(unsigned int tc, struct pt_regs *regs) {
	unsigned long flags;
	unsigned long vpflags;
	unsigned long mvpconf0;
	int ntc;
	int ret = 0;
	unsigned long haltval;
#ifdef CONFIG_MIPS_MT_SMTC
	void smtc_soft_dump(void);
#endif /* CONFIG_MIPT_MT_SMTC */

    memset(regs, 0, sizeof(*regs));
	local_irq_save(flags);
	vpflags = dvpe();
	mvpconf0 = read_c0_mvpconf0();
	ntc = ((mvpconf0 & MVPCONF0_PTC) >> MVPCONF0_PTC_SHIFT) + 1;
    if(tc >= ntc) {
        ret = -1;
        goto end_mt_prepare_registers;
    }
    settc(tc);
	if (read_tc_c0_tcbind() == read_c0_tcbind()) {
        /*--- printk(KERN_ERR "[%s]: read_tc_c0_tcbind() == read_c0_tcbind()\n", __FUNCTION__); ---*/
        /* Are we dumping ourself?  */
		haltval = 0; /* Then we're not halted, and mustn't be */
        /*--- goto end_mt_prepare_registers; ---*/
    } else {
        haltval = read_tc_c0_tchalt();
        /*--- printk(KERN_ERR "[%s]: %ld = read_tc_c0_tchalt()\n", __FUNCTION__, read_tc_c0_tchalt()); ---*/
        write_tc_c0_tchalt(1);
        ret = haltval;
    }
    /*--- regs->regs[28] = read_tc_gpr_gp(); ---*/
    regs->regs[29] = read_tc_gpr_sp();
    regs->regs[31] = read_tc_gpr_ra();
    regs->cp0_epc =  read_tc_c0_tcrestart();  /*--- pc ---*/
    /*--- printk("%s: $28 %08lx $29 %08lx $31 %08lx PC=%08lx\n", __func__, regs->regs[28], regs->regs[29], regs->regs[31], regs->cp0_epc); ---*/
	if (!haltval) {
        write_tc_c0_tchalt(0);
    }
end_mt_prepare_registers:
#ifdef CONFIG_MIPS_MT_SMTC
/*--- 	smtc_soft_dump(); ---*/
#endif /* CONFIG_MIPT_MT_SMTC */
	evpe(vpflags);
	local_irq_restore(flags);
    return ret;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
bool arch_trigger_all_cpu_backtrace(void) {
#if defined(CONFIG_MIPS_MT_SMP) || defined(CONFIG_MIPS_MT_SMTC)
    struct pt_regs regs;
    int tc = 0, cur_tc;
    while((cur_tc = mips_mt_prepare_frametrace(tc, &regs)) != -1) {
        if(cur_tc == 0) {
            printk("TC %x:\n", tc);
            show_backtrace(NULL, &regs);
        }
        tc++;
    }
    return 1;
#endif /*--- #if defined(CONFIG_MIPS_MT_SMP) || defined(CONFIG_MIPS_MT_SMTC) ---*/
    return 0;
}
#define IS_MIPS16_EXTEND_OR_JAL(a) ((((a) >> (27 - 16)) == 30) | (((a) >> (27 - 16)) == 3))  /*--- Opcode EXTEND oder JAL ---*/
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int check_pc_adress(unsigned int __user *pc, unsigned int usermode) {

    if(((unsigned long)pc & (0xFF << 24)) == (PAGE_POISON << 24)) {
        return 1;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_INUSE	<< 24)) {
        return 2;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_FREE	<< 24)) {
        return 3;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_END	<< 24)) {
        return 4;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_FREE_INITMEM << 24)) {
        return 5;
    }
    if(!usermode && ((unsigned long)pc < 0x80000000UL)) {
        return 6;
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int print_code_range(struct seq_file *seq, const char *prefix, unsigned int __user *pc, unsigned int mips16, unsigned int usermode, int left_offset, int right_offset, unsigned long *badaddr) {
    int ret;

    if((ret = check_pc_adress(pc, usermode))) {
        if(seq == NULL) printk(KERN_ERR "no code-dump, address could be %s (no memory at this address).\n", 
                                            ret == 1 ? "PAGE_POISON" :
                                            ret == 2 ? "POISON_INUSE" :
                                            ret == 3 ? "POISON_FREE" :
                                            ret == 4 ? "POISON_END" :
                                            ret == 5 ? "POISON_FREE_INITMEM" : "INVAL_KSEGx");
        return 0;
    }
    if(!mips16) {
        signed int i;
        unsigned long access_addr = (unsigned long)pc + sizeof(unsigned int) * left_offset;
        if(seq == NULL) printk(KERN_ERR"Code(0x%08lx):", access_addr);
        for(i = left_offset; i < right_offset; i++) {
            unsigned int pc_value;

            access_addr = (unsigned long)pc + sizeof(unsigned int) * i;
            if(usermode && !unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
                if(seq == NULL)printk(KERN_ERR "[%s] illegal address 0x%lx (sigill)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -1; /*--- sigill; ---*/
            }
            if(__get_user(pc_value, (unsigned int __user *)access_addr)) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from address 0x%lx failed (sigbus)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(seq) {
                seq_printf(seq, "%s %s0x%08x%s", i == left_offset ? prefix : "", i == 0 ? "<" : "", pc_value, i == 0 ? ">" : "");
            } else {
                printk(" %s0x%08x%s", i == 0 ? "<" : "", pc_value, i == 0 ? ">" : "");
            }
        }
    } else {
        /*--- wegen EXT-Code nur step by step vorhangeln: ---*/
        unsigned short code0, code1;
        unsigned long pc_addr = (unsigned long)pc & ~0x1;
        unsigned long access_addr = (unsigned long)pc & ~0x1;
        unsigned long end_addr = ((unsigned long)pc & ~0x1) + sizeof(unsigned short) * right_offset;
    
        while(left_offset < 0) {
            if(usermode && !unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
                if(seq == NULL)printk(KERN_ERR "[%s] illegal address 0x%lx (sigill)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -1; /*--- sigill; ---*/
            }
            if(__get_user(code1, (unsigned short __user *)(access_addr - sizeof(short)))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(__get_user(code0, (unsigned short __user *)(access_addr - 2 * sizeof(short)))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(IS_MIPS16_EXTEND_OR_JAL(code0)) {
                access_addr -= 2 * sizeof(short);
            } else {
                access_addr -= sizeof(short);
            }
            left_offset++;
        }
        if(seq) {
            seq_printf(seq, "%s", prefix);
        } else {
            printk(KERN_ERR"Code(0x%08lx):", access_addr);
        }
        while(access_addr < end_addr) {
            if(__get_user(code0, (unsigned short __user *)(access_addr))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(access_addr == pc_addr) {
                if(IS_MIPS16_EXTEND_OR_JAL(code0)) {
                    access_addr += sizeof(short);
                    if(__get_user(code1, (unsigned short __user *)(access_addr))) {
                        if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, access_addr);
                        *badaddr = (unsigned long)access_addr;
                        return -2; /*--- sigbus; ---*/
                    }
                    if(seq) {
                        seq_printf(seq, " <0x%04x %04x>", code0, code1);
                    } else {
                        printk(" <0x%04x %04x>", code0, code1);
                    }
                } else {
                    if(seq) {
                        seq_printf(seq, " <0x%04x>", code0);
                    } else {
                        printk(" <0x%04x>", code0);
                    }
                }
            } else {
                if(seq) {
                    seq_printf(seq, " 0x%04x", code0);
                } else {
                    printk(" 0x%04x", code0);
                }
            }
            access_addr += sizeof(short);
        }
    }
    if(seq == NULL) printk("\n");
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int print_mem_config(struct mm_struct *mmm, unsigned long addr) {
    struct vm_area_struct *vm;
    unsigned int i = 0;
    if(mmm == NULL) 
        return 0;
    vm = mmm->mmap;
    while(vm) {
        if((addr >= vm->vm_start) && (addr < vm->vm_end)) {
            printk(KERN_ERR"Adresse-Segment(%d): 0x%lx: 0x%lx :0x%lx (offset 0x%lx)", 
                    i, vm->vm_start, addr, vm->vm_end, addr - vm->vm_start);
            if(vm->vm_file) {
                printk(" Path='%s'", vm->vm_file->f_path.dentry->d_name.name);
            }
            printk("\n");
            return 0;
        }
        vm = vm->vm_next;
        i++;
    }
    return 1;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void show_code_position_by_epc(char *prefix, struct pt_regs *regs) {
	unsigned int __user *pc;
    unsigned long addr = 0UL;

	pc = (unsigned int __user *) exception_epc(regs);
    printk(KERN_ERR "[%s] pc=0x%p(%pF) addr=0x%08lx task=%s pid=%d ra=0x%08lx(%pF)\n", 
            prefix, pc, pc, regs->cp0_badvaddr, current->comm, current->pid,
            regs->regs[31], (void *)regs->regs[31]
          );
    print_code_range(NULL, prefix, pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
	if (user_mode(regs)) {
        if(print_mem_config(current->active_mm, (unsigned long)pc))
            print_mem_config(current->mm, (unsigned long)pc);
    }
}

/*------------------------------------------------------------------------------------------*\
 * Unaligned-Helper
\*------------------------------------------------------------------------------------------*/
#define UNALIGNED_WARN      0x1 
#define UNALIGNED_BACKTRACE 0x8 
enum _unaligned_mode {
	UNALIGNED_ACTION_FIXUP             = 2,
	UNALIGNED_ACTION_FIXUP_WARN        = 3 | UNALIGNED_WARN, /*-- verodern eigentlich Dummy :-) ---*/
	UNALIGNED_ACTION_SIGNAL            = 4,
	UNALIGNED_ACTION_SIGNAL_WARN       = 5 | UNALIGNED_WARN, /*-- verodern eigentlich Dummy :-) ---*/
	UNALIGNED_ACTION_FIXUP_WARN_BT     = UNALIGNED_ACTION_FIXUP_WARN | UNALIGNED_WARN | UNALIGNED_BACKTRACE
};

static int ai_usermode	  = UNALIGNED_ACTION_FIXUP
		    		      /*--- | UNALIGNED_WARN ---*/
                                ;
static int ai_kernelmode =  UNALIGNED_ACTION_FIXUP
		    		      /*--- | UNALIGNED_WARN ---*/
                                ;
static unsigned long ai_user;
static unsigned long ai_sys;

#define UNALIGNED_MAX_SCORE_ENTRIES       8
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _unaligned_access_score_entry {
    unsigned long unaligneds;
    unsigned long ts;
    unsigned short mips16;
    unsigned short caddr_of; /*--- > 0: pc-offset in codetable in bytes---*/
#define CODE_ENTRIES    5
    unsigned int code[CODE_ENTRIES];    /*--- Code muss gesichert werden, da evtl. Virtual Memory ---*/
    char shortcomm[8];                  /*--- Name mal sichern falls beendet ---*/
    pid_t pid;
    void *pc;
};
#define LEFT_CODE_OFFSET 2
#define RIGHT_CODE_OFFSET(a) min((a), CODE_ENTRIES - LEFT_CODE_OFFSET)
/*--------------------------------------------------------------------------------*\
 * 0: Fehler
 * sonst: liefert byte-offset zum pc
\*--------------------------------------------------------------------------------*/
static unsigned short cpy_code_range(unsigned int __user *pc, unsigned int usermode, unsigned int codetable[], unsigned int code_entries, int left_offset) {
    signed int i;
    unsigned int idx = 0;
    int right_offset;
    unsigned short ret = 0;
    int mips16_offset = ((unsigned long)pc) & 0x2;
    unsigned long access_addr = ((unsigned long)pc) & ~0x3;

    right_offset = left_offset + code_entries;
    if(check_pc_adress(pc, usermode)) {
        memset(codetable, 0, code_entries * sizeof(unsigned int));
        return ret; 
    }
    access_addr += sizeof(unsigned int) * left_offset;
    for(i = left_offset; i < right_offset; i++) {
        unsigned int pc_value;

        if(usermode && !unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
            return ret;
        }
        if(__get_user(pc_value, (unsigned int __user *)access_addr)) {
            return ret;
        }
        codetable[idx] = pc_value;
        if(i == 0) {
            ret = (unsigned char *)&codetable[idx] - (unsigned char *)&codetable[0] + mips16_offset;
        }
        idx++;
        access_addr += sizeof(unsigned int);
    }
    return ret;
}
/*--------------------------------------------------------------------------------*\
 * usermode: 0 Kernel
 *           1 MIPS32 
 *           2 MIPS16
\*--------------------------------------------------------------------------------*/
static void add_unaligned_access_to_table(void *pc, struct _unaligned_access_score_entry table[], unsigned int table_entries, unsigned int usermode) {
    unsigned int ts_idx = 0, ts_score = 0;
    unsigned int i;
    for(i = 0; i < table_entries; i++) {
        if(table[i].unaligneds) {
            if(usermode) {
                if(task_pid_nr(current) != table[i].pid) {
                    continue;
                }
            }
            if(pc == table[i].pc) {
                table[i].unaligneds++;
                /*--- printk(KERN_ERR "%s:[%u]add %s %s pid=%u pc=%p %lu\n", __func__, i, usermode ? "USER" : "KERNEL",  current->comm, table[i].pid, pc, table[i].unaligneds); ---*/
                return;
            }
            continue;
        }
        /*--- printk(KERN_ERR "%s:[%u]initial %s %s pc=%p pid=%u\n", __func__, i, usermode ? "USER" : "KERNEL",  current->comm, pc, table[i].pid); ---*/
        ts_idx = i;
        goto table_settings;
    }
    /*--- alle besetzt: bewerte per unaligneds / per time ---*/
    for(i = 0; i < table_entries; i++) {
        unsigned long diff = jiffies - table[i].ts;
        unsigned int score = diff / (table[i].unaligneds | 0x1);
        /*--- printk(KERN_ERR "%s:[%u]score %d diff %u unaligneds=%lu ts_score %u idx=%u\n", __func__,  i, score, diff, table[i].unaligneds, ts_score, ts_idx); ---*/
        if(score > ts_score) {
            ts_score = score;
            ts_idx = i;
        }
    }
    /*--- printk(KERN_ERR "%s:[%u]replace %s old: unaligneds=%lu pc=%p pid=%u ts_score=%u new=%s pc=%p\n", __func__, ts_idx, usermode ? "USER" : "KERNEL", table[ts_idx].unaligneds, table[ts_idx].pc, table[ts_idx].pid, ts_score, current->comm, pc); ---*/
table_settings:
    table[ts_idx].unaligneds = 1;
    table[ts_idx].pc         = pc;
    table[ts_idx].ts         = jiffies;
    table[ts_idx].pid        = task_pid_nr(current);
    table[ts_idx].mips16     = usermode & 0x2 ? 1 : 0;
    table[ts_idx].caddr_of   = cpy_code_range(pc, usermode, table[ts_idx].code, ARRAY_SIZE(table[ts_idx].code), -LEFT_CODE_OFFSET);
    strncpy(table[ts_idx].shortcomm, current->comm, sizeof(table[ts_idx].shortcomm));
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned long show_unaligned_access_table(struct seq_file *seq, struct _unaligned_access_score_entry table[], unsigned int table_entries, unsigned int usermode) {
    char comm[sizeof(table[0].shortcomm) + 1];
    unsigned int i;
    unsigned long sum = 0;
    unsigned long address;

    seq_printf(seq, "%s:\nunaligneds\t unaligneds/hour\t\n", usermode ? "User-Scorelist" : "System-Scorelist");
    for(i = 0; i < table_entries; i++) {
        if(table[i].unaligneds) {
            unsigned int diff_hour = (jiffies - table[i].ts) / HZ / 3600;
            unsigned long unaligneds_per_hour = table[i].unaligneds;
            sum += table[i].unaligneds;
            if(diff_hour) {
                unaligneds_per_hour /= diff_hour; 
            }
            if(usermode) {
                struct pid *ppid;
                struct task_struct *tsk;
                char *pcomm; 
                if((ppid = find_get_pid(table[i].pid)) && (tsk = get_pid_task(ppid, PIDTYPE_PID))) {
                    pcomm = tsk->comm;
                } else {
                    memcpy(comm, table[i].shortcomm, sizeof(comm) - 1);
                    comm[sizeof(comm) - 1] = 0;
                    pcomm = comm;
                    tsk = NULL;
                }
                seq_printf(seq, "%10lu \t%10lu \t %s(%u) pc=0x%p ", table[i].unaligneds, unaligneds_per_hour, pcomm, table[i].pid, table[i].pc);
                if(table[i].caddr_of) {
                    print_code_range(seq, ":", (unsigned int __user *)((unsigned long )&table[i].code + table[i].caddr_of), table[i].mips16, 0 /* use copy in kernel space */, -LEFT_CODE_OFFSET, RIGHT_CODE_OFFSET(2), &address);
                }
                if(tsk) {
                    put_task_struct(tsk);
                }
                if(ppid) {
                    put_pid(ppid);
                }
                seq_printf(seq, "\n");
            } else {
                seq_printf(seq, "%10lu \t%10lu \t 0x%p(%pF) ", table[i].unaligneds, unaligneds_per_hour, table[i].pc, table[i].pc);
                if(table[i].caddr_of) {
                    print_code_range(seq, ":", (unsigned int __user *)((unsigned long )&table[i].code + table[i].caddr_of), table[i].mips16, 0, -LEFT_CODE_OFFSET, RIGHT_CODE_OFFSET(2), &address);
                }
                seq_printf(seq, "\n");
            }
        }
    }
    return sum;
}
static struct _unaligned_access_score_entry user_score_table[UNALIGNED_MAX_SCORE_ENTRIES], sys_score_table[UNALIGNED_MAX_SCORE_ENTRIES];
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/

/*--------------------------------------------------------------------------------*\
 * ret: 0 ok, sonst -1: sigill, -2 sigbus ausloesen
\*--------------------------------------------------------------------------------*/
int show_unaligned_position(struct pt_regs *regs){
    unsigned long addr = 0UL;
	unsigned int __user *pc;
	pc = (unsigned int __user *) exception_epc(regs);

    if (!user_mode(regs)) {
        ai_sys++;
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
        add_unaligned_access_to_table(pc, sys_score_table, ARRAY_SIZE(sys_score_table), 0);
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/
        if(ai_kernelmode & UNALIGNED_WARN) {
            int ret;
            printk(KERN_ERR "[kernel-unaligned %lu] pc=0x%p(%pF) addr=0x%08lx task=%s pid=%d ra=0x%08lx(%pF)\n", 
                  ai_sys, pc, pc, regs->cp0_badvaddr, current->comm, current->pid,
                  regs->regs[31], (void *)regs->regs[31]
            );
            ret = print_code_range(NULL, "kernel-unaligned", pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
            if(ret) {
				return ret;
            }
	        if(print_mem_config(current->active_mm, (unsigned long)pc))
	            print_mem_config(current->mm, (unsigned long)pc);

            if(ai_kernelmode & UNALIGNED_BACKTRACE) {
                show_backtrace(current, regs);
            }
        }
		return (ai_kernelmode & UNALIGNED_ACTION_SIGNAL) ? -2 : 0;
	}
	ai_user++;
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
    add_unaligned_access_to_table(pc, user_score_table, ARRAY_SIZE(user_score_table), 1 + (regs->cp0_epc & 0x1));
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/

	if(ai_usermode & UNALIGNED_WARN) {
		int ret;
		printk(KERN_ERR"Alignment trap: %s (%d) PC=0x%p Address=0x%08lx\n", current->comm, task_pid_nr(current), pc, regs->cp0_badvaddr);
		ret = print_code_range(NULL, "kernel-unaligned", pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
		if(ret) {
			return ret;
		}
		if(print_mem_config(current->active_mm, (unsigned long)pc)) {
			print_mem_config(current->mm, (unsigned long)pc);
		}
		/*--- show_registers(regs); ---*/
	}
	return (ai_usermode & UNALIGNED_ACTION_SIGNAL) ? -2 : 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static const char *map_mode(enum _unaligned_mode mode) {
    switch(mode) {
        case UNALIGNED_ACTION_FIXUP:            return "(fixup)      ";
        case UNALIGNED_ACTION_FIXUP_WARN:       return "(fixup+warn) ";
        case UNALIGNED_ACTION_SIGNAL:           return "(signal)     ";
        case UNALIGNED_ACTION_SIGNAL_WARN:      return "(signal+warn)";
        case UNALIGNED_ACTION_FIXUP_WARN_BT:    return "(fixup+warn+backtrace)";
    }
    return "";
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void unalignment_proc_read(struct seq_file *seq, void *ref __maybe_unused){
	seq_printf(seq, "User:\t\t%lu\n", ai_user);
	seq_printf(seq, "System:\t\t%lu\n", ai_sys);
	seq_printf(seq, "User faults:\t%i %s\n", ai_usermode, map_mode(ai_usermode));
	seq_printf(seq, "Kernel faults:\t%i %s\n", ai_kernelmode, map_mode(ai_kernelmode));
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
    if(ai_user) {
        if(show_unaligned_access_table(seq,  user_score_table, ARRAY_SIZE(user_score_table), 1) != ai_user) {
            seq_printf(seq, "... only the newest user-unaligneds shown\n");
        }
    }
    if(ai_sys) {
        if(show_unaligned_access_table(seq,  sys_score_table, ARRAY_SIZE(sys_score_table), 0) != ai_sys) {
            seq_printf(seq, "... only the newest kernel-unaligneds shown\n");
        }
    }
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/
}
/*--- #define DSP_ASE_UNALIGNED_CHECK ---*/
#if defined(DSP_ASE_UNALIGNED_CHECK)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int lwx_unaligned_check(void *addr, unsigned int index) {
    unsigned int erg;
    __asm__ __volatile__ (".set\tnoat\n"
                          "LWX %0, %1(%2) \n"
                            : "=r" (erg)
                            : "r" (index), "r" ((unsigned long)addr)
                            : "cc");
    return erg;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline signed int lhx_unaligned_check(void *addr, unsigned int index) {
    signed int erg;
    __asm__ __volatile__ (".set\tnoat\n"
                          "LHX %0, %1(%2) \n"
                            : "=r" (erg)
                            : "r" (index), "r" ((unsigned long)addr)
                            : "cc");
    return erg;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int test_table[] = {
      0x1 <<  0, 0xF1 <<  0, 0x3 <<  0, 0xFF <<  0,
      0x1 <<  8, 0xF1 <<  8, 0x3 <<  8, 0xFF <<  8, 
      0x1 << 16, 0xF1 << 16, 0x3 << 16, 0xFF << 16, 
      0x1 << 24, 0xF1 << 24, 0x3 << 24, 0xFF << 24 
};
static signed short test2_table[] = {
      0x1 <<  0, 0xF1 <<  0, 0x3 <<  0, 0xFF <<  0,
      0x1 <<  8, 0xF1 <<  8, 0x3 <<  8, 0xFF <<  8, 
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void check_dsp_ase_unaligneds(void) {
    unsigned int i;
    unsigned int unalignedtable[16 + 1];
    unsigned int *p = (unsigned int *)((unsigned char *)unalignedtable +1);

    memcpy(p, test_table, sizeof(test_table));
    for(i = 0; i < ARRAY_SIZE(test_table); i++) {
        unsigned int val = lwx_unaligned_check(p, i * 4);
        if(val != test_table[i]) {
            printk(KERN_ERR"%s:#1 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, test_table[i]);
        }
    }
    for(i = 0; i < ARRAY_SIZE(test_table); i++) {
        unsigned int val = lwx_unaligned_check(unalignedtable, (i * 4) + 1);
        if(val != test_table[i]) {
            printk(KERN_ERR"%s:#2 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, test_table[i]);
        }
    }
    memcpy(p, test2_table, sizeof(test2_table));
    for(i = 0; i < ARRAY_SIZE(test2_table); i++) {
        signed int val = lhx_unaligned_check(p, i * 2);
        if(val != (signed int)test2_table[i]) {
            printk(KERN_ERR"%s:#3 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, (signed int)test2_table[i]);
        }
    }
    for(i = 0; i < ARRAY_SIZE(test2_table); i++) {
        signed int val = lhx_unaligned_check(unalignedtable, (i * 2) + 1);
        if(val != (signed int)test2_table[i]) {
            printk(KERN_ERR"%s:#4 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, (signed int)test2_table[i]);
        }
    }
}
#endif/*--- #if defined(DSP_ASE_UNALIGNED_CHECK) ---*/
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int unalignment_proc_write(char *cmd, void *ref __maybe_unused) {
	int mode = cmd[0];
#if defined(DSP_ASE_UNALIGNED_CHECK)
    if(mode == 'T') {
        check_dsp_ase_unaligneds();
        return 0;
    }
#endif/*--- #if defined(DSP_ASE_UNALIGNED_CHECK) ---*/
	if (mode >= '2' && mode <= '5') {
		ai_usermode = mode - '0';
		printk(KERN_ERR "set user unaligned-mode: %s\n", map_mode(ai_usermode));
	} else if (mode >= '6' && mode <= '8') {
		ai_kernelmode = mode == '6' ? UNALIGNED_ACTION_FIXUP : 
                        mode == '7' ? UNALIGNED_ACTION_FIXUP_WARN : UNALIGNED_ACTION_FIXUP_WARN_BT;
		printk(KERN_ERR "set kernel unaligned-mode: %s\n", map_mode(ai_kernelmode));
	} else {
		printk(KERN_ERR "parameter: user   '2' %s '3' %s '4' %s '5' %s \n", map_mode(UNALIGNED_ACTION_FIXUP), map_mode(UNALIGNED_ACTION_FIXUP_WARN), map_mode(UNALIGNED_ACTION_SIGNAL), map_mode(UNALIGNED_ACTION_SIGNAL_WARN));
		printk(KERN_ERR "           system '6' %s '7' %s '8' %s\n", map_mode(UNALIGNED_ACTION_FIXUP), map_mode(UNALIGNED_ACTION_FIXUP_WARN), map_mode(UNALIGNED_ACTION_FIXUP_WARN_BT));
	}
	return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init alignment_init(void) {
	int ret; 
	proc_mkdir("cpu", NULL);
	ret = add_simple_proc_file(
			"cpu/alignment", 
			unalignment_proc_write,
			unalignment_proc_read,
			NULL);
	return ret;
}
__initcall(alignment_init);
