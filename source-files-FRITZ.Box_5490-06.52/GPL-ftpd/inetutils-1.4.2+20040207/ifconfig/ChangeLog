2003-10-24  Marcus Brinkmann  <marcus@gnu.org>

	* options.c (PARSE_OPT_SET_ADDR): Fix macro for gcc 3.3.
	(PARSE_OPT_SET_INT): Likewise.

2002-05-14  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* Makefile.am: Removed obsolete OMIT_DEPENDENCIES.
	* system.h: Use HAVE_STRUCT_type_member instead of
	  HAVE_type_member.

2002-04-29  Alfred M. Szmidt  <ams@kemisten.nu>

	* system/solaris.c, system/qnx.c, system/osf.c, system/linux.c,
	system/hpux.c, system/generic.c, printif.c, ifconfig.c, changeif.c,
	options.c <version.h>: Include removed.

	* options.c (inetutils_package): Renamed to PACKAGE_NAME.
	(inetutils_version): Renamed to PACKAGE_VERSION.
	(inetutils_bugaddr): Renamed to PACKAGE_BUGREPORT.

2002-04-21  Alfred M. Szmidt <ams@kemisten.nu>

	* Makefile.am (bin_PROGRAMS): Use ifconfig_BUILD instead of
	BUILD_IFCONFIG.

2001-07-10  Sergey Poznyakoff
	* if_index.c: Include ifconfig.h instead of if_index.h.
	Reason: ifconfig.h includes system.h which contains
	definition of ifr_index member for systems lacking it.
	It also includes if_index.h. Thanks to Alain.

2001-06-30  Sergey Poznyakoff
	* if_index.c: if_nameindex(): Actually fall back :^). Besides,
	the return value was unspecified if socked() failed.
	Removed # define ifr_ifindex ifr_index.
	Use ifr_index instead of ifr_ifindex throughout the module, since 
	the check for its presence is already made in configure.in and
	appropriate define is in system.h.
	if_nameindex(): reverted condition on ioctl() return.

2001-06-26  Alain Magloire

	* if_index.c: Solaris does not define ifr_index but rather
	ifr_ifindex ... sigh ..  redefine it to ifr_index to be
	consistent.
	(if_indextoname): Typo missing closing bracket.

	* options.c (parse_opt_finalize): The return of getopt()
	should be an int not a char since on many OS for example
	QNX4/Watcomm C char is unsigned.

	* system.c:  Use __QNX__
	* system.h:  Use __QNX__
	* system/qnx.h: remove qfh_netmask and qfh_brdaddr, the generic
	versions will work fine.

2001-06-25  Alain Magloire

	* if_index.c: Fall back to the generic version if
	SIOCGIFINDEX and SIOCGIFNAME fail.
	Note from Sergey.
	Define BSD_COMP for solaris machines to include
	the ioctl definitions.

	* options.c (usage): Terminate the string with '\n\'.
	* printif.c (print_interfaceX): Use alloca() instead
	of dynamique allocated arrays, may compiler can not
	cope with this yet.

2001-06-23  Marcus Brinkmann  <marcus@gnu.org>

	* system/qnx.c (qfh_brdaddr, qfh_netmask): Functions removed,
	the generic ones will work fine with the changes to system.h.
	* system/qnx.h: Prototypes for these functions removed.

	* system/qnx.h: Do not define ifr_netmask.

	* printif.h: Move check for HAVE_* introduced in last change to ...
	system.h: ... here.

	* options.c: Revert change from Alain.

2001-06-04  Alain Magloire

	After patches and feedback From Sergey and Marcus.

	* ifconfig/if_index.h: New file, prototypes for if_nameindex().
	* ifconfig/if_index.c: New version base on the one in GLIBC.
	* ifconfig/Makefile.am: Added if_index.c unconditionnaly.
	* ifconfig/changeif.c(configure_if): memset(0) ifr, it helps debugging.
	* ifconfig/flags.c: const consistencies.
	* ifconfig/ifconfig.h: include if_index.h
	* ifconfig/options.c: Defined SYSTEM_SHORT_OPTIONS SYSTEM_LONG_OPTIONS.
	* ifconfig/options.h: const consistencies.
	* ifconfig/printif.c: const consistencies.
	* ifconfig/printif.h: const consistencies.
	Check for HAVE_IFREQ_IFR_INDEX HAVE_IFREQ_IFR_NETMASK
	HAVE_IFREQ_IFR_BROADADDR.
	* ifconfig/system.c: Use marcor processor #elif for clarity.
	* ifconfig/system.h: const consistencies.
	* ifconfig/system/qnx.c: New file.
	* ifconfig/system/qnx.h: New file.
	* ifconfig/system/solaris.c: Define system_configure.

2001-06-04  Marcus Brinkmann  <marcus@gnu.org>

	* options.c (long_options): Only add SYSTEM_LONG_OPTIONS
	if it is defined.

	Lots of small fixes submitted Sergey Poznyakoff
	<gray@Mirddin.farlep.net>:
	* ifconfig.h: Do not include <config.h>.
	* printif.h: Likewise.
	* changeif.c: Include <sys/socket.h>.
	* ifconfig.c: Likewise.
	* printif.c: Likewise.
	* system/hpux.c: Don't include <stdint.h>.
	* system/osf.c: Likewise.
	* system/linux.c: Likewise.
	* system/solaris.c: Likewise.
	* printif.c: Likewise.
	* system/generic.c (system_configure): New function.
	* system/osf.c (system_configure): Likewise.
	* system/hpux.c (system_configure): Likewise.
	* system/solaris.c (system_parse_opt_rest): Fix order of operators.
	(system_default_format): Fix syntax.
	Include <sys/socket.h>, <netinet/in.h>, <netinet/if_ether.h> (rather than
	<linux/if_ether.h>).
	* system.h: Change __solaris__ to __sun__.
	* system.c: Likewise.

2001-05-07  Jeff Bailey  <jbailey@oasis.zugzug.com>

	* system/Makefile.am: New File, for 'make dist' derived targets.

	* Makefile.am: Add printif.h and system.h to noinst_HEADERS.

2001-02-27  Marcus Brinkmann  <marcus@gnu.org>

	* system/linux.c (system_configure): Move return statement outside
	of if-block.

	* printif.h: Declare had_output.
	* printif.c (had_output): New global variable.
	(many functions): Whereever we output something, set had_output to 1.
	(join): Remove variable first. New variable had_output_saved.
	Clear had_output after storing it in had_output_saved. Use that to
	determine if to print delimiter. Later on, restore had_output.
	* system/linux.c (some functions): Whereever we output something,
	set had_output to 1.

2001-02-22  Marcus Brinkmann  <marcus@gnu.org>

	* flags.c: Protect inclusion of config.h.

2001-02-22  Marcus Brinkmann  <marcus@gnu.org>

	* Initial release.

