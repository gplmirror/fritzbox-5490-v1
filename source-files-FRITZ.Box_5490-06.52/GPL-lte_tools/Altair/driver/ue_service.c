/*
 * This driver is based on the usb-skeleton driver from the Linux kernel code.
 * The hardware, it is not intended for general usage except by Altair's software
 * and personnel.
 *
 * Copyright (C) 2010 by Altair Semiconductor Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the linux kernel you work with; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
  This is a driver that exports character device access to the device's service and debug ports.
  this driver claims the 3rd interface of the UE device.

  we have 5 endpoints:
  0x83 fw-in/log
  0x2 fw-out/console
  0x3 reset
  0x4 vl1-out
  0x84 vl1-in

  The driver exports two character device nodes for each device
  /dev/ueservice<N> - user reads from EP 0x83, writes to EP 0x2
  /dev/uedebug<N> - user reads from EP 0x84, writes to EP 0x4
  the reset port 0x3 will be operated via ioctl only(). the ioctl will be supported on /dev/ueservice<N> interface
  for simplicity, this driver's I/O is synchronous as we don't need any speed here.

  since this is not a general purpose driver but a driver that is intended to be opened by internally developed
  applications, we know that we will not lose data due to rx messages that are not being read. also, we assume
  that the reading application reads the entire message. therefore, each read() causes the buffer to be reused
  by the RX loop.
  RX flow:
  we have two buffers: inbuf[0] is always submitted to the USB stack via a urb, and inbuf[1] contains data that was
  returned from a previously completed urb. data_len contains the number of bytes available for a client application.
  on open() we submit a URB for the first time. when the URB's callback is returned, we either have data, or we are
  recovering from disconnect/error.
  if we have data we look at data_len. if data_len is 0, that means that the application consumed any previous RX data
  and we can swap buffers - we swap buffers and set data_len to the urb's actual_length.
  however, if data_len is not 0, that means that the application did not read the data from a previously submitted URB
  in such a case, we resubmit inbuf[0] and lose the most recent data.
  the read() operation, copies data from inbuf[1], and sets data_len to 0. this frees the buffer to be swapped when
  the currently submitted urb returns.
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/signal.h>
#include <linux/poll.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/usb.h>
#include <linux/fcntl.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/list.h>
#include <linux/proc_fs.h>
#include <linux/version.h>

#include "ue.h"

MODULE_AUTHOR("Yanir Lubetkin");
MODULE_DESCRIPTION("Altair LTE device - debug/service ports");
MODULE_LICENSE("GPL");

#define UESERVICE_MINORS 2  //2 Char devices

/* == AVM/Bruno 10.08.2011 Buffer Length = bMaxPacketSize (-EOVERLFOW Error) == */
#define UESERVICE_BUF_SIZE_IN	512
#define N_READ_URBS				4

#define IOCTL_RESET 0x0d04e5e7
#define PANIC_TEXT "LTE modem not found"
#define PANIC_PROC_NAME "lte_panic"

#define MAGIC_PANIC_STRING "LTE:"

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
#ifdef DEBUG
	#define dbg(format, arg...) printk(KERN_DEBUG "%s: " format "\n" , __FILE__ , ## arg)
#else
	#define dbg(format, arg...) do {} while (0)
#endif
#endif	

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,5,0)
	#define err(format, arg...) printk(KERN_ERR KBUILD_MODNAME ": " format "\n" , ## arg)
#endif	

/*
  For each device, we have two character interfaces. the device structure represents it in the following manner.
  usb data which is common to both char devices is stored in struct ueservice. it also contains two members of
  struct uechr. an instance of struct uechr contains all the data required to implement the character device access.
*/

struct _urb_handle {
	struct list_head	list_entry;
	void			 	*urb;
	char				*readbuf;
	int                 rstatus;        /* bytes ready or error */
	int					readcount;
	struct uechr 		*uechr;
};

struct ueservice;

struct uechr{
	struct ueservice 		*ue;
	struct cdev 			cdev;
	struct device 			*device;
	struct list_head 		free_urbs;
	struct list_head 		done_urbs;
	spinlock_t	          	lock_urb;
	int 					minor;
	struct mutex            mut;
	struct mutex            wmut;
	spinlock_t              lock;           /* locks rcomplete, wcomplete */
	wait_queue_head_t       rwait;
	struct usb_anchor       urbs;
	int                     rcomplete;
	unsigned char           used;			/* True if open */
	unsigned char           present;		/* True if not disconnected */
	unsigned char           bidir;                  /* interface is bidirectional */
	unsigned char           sleeping;		/* interface is suspended */
	unsigned char			in_ep;
	unsigned char			out_ep;

};

struct ueservice {

	struct usb_device 		*udev;			/* the usb device for this device */
	struct usb_interface 	*intf;			/* the interface for this device */
	unsigned char 			reset_ep;		/* the address of the bulk reset endpoint */
	struct uechr 			dbg;			/* char device vl1 (debug) context */
	struct uechr 			ctl;			/* char device log/console/fwdownload (service) context */
};

static DEFINE_MUTEX(ueservice_mutex);       /* locks the existence of usblp's */

static struct class *ue_class;
static struct class *ue2_class;
static struct ueservice ueservice;
static int major;

static int bus = 0;
static int hub_port = 0;

static int ueservice_probe(struct usb_interface *intf, const struct usb_device_id *id);
static void ueservice_disconnect(struct usb_interface *intf);


module_param(bus, int, S_IRUGO | S_IWUSR);
module_param(hub_port, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(bus, "USB Bus supported (0: all, 1: Only bus 1, 2: Only Bus 2)");
MODULE_PARM_DESC(hub_port, "if used internal hub, which port is supported");

static const struct usb_device_id	products [] = {
	{
		/* altair LTE 216f:0040*/
		USB_DEVICE_AND_INTERFACE_INFO(0x216f, 0x0040, 0xff, 0, 0),
	},
    {
		/* altair LTE 216f:0041*/
		USB_DEVICE_AND_INTERFACE_INFO(0x216f, 0x0041, 0xff, 0, 0),
	},
	{ },// END
};
MODULE_DEVICE_TABLE(usb, products);


static struct usb_driver ueservice_driver = {
	.name 		= "ueservice",
	.id_table 	= products,
	.probe 		= ueservice_probe,
	.disconnect = ueservice_disconnect,
};



static void ueservice_bulk_read(struct urb *urb)
{
	//struct uechr *uechr = urb->context;
	struct _urb_handle *element = (struct _urb_handle *) urb->context;
	struct uechr *uechr = element->uechr;
	int status = urb->status;
	unsigned long flags;

	if (uechr->present && uechr->used) {
		//if (status)
		//	printk(KERN_WARNING "ueservice %d: nonzero read bulk status received: %d\n",	uechr->minor, status);
	}

	if (status < 0) {
		element->rstatus = status;
	} else {
		element->rstatus = urb->actual_length;
	}


	spin_lock_irqsave(&uechr->lock_urb, flags);
	list_add_tail(&(element->list_entry), &(uechr->done_urbs));
	spin_unlock_irqrestore(&uechr->lock_urb, flags);


	spin_lock(&uechr->lock);

	if (uechr->rcomplete == 0) {

		uechr->rcomplete = 1;

	}
	wake_up(&uechr->rwait);

	spin_unlock(&uechr->lock);


	usb_free_urb(urb);
}


static int ueservice_submit_read(struct uechr *uechr)
{
	struct ueservice *ue = uechr->ue;
	unsigned long flags;
	int rc;
	struct list_head *ptr, *nxt;
	struct _urb_handle *element;

	rc = -ENOMEM;

	spin_lock_irqsave(&uechr->lock_urb, flags);

	if(list_empty(&uechr->free_urbs)) {
		spin_unlock_irqrestore(&uechr->lock_urb, flags);
		spin_lock_irqsave(&uechr->lock, flags);
		uechr->rcomplete = 0;
		spin_unlock_irqrestore(&uechr->lock, flags);
	} else {

		list_for_each_safe(ptr, nxt, &uechr->free_urbs) {

			element = list_entry(ptr, struct _urb_handle, list_entry);
			
			list_del(ptr);
			
			if ((element->urb = usb_alloc_urb(0, GFP_KERNEL)) == NULL)
				goto raise_urb;

			/* prepare a read */
			usb_fill_bulk_urb(element->urb, ue->udev, usb_rcvbulkpipe(ue->udev, uechr->in_ep),
								element->readbuf, UESERVICE_BUF_SIZE_IN,
								ueservice_bulk_read, element);

			usb_anchor_urb(element->urb, &uechr->urbs);

			if ((rc = usb_submit_urb(element->urb, GFP_KERNEL)) < 0) {

				dbg("error submitting urb (%d)", rc);
				spin_lock_irqsave(&uechr->lock, flags);

				element->rstatus = rc;
				uechr->rcomplete = 1;

				spin_unlock_irqrestore(&uechr->lock, flags);

				usb_unanchor_urb(element->urb);
				usb_free_urb(element->urb);

				goto raise_urb;
			}

			element->readcount = 0;

		}
		spin_unlock_irqrestore(&uechr->lock_urb, flags);
	}

	return 0;

raise_urb:
	spin_unlock_irqrestore(&uechr->lock_urb, flags);
	// FIXME ptr wieder in die Liste ?? 
	return rc;
}


static void ueservice_unlink_urbs(struct uechr *uechr)
{
	usb_kill_anchored_urbs(&uechr->urbs);
}



static int handle_bidir (struct uechr *uechr)
{
	if (uechr->bidir && uechr->used && !uechr->sleeping) {
		if (ueservice_submit_read(uechr) < 0)
			return -EIO;
	}
	return 0;
}


static int ueservice_open(struct inode *inode, struct file *file)
{
	struct cdev *cdev = inode->i_cdev;
	struct uechr * uechr = container_of(cdev, struct uechr, cdev);

	struct ueservice *ue;
	struct usb_interface *interface;
	int subminor =  iminor(inode);
	int rv = 0;
	unsigned long flags;

	struct list_head *ptr, *nxt;
	struct _urb_handle *element;


	/*== AVM/BC 20101203 Workaround uedebug hat a diferent Minor Id ==*/
 	if(subminor >= UE_DEVS)
 		subminor = subminor - UE_DEVS;

	mutex_lock(&ueservice_mutex);

	interface = usb_find_interface(&ueservice_driver, subminor);
 	if (!interface) {
		err ("%s - error, can't find device for minor %d", __func__, subminor);
 		rv = -ENODEV;
 		goto exit;
 	}

	ue = usb_get_intfdata(interface);
	if (!ue) {
		rv = -ENODEV;
		goto exit;
	}

	// notify the power management that we are busy
	rv = usb_autopm_get_interface(ue->intf);
	if (rv) {
		dev_err(uechr->device,"%s - usb_autopm_get_interface() returned error %d\n",
				__func__, rv);
		return rv;
	}

	uechr->used = 1;
	uechr->rcomplete = 0;

    /* save our object in the file's private structure */
	file->private_data = uechr;


	spin_lock_irqsave(&uechr->lock_urb, flags);

	if(!list_empty(&uechr->done_urbs)) {
		list_for_each_safe(ptr, nxt, &uechr->done_urbs) {

			element = list_entry(ptr, struct _urb_handle, list_entry);
			list_move_tail(&element->list_entry, &uechr->free_urbs);
		}
	}

	spin_unlock_irqrestore(&uechr->lock_urb, flags);


	if (handle_bidir(uechr) < 0) {
		usb_autopm_put_interface(interface);
		uechr->used = 0;
		file->private_data = NULL;
		rv = -EIO;
	}



exit:
	mutex_unlock(&ueservice_mutex);

	return rv;
}

static int ueservice_release(struct inode *inode, struct file *file)
{

	struct uechr *uechr = (struct uechr *)file->private_data;

	struct ueservice *ue;

	if (uechr == NULL)
		return -ENODEV;

	ue = uechr->ue;

	if(!ue->intf)
		return -ENODEV;


	mutex_lock (&ueservice_mutex);

	uechr->used = 0;
	if( uechr->present) {

		PDEBUG("unlink urbs\n");
		ueservice_unlink_urbs(uechr);
		usb_autopm_put_interface(ue->intf);
	}


	file->private_data = 0;

	mutex_unlock(&ueservice_mutex);

	return 0;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
	long ueservice_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
#else
	int ueservice_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
#endif	
{
	__u32 val = __constant_cpu_to_be32(1);
	int retval = 0, bytes_written;
	struct ueservice *ue;

	struct uechr *uechr = (struct uechr *)filp->private_data;

	if (uechr == NULL)
		return -ENODEV;

	ue = uechr->ue;

	if(!ue->intf)
		return -ENODEV;

	switch(cmd){
		case IOCTL_RESET:
            // Mutex wird in ueservice_disconnect() freigegeben
            if (!mutex_trylock(&uechr->wmut)) {
                printk(KERN_ERR "ueservice: IOCTL_RESET ignored, FW upload in progress\n");
                break;
            }             
			retval = usb_bulk_msg(uechr->ue->udev,
							  usb_sndbulkpipe(ue->udev, ue->reset_ep),
							  &val, 4, &bytes_written, 100);
			PDEBUG("exiting: written %d, retval %d\n", bytes_written, retval);
		        printk(KERN_ERR "ueservice: IOCTL_RESET\n"); // Loglevel: nur KERN_ERR kommt ins Logfile der Box. 
			break;
		default:
			PDEBUG("ioctl %x out of range, exiting\n", cmd);
			return -ERANGE;
	}
	return retval;
}


static int ueservice_rtest(struct uechr *uechr, int nonblock)
{
	unsigned long flags;

	if (!uechr->present)
		return -ENODEV;
	if (signal_pending(current))
		return -EINTR;
	spin_lock_irqsave(&uechr->lock, flags);
	if (uechr->rcomplete) {
		spin_unlock_irqrestore(&uechr->lock, flags);
		return 0;
	}
	spin_unlock_irqrestore(&uechr->lock, flags);
	if (uechr->sleeping)
		return -ENODEV;
	if (nonblock)
		return -EAGAIN;

	return 1;
}


static int ueservice_rwait_and_lock(struct uechr *uechr, int nonblock) {

	DECLARE_WAITQUEUE(waita, current);
	int rc;

	add_wait_queue(&uechr->rwait, &waita);
	for(;;){
		if (mutex_lock_interruptible(&uechr->mut)) {
			rc = -EINTR;
			break;
		}
		set_current_state(TASK_INTERRUPTIBLE);
		if ((rc = ueservice_rtest(uechr, nonblock)) < 0) {
			mutex_unlock(&uechr->mut);
			break;
		}
		if (rc == 0)    /* Keep it locked */
			break;
		mutex_unlock(&uechr->mut);
		schedule();
	}
	set_current_state(TASK_RUNNING);
	remove_wait_queue(&uechr->rwait, &waita);

	return rc;
}



static ssize_t ueservice_read(struct file *file, char *buffer, size_t len, loff_t *ppos) {

	struct uechr * uechr = (struct uechr *)file->private_data;
	ssize_t count;
 	ssize_t avail;
	int rv;
	unsigned long flags;

	struct _urb_handle *element;


	rv = ueservice_rwait_and_lock(uechr, !!(file->f_flags & O_NONBLOCK));
	if (rv < 0)
		return rv;

	spin_lock_irqsave(&uechr->lock_urb, flags);

	if(list_empty(&uechr->done_urbs)) {

		spin_unlock_irqrestore(&uechr->lock_urb, flags);

		ueservice_submit_read(uechr);
		count = -EIO;
		goto done;
	}


	element = list_entry(uechr->done_urbs.next, struct _urb_handle, list_entry);


	if ((avail = element->rstatus) < 0) {

		printk(KERN_ERR "uechr%d: error %d reading\n", uechr->minor, (int)avail);

		list_move_tail(&element->list_entry, &uechr->free_urbs);
		spin_unlock_irqrestore(&uechr->lock_urb, flags);

		ueservice_submit_read(uechr);
		count = -EIO;
		goto done;
	}
	
	spin_unlock_irqrestore(&uechr->lock_urb, flags);

	count = len < avail - element->readcount ? len : avail - element->readcount;
	if (count != 0 && copy_to_user(buffer, element->readbuf + element->readcount, count)) {
		count = -EFAULT;
		goto done;
	}

	if ((element->readcount += count) == avail) {
	
		spin_lock_irqsave(&uechr->lock_urb, flags);

		list_move_tail(&element->list_entry, &uechr->free_urbs);

		if(list_empty(&uechr->done_urbs)) {
			if (uechr->rcomplete == 1) {
				uechr->rcomplete = 0;
			}
		}

		spin_unlock_irqrestore(&uechr->lock_urb, flags);

		if (ueservice_submit_read(uechr) < 0) {
			/* We don't want to leak USB return codes into errno. */
			if (count == 0)
				count = -EIO;
		}	
		goto done;
	}

done:
	mutex_unlock (&uechr->mut);

	return count;
}


static ssize_t ueservice_write(struct file *filp, const char *user_buffer, size_t count, loff_t *ppos)
{

	struct uechr * uechr = (struct uechr *)filp->private_data;
	int retval = -ENODEV;
	int bytes_written;
	char *buf;

	if (!mutex_trylock(&uechr->wmut)) { 
        printk(KERN_ERR "ueservice: IOCTL_RESET pending, FW upload ignored\n");
        return retval;
    }

	if (!uechr->ue->intf) {
		retval = -ENODEV;
		goto exit;
	}

	buf = kmalloc(count, GFP_KERNEL);
	if(!buf){
		retval = -ENOMEM;
		goto exit;
	}

	if(copy_from_user(buf, user_buffer, count)){
		retval = -EFAULT;
		goto exit_free;
	}

	retval = usb_bulk_msg(uechr->ue->udev,
			      usb_sndbulkpipe(uechr->ue->udev, uechr->out_ep),
			      buf, count, &bytes_written, 1000);

	if(!retval)
		retval = bytes_written;

exit_free:
	kfree(buf);
exit:
	mutex_unlock(&uechr->wmut);
	return retval;
}

/* No kernel lock - fine */
static unsigned int ueservice_poll(struct file *filp, struct poll_table_struct *wait)
{
	struct uechr *uechr = (struct uechr *)filp->private_data;
	unsigned long flags;
	int ret;

	PDEBUG("enter\n");

	/* Should we check file->f_mode & FMODE_WRITE before poll_wait()? */
	poll_wait(filp, &uechr->rwait, wait);

	spin_lock_irqsave(&uechr->lock, flags);

	PDEBUG("uechr->bidir %d, uechr->rcomplete %d\n", uechr->bidir, uechr->rcomplete);

	ret = (uechr->bidir && uechr->rcomplete) ? POLLIN  | POLLRDNORM : 0;

	ret |= POLLOUT  | POLLWRNORM;

	spin_unlock_irqrestore(&uechr->lock, flags);

	PDEBUG("exit\n");

	return ret;
}



static const struct file_operations ueservice_fops = {
	.owner =	THIS_MODULE,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
	.unlocked_ioctl = ueservice_ioctl,
#else
	.ioctl =	ueservice_ioctl,
#endif	
	.read =		ueservice_read,
	.write =	ueservice_write,
	.open =		ueservice_open,
	.release =	ueservice_release,
	.poll = 	ueservice_poll,
};


////////////////////////////////////////////////////////////////////////////////////////////////


static int ueservice_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
	struct ueservice *ue;
	struct usb_host_interface *iface_desc;
	struct usb_endpoint_descriptor *endpoint;
	struct usb_device *udev = interface_to_usbdev(intf);
	struct uechr *dbg, *ctl;
	struct _urb_handle *urb_tmp_ctl[N_READ_URBS], *urb_tmp_dbg[N_READ_URBS];

	dev_t devnum;
	int i, minor = 0;
	int retval = -ENOMEM;


	iface_desc = intf->cur_altsetting;

	if(iface_desc->desc.bNumEndpoints != 5)
		return -ENODEV;

	/* Check if bus is supported */
	if((bus != 0) && (bus != udev->bus->busnum)){
		printk(KERN_ERR "ueservice: device ignored (bus %d)\n", udev->bus->busnum);
		return -ENODEV;
	}

	/* Check if port of internal hub is supported (6841). Level must be 2 */
	if(hub_port != 0) {
		if ((udev->level != 2) || (hub_port != udev->portnum)){
			printk(KERN_ERR "ueservice: device ignored (port %d level %d)\n", udev->portnum, udev->level);
			return -ENODEV;
		}
	}
	ue = &ueservice;

	dbg = &ue->dbg;
	ctl = &ue->ctl;

	INIT_LIST_HEAD(&dbg->free_urbs);
	INIT_LIST_HEAD(&dbg->done_urbs);
	INIT_LIST_HEAD(&ctl->free_urbs);
	INIT_LIST_HEAD(&ctl->done_urbs);

	for(i = 0; i < N_READ_URBS; i++) {

		urb_tmp_dbg[i] = (struct _urb_handle *) kmalloc(sizeof(struct _urb_handle), GFP_KERNEL);
		if (urb_tmp_dbg == NULL) {
			retval = -ENOMEM;
			goto error;
		}
		urb_tmp_ctl[i] = (struct _urb_handle *) kmalloc(sizeof(struct _urb_handle), GFP_KERNEL);
		if (urb_tmp_ctl == NULL) {
			retval = -ENOMEM;
			kfree(urb_tmp_dbg[i]);
			goto error;
		}

		urb_tmp_dbg[i]->uechr = dbg;
		urb_tmp_ctl[i]->uechr = ctl;

		if (!(urb_tmp_dbg[i]->readbuf = kmalloc(UESERVICE_BUF_SIZE_IN, GFP_KERNEL))) {
			retval = -ENOMEM;
			kfree(urb_tmp_dbg[i]);
			kfree(urb_tmp_ctl[i]);
			goto error;
		}
		if (!(urb_tmp_ctl[i]->readbuf = kmalloc(UESERVICE_BUF_SIZE_IN, GFP_KERNEL))) {
			kfree(urb_tmp_dbg[i]->readbuf);
			kfree(urb_tmp_dbg[i]);
			kfree(urb_tmp_ctl[i]);
			retval = -ENOMEM;
			goto error;
		}
		
		list_add_tail(&(urb_tmp_dbg[i]->list_entry), &(dbg->free_urbs));
		list_add_tail(&(urb_tmp_ctl[i]->list_entry), &(ctl->free_urbs));
	}

	ue->udev = usb_get_dev(interface_to_usbdev(intf));
	mutex_init(&dbg->mut);
	mutex_init(&ctl->mut);
	mutex_init(&dbg->wmut);
	mutex_init(&ctl->wmut);
	spin_lock_init(&dbg->lock_urb);
	spin_lock_init(&ctl->lock_urb);
	spin_lock_init(&dbg->lock);
	spin_lock_init(&ctl->lock);
	init_waitqueue_head(&dbg->rwait);
	init_waitqueue_head(&ctl->rwait);
	init_usb_anchor(&dbg->urbs);
	init_usb_anchor(&ctl->urbs);

	dbg->ue = ue;
	ctl->ue = ue;

	dbg->minor = minor+1;
	ctl->minor = minor;

	devnum = MKDEV(major, minor);

	dbg->bidir = 1;
	ctl->bidir = 1;

	for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {

		endpoint = &iface_desc->endpoint[i].desc;
		switch(i){
		case 0:
            // this is the read ep of the service driver
			if(!usb_endpoint_is_bulk_in(endpoint))
				goto error;

			/*== AVM/BC 20100830 Bigger Buffer == */
			ctl->in_ep = endpoint->bEndpointAddress;
			break;
		case 1:
            // this is the write ep of the service driver
			if(!usb_endpoint_is_bulk_out(endpoint))
				goto error;
			ctl->out_ep = endpoint->bEndpointAddress;
			break;
		case 2:
            // this is the reset ep of the service driver
			if(!usb_endpoint_is_bulk_out(endpoint))
				goto error;
			ue->reset_ep = endpoint->bEndpointAddress;
			break;
		case 3:
            // this is the read ep of the debug driver
			if(!usb_endpoint_is_bulk_out(endpoint))
				goto error;
			dbg->out_ep = endpoint->bEndpointAddress;
			break;
		case 4:
            // this is the write ep of the debug driver
			if(!usb_endpoint_is_bulk_in(endpoint))
				goto error;
			dbg->in_ep = endpoint->bEndpointAddress;
			break;
		}
	}
	if (i != 5) {
		err("Could not find all required endpoints. found %d\n", i);
		goto error;
	}

	dbg->present = 1;
	ctl->present = 1;

    // register your context with the usb interface
	usb_set_intfdata(intf, ue);

	intf->minor = ctl->minor;

	ue->intf = intf;

	// init the service char device.
	cdev_init(&ctl->cdev, &ueservice_fops);
	ctl->cdev.owner = THIS_MODULE;
	retval = cdev_add(&ctl->cdev, devnum, 1);
	if(retval)
		goto error;

	// create the service char device
	ctl->device = device_create(ue_class, NULL, devnum, NULL, "ueservice%d", minor);
	if(!ctl->device){
		retval = -ENODEV;
		goto error;
	}

	devnum = MKDEV(major, minor+UE_DEVS);

	// init the debug char device.
	cdev_init(&dbg->cdev, &ueservice_fops);
	dbg->cdev.owner = THIS_MODULE;
	retval = cdev_add(&dbg->cdev, devnum, 1);
	if(retval)
		goto error;

	// create the debug char device
	dbg->device = device_create(ue2_class, NULL, devnum, NULL, "uedebug%d", minor);
	if(!dbg->device){
		retval = -ENODEV;
		goto error;
	}

	return 0;

error:

	for(i = 0; i < N_READ_URBS; i++) {
		if (urb_tmp_dbg[i]) {		
			if (urb_tmp_dbg[i]->readbuf)
				kfree(urb_tmp_dbg[i]->readbuf);
			kfree(urb_tmp_dbg[i]);
		}
		if (urb_tmp_ctl[i]) {		
			if (urb_tmp_ctl[i]->readbuf)
				kfree(urb_tmp_ctl[i]->readbuf);
			kfree(urb_tmp_ctl[i]);
		}			
		kfree(urb_tmp_ctl[i]);
	}

	if(ctl->device){
		devnum = MKDEV(major, ue->ctl.minor);
		device_destroy(ue_class, devnum);
	}

	if(dbg->device){
		devnum = MKDEV(major, dbg->minor);
		device_destroy(ue2_class, devnum);
	}

	cdev_del(&dbg->cdev);
	cdev_del(&ctl->cdev);

	usb_set_intfdata(intf, NULL);

	return retval;
}




static void ueservice_disconnect(struct usb_interface *intf)
{
	struct ueservice *ue = usb_get_intfdata(intf);
	dev_t devnum;
	struct list_head *ptr, *nxt;
	struct _urb_handle *element;

	mutex_lock (&ueservice_mutex);

    // disconnet from usb interface.
	usb_set_intfdata(intf, NULL);

	mutex_lock(&ue->dbg.mut);
	mutex_lock(&ue->ctl.mut);

	ue->dbg.present = 0;
	ue->ctl.present = 0;

	wake_up(&ue->dbg.rwait);
	wake_up(&ue->ctl.rwait);

	usb_set_intfdata (intf, NULL);

	ueservice_unlink_urbs(&ue->dbg);
	ueservice_unlink_urbs(&ue->ctl);

	mutex_unlock(&ue->ctl.mut);
	mutex_unlock(&ue->dbg.mut);


	devnum = MKDEV(major, ue->ctl.minor);
	device_destroy(ue_class, devnum);

	devnum = MKDEV(major, ue->dbg.minor);
	device_destroy(ue2_class, devnum);

	cdev_del(&ue->dbg.cdev);
	cdev_del(&ue->ctl.cdev);

	usb_put_dev(ue->udev);

	mutex_unlock(&ue->ctl.wmut);

	list_for_each_safe(ptr, nxt, &ue->dbg.free_urbs) {
		element = list_entry(ptr, struct _urb_handle, list_entry);
		if(element->readbuf)
			kfree(element->readbuf);
		list_del(ptr);
		kfree(element);
	}

	list_for_each_safe(ptr, nxt, &ue->dbg.done_urbs) {
		element = list_entry(ptr, struct _urb_handle, list_entry);
		if(element->readbuf)
			kfree(element->readbuf);
		list_del(ptr);
		kfree(element);
	}

	list_for_each_safe(ptr, nxt, &ue->ctl.free_urbs) {
		element = list_entry(ptr, struct _urb_handle, list_entry);
		if(element->readbuf)
			kfree(element->readbuf);
		list_del(ptr);
		kfree(element);
	}

	list_for_each_safe(ptr, nxt, &ue->ctl.done_urbs) {
		element = list_entry(ptr, struct _urb_handle, list_entry);
		if(element->readbuf)
			kfree(element->readbuf);
		list_del(ptr);
		kfree(element);
	}

	mutex_unlock (&ueservice_mutex);

}

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
static ssize_t ueservice_procfs_panic_store(struct file *file, const char *buffer, unsigned long count, void *data)
{
	char *command; 

	if (count < strlen(MAGIC_PANIC_STRING)) 
		return count;

	command = kmalloc(count, GFP_KERNEL);
	if (command == NULL)
		return -EFAULT;

	if (copy_from_user(command, buffer, count)) {
		kfree(command);	
		return -EFAULT;
	}

	if (strncmp(command, MAGIC_PANIC_STRING, strlen(MAGIC_PANIC_STRING)) == 0) {
		panic(command);
	}

	if (command)
		kfree(command);	

	return count;
}
#endif

static void __exit ueservice_exit(void)
{
	remove_proc_entry(PANIC_PROC_NAME, NULL);

	usb_deregister(&ueservice_driver);

	if(ue_class)
		class_destroy(ue_class);
	if(ue2_class)
		class_destroy(ue2_class);
	if(major)
		unregister_chrdev_region(MKDEV(major, 0), UESERVICE_MINORS);

}

static int __init ueservice_init(void)
{
	int retval;
	dev_t devnum;
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
	struct proc_dir_entry *pe;
#endif	
	
	retval = alloc_chrdev_region(&devnum, 0, UESERVICE_MINORS,"ueservice");
	if (retval < 0)
		return retval;
	major = MAJOR(devnum);

	ue_class = class_create(THIS_MODULE, "ueservice");
	if (IS_ERR(ue_class)){
		retval = PTR_ERR(ue_class);
		goto error;
	}
	ue2_class = class_create(THIS_MODULE, "uedebug");
	if (IS_ERR(ue2_class)){
		retval = PTR_ERR(ue2_class);
		goto error;
	}

 	if ((retval = usb_register(&ueservice_driver))) {
		printk(KERN_ERR "usb_register failed. Error number %d", retval);
		goto error;
	}

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
	pe = create_proc_entry(PANIC_PROC_NAME, S_IRUGO | S_IWUGO, NULL);

	if (pe)	{
		pe->read_proc = NULL;
		pe->write_proc = ueservice_procfs_panic_store;
	} else {
		printk(KERN_ERR "ERROR in creating proc entry (%s)!\n", PANIC_PROC_NAME);
		goto usb_error;
	}
#endif

	if(!retval)
		return 0;

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
usb_error:
	usb_deregister(&ueservice_driver);
#endif

error:
	if(ue_class)
		class_destroy(ue_class);
	if(ue2_class)
		class_destroy(ue2_class);
	if(major)
		unregister_chrdev_region(MKDEV(major, 0), UESERVICE_MINORS);
	return retval;

}

module_init(ueservice_init);
module_exit(ueservice_exit);
