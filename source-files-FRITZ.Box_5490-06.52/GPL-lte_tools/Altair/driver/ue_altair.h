/*
 * UE Altair Semiconductor FourGee-3100
 *
 * 2011 Bruno Caballero / AVM <b.caballero@avm.de>
 *
 * This driver is based on the ue_cdc driver by Altair and the cdc_ether
 * driver from the Linux kernel code. The hardware, although identifying
 * as CDC ECM class, requires a TTY interface for control.
 * The code is also "inspired" by: usbnet, usb-serial, cdc-acm, and more
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the linux kernel you work with; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __UE_ALTAIR_H__
#define __UE_ALTAIR_H__

#include <linux/tty.h>
#include "../../glb_pdn_count.h"

/*****************************************************************************/
/* Driver Options                                                            */
/*****************************************************************************/

/*
 * --  VLAN  --
 * This option activates ETHERNET VLAN header.
 * This is an alternative to 2 Interfaces
 * Default: Not active
 */
//#define UE_ALTAIR_VLAN

/*
 * --  ZEROCOPY  --
 * Use altair header version 2 (20 Bytes long)
 * skb_clone is used instead of skb_alloc for each packet.
 * Only compatible with Firmware older than R4.0.2.0.24.
 */
#define UE_ALTAIR_ZEROCOPY

/*****************************************************************************/
/* Definitions                                                               */
/*****************************************************************************/

#define DRIVER_VERSION		"0.1"
#define MOD_AUTHOR			"Bruno Caballero"
#define MOD_DESCRIPTION		"Altair FourGee-3100 LTE Device"
#define MOD_LICENSE			"GPL"

#define UE_NET_INTERFACES	GLOBAL_LTE_PDN_COUNT
#define INTERFACE_NAME		"lte%d"
#define TTY_NAME			"ttyLTE"
#define DRIVER_NAME			"ue_altair"
#define UE_DEVS 			4

#define TX_NUM_URBS		6
#define RX_NUM_URBS		8
#define RX_URB_BUFSIZE	10640
#define TX_URB_BUFSIZE	10640

#define UE_CONTROL_MSG_SIZE 	1024
#define MAX_SKB_URB 			7

#define UE_INTERRRUPT_INTERVAL	7


#define USB_INTF_NET_DATA	1

#define TX_TIMEOUT_JIFFIES	(5*HZ)
#define THROTTLE_JIFFIES	(HZ/8)

#define TXURB_OK 0
#define TXURB_ERR -1
#define TXURB_NOMEM -2

#define STATS_MAX_SKB_URBS 5


#define UE_EVENT_TX_HALT	0
#define UE_EVENT_RX_HALT	1
#define UE_EVENT_RX_MEMORY	2
#define UE_EVENT_STS_SPLIT	3
#define UE_EVENT_LINK_RESET	4

#define UE_CMD_CONCAT_FORMAT_CHANGE (0xAABBCCDD)
#define UE_CONTROL_TIMEOUT_MS (5000)

#define ALTAIR_NOTIFY_QUEUE 0xAA
#define ALTAIR_NOTIFY_QUEUE_TH 0x01
#define ALTAIR_NOTIFY_QUEUE_TL 0x02

/*****************************************************************************/
/* Debugging functions                                                       */
/*****************************************************************************/

#define __DEBUG__

#ifdef __DEBUG__

#define ue_err(fmt, arg...) \
        printk(KERN_ERR "ue_altair: " fmt "\n", ## arg)
#define ue_warn(fmt, arg...) \
        printk(KERN_WARNING "ue_altair: " fmt "\n", ## arg)
#define ue_info(fmt, arg...) \
        printk(KERN_INFO "ue_altair: " fmt "\n", ## arg)

#else

#define ue_err(fmt, arg...) do {} while (0)
#define ue_warn(fmt, arg...) do {} while (0)
#define ue_info(fmt, arg...) do {} while (0)

#endif


#define MAX(a,b) ((a<b) ? (b):(a))

#define BACKPRESSURE_COND (uenet->skb_list.qlen > (ue->tx_urbs * MAX_SKB_URB))


/*****************************************************************************/
/* Structs                                                                   */
/*****************************************************************************/

struct txurb {
	struct list_head list;
	struct urb *urb;
	struct ue_device *ue;
	struct ue_net *uenet;
	struct list_head skbs;
	int nskbs;
	dma_addr_t buf_dma;
	u8 *buf;
	int len;
};

#ifdef UE_ALTAIR_ZEROCOPY

struct rxurb {
	struct list_head list;
	struct urb *urb;
	struct ue_device *ue;
	dma_addr_t buf_dma;
	struct sk_buff *skb;
	int len;
};

/* New 20B altair LTE header (padding: 4 Bytes) */
struct ue_hdr{
	__be32 signature;
	__be16 len;
	__be16 offset;
	u8 vlan_id;
	u8 spacer[11];
};

#else //UE_ALTAIR_ZEROCOPY

struct rxurb {
	struct list_head list;
	struct urb *urb;
	struct ue_device *ue;
	dma_addr_t buf_dma;
	u8 *buf;
	int len;
};

struct ue_hdr{
	u8 reserved;
	u8 vlan_id;
	__be16 len;
};

#endif //UE_ALTAIR_ZEROCOPY

struct my_stats {
	int backpressure;
	int backpressure_status;
	int device_errors;
	int rx_num_skbs[STATS_MAX_SKB_URBS];
	int tx_num_skbs[STATS_MAX_SKB_URBS];
	int tx_onthefly;
	int qf_status;
	int qf_count;
};

struct ue_net {
	struct ue_device *parent;
	struct net_device *net;
	struct net_device_stats stats;
	struct my_stats my_stats;
	struct sk_buff_head skb_list;
	u8 *rx_ethhdr;
	u8 *rx_vlan_ethhdr;
	int queued_data_len;
	int vlan_id;	// lte0 id = 1, lte1 id = 2
	spinlock_t lock;

	/* TX */
	struct txurb* txurbs_pool;
	struct list_head tx_urbs_ready;		//txurbs for use in future transactions
	struct list_head tx_urbs_compl;		//txurbs returned via complete()
	spinlock_t tx_urbs_ready_lock;
	spinlock_t tx_urbs_compl_lock;

};

struct ue_serial {
	struct ue_device *parent;
	u8 *ttybuf;
	int minor;
	struct tty_port port; 			/* our tty port data */
	unsigned int ctrlin;			/* input control lines (DCD, DSR, RI, break, overruns) */
	unsigned int ctrlout;			/* output control lines (DTR, RTS) */
	struct work_struct readwork;

};

struct ue_device {

	struct ue_serial *ueserial;
	struct ue_net *uenet[UE_NET_INTERFACES];

	struct usb_device *usb;
	struct usb_interface *comm_interface;
	struct usb_interface *data_interface;

	struct usb_endpoint_descriptor *intr_endp;
	struct usb_endpoint_descriptor *in_endp;
	struct usb_endpoint_descriptor *out_endp;

	int carrier_on;

	/* Status Interrupt */
	struct urb *intr_urb;
	int intr_urb_active;
	unsigned char *intr_buf;

	/* URBS */
	int tx_urbs;
	int rx_urbs;
	int tx_size;
	int rx_size;

	/* TX */
	struct tasklet_struct do_tx;
	struct usb_anchor tx_urb_anchor;

	/* RX */
	struct tasklet_struct do_rx;
	int rx_allow_resched;
	spinlock_t rx_allow_resched_lock;
	struct rxurb* rxurbs_pool;
	struct list_head rx_urbs_ready;		//txurbs for use in future transactions
	struct list_head rx_urbs_compl;		//txurbs returned via rx_complete()
	spinlock_t rx_urbs_ready_lock;
	spinlock_t rx_urbs_compl_lock;
	struct usb_anchor rx_urb_anchor;

	struct device *dev;
	struct kref ref;
	struct mutex mutex;
	int disconnect_counter;

	struct timer_list delay_tx;

	struct work_struct kevent;
	unsigned long flags;
};

extern struct ue_device *uedevs_table[UE_DEVS];

//defined in ue_altair_tty.c, called from ue_altair.c
int uetty_init(void);
void uetty_exit(void);
void uetty_bind(struct ue_serial *ue);
void uetty_unbind(struct ue_serial *ue);

//defined in ue_altair_tty.c, called from ue_altair.c
int uenet_init_module(struct ue_device *ue);
void uenet_cleanup(struct ue_device *ue);
void ue_do_tx(unsigned long param);
void ue_do_rx(unsigned long param);
void kevent (struct work_struct *work);



#endif //__UE_ALTAIR_H__
