/*
 * Virtual Filesystem Root
 *
 * Copyright (c) 2009
 *  AVM GmbH, Berlin, Germany
 *
 * License: Free, use with no restriction.
 */
#ifndef AR7_AVMACL2_VIRTUAL_H_
#define AR7_AVMACL2_VIRTUAL_H_

#ifdef __cplusplus
extern "C" {
#endif

// set the real path of virtual "/"
void virtual_set_root(const char *real_root);

// change the real path to a virtual client path
void virtual_make_from_real_path(char **ppath);

FILE *virtual_fopen(const char *fn, char *mode);
int virtual_mkdir(const char *dir, mode_t mode);
int virtual_rename(const char *from, const char *to);
int virtual_stat(const char *path, struct stat *buf);
int virtual_rmdir(const char *pathname);
int virtual_unlink(const char *pathname);
int virtual_chdir(const char *path);
DIR *virtual_opendir(const char *name);
int virtual_chmod(const char *path, mode_t mode);

int virtual_is_write_access(const char *path, /*OUT*/char **preal_path);
int virtual_is_read_access(const char *path, /*OUT*/char **preal_path);

#ifdef __cplusplus
}
#endif

#endif /* !AR7_AVMACL2_VIRTUAL_H_ */
