/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _avm_event_h_
#define _avm_event_h_

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _avm_linux_event_h_
#define MAX_EVENT_CLIENT_NAME_LEN   32
#define MAX_EVENT_SOURCE_NAME_LEN   32
#endif /*--- #ifndef _avm_linux_event_h_ ---*/

#include <linux/avm_event.h>

/*------------------------------------------------------------------------------------------*\
 * Returnwerte
\*------------------------------------------------------------------------------------------*/
#define AVM_EVENT_UNKNOWN_HANDLE        -2
#define AVM_EVENT_RESOURCE_ERROR        -1
#define AVM_EVENT_OK                     0

/*------------------------------------------------------------------------------------------*\
 * Vor allen anderen Aktionen muss man sich registrieren. Der Name kann frei gew�hlt werden.
 * Die L�nge des Namens ist auf MAX_EVENT_CLIENT_NAME_LEN Bytes beschraenkt. Die Maske 
 * beinhaltet die Bits der IDs von denen man Informationen erhalten will.
 *
 * mask |= ((unsigned long long)1 << Id);
 *
 * Wenn man auch Informationen liefern will sollte das Bit 
 *              ((unsigned long long)1 << avm_event_id_user_source_notify);
 * gesetzt sein. Andernfalls erh�lt man keine Notifizierung wenn sich ein Client angemeldet
 * hat der die von uns gelieferte Information erwartet.
 * (siehe avm_event_source_register(), avm_event_source_trigger(), avm_event_source_release()
 *
 * Aber bitte in Zukunft avm_event_register20() verwenden
\*------------------------------------------------------------------------------------------*/
int avm_event_register(char* name, unsigned long long mask) __attribute__((deprecated));

/*--------------------------------------------------------------------------------*\
 *
 *  Event 2.0 Schnittstelle (mask_id > 64 moeglich)
 *  num_ids: Anzahl der folgenden ids
\*--------------------------------------------------------------------------------*/
int avm_event_register20(char* name, int num_ids, ...);

/*------------------------------------------------------------------------------------------*\
 * Liefert den Filediscriptor des Handles zur�ck. Dieser wird f�r select(), poll() und fnctl()
 * ben�tigt.
\*------------------------------------------------------------------------------------------*/
int avm_event_get_fd(int handle);

/*------------------------------------------------------------------------------------------*\
 * dupliziert einen Handle (wird bei fork() ben�tigt)
\*------------------------------------------------------------------------------------------*/
int avm_event_dup(int handle);

/*------------------------------------------------------------------------------------------*\
 * schlie�t einen mittels 'avm_event_dup' erzeugen handle
\*------------------------------------------------------------------------------------------*/
int avm_event_undup(int handle);

/*------------------------------------------------------------------------------------------*\
 * Abmelden beim Treiber, der Handle ist anschie�end ung�ltig.
\*------------------------------------------------------------------------------------------*/
int avm_event_release(int handle);

/*------------------------------------------------------------------------------------------*\
 * triggern einer Event source
 * ACHTUNG: dadurch dass das 'avm_event_register' impleziet ein trigger ausloesst
 *          sind die angeforderten Daten schon vorhanden, es muss also kein weiteres
 *          trigger ausgefuehrt werden.
\*------------------------------------------------------------------------------------------*/
int avm_event_trigger(int handle, unsigned int id);

/*------------------------------------------------------------------------------------------*\
 * Der Aufruf dieser Funktion sogt daf�r dass der read() nicht mehr blockiert
\*------------------------------------------------------------------------------------------*/
int avm_event_set_non_blocking(int handle);

/*------------------------------------------------------------------------------------------*\
 * Der Aufruf dieser Funktion sogt daf�r dass der read() blockiert (default)
\*------------------------------------------------------------------------------------------*/
int avm_event_set_blocking(int handle);

/*------------------------------------------------------------------------------------------*\
 * Lesen der Eventdaten: 
 *      handle      (siehe avm_event_register)
 *      data        Buffer f�r Daten
 *      length      input: maximal datenlaenge, output: gelesene Bytes
\*------------------------------------------------------------------------------------------*/
int avm_event_read(int handle, void *data, unsigned int *length);

/*------------------------------------------------------------------------------------------*\
 * Anmeldung zum "Liefern" von Informationen. Der Name kann frei gew�hlt werden.
 * Die L�nge des Namens ist auf MAX_EVENT_CLIENT_NAME_LEN Bytes beschraenkt. Die Maske 
 * beinhaltet die Bits der IDs zu denen man Informationen liefern will.
 *
 * mask |= ((unsigned long long)1 << Id);
 *
 * ACHTUNG: ((unsigned long long)1 << avm_event_id_user_source_notify);
 *      darf nicht gesetzt sein.
 *
 * Aber bitte in Zukunft avm_event_source_register20() verwenden
\*------------------------------------------------------------------------------------------*/
int avm_event_source_register(int handle, char *name, unsigned long long mask) __attribute__((deprecated));

/*--------------------------------------------------------------------------------*\
 *  Event 2.0 Schnittstelle (mask_id > 64 moeglich)
 *  num_ids: Anzahl der folgenden ids
 *
\*--------------------------------------------------------------------------------*/
int avm_event_source_register20(int handle, char *name,  int num_ids, ...);

/*------------------------------------------------------------------------------------------*\
 * Liefern von Informationen. Die Informationesstruktur besteht aus zwei Teilen die direkt
 * aufeinander folgen. Der Erste Teil ist die Struktur _avm_event_header. Der Zweite die
 * zu der ID geh�renden Daten. Die data_length ist die Gesamtl�nge beider Teile.
\*------------------------------------------------------------------------------------------*/
int avm_event_source_trigger(int handle, unsigned int data_length, struct _avm_event_header *data);

/*------------------------------------------------------------------------------------------*\
 * Als Informationslieferand Abmelden !
\*------------------------------------------------------------------------------------------*/
int avm_event_source_release(int handle);


#endif /*--- #ifndef _avm_event_h_ ---*/
