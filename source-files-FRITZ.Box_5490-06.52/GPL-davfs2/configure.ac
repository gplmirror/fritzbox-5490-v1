#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# Copyright (C) 2006, 2007, 2008, 2009 Werner Baumann
#
# This file is part of davfs2.
#
# davfs2 is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# davfs2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with davfs2; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */


AC_PREREQ(2.59)
AC_INIT(davfs2, 1.3.3, dav-linuxfs@lists.sf.net)
AC_CONFIG_SRCDIR([src/dav_coda2.c])
AC_CONFIG_AUX_DIR([config])
AM_INIT_AUTOMAKE

AC_CONFIG_HEADER([config.h])

# Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S

# Checks for libraries.
#AVM AM_GNU_GETTEXT_VERSION(0.14.4)
#AVM AM_GNU_GETTEXT([external])
AC_CHECK_LIB(pthread, pthread_create) #AVM
NE_REQUIRE_VERSIONS([0], [25 26 27 28 29])
DAV_CHECK_NEON

# Checks for header files.
AC_HEADER_DIRENT
AC_HEADER_STDC
AC_CHECK_HEADERS([fstab.h argz.h fcntl.h libintl.h langinfo.h limits.h locale.h mntent.h stddef.h stdint.h stdlib.h string.h sys/mount.h sys/time.h syslog.h termios.h unistd.h utime.h]) #AVM

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_UID_T
AC_C_INLINE
AC_TYPE_INT16_T
AC_TYPE_INT32_T
AC_TYPE_INT8_T
AC_TYPE_MODE_T
AC_TYPE_OFF_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T
AC_CHECK_MEMBERS([struct stat.st_blksize])
AC_HEADER_TIME
AC_STRUCT_TM
AC_TYPE_UINT32_T
AC_TYPE_UINT64_T
AC_C_VOLATILE
AC_CHECK_SIZEOF([void *])

# Checks for library functions.
AC_FUNC_CHOWN
AC_FUNC_CLOSEDIR_VOID
AC_FUNC_ERROR_AT_LINE
AC_FUNC_FORK
AC_FUNC_GETMNTENT
AC_PROG_GCC_TRADITIONAL
AC_HEADER_MAJOR
#AVM AC_FUNC_MALLOC
AC_FUNC_SELECT_ARGTYPES
AC_FUNC_STRFTIME
AC_FUNC_STAT
AC_FUNC_UTIME_NULL
AC_CHECK_FUNCS([endpwent getmntent memset mkdir nl_langinfo rpmatch select setlocale strcasecmp strchr strdup strerror strpbrk strrchr strstr strtol utime canonicalize_file_name rpmatch]) #AVM

# Misc.
DAV_DEFAULTS
DAV_LFS
DAV_LINGUAS([de])
AC_DEFINE([_GNU_SOURCE], 1, [Define to enable GNU extensions])

AC_ARG_ENABLE(utf8,
[  --enable-utf8  Compile with CONFIG_UTF8_FULL],
[case "${enableval}" in
yes) isutf8=true ;;
no)  isutf8=false ;;
*) AC_MSG_ERROR(bad value ${enableval} for --enable-utf8) ;;
esac],[isutf8=false])
AM_CONDITIONAL(WITH_UTF8, test x$isutf8 = xtrue)

AC_ARG_ENABLE(armpuma6,
[  --enable-armpuma6  Compile with PLATFORM armPuma6],
[case "${enableval}" in
yes) isarmpuma6=true ;;
no)  isarmpuma6=false ;;
*) AC_MSG_ERROR(bad value ${enableval} for --enable-armpuma6) ;;
esac],[isarmpuma6=false])
AM_CONDITIONAL(IS_PUMA6_ARM, test x$isarmpuma6 = xtrue)

AC_CONFIG_FILES([Makefile
                 etc/Makefile
                 src/Makefile])
#AVM po/Makefile.in
#AVM etc/Makefile
#AVM man/Makefile
#AVM man/de/Makefile
#AVM man/es/Makefile
AC_OUTPUT
DAV_MESSAGE
