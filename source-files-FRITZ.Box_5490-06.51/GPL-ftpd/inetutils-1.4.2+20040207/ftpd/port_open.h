/*
 * Open a Port from Internet to a local process on FRITZ!Box
 *
 */

int OpenIPv4TCPPort(unsigned short port);
int CloseIPv4TCPPort(unsigned short port);

#ifdef USE_IPV6
int OpenIPv6TCPPort(unsigned short port);
int CloseIPv6TCPPort(unsigned short port);
#endif

