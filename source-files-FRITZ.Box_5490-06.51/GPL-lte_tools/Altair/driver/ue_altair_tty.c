/*
 * UE Altair Semiconductor FourGee-3100
 *
 * 2011 Bruno Caballero / AVM <b.caballero@avm.de>
 *
 * This driver is based on the ue_cdc driver by Altair and the cdc_ether
 * driver from the Linux kernel code. The hardware, although identifying
 * as CDC ECM class, requires a TTY interface for control.
 * The code is also "inspired" by: usb-serial, usb-skeleton, cdc-acm, and more
 */
 
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the linux kernel you work with; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/ethtool.h>
#include <linux/workqueue.h>
#include <linux/mii.h>
#include <linux/usb.h>
#include <linux/usb/cdc.h>
#include <linux/usb/usbnet.h>
#include <linux/if_arp.h>
#include <linux/ip.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/version.h>
#include <linux/ctype.h>

#include "ue_altair.h"

#define UE_CONTROL_MSG_SIZE 1024

/* Output control lines. */
#define UECTL_CTRL_DTR		0x01
#define UECTL_CTRL_RTS		0x02

/* Input control lines and line errors. */
#define UECTL_CTRL_DCD		0x01
#define UECTL_CTRL_DSR		0x02
#define UECTL_CTRL_BRK		0x04
#define UECTL_CTRL_RI		0x08
#define UECTL_CTRL_FRAMING	0x10
#define UECTL_CTRL_PARITY	0x20
#define UECTL_CTRL_OVERRUN	0x40

static DEFINE_MUTEX(open_mutex);

static const struct tty_port_operations uectl_port_ops = {
};
#define UECTL_READY(uectl)	(uectl && uectl->parent->comm_interface && uectl->port.count)

static struct tty_driver *uectl_driver;

/* TTY operations */
static int uectl_open(struct tty_struct *tty, struct file *filp);
static void uectl_close(struct tty_struct *tty, struct file *filp);
static int uectl_write(struct tty_struct *tty, const unsigned char *buf, int count);
static int uectl_write_room(struct tty_struct *tty);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,39)
static int uectl_tiocmget(struct tty_struct *tty);
static int uectl_tiocmset(struct tty_struct *tty, unsigned int set, unsigned int clear);
#else
static int uectl_tiocmget(struct tty_struct *tty, struct file *file);
static int uectl_tiocmset(struct tty_struct *tty, struct file *file,
						  unsigned int set, unsigned int clear);
#endif

/* tty read path */
static int uectl_syncread(struct ue_serial *ue);
static void uectl_readerwork(struct work_struct *work);

//tty ops virtual table.
static const struct tty_operations uectl_ops = {
	.open =			uectl_open,
	.close =		uectl_close,
	.write =		uectl_write,
	.write_room =	uectl_write_room,
	.tiocmget =		uectl_tiocmget,
	.tiocmset =		uectl_tiocmset,
};

/* destroy an instance data. this may be called only after all open() calls were matched by close()
   and disconnect() was also called.
*/
static void uectl_unregister(struct ue_serial *ue)
{
	tty_unregister_device(uectl_driver, ue->minor);
	uedevs_table[ue->minor] = NULL;

}

static int uectl_open(struct tty_struct *tty, struct file *filp)
{
	struct ue_serial *ues;
	int rv = -ENODEV, rv2 = -ENODEV;

	mutex_lock(&open_mutex);

	ues = uedevs_table[tty->index]->ueserial;
	if (!ues){
		printk(KERN_ERR "%s: Initialization error, not ue found\n", __FUNCTION__);
		goto out;
	}else if(!ues->parent->comm_interface){
		printk(KERN_ERR "%s: ues->parent->comm_interface is NULL\n", __FUNCTION__);
		goto out;
	}else if(ues->port.count) {
		printk(KERN_ERR "%s: Multiple use is forbidden\n", __FUNCTION__);
		goto out;
	} 

	tty_port_tty_set(&ues->port, tty);
	ues->port.count++;
	rv = tty_port_block_til_ready(&ues->port, tty, filp);
	if (rv < 0) {
		printk(KERN_ERR "%s: tty_port_block_til_ready failed %d\n", __FUNCTION__, rv);
		goto out;
	}
	set_bit(TTY_NO_WRITE_SPLIT, &tty->flags);
	tty->driver_data = ues;
	
	/* start any status interrupt transfer */
	if(ues->parent->intr_urb_active == 0) {
		rv2 = usb_submit_urb (ues->parent->intr_urb, GFP_KERNEL);
		ues->parent->intr_urb_active = 1;
		if (rv2 < 0) {
			printk(KERN_ERR "%s: error %d\n",  __FUNCTION__, rv2);
		}
	}
out:
	mutex_unlock(&open_mutex);


	return rv;
}

static void uectl_close(struct tty_struct *tty, struct file *filp)
{
	struct ue_serial *uectl;

	if(!tty){
		printk(KERN_INFO "%s: tty is NULL\n", __FUNCTION__);
		return;// -ENODEV;
	}
	uectl = tty->driver_data;

	/* Perform the closing process and see if we need to do the hardware
	   shutdown */
	if (!uectl){
		printk(KERN_ERR "%s: uectl is NULL\n", __FUNCTION__);
		return;
	}

	mutex_lock(&open_mutex);
	if(uectl->port.count == 0){
		mutex_unlock(&open_mutex);
		printk(KERN_ERR "%s: port.count\n", __FUNCTION__);
		return;
	}
	if(tty_port_close_start(&uectl->port, tty, filp) == 0){
		tty_port_tty_set(&uectl->port, NULL);
		uectl_unregister(uectl);
		mutex_unlock(&open_mutex);
		return;
	}
	tty_port_hangup(&uectl->port);
	tty->driver_data = NULL;
	
	tty_port_close_end(&uectl->port, tty);
	tty_port_tty_set(&uectl->port, NULL);
	//we were ejected but our tty was still open. time to close down...
	mutex_unlock(&open_mutex);
}

static int uectl_write(struct tty_struct *tty,
					 const unsigned char *buf, int count)
{
	struct ue_serial *ue = tty->driver_data;
	int retval = 0, ifnum;
	struct usb_device *udev;

	if(!ue || !ue->parent || !ue->parent->comm_interface)
		return -ENODEV;

	udev = interface_to_usbdev (ue->parent->comm_interface);
	ifnum = ue->parent->comm_interface->cur_altsetting->desc.bInterfaceNumber;

    // send down the encapuslated command.
	retval = usb_control_msg(
		udev, usb_sndctrlpipe(udev, 0),
		USB_CDC_SEND_ENCAPSULATED_COMMAND,
		USB_TYPE_CLASS | USB_RECIP_INTERFACE,
		0, ifnum, (u8*)buf, count,
		UE_CONTROL_TIMEOUT_MS);

	if(retval < 0)
		return retval;

	return count;
}

static int uectl_write_room(struct tty_struct *tty)
{
	return UE_CONTROL_MSG_SIZE;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,39)
static int uectl_tiocmget(struct tty_struct *tty)
#else
static int uectl_tiocmget(struct tty_struct *tty, struct file *file)
#endif
{
	struct ue_serial *uectl = tty->driver_data;
	int ret = -EINVAL;

	mutex_lock(&open_mutex);

	if (!UECTL_READY(uectl))
		goto out;

	ret =  (uectl->ctrlout & UECTL_CTRL_DTR ? TIOCM_DTR : 0) |
	       (uectl->ctrlout & UECTL_CTRL_RTS ? TIOCM_RTS : 0) |
	       (uectl->ctrlin  & UECTL_CTRL_DSR ? TIOCM_DSR : 0) |
	       (uectl->ctrlin  & UECTL_CTRL_RI  ? TIOCM_RI  : 0) |
	       (uectl->ctrlin  & UECTL_CTRL_DCD ? TIOCM_CD  : 0) |
	       TIOCM_CTS;
out:
	mutex_unlock(&open_mutex);
	return ret;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,39)
static int uectl_tiocmset(struct tty_struct *tty, unsigned int set, unsigned int clear)
#else
static int uectl_tiocmset(struct tty_struct *tty, struct file *file,
						  unsigned int set, unsigned int clear)
#endif
{
	int ret = -EINVAL;
	struct ue_serial *uectl = tty->driver_data;
	unsigned int newctrl;

	mutex_lock(&open_mutex);

	if (!UECTL_READY(uectl))
		goto out;

	newctrl = uectl->ctrlout;
	set = (set & TIOCM_DTR ? UECTL_CTRL_DTR : 0) |
		(set & TIOCM_RTS ? UECTL_CTRL_RTS : 0);
	clear = (clear & TIOCM_DTR ? UECTL_CTRL_DTR : 0) |
		(clear & TIOCM_RTS ? UECTL_CTRL_RTS : 0);
	newctrl = (newctrl & ~clear) | set;
	uectl->ctrlout = newctrl;
	ret = 0;
out:
	mutex_unlock(&open_mutex);
	return ret;
}

/*
  reader support for the control path.
  we run it in a work queue as we don't need any speed or throughput here.
  also, the use of this port is very rare and short.
  we expect only specialized software (modem managers) to use this interface and these
  apps are writing and reading responses based on a known sequence and protocol.
 */

static int uectl_syncread(struct ue_serial *ue)
{
	struct tty_struct *tty;
	int ifnum, size;

	struct usb_device *udev = interface_to_usbdev (ue->parent->comm_interface);

	ifnum = ue->parent->comm_interface->cur_altsetting->desc.bInterfaceNumber;

	size = usb_control_msg( udev, usb_rcvctrlpipe(udev, 0),
		USB_CDC_GET_ENCAPSULATED_RESPONSE,
		USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
		0, ifnum, ue->ttybuf, UE_CONTROL_MSG_SIZE, UE_CONTROL_TIMEOUT_MS);

	if(size > 0){
		tty = tty_port_tty_get(&ue->port);
		if(tty){
			// push to the tty layer the data
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,9,0)
			tty_insert_flip_string(tty->port, ue->ttybuf, size);
            tty_flip_buffer_push(tty->port);
#else
			tty_insert_flip_string(tty, ue->ttybuf, size);
    		tty_flip_buffer_push(tty);
#endif
			tty_kref_put(tty);
		}
	}
	memset(ue->ttybuf, 0, UE_CONTROL_MSG_SIZE);
	return size;
}

static void uectl_readerwork(struct work_struct *work)
{
	struct ue_serial *u = container_of(work, struct ue_serial, readwork);
	uectl_syncread(u);
}


void uetty_unbind(struct ue_serial *ue)
{
	struct tty_struct *tty;
	mutex_lock(&open_mutex);
	if (ue->port.count == 0) {
		uectl_unregister(ue);
	} else {
		tty = tty_port_tty_get(&ue->port);
		if (tty) {
			tty_kref_put(tty);
			tty_hangup(tty);
		}
	}
	mutex_unlock(&open_mutex);
}


void uetty_bind(struct ue_serial *ues)
{
	int minor;

    // init the reader worker queue.
	INIT_WORK(&ues->readwork, uectl_readerwork);

	mutex_lock(&open_mutex);
	//init the TTY device.
	tty_port_init(&ues->port);
	mutex_unlock(&open_mutex);

	ues->port.ops = &uectl_port_ops;

	// look for free context
	for(minor = 0; minor < UE_DEVS && uedevs_table[minor]; minor++);
	if(minor == UE_DEVS) {
		return;
	}
	uedevs_table[minor] = ues->parent;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
    tty_port_register_device(&ues->port, uectl_driver, minor, ues->parent->dev);
#else
    tty_register_device(uectl_driver, minor, ues->parent->dev);
#endif
}

int __init uetty_init(void)
{
	int ret = 0;

	uectl_driver = alloc_tty_driver(UE_DEVS);
	if (!uectl_driver)
		return -ENOMEM;

	uectl_driver->owner = THIS_MODULE;
	uectl_driver->driver_name = DRIVER_NAME;
	uectl_driver->name = TTY_NAME;
	uectl_driver->major = 0; /* auto assign */
	uectl_driver->minor_start = 0;
	uectl_driver->type = TTY_DRIVER_TYPE_SERIAL;
	uectl_driver->subtype = SERIAL_TYPE_NORMAL;
	uectl_driver->flags = /*TTY_DRIVER_REAL_RAW |*/ TTY_DRIVER_DYNAMIC_DEV;
	uectl_driver->init_termios = tty_std_termios;
	uectl_driver->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;

	tty_set_operations(uectl_driver, &uectl_ops);

	ret = tty_register_driver(uectl_driver);
	if (ret)
		put_tty_driver(uectl_driver);
	return ret;
}

void __exit uetty_exit(void)
{
	tty_unregister_driver(uectl_driver);
	put_tty_driver(uectl_driver);
}

