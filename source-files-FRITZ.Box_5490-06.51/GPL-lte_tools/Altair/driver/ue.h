#ifndef __UE_H__
#define __UE_H__

//#define __DEBUGGING__ 1

#ifdef __DEBUGGING__
#define PDEBUG(fmt, ...) printk(KERN_DEBUG "debug  %s:%d %s() : " fmt, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#define PDEBUG(fmt, ...) do{}while(0)
#endif

//#define UE_DEVS 32
//Don't need 32 Devices
#define UE_DEVS 1

#endif
