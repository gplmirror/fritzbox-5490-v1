(IPQ) # printenv
baudrate=115200
bootargs=earlyprintk=serial,ttyMSM0,115200,n8 console=ttyMSM0,115200,n8 root=/dev/ram debug
bootcmd=tftpbootdtb
bootdelay=2
dtb_addr=0x46000000
dtb_file=Fritz_Box_HW211.dtb
ethact=eth0
ipaddr=192.168.178.201
kernel_addr=0x44000000
kernel_file=uImage
machid=ff0
serverip=192.168.178.251
stderr=serial
stdin=serial
stdout=serial
tftpbootdtb=tftpboot ${kernel_addr} ${kernel_file} && tftpboot ${dtb_addr} ${dtb_file} && bootm ${kernel_addr} - ${dtb_addr}

Environment size: 603/262140 bytes
(IPQ) # set kernel_file uImage-avm
(IPQ) # run tftpbootdtb







ALT (für QCA-SDK):

bootargs=console=ttyHSL1,115200n8 debug ubi.mtd=rootfs root=mtd:ubi_rootfs msm_watchdog.enable=0

