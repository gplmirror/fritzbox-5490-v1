#
# 802.1d Ethernet Bridging
#

config BRIDGE
	tristate "802.1d Ethernet Bridging"
	select LLC
	select STP
	depends on IPV6 || IPV6=n
	---help---
	  If you say Y here, then your Linux box will be able to act as an
	  Ethernet bridge, which means that the different Ethernet segments it
	  is connected to will appear as one Ethernet to the participants.
	  Several such bridges can work together to create even larger
	  networks of Ethernets using the IEEE 802.1 spanning tree algorithm.
	  As this is a standard, Linux bridges will cooperate properly with
	  other third party bridge products.

	  In order to use the Ethernet bridge, you'll need the bridge
	  configuration tools; see <file:Documentation/networking/bridge.txt>
	  for location. Please read the Bridge mini-HOWTO for more
	  information.

	  If you enable iptables support along with the bridge support then you
	  turn your bridge into a bridging IP firewall.
	  iptables will then see the IP packets being bridged, so you need to
	  take this into account when setting up your firewall rules.
	  Enabling arptables support when bridging will let arptables see
	  bridged ARP traffic in the arptables FORWARD chain.

	  To compile this code as a module, choose M here: the module
	  will be called bridge.

	  If unsure, say N.

config BRIDGE_IGMP_SNOOPING
	bool "IGMP/MLD snooping"
	depends on BRIDGE
	depends on INET
	default y
	---help---
	  If you say Y here, then the Ethernet bridge will be able selectively
	  forward multicast traffic based on IGMP/MLD traffic received from
	  each port.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.

config AVM_BRIDGE_MULTICAST_TO_UNICAST
	bool "bridge multicast to unicast conversion"
	depends on BRIDGE_IGMP_SNOOPING
	default y
	---help---
	  AVM extension

	  If you say Y here, then the Ethernet bridge ports record the source
	  MAC addresses of IGMP report senders, and perform a multicast-to-unicast
	  conversion (Layer 2) before forwarding corresponding multicast packets. This
	  is useful in wireless networks where multicast traffic is severely limited
	  by 802.11.

	  Even with this being configured, the feature must be turned on on a per-port
	  basis at runtime.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.

config AVM_BRIDGE_MULTICAST_TO_UNICAST_DEFAULT_THRESHOLD
	int "multicast to unicast default threshold"
	depends on AVM_BRIDGE_MULTICAST_TO_UNICAST
	default 3
	---help---
	  AVM extension

	  This selects the default threshold for switching from multicast-as-unicast
	  transmission back to plain multicast. This is really only the default,
	  the threshold can be configured on a per port basis on sysfs. The threshold
	  is multicast group specific.


config BRIDGE_VLAN_FILTERING
	bool "VLAN filtering"
	depends on BRIDGE
	depends on VLAN_8021Q
	default n
	---help---
	  If you say Y here, then the Ethernet bridge will be able selectively
	  receive and forward traffic based on VLAN information in the packet
	  any VLAN information configured on the bridge port or bridge device.

	  Say N to exclude this support and reduce the binary size.

	  If unsure, say Y.

config AVM_BRIDGE_ISOLATION
	bool "Isolate bridge ports from each other"
	depends on BRIDGE
	depends on NETFILTER
	depends on SYSFS
	depends on AVM_ENHANCED
	default y
	---help---
	  AVM extension

	  This feature allows to isolate bridge ports from each other so that clients
	  connected through a port can only communicate with the local system, for DHCP
	  or accessing the Internet, if the local system is a gateway.

config AVM_BRIDGE_ISOLATION_UPSTREAM
	bool "Break port isolation for select ports"
	depends on AVM_BRIDGE_ISOLATION
	default n
	---help---
	  AVM extension

	  This feature allows clients connected through a bridge port to communicate
	  not only with the local system but also with clients connected through "upstream"
	  ports. This is only useful if the bridge port is otherwise isolated. Use this
	  if the local system is not the gateway, but rather the gateway is connected through
	  another bridge port.

	  Say n if the box has both WLAN bands in the bridges (lan, guest) on the host.
	  This option is useful for offload platforms where WLAN interfaces are on a
	  offload target which bridge traffic through a upstream interface to the host (7490)
