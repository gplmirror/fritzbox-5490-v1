/*
 * Bridge isolate bridge clients through netfilter
 *
 * vim:set noexpandtab shiftwidth=8 softtabstop=8:
 * 
 * Copyright (c) 2015 AVM GmbH <info@avm.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions, and the following disclaimer,
 *    without modification.
 * 2. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * Alternatively, this software may be distributed and/or modified under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,38)
#include <linux/printk.h>
#endif
#include <linux/netdevice.h>
#include <linux/netfilter.h>
#include <linux/netfilter_bridge.h>
#include <linux/types.h>
#include <linux/bitmap.h>
#include <asm/bitops.h>
#include <net/net_namespace.h>

#include "br_private.h"

/* 64 should be enough to completely avoid hash collisions in practice,
 * so lookup should be really fast */
#define HASH_SIZE 64
#define HASH_MASK (HASH_SIZE-1)

struct isol_entry {
	int ifindex;
	struct hlist_node list;
};

static struct hlist_head isolated[HASH_SIZE];
#ifdef CONFIG_AVM_BRIDGE_ISOLATION_UPSTREAM
static struct hlist_head upstreams[HASH_SIZE];
#endif

static struct isol_entry *find_if(int ifindex, struct hlist_head *map)
{
	struct isol_entry *pos;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
	hlist_for_each_entry(pos, &map[ifindex & HASH_MASK], list) {
#else
	struct hlist_node *_pos;
	hlist_for_each_entry(pos, _pos, &map[ifindex & HASH_MASK], list) {
#endif

		if (pos->ifindex == ifindex)
			return pos;
	}
	return NULL;
}

static bool contains_if(int ifindex, struct hlist_head *map)
{
	if (hlist_empty(&map[ifindex & HASH_MASK]))
		return false;

	return find_if(ifindex, map) != NULL;
}

static void add_if(int ifindex, struct hlist_head *map)
{
	struct isol_entry *e;

	if (contains_if(ifindex, map))
		return;

	e = kzalloc(sizeof(struct isol_entry), GFP_KERNEL);
	e->ifindex = ifindex;
	hlist_add_head(&e->list, &map[ifindex & HASH_MASK]);
}

static void del_if(int ifindex, struct hlist_head *map)
{
	struct isol_entry *pos = find_if(ifindex, map);

	if (pos) {
		hlist_del(&pos->list);
		kfree(pos);
	}
}

static unsigned int isolate_nf_hook(unsigned int hooknum, struct sk_buff *skb,
        const struct net_device *in, const struct net_device *out,
        int (*okfn)(struct sk_buff *))
{
#ifdef CONFIG_AVM_BRIDGE_ISOLATION_UPSTREAM
	/* Never drop packets to/from the upstream port if this is
	 * required for internet access */
	if (contains_if(in->ifindex, upstreams) || contains_if(out->ifindex, upstreams))
		return NF_ACCEPT;
#endif
	/* block traffic to/from isolated ports */
	if (contains_if(in->ifindex, isolated) || contains_if(out->ifindex, isolated))
		return NF_DROP;

	return NF_ACCEPT;
}


static struct nf_hook_ops isolate_filter __read_mostly = {
	.hook		= isolate_nf_hook,
	.owner		= THIS_MODULE,
	.pf		= NFPROTO_BRIDGE,
	.hooknum	= NF_BR_FORWARD,
	.priority	= NF_BR_PRI_FILTER_BRIDGED
};

ssize_t br_isol_show(struct net_bridge_port *p, char *buf)
{
	return sprintf(buf, "%d\n", contains_if(p->dev->ifindex, isolated));
}

/* count number of isolated so that the netfilter isn't executed if not needed */
static int count;
static int _br_isol_store(struct net_bridge_port *p, unsigned long v)
{
	int ret = 0;
	int ifindex = p->dev->ifindex;

	if (v && !contains_if(ifindex, isolated)) {
		add_if(ifindex, isolated);
		if (++count == 1)
			ret = nf_register_hook(&isolate_filter);
#ifdef CONFIG_AVM_PA
		/* flush out avm_pa, otherwise the hook will see no packets */
		avm_pa_flush_sessions_for_pid(AVM_PA_DEVINFO(p->dev)->pid_handle);
#endif
	}
	else if (!v && contains_if(ifindex, isolated)) {
		del_if(ifindex, isolated);
		if (--count == 0)
			nf_unregister_hook(&isolate_filter);
	}

	return ret;
}

int br_isol_store(struct net_bridge_port *p, unsigned long v)
{
	int ret;
	/* We're called under BH disabled, but we might sleep. We can release
	 * the lock since this code doesn't touch any bridge data. We're still under
	 * RTNL so the bridge port is protected */
	spin_unlock_bh(&p->br->lock);
	might_sleep();
	ret = _br_isol_store(p, v);
	spin_lock_bh(&p->br->lock);
	return ret;
}

#ifdef CONFIG_AVM_BRIDGE_ISOLATION_UPSTREAM
ssize_t br_isol_us_show(struct net_bridge_port *p, char *buf)
{
	return sprintf(buf, "%d\n", contains_if(p->dev->ifindex, upstreams));
}

static void _br_isol_us_store(struct net_bridge_port *p, unsigned long v)
{
	(v ? add_if : del_if)(p->dev->ifindex, upstreams);
}

static int br_isol_us_store(struct net_bridge_port *p, unsigned long v)
{
	/* We're called under BH disabled, but we might sleep. We can release
	 * the lock since this code doesn't touch any bridge data. We're still under
	 * RTNL so the bridge port is protected */
	spin_unlock_bh(&p->br->lock);
	might_sleep();
	_br_isol_us_store(p, v);
	spin_lock_bh(&p->br->lock);

	return 0;
}
#endif

void br_isol_remove_port(struct net_bridge_port *p)
{
	/* this ensures that no reference to the ifindex is left in the hash tables */
	_br_isol_store(p, 0);
#ifdef CONFIG_AVM_BRIDGE_ISOLATION_UPSTREAM
	_br_isol_us_store(p, 0);
#endif
}
