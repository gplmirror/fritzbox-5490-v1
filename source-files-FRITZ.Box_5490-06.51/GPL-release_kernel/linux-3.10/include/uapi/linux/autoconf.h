#ifndef __linux__autoconf_h_
#define __linux__autoconf_h_
#warning Inclusion of <linux/autoconf.h> is obsolete! Please include <linux/kconfig.h> instead!
#include <linux/kconfig.h>
#endif
