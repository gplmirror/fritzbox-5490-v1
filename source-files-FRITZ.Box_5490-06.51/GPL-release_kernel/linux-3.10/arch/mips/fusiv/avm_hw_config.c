#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <linux/avm_hw_config.h>

#if defined(CONFIG_FUSIV_VX180)
#include <asm/mach-fusiv/vx180.h>
#elif defined(CONFIG_FUSIV_VX185)
#include <asm/mach-fusiv/vx185.h>
#endif
#include <asm/hw_gpio.h>

#if defined(CONFIG_FUSIV_VX180)
#include "avm_hw_config_hw156.h"
#include "avm_hw_config_hw171.h"
#elif defined(CONFIG_FUSIV_VX185)
#include "avm_hw_config_hw191.h"
#endif

/*------------------------------------------------------------------------------------------*\
 * Für nicht mehr unterstützte Produkte
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config avm_hardware_config_dummy[] = {
    { .name = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_dummy);


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config_table avm_hw_config_tables[] = {
#if defined(CONFIG_FUSIV_VX180)
    { .hwrev=150, .table=avm_hardware_config_dummy }, /*--- FRITZ!Box Ikanos ---*/
    { .hwrev=152, .table=avm_hardware_config_dummy }, /*--- Speedport W 722V ---*/
    { .hwrev=156, .table=avm_hardware_config_hw156 }, /*--- FRITZ!Box Fon WLAN 7390 ---*/
    { .hwrev=165, .table=avm_hardware_config_dummy }, /*--- FRITZ!Box Fon WLAN 7541 vDSL ---*/
    { .hwrev=171, .table=avm_hardware_config_hw171 }, /*--- FRITZ!Box Fon WLAN 7340 ---*/
#elif defined(CONFIG_FUSIV_VX185)
    { .hwrev=191, .table=avm_hardware_config_hw191 }, /*--- FRITZ!Box Fon WLAN 7369 ---*/
#endif
};
EXPORT_SYMBOL(avm_hw_config_tables);


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_get_hw_config_table(void)
{
    if(avm_current_hw_config == NULL){
        pr_emerg("[%s] avm_current_hw_config not initialised!\n", __func__);
        panic("avm_current_hw_config not initialised");
    }

    return avm_current_hw_config;

#if 0
    static struct _avm_hw_config *current_config_table = NULL;
    unsigned int hwrev, hwsubrev, i;
    char *s;

    if(current_config_table) {
        return current_config_table;
    }

    s = prom_getenv("HWRevision");
    if (s) {
        hwrev = simple_strtoul(s, NULL, 10);
    } else {
        hwrev = 0;
    } 
    s = prom_getenv("HWSubRevision");
    if (s) {
        hwsubrev = simple_strtoul(s, NULL, 10);
    } else {
        hwsubrev = 1;
    }    
    if(hwrev == 0) {
        printk("[%s] error: no HWRevision detected in environment variables\n", __FUNCTION__);
        BUG_ON(1);
        return NULL;
    }
    for(i = 0; i < sizeof(avm_hw_config_tables)/sizeof(struct _avm_hw_config_table); i++) {
        if(avm_hw_config_tables[i].hwrev == hwrev) {
            if((avm_hw_config_tables[i].hwsubrev == 0) || (avm_hw_config_tables[i].hwsubrev == hwsubrev)) {
                current_config_table = avm_hw_config_tables[i].table;
                return current_config_table;
            }
        }
    }
    printk("[%s] error: No hardware configuration defined for HWRevision %d\n", __FUNCTION__, hwrev);
    BUG_ON(1);
    return NULL;
#endif
}
EXPORT_SYMBOL(avm_get_hw_config_table);

