/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _asm_mips_mach_ikan_mips_vx185_h_
#define _asm_mips_mach_ikan_mips_vx185_h_

#include <asm/addrspace.h>
#include <linux/types.h>
#include <linux/kernel.h>

// stolen from ifx_regs.h
#define IKS_REG_R32(_r)                   __raw_readl((u32 *)(_r))
#define IKS_REG_W32(_v, _r)               __raw_writel((_v), (u32 *)(_r))


/*------------------------------------------------------------------------------------------*\
 * System Configuration
\*------------------------------------------------------------------------------------------*/
#define VX185_SYSTEM_CONFIG_BASE 0x19000000

/*------------------------------------------------------------------------------------------*\
 * GPIO
\*------------------------------------------------------------------------------------------*/

#define VX185_GPIO_MAX                          31
#define VX185_GPIO_BASE                         KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x30000)
#define VX185_GPIO_INPUT                        (VX185_GPIO_BASE + 0x00)
#define VX185_GPIO_OUTPUT_SET                   (VX185_GPIO_BASE + 0x04)
#define VX185_GPIO_OUTPUT_CLEAR                 (VX185_GPIO_BASE + 0x08)
#define VX185_GPIO_MODE1                        (VX185_GPIO_BASE + 0x0C)
#define VX185_GPIO_MODE2                        (VX185_GPIO_BASE + 0x10)
#define VX185_GPIO_ALTFUNC_SEL                  (VX185_GPIO_BASE + 0x14)
#define     VX185_GPIO_ALTFUNC_SFLASH_CS        (1 << 0)
#define     VX185_GPIO_ALTFUNC_SFLASH_HOLD      (1 << 1)
#define     VX185_GPIO_ALTFUNC_SFLASH_WP        (1 << 2)
#define     VX185_GPIO_ALTFUNC_SPI_SLV_CS       (1 << 3)
#define     VX185_GPIO_ALTFUNC_SPI_CS0_OUT      (1 << 4)
#define     VX185_GPIO_ALTFUNC_SPI_CS1_OUT      (1 << 5)
#define     VX185_GPIO_ALTFUNC_SPI_CS2_OUT      (1 << 6)
#define     VX185_GPIO_ALTFUNC_SPI_CS3_OUT      (1 << 7)
#define     VX185_GPIO_ALTFUNC_SPI_CS5_OUT      (1 << 9)
#define     VX185_GPIO_ALTFUNC_SPI_CS4_OUT      (1 << 8)
#define     VX185_GPIO_ALTFUNC_SPI_CS6_OUT      (1 << 10)
#define     VX185_GPIO_ALTFUNC_UART2_TXD        (1 << 11)
#define     VX185_GPIO_ALTFUNC_UART2_RXD        (1 << 12)
#define     VX185_GPIO_ALTFUNC_UART2_RTS        (1 << 13)
#define     VX185_GPIO_ALTFUNC_UART2_CTS        (1 << 14)
#define     VX185_GPIO_ALTFUNC_DSP_ELOUT        (1 << 14)
#define     VX185_GPIO_ALTFUNC_SATA_MP_SWITCH   (1 << 15)
#define     VX185_GPIO_ALTFUNC_DSP_NEMS         (1 << 15)
#define     VX185_GPIO_ALTFUNC_SATA_CP_DETECT   (1 << 16)
#define     VX185_GPIO_ALTFUNC_DSP_NEBG         (1 << 16)
#define     VX185_GPIO_ALTFUNC_DSP_ECLK         (1 << 17)
#define     VX185_GPIO_ALTFUNC_PLL_TEST         (1 << 17)
#define     VX185_GPIO_ALTFUNC_DSP_NERESET      (1 << 18)
#define     VX185_GPIO_ALTFUNC_SATA_CP_POD      (1 << 18)
#define     VX185_GPIO_ALTFUNC_DSP_EE           (1 << 19)
#define     VX185_GPIO_ALTFUNC_DSP0_PF2         (1 << 19)
#define     VX185_GPIO_ALTFUNC_DSP_NEINT        (1 << 20)
#define     VX185_GPIO_ALTFUNC_DSP0_PF3         (1 << 20)
#define     VX185_GPIO_ALTFUNC_DSP_NEBR         (1 << 21)
#define     VX185_GPIO_ALTFUNC_DSP1_PF2         (1 << 21)
#define     VX185_GPIO_ALTFUNC_DSP_ELIN         (1 << 22)
#define     VX185_GPIO_ALTFUNC_DSP1_PF3         (1 << 22)
#define     VX185_GPIO_ALTFUNC_GIGE0_AP_DEBUG   (1 << 23)
#define     VX185_GPIO_ALTFUNC_INT_56           (1 << 24)
#define     VX185_GPIO_ALTFUNC_GIGE1_AP_DEBUG   (1 << 24)
#define     VX185_GPIO_ALTFUNC_INT_57           (1 << 25)
#define     VX185_GPIO_ALTFUNC_GIGE2_VAP_AP_DEBUG (1 << 25)
#define     VX185_GPIO_ALTFUNC_INT_58           (1 << 26)
#define     VX185_GPIO_ALTFUNC_CLASS1_AP_DEBUG  (1 << 26)
#define     VX185_GPIO_ALTFUNC_INT_59           (1 << 27)
#define     VX185_GPIO_ALTFUNC_CLASS2_AP_DEBUG  (1 << 27)
#define     VX185_GPIO_ALTFUNC_INT_60           (1 << 28)
#define     VX185_GPIO_ALTFUNC_BMU_AP_DEBUG     (1 << 28)
#define     VX185_GPIO_ALTFUNC_INT_61           (1 << 29)
#define     VX185_GPIO_ALTFUNC_SPA_DEBUG        (1 << 29)
#define     VX185_GPIO_ALTFUNC_INT_62           (1 << 30)
#define     VX185_GPIO_ALTFUNC_HAP_AP_DEBUG     (1 << 30)
#define     VX185_GPIO_ALTFUNC_INT_63           (1 << 31)
#define     VX185_GPIO_ALTFUNC_PCIE1_MSI_IRQ    (1 << 31)

#define VX185_GPIO_ALTFUNC_VAL  (VX185_GPIO_BASE + 0x18)
#define VX185_GPIO_GETMODE(pin) ((IKS_REG_R32((*((u_int32_t *)(VX185_GPIO_MODE1 + 4 * ((pin) / 16))))) >> 2 * ((pin) % 16)) & 0x3)

#if 0
#define VX185_GPIO_SETMODE(pin, mode)  \
                                    do{ \
                                            u_int32_t reg; \
                                            reg = IKS_REG_R32((volatile u_int32_t *)(VX185_GPIO_MODE1 + 4 * ((pin) / 16))); \
                                            reg &= ~(0x3 << (2 * ((pin) % 16))); \
                                            reg |= ((mode) << (2 * ((pin) % 16))); \
                                            IKS_REG_W32(reg, (volatile u_int32_t *)(VX185_GPIO_MODE1 + 4 * ((pin) / 16))); \
                                     }while(0)
#endif

#define VX185_GPIO_MODE_INPUT   0x00
#define VX185_GPIO_MODE_OUTPUT  0x01
#define VX185_GPIO_MODE_OD      0x02
#define VX185_GPIO_MODE_OS      0x03
#define VX185_GPIO_MODE_ALTFUNC 0x10

/*------------------------------------------------------------------------------------------*\
 * UART
\*------------------------------------------------------------------------------------------*/

#define VX185_UART1_BASE               KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x10000)
#define VX185_UART2_BASE               KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x18000)

/*------------------------------------------------------------------------------------------*\
 * SPI-0
\*------------------------------------------------------------------------------------------*/

#define VX185_SPI0_BASE             (VX185_SYSTEM_CONFIG_BASE + 0x20000)
#define VX185_SPI0_GLOB_CONFIG      (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE)
#define VX185_SPI0_GLOB_CONFIG_OFF  (0)
#define SPI0_GLOB_CONFIG_AHB_RESP_ACTIVE_MASK        (0x3 << 30)
#define SPI0_GLOB_CONFIG_AHB_RESP_ACTIVE_ERROR       (0x1 << 30)
#define SPI0_GLOB_CONFIG_AHB_RESP_ACTIVE_RETRY       (0x2 << 30)
#define SPI0_GLOB_CONFIG_AHB_RESP_ACTIVE_SPLIT       (0x3 << 30)
#define SPI0_GLOB_CONFIG_INTR_PORT_0                 (0x1 << 26)
#define SPI0_GLOB_CONFIG_FIFO_RST                    (0x1 << 11)
#define SPI0_GLOB_CONFIG_AHB_RESP_CTRL_MASK          (0x3 << 5)
#define SPI0_GLOB_CONFIG_AHB_RESP_CTRL_RETRY_WAIT    (0x0 << 5)
#define SPI0_GLOB_CONFIG_AHB_RESP_CTRL_ERROR         (0x1 << 5)
#define SPI0_GLOB_CONFIG_AHB_RESP_CTRL_RETRY_IM      (0x2 << 5)
#define SPI0_GLOB_CONFIG_AHB_RESP_CTRL_RETRY_SPLIT   (0x3 << 5)
#define SPI0_GLOB_CONFIG_SR_WARM                     (0x1 << 4)
#define SPI0_GLOB_CONFIG_SR_COLD                     (0x1 << 3)
#define SPI0_GLOB_CONFIG_PD                          (0x1 << 2)
#define SPI0_GLOB_CONFIG_SPI_CLK_POL                 (0x1 << 1)
#define SPI0_GLOB_CONFIG_CE                          (0x1 << 0)
union __vx185_spi0_glob_config_register
{
    volatile struct _vx185_spi0_glob_config_register
    {
        volatile unsigned int AHB_RESP_ACTIVE   :2;  // 31
        volatile unsigned int Reserved_29_27    :3;  // 29 - 27
        volatile unsigned int INTR_PORT_0       :1;  // 26
        volatile unsigned int Reserved_25_12    :14; // 25 - 12
        volatile unsigned int FIFO_RST          :1;  // 11
        volatile unsigned int SPI_AHB_WAIT_MAX  :4;  // 10 - 7
        volatile unsigned int AHB_RESP_CTRL     :2;  // 6 - 5
        volatile unsigned int SR_WARM           :1;  // 4
        volatile unsigned int SR_COLD           :1;  // 3
        volatile unsigned int PD                :1;  // 2
        volatile unsigned int SPI_CLK_POL       :1;  // 1
        volatile unsigned int CE                :1;  // 0
    }Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_PORT_CONFIG      (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x04)
#define VX185_SPI0_PORT_CONFIG_OFF  (0x04)
#define SPI0_PORT_CONFIG_RES_INSTR_TYPE            (0x1 << 16)
#define SPI0_PORT_CONFIG_WRR_SIZE_MASK             (0x3 << 14)
#define SPI0_PORT_CONFIG_TF_RST                    (0x1 << 13)
#define SPI0_PORT_CONFIG_RF_RST                    (0x1 << 12)
#define SPI0_PORT_CONFIG_SPI_CLK_SEL_MASK          (0x3 << 7)
#define SPI0_PORT_CONFIG_DEV_SEL_MASK              (0x7 << 4)
#define SPI0_PORT_CONFIG_HOLD                      (0x1 << 3)
#define SPI0_PORT_CONFIG_STOP                      (0x1 << 2)
#define SPI0_PORT_CONFIG_START                     (0x1 << 1)
#define SPI0_PORT_CONFIG_IE                        (0x1 << 0)

union __vx185_spi0_port_config_register
{
    struct _vx185_spi0_port_config_register
    {
        volatile unsigned int Unused_31_17      :15; // 31 - 17
        volatile unsigned int RES_INSTR_TYPE    :1;  // 16
        volatile unsigned int WRR_SIZE          :2;  // 15 - 14
        volatile unsigned int TF_RST            :1;  // 13
        volatile unsigned int RF_RST            :1;  // 12
        volatile unsigned int Reserved_11_9     :3;  // 11 - 9
        volatile unsigned int SPI_CLK_SEL       :2;  // 8 - 7
        volatile unsigned int DEV_SEL           :3;  // 6 - 4
        volatile unsigned int HOLD              :1;  // 3
        volatile unsigned int STOP              :1;  // 2
        volatile unsigned int START             :1;  // 1
        volatile unsigned int IE                :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_BLOCK_SIZE       (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x08)
#define VX185_SPI0_BLOCK_SIZE_OFF   (0x08)
union __vx185_spi0_block_size_register
{
    struct _vx185_spi0_block_size_register
    {
        volatile unsigned int BLOCKSIZE        :32;
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_SPI_INSTR        (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x0C)
#define VX185_SPI0_SPI_INSTR_OFF    (0x0C)
#define SPI0_SPI_INSTR_MASK                          (0xff << 0)
union __vx185_spi0_spi_instr_register
{
    volatile struct _vx185_spi0_spi_instr_register
    {
        volatile unsigned int Reserved_31_8     :24; // 31 - 8
        volatile unsigned int SPI_INSTR         :8;  // 7 - 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_SPI_ADDR         (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x10)
#define VX185_SPI0_SPI_ADDR_OFF     (0x10)
#define SPI0_SPI_ADDR_MASK                           (0xffffff << 0)
union __vx185_spi0_spi_addr_register
{
    volatile struct _vx185_spi0_spi_addr_register
    {
        volatile unsigned int Reserved_31_8     :24; // 31 - 8
        volatile unsigned int SPI_ADDR          :8;  // 7 - 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_DEV_MODE_BYTE        (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x14)
#define VX185_SPI0_DEV_MODE_BYTE_OFF    (0x14)
#define SPI0_DEV_MODE_BYTE_MASK                      (0xff << 0)
union __vx185_spi0_dev_mode_byte_register
{
    struct _vx185_spi0_dev_mode_byte_register
    {
        volatile unsigned int Reserved_31_8     :24; // 31 - 8
        volatile unsigned int DEV_MODE_BYTE     :8;  // 7 - 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_FIFO_BNDR        (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x18)
#define VX185_SPI0_FIFO_BNDR_OFF    (0x18)
#define SPI0_FIFO_BNDR_RF_ALMOST_FULL_BNDR_MASK          (0xf << 0)
#define SPI0_FIFO_BNDR_RF_ALMOST_EMPTY_BNDR_MASK         (0xf << 8)
#define SPI0_FIFO_BNDR_TF_ALMOST_FULL_BNDR_MASK          (0xf << 16)
#define SPI0_FIFO_BNDR_TF_ALMOST_EMPTY_BNDR_MASK         (0xf << 24)
union __vx185_spi0_fifo_bndr_register
{
    struct _vx185_spi0_fifo_bndr_register
    {
        volatile unsigned int TF_ALMOST_EMPTY_BNDR  :8; // 31 - 24
        volatile unsigned int TF_ALMOST_FULL_BNDR   :8; // 23 - 16
        volatile unsigned int RF_ALMOST_EMPTY_BNDR  :8; // 15 - 8
        volatile unsigned int RF_ALMOST_FULL_BNDR   :8; // 7 - 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_DEV_RSR          (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x1C)
#define VX185_SPI0_DEV_RSR_OFF      (0x1C)
#define SPI0_DEV_RSR_SRWD                            (0x1 << 7)
#define SPI0_DEV_RSR_P_ERR                           (0x1 << 6)
#define SPI0_DEV_RSR_E_RR                            (0x1 << 5)
#define SPI0_DEV_RSR_BP2                             (0x1 << 4)
#define SPI0_DEV_RSR_BP1                             (0x1 << 3)
#define SPI0_DEV_RSR_BP0                             (0x1 << 2)
#define SPI0_DEV_RSR_WEL                             (0x1 << 1)
#define SPI0_DEV_RSR_WIP                             (0x1 << 0)
union __vx185_spi0_dev_rsr_register
{
    struct _vx185_spi0_dev_rsr_register
    {
        volatile unsigned int Reserved_31_8     :24; // 31 - 8
        volatile unsigned int SRWD              :1;  // 7
        volatile unsigned int P_ERR             :1;  // 6
        volatile unsigned int E_RR              :1;  // 5
        volatile unsigned int BP                :3;  // 4 - 2
        volatile unsigned int WEL               :1;  // 1
        volatile unsigned int WIP               :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_DEV_RCR          (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x20)
#define VX185_SPI0_DEV_RCR_OFF      (0x20)
#define SPI0_DEV_RCR_TBPROT                          (0x1 << 5)
#define SPI0_DEV_RCR_LOCK                            (0x1 << 4)
#define SPI0_DEV_RCR_BPNV                            (0x1 << 3)
#define SPI0_DEV_RCR_TBPARM                          (0x1 << 2)
#define SPI0_DEV_RCR_QUAD                            (0x1 << 1)
#define SPI0_DEV_RCR_FREEZE                          (0x1 << 0)
union __vx185_spi0_dev_rcr_register
{
    struct _vx185_spi0_dev_rcr_register
    {
        volatile unsigned int Reserved_31_6     :26; // 31 - 6
        volatile unsigned int TBPROT            :1;  // 5
        volatile unsigned int LOCK              :1;  // 4
        volatile unsigned int BPNV              :1;  // 3
        volatile unsigned int TBPARM            :1;  // 2
        volatile unsigned int QUAD              :1;  // 1
        volatile unsigned int FREEZE            :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_STATUS           (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x24)
#define VX185_SPI0_STATUS_OFF       (0x24)
#define SPI0_STATUS_BUSY                            (0x1 << 0)
#define SPI0_STATUS_RF_RST_DONE                     (0x1 << 1)
#define SPI0_STATUS_TF_RST_DONE                     (0x1 << 2)
#define SPI0_STATUS_SPI_CLK_STABLE                  (0x1 << 3)
#define SPI0_STATUS_SPI_CLK_POL_DONE                (0x1 << 4)
#define SPI0_STATUS_HOLD_RESUME_DONE                (0x1 << 5)
#define SPI0_STATUS_STOP_DONE                       (0x1 << 6)
#define SPI0_STATUS_SPI_CLK_CHANGE_DONE             (0x1 << 7)
union __vx185_spi0_status_register
{
    volatile struct _vx185_spi0_status_register
    {
        volatile unsigned int Reserved_31_8         :24; // 31 - 8
        volatile unsigned int SPI_CLOCK_CHG_DONE    :1;  // 7
        volatile unsigned int STOP_DONE             :1;  // 6
        volatile unsigned int HOLD_RESUME_DONE      :1;  // 5
        volatile unsigned int SPI_CLK_POL_DONE      :1;  // 4
        volatile unsigned int SPI_CLK_STABLE        :1;  // 3
        volatile unsigned int TF_RST_DONE           :1;  // 2
        volatile unsigned int RF_RST_DONE           :1;  // 1
        volatile unsigned int BUSY                  :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_INT_STATUS       (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x28)
#define VX185_SPI0_INT_STATUS_OFF   (0x28)
#define SPI0_INT_RF_FULL                            (0x1 << 0)
#define SPI0_INT_TF_FULL                            (0x1 << 1)
#define SPI0_INT_RF_EMPTY                           (0x1 << 2)
#define SPI0_INT_TF_EMPTY                           (0x1 << 3)
#define SPI0_INT_RF_ALMOST_FULL                     (0x1 << 4)
#define SPI0_INT_TF_ALMOST_FULL                     (0x1 << 5)
#define SPI0_INT_RF_ALMOST_EMPTY                    (0x1 << 6)
#define SPI0_INT_TF_ALMOST_EMPTY                    (0x1 << 7)
#define SPI0_INT_TRNF_END                           (0x1 << 8)
#define SPI0_INT_TRNF_ERR                           (0x1 << 9)
#define SPI0_INT_SPI_DONE                           (0x1 << 10)
union __vx185_spi0_int_status_register
{
    volatile struct _vx185_spi0_int_status_register
    {
        volatile unsigned int Unused_31_11          :21; // 31 - 11
        volatile unsigned int SPI_DONE              :1;  // 10
        volatile unsigned int TRNF_ERR              :1;  // 9
        volatile unsigned int TRNF_END              :1;  // 8
        volatile unsigned int TF_ALMOST_EMPTY_EN    :1;  // 7
        volatile unsigned int RF_ALMOST_EMPTY_EN    :1;  // 6
        volatile unsigned int TF_ALMOST_FULL_EN     :1;  // 5
        volatile unsigned int RF_ALMOST_FULL_EN     :1;  // 4
        volatile unsigned int TF_EMPTY_EN           :1;  // 3
        volatile unsigned int RF_EMPTY_EN           :1;  // 2
        volatile unsigned int TF_FULL_EN            :1;  // 1
        volatile unsigned int RF_FULL_EN            :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

#define VX185_SPI0_INT_EN           (volatile unsigned int *)KSEG1ADDR(VX185_SPI0_BASE + 0x2C)
#define VX185_SPI0_INT_EN_OFF       (0x2C)
union __vx185_spi0_int_en_register
{
    struct _vx185_spi0_int_en_register
    {
        volatile unsigned int Unused_31_11          :21; // 31 - 11
        volatile unsigned int SPI_DONE              :1;  // 10
        volatile unsigned int TRNF_ERR              :1;  // 9
        volatile unsigned int TRNF_END              :1;  // 8
        volatile unsigned int TF_ALMOST_EMPTY_EN    :1;  // 7
        volatile unsigned int RF_ALMOST_EMPTY_EN    :1;  // 6
        volatile unsigned int TF_ALMOST_FULL_EN     :1;  // 5
        volatile unsigned int RF_ALMOST_FULL_EN     :1;  // 4
        volatile unsigned int TF_EMPTY_EN           :1;  // 3
        volatile unsigned int RF_EMPTY_EN           :1;  // 2
        volatile unsigned int TF_FULL_EN            :1;  // 1
        volatile unsigned int RF_FULL_EN            :1;  // 0
    } Bits;
    volatile u_int32_t Reg;
};

//#define VX185_SPI0_RX_FIFO  (volatile char *)KSEG1ADDR(VX185_SPI0_BASE + 0xB00)
//#define VX185_SPI0_TX_FIFO  (volatile char *)KSEG1ADDR(VX185_SPI0_BASE + 0xB80)
#define VX185_SPI0_RX_FIFO  (VX185_SPI0_BASE + 0xB80)
#define VX185_SPI0_TX_FIFO  (VX185_SPI0_BASE + 0xB00)
#define VX185_SPI0_SPEED    40

struct __vx185_spi0_regs
{
    union __vx185_spi0_glob_config_register     glob_config;
    union __vx185_spi0_port_config_register     port_config;
    union __vx185_spi0_block_size_register      block_size;
    union __vx185_spi0_spi_instr_register       spi_instr;
    union __vx185_spi0_spi_addr_register        spi_addr;
    union __vx185_spi0_dev_mode_byte_register   mode_byte;
    union __vx185_spi0_fifo_bndr_register       fifo_bndr;
    union __vx185_spi0_dev_rsr_register         dev_rsr;
    union __vx185_spi0_dev_rcr_register         dev_rcr;
    union __vx185_spi0_status_register          status;
    union __vx185_spi0_int_status_register      int_status;
    union __vx185_spi0_int_en_register          int_en;
};

/*------------------------------------------------------------------------------------------*\
 * SPI-1
\*------------------------------------------------------------------------------------------*/

#define VX185_SPI1_BASE             KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x28000)
#define VX185_SPI1_CTL              (volatile unsigned short *)(VX185_SPI1_BASE + 0x0)
union __vx185_spi1_ctl {
    struct _vx185_spi1_ctl {
        volatile unsigned short Unused_15        :1;
        volatile unsigned short SPE              :1;  /*--- 14 Enables SPI1 Module ---*/
        volatile unsigned short WOM              :1;  /*--- 13 Enables open-drain output 0: Normal, 1 Open Drain ---*/
        volatile unsigned short MSTR             :1;  /*--- 12 Configure SP1 as Master(1)/Slave(0) ---*/
        volatile unsigned short CPOL             :1;  /*--- 11 Clockpolarity of SPI1_CLK_B Active High(0)/Low(1) ---*/
        volatile unsigned short CPHA             :1;  /*--- 10 Clockphase 0: toggle of middle(0)/start(1) of first data-bit ---*/
        volatile unsigned short LSBF             :1;  /*---  9 0: MSB(0)/LSB(1) first---*/
        volatile unsigned short SIZE             :1;  /*---  8 Wordlength 0=8 bits, 1=16 bits ---*/
        volatile unsigned short Unused_7_6       :2;  /*---  7:6 ---*/
        volatile unsigned short EMISO            :1;  /*---  5 Enables MISO pin as output when SPI1 Controller as Slave ---*/
        volatile unsigned short PSSE             :1;  /*---  4 Enables SPI_SLV_CS pin as error input to SPI1, when SPI1 Master ---*/
        volatile unsigned short GM               :1;  /*---  3 When RDBR is full, fetch data (1) or discard incoming data (0)---*/
        volatile unsigned short SZ               :1;  /*---  2 Send 0 or last word when TDBR is empty  =: send last word, 1 send zeros ---*/
        volatile unsigned short TIMOD            :2;  /*--- 1:0  Defines initiation mode and interrupt generation: */
                                                    /*---      00: Initiate transfer by read of rcv-buffer (irq is active when rcv-buffer is full) ---*/
                                                    /*---      01: Initiate transfer by write to transmit-buffer (irq is active when transmitt-buffer is empty) ---*/
        volatile unsigned short Unused;
    } Bits;
    volatile unsigned short Reg;
    volatile unsigned short Unused;
};

#define VX185_SPI1_FLG              (volatile unsigned short *)(VX185_SPI1_BASE + 0x04)
union __vx185_spi1_flg {
    struct _vx185_spi1_flg {
        volatile unsigned short FLG              :7;  /*--- 15:9    ---*/
        volatile unsigned short Unused_8         :1;  /*--- 8       ---*/
        volatile unsigned short FLS              :7;  /*--- 7:1     ---*/
        volatile unsigned short Unused_0         :1;  /*--- 0       ---*/
        volatile unsigned short Unused;
    } Bits;
    volatile unsigned short Reg;
    volatile unsigned short Unused;
};
#define VX185_SPI1_ST              (volatile unsigned short *)(VX185_SPI1_BASE + 0x08)
union __vx185_spi1_st {
    struct _vx185_spi1_st {
        volatile unsigned short Unused_15_7      :9; /*--- 15 - 7 ---*/
        volatile unsigned short TXCOL            :1;  /*--- 6 transmit Collision error (W1C) set when corrupt data was transmitted ---*/
        volatile unsigned short RXS              :1;  /*--- 5 Rx Data buffer status 0: empty 1: full ---*/
        volatile unsigned short RBSY             :1;  /*--- 4 Rcv error (set when data received with receive buffer full (W1C) ---*/
        volatile unsigned short TXS              :1;  /*--- 3 Tx Data buffer status 0: empty 1: full ---*/
        volatile unsigned short TXE              :1;  /*--- 2 Transmission error Set when transmisson occured with no new data in TDBR (W1C) ---*/
        volatile unsigned short MODF             :1;  /*--- 1 Mode fault error (W1C) ---*/
        volatile unsigned short SPIF             :1;  /*--- 0 Set when an SPI single word transfer is complete ---*/
        volatile unsigned short Unused;
    } Bits;
    volatile unsigned short Reg;
    volatile unsigned short Unused;
};

#define VX185_SPI1_TDBR             (volatile unsigned short *)(VX185_SPI1_BASE + 0x0C)
#define VX185_SPI1_RDBR             (volatile unsigned short *)(VX185_SPI1_BASE + 0x10)

/*--- Spi-Baudrate= System-Clock-Rate / (2 * BAUD[15:0]) ---*/
#define VX185_SPI1_BAUD             (volatile unsigned short *)(VX185_SPI1_BASE + 0x14)

struct _vx185_spi_1 {
    union __vx185_spi1_ctl spi1_ctl;        /*--- Offset: 0x00 SP-1 Control Register ---*/
    union __vx185_spi1_flg spi1_flg;        /*--- Offset: 0x04 SP-1 Flag Register ---*/
    union __vx185_spi1_st  spi1_st;         /*--- Offset: 0x08 SP-1 Status Register ---*/
    volatile unsigned short tdbr;           /*--- Offset: 0x0C SP-1 Transmit Data Buffer Register ---*/
    volatile unsigned short unused1;
    volatile unsigned short rdbr;           /*--- Offset: 0x10 SP-1 Receive Data Buffer Register ---*/
    volatile unsigned short unused2;
    volatile unsigned short baud;           /*--- Offset: 0x14 SP-1 SPI-1 Baud Rate Register ---*/
};

/*------------------------------------------------------------------------------------------*\
 * PLL
\*------------------------------------------------------------------------------------------*/

#define VX185_CLOCK_GATE_CTRL                   (volatile unsigned int *)KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x1C)
#define VX185_PLL_POWER_RESET                   (volatile unsigned int *)KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x20)
#define VX185_PLL_PD1                           (volatile unsigned int *)KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x2C)
#define     PLL_PD1_DIV128_MASK                 (0xFF)
#define     PLL_PD1_DIV8_MASK                   (0xF)
#define     PLL_PD1_GW_DIV128_OFF               (0)
#define     PLL_PD1_GW_DIV128                   (((*VX185_PLL_PD1) >> PLL_PD1_GW_DIV128_OFF) & PLL_PD1_DIV128_MASK)
#define     PLL_PD1_BME_DIV128_OFF              (8)
#define     PLL_PD1_BME_DIV128                  (((*VX185_PLL_PD1) >> PLL_PD1_BME_DIV128_OFF) & PLL_PD1_DIV128_MASK)
#define     PLL_PD1_DSP_DIV128_OFF              (16)
#define     PLL_PD1_DSP_DIV128                  (((*VX185_PLL_PD1) >> PLL_PD1_DSP_DIV128_OFF) & PLL_PD1_DIV128_MASK)
#define     PLL_PD1_SPI_DIV8_OFF                (24)
#define     PLL_PD1_SPI_DIV8                    (0x2 << (((*VX185_PLL_PD1) >> PLL_PD1_SPI_DIV8_OFF) & PLL_PD1_DIV8_MASK))
#define     PLL_PD1_SPI_PD2_MASK                (0x3)
#define     PLL_PD1_SPI_PD2_OFF                 (28)

#define VX185_PLL_PD2                           (volatile unsigned int *)KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x30)
#define     PLL_PD2_HOST_PD1_DIV                (((*VX185_PLL_PD2) >>  0) & 0xf)
#define     PLL_PD2_SYS_PD1_DIV                 (((*VX185_PLL_PD2) >>  4) & 0xf)
#define     PLL_PD2_HOST_PD2                    (0x2 << (((*VX185_PLL_PD2) >>  8) & 0x3))
#define     PLL_PD2_SYS_PD2                     (0x2 << (((*VX185_PLL_PD2) >> 10) & 0x3))
#define     PLL_PD2_BME_PD1_DIV                 (((*VX185_PLL_PD2) >> 12) & 0xf)
#define     PLL_PD2_FD_PD1_DIV                  (((*VX185_PLL_PD2) >> 16) & 0xf)
#define     PLL_PD2_BME_PD2                     (0x2 << (((*VX185_PLL_PD2) >> 20) & 0x3))
#define     PLL_PD2_FD_PD2                      (0x2 << (((*VX185_PLL_PD2) >> 22) & 0x3))
#define     PLL_PD2_DSP_PD1_DIV                 (((*VX185_PLL_PD2) >> 24) & 0xf)
#define     PLL_PD2_DSP_PD2                     (0x2 << (((*VX185_PLL_PD2) >> 28) & 0x3))

#define VX185_PLL_PD3                           (volatile unsigned int *)KSEG1ADDR(VX185_SYSTEM_CONFIG_BASE + 0x34)

/*--------------------------------------------------------------------------------*\
 * DSP + IDMA
\*--------------------------------------------------------------------------------*/
#define VX185_IDMA_ERROR_STATUS_REGISTER(idma) (volatile unsigned int *)(((idma) == 0) ? KSEG1ADDR(0x190B8014) : KSEG1ADDR(0x190B8030))
#define VX185_IDMA_ERROR_ADDR_REGISTER(idma)   (volatile unsigned int *)(((idma) == 0) ? KSEG1ADDR(0x190B8018) : KSEG1ADDR(0x190B8034))

#define VX185_IACK_MAXIMUMCOUNT_REGISTER          (volatile unsigned int *)KSEG1ADDR(0x190B8048)

#define VX185_IDMA_BASE             (0x190B0000)

#define VX185_IDMA_BASE_REGISTER(dsp)             (volatile unsigned int *)(((dsp) == 0) ? KSEG1ADDR(VX185_IDMA_BASE + 0x0) : KSEG1ADDR(VX185_IDMA_BASE + 0x10))
#define VX185_IDMA_PROG_MEMORY_ACCESS(dsp)        (volatile unsigned int *)(((dsp) == 0) ? KSEG1ADDR(VX185_IDMA_BASE + 0x4) : KSEG1ADDR(VX185_IDMA_BASE + 0x14))
#define VX185_IDMA_DATA_MEMORY_ACCESS(dsp)        (volatile unsigned short *)(((dsp) == 0) ? KSEG1ADDR(VX185_IDMA_BASE + 0x8) : KSEG1ADDR(VX185_IDMA_BASE + 0x18))

#define DSPx_DATAMEM_OFFSET     0x4000
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void adsp_writedataword(unsigned int dsp, unsigned int addr, unsigned short value) {
    /*--- printk("dw%d %p %p addr %x val=%x\n", dsp, VX185_IDMA_BASE_REGISTER(dsp), VX185_IDMA_DATA_MEMORY_ACCESS(dsp), addr, value); ---*/
    *VX185_IDMA_BASE_REGISTER(dsp)      = addr + DSPx_DATAMEM_OFFSET;
    *VX185_IDMA_DATA_MEMORY_ACCESS(dsp) = (unsigned short)value;
    /*--- printk("dw%d status=%x err_addr=%x iack=%d\n", dsp, *VX185_IDMA_ERROR_STATUS_REGISTER(dsp), *VX185_IDMA_ERROR_ADDR_REGISTER(dsp), *VX185_IACK_MAXIMUMCOUNT_REGISTER); ---*/
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void adsp_writeprogword(unsigned int dsp, unsigned int addr, unsigned int value) {
    /*--- printk("pw%d %p %p addr %x val=%x\n", dsp, VX185_IDMA_BASE_REGISTER(dsp), VX185_IDMA_PROG_MEMORY_ACCESS(dsp), addr, value); ---*/
    *VX185_IDMA_BASE_REGISTER(dsp)      = addr;
    *VX185_IDMA_PROG_MEMORY_ACCESS(dsp) = value;
    /*--- printk("pw%d status=%x err_addr=%x iack=%d\n", dsp, *VX185_IDMA_ERROR_STATUS_REGISTER(dsp), *VX185_IDMA_ERROR_ADDR_REGISTER(dsp), *VX185_IACK_MAXIMUMCOUNT_REGISTER); ---*/
}
/*--------------------------------------------------------------------------------*\
 * ab  0x4000 DataMem ??
\*--------------------------------------------------------------------------------*/
static inline void adsp_writedatamemory(unsigned int dsp, unsigned int destaddr, unsigned short *srcaddr, unsigned short words) {
    volatile unsigned short *accessaddr;
    if(words == 0) {
        return;
    }
    *VX185_IDMA_BASE_REGISTER(dsp) = destaddr + DSPx_DATAMEM_OFFSET;
    accessaddr = VX185_IDMA_DATA_MEMORY_ACCESS(dsp);
    while(words--) {
        *accessaddr = *srcaddr++;
    }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void adsp_writeprogmemory(unsigned int dsp, unsigned int destaddr, unsigned int *srcaddr, unsigned short words) {
    if(words == 0) {
        return;
    }
    *VX185_IDMA_BASE_REGISTER(dsp) = destaddr;
    while(words--) {
        *VX185_IDMA_PROG_MEMORY_ACCESS(dsp) = *srcaddr++;
    }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned short adsp_readdataword(unsigned int dsp, unsigned int addr) {
    register unsigned short val; 
    *VX185_IDMA_BASE_REGISTER(dsp) = addr + DSPx_DATAMEM_OFFSET;
    val =  *VX185_IDMA_DATA_MEMORY_ACCESS(dsp);
    /*--- printk("dr%d %p %p addr %x val=%x\n", dsp, VX185_IDMA_BASE_REGISTER(dsp), VX185_IDMA_DATA_MEMORY_ACCESS(dsp), addr, val); ---*/
    return val;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int adsp_readprogword(unsigned int dsp, unsigned int addr) {
    /*--- printk("pr%d %p %p addr %x val=%x\n", dsp, VX185_IDMA_BASE_REGISTER(dsp), VX185_IDMA_PROG_MEMORY_ACCESS(dsp), addr, *VX185_IDMA_PROG_MEMORY_ACCESS(dsp)); ---*/
    *VX185_IDMA_BASE_REGISTER(dsp) = addr;
    return *VX185_IDMA_PROG_MEMORY_ACCESS(dsp);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void adsp_readdatamemory(unsigned int dsp, unsigned short *destaddr, unsigned int srcaddr, unsigned short words) {
    volatile unsigned short *acessaddr;
    if(words == 0) {
        return;
    }
    *VX185_IDMA_BASE_REGISTER(dsp) = srcaddr + DSPx_DATAMEM_OFFSET;
    acessaddr = VX185_IDMA_DATA_MEMORY_ACCESS(dsp);
    while(words--) {
        *destaddr++ = (unsigned short)*acessaddr;
    }
}
#if 0
#define VX185_INTERRUPT_PRIO_0_BASE KSEG1ADDR(0x19050100)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union __vx185_interrupt_prio_0_register {
    struct _vx185_interrupt_prio_0_register {
        volatile unsigned int Reserved0:4;      /*--- 28: 31 ---*/ 
        volatile unsigned int MIPS_TIMER:4;     /*--- 27: 24 Routes Interrupt from MIPS TIMER to SPE/AP/PCI ---*/
        volatile unsigned int Reserved1:4;      /*--- 23: 20 ---*/
        volatile unsigned int SCUINT   :4;       /*--- 19: 16 Routes Interrupt from SCU to CPE/AP/PCI ---*/
        volatile unsigned int Reserved2:4;      /*--- 15: 12 ---*/
        volatile unsigned int CBUS_ARB_INT:4;   /*--- 11: 8  Routes interrupt from CBUS ARBITER to CPE/AP/PCI ---*/
        volatile unsigned int Reserved3:4;      /*--- 7 : 4  ---*/
        volatile unsigned int DMA_CPE  :4;      /*--- 28: 3  Routes interrupt from DMA to CPE/AP/PCI ---*/
    } Bits;
    volatile unsigned int Reg;
};

#define VX185_CPE_INT_STATUS_0_1 KSEG1ADDR(0x19050128)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union __vx185_cpe_int_status_0_1_register {
    struct _vx185_cpe_int_status_0_1_register {
        volatile unsigned int BMU_GIGE_AP:1; /*--- 31 Conveys the status of BMU AP interrupt to CPE ---*/    
        volatile unsigned int WAP_GIGE_AP:1; /*--- 30 Conveys the status of WAP AP interrupt to CPE ---*/    
        volatile unsigned int UART_2:1;		 /*--- 29 Conveys the status of UART_2 interrupt to CPE ---*/    
        volatile unsigned int SPA_AP:1;		 /*--- 28 Conveys the status of SPA AP Interrupt to CPE ---*/    
        volatile unsigned int DBUSC_ARB:1;	 /*--- 27 Conveys the status of DBUS-C ARB interrupt to CPE ---*/
        volatile unsigned int VDSL_PHY:1;	 /*--- 26 Conveys the status of xDSL PHY interrupt to CPE ---*/  
        volatile unsigned int PCI:1;		 /*--- 25 Conveys the status of PCI interrupt to CPE ---*/       
        volatile unsigned int DSP1_PF1:1;	 /*--- 24 Conveys the status of DSP1 PF1 interrupt to CPE ---*/  
        volatile unsigned int DSP1_PF0:1;	 /*--- 23 Conveys the status of DSP1 PF0 interrupt to CPE ---*/  
        volatile unsigned int DSP1_FL1:1;	 /*--- 22 Conveys the status of DSP1 flag1 interrupt to CPE ---*/
        volatile unsigned int DSP1_FL0:1;	 /*--- 21 Conveys the status of DSP1 flag0 interrupt to CPE ---*/
        volatile unsigned int DSP0_PF1:1;	 /*--- 20 Conveys the status of DSP0 PF1 interrupt to CPE ---*/  
        volatile unsigned int DSP0_PF0:1;	 /*--- 19 Conveys the status of DSP0 PF0 interrupt to CPE ---*/  
        volatile unsigned int DSP0_FL1:1;	 /*--- 18 Conveys the status of DSP0 flag1 interrupt to CPE ---*/
        volatile unsigned int DSP0_FL0:1;	 /*--- 17 Conveys the status of DSP0 flag0 interrupt to CPE ---*/
        volatile unsigned int VDSP_IDMA1:1;	 /*--- 16 Conveys the status of VDSP IDMA1 interrupt to CPE ---*/
        volatile unsigned int VDSP_IDMA2:1;	 /*--- 15 Conveys the status of VDSP IDMA2 interrupt to CPE ---*/
        volatile unsigned int GIGE1:1;		 /*--- 14 Conveys the status of GIGE1 interrupt to CPE ---*/     
        volatile unsigned int GIGE2:1;		 /*--- 13 Conveys the status of GIGE2 interrupt to CPE ---*/     
        volatile unsigned int DBUSB_ARB:1;	 /*--- 12 Conveys the status of DBUS-B_ARB interrupt to CPE ---*/
        volatile unsigned int GPT2:1;	     /*--- 11 Conveys the status of GPT2 interrupt to CPE ---*/      
        volatile unsigned int GPT1:1;	     /*--- 10 Conveys the status of GPT1 interrupt to CPE ---*/      
        volatile unsigned int GPT0:1;	     /*--- 9  Conveys the status of GPT0 interrupt to CPE ---*/       
        volatile unsigned int GPIO:1;	     /*--- 8  Conveys the status of GPIO interrupt to CPE (For GPIO 0-to-15 Flags) ---*/                                      
        volatile unsigned int SPI_AP:1;		 /*--- 7  Conveys the status of SPI interrupt to CPE ---*/        
        volatile unsigned int UART_1:1;		 /*--- 6  Conveys the status of UART_1 interrupt to CPE ---*/     
        volatile unsigned int DBUSA_ARB:1;	 /*--- 5  Conveys the status of DBUS-A ARB Interrupt to CPE ---*/ 
        volatile unsigned int GPIO2:1;		 /*--- 4  Conveys the status of GPIO2 interrupt to CPE (For GPIO 16-to-31 Flags) ---*/                                     
        volatile unsigned int MIPS_TIMER:1;	 /*--- 3  Conveys the status of MIPS TIMER interrupt to CPE ---*/ 
        volatile unsigned int SCU_AP:1;		 /*--- 2  Conveys the status of SCU interrupt to CPE ---*/        
        volatile unsigned int CBUS_ARB:1;	 /*--- 1  Conveys the status of CBUS ARB interrupt to CPE ---*/   
        volatile unsigned int DMA_CPE:1;     /*--- 0  Conveys the status of DMA interrupt to CPE ---*/        
    } Bits;
    volatile unsigned int Reg;
};
#endif
#define VX185_DSP_TO_CPE_PERIPHERAL_INTERRUPT_BASE KSEG1ADDR(0x190B803C)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union __vx185_cpe_peripheral_interrupt_register {
    struct _vx185_cpe_peripheral_interrupt_register {
        volatile unsigned int DSP1_FLAG_POLARITY:1;      /*--- 31: DSP1 Flag Polarity ---*/
        volatile unsigned int DSP0_FLAG_POLARITY:1;      /*--- 30: DSP0 Flag Polarity ---*/
        volatile unsigned int Reserved1:4;      /*--- 29:26 R   Reserved ---*/
        volatile unsigned int DSP1_RESET_N:1;   /*--- 25    R/W DSP1 Reset (active HIGH). ---*/
        volatile unsigned int DSP1_PWDACK:1;    /*--- 24    R   DSP1 PWD acknowledgment. ---*/
        volatile unsigned int DSP1_PDWN:1;      /*--- 23    R/W DSP1 Power Down (Active HIGH). ---*/
        volatile unsigned int Reserved2:2;      /*--- 22:21 R Reserved ---*/
        volatile unsigned int DSP1_NINTE:1;     /*--- 20 R/W Edge Sensitive interrupt from CPE to DSP1.  Toggles when 1 is written. ---*/
        volatile unsigned int DSP1_NINTL1:1;     /*--- 19 R/W Level sensitive interrupt1 from CPE to DSP1.
                                                             Write zero to clear.
                                                             Write 1 to generate interrupt. ---*/
        volatile unsigned int Reserved3:1;      /*--- 18 R Reserved ---*/
        volatile unsigned int IPC_DSP1_PF1:1;   /*--- 17 R/W Programmable Flag interrupt-1 from CPE to DSP1. ---*/
        volatile unsigned int IPC_DSP1_PF0:1;   /*--- 16 R/W Programmable Flag interrupt-0 from CPE to DSP1. ---*/
        volatile unsigned int Reserved4:6;      /*--- 15:10 Reserved ---*/
        volatile unsigned int DSP0_RESET_N:1;    /*--- 9 R/W DSP0 Reset (Active HIGH). ---*/
        volatile unsigned int DSP0_PWDACK:1;     /*--- 8 R DSP0 PWD Acknowledgment ---*/
        volatile unsigned int DSP0_PDWN:1;       /*--- 7 R/W DSP0 Power down. (Active HIGH). ---*/
        volatile unsigned int Reserved5:2;       /*--- 6:5 R Reserved. ---*/
        volatile unsigned int DSP0_NINTE:1;      /*--- 4 R/W Edge Sensitive interrupt from CPE to DSP0. Toggles when 1 is written. ---*/
        volatile unsigned int DSP0_NINTL1:1;     /*--- 3 R/W Level sensitive interrupt-1 from CPE to DSP0.
                                                            Write ?0? to clear.
                                                            Write ?1? to generate interrupt. ---*/
        volatile unsigned int Reserved6:1;       /*--- 2 R Reserved ---*/
        volatile unsigned int IPC_DSP0_PF1:1;    /*--- 1 R/W Programmable Flag interrupt-1 from CPE to DSP0 ---*/
        volatile unsigned int IPC_DSP0_PF0:1;    /*--- 0 R/W Programmable Flag interrupt-0 from CPE to DSP0 ---*/
    } Bits;
    volatile unsigned int Reg;
};

#define VX185_DSP_TO_CPE_INTERRUPT_FLAG_BASE             KSEG1ADDR(0x190B8040)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union __vx185_dsp_to_cpe_flag_interrupt_register {
    struct _vx185_dsp_to_cpe_flag_interrupt_register {
        volatile unsigned int Reserved0:  7;    /*--- 31:25 ---*/
        volatile unsigned int DSP1_FL1INT:1;   /*--- 24 R/W Flag-1 Interrupt from DSP1 to CPE ---*/
        volatile unsigned int Reserved1:  7;    /*--- 17:23 ---*/
        volatile unsigned int DSP1_FL0INT:1;   /*--- 16 R/W Flag-0 Interrupt from DSP1 to CPE ---*/
        volatile unsigned int Reserved2:  7;    /*--- 9:15 ---*/
        volatile unsigned int DSP0_FL1INT:1;   /*--- 8 R/W Flag-1 Interrupt from DSP0 to CPE ---*/
        volatile unsigned int Reserved3:  7;    /*--- 1:7 ---*/
        volatile unsigned int DSP0_FL0INT:1;   /*--- 0 R/W Flag-0 Interrupt from DSP0 to CPE ---*/
    } Bits;
    volatile unsigned int Reg;
};
#define VX185_DSP_TO_CPE_PROGRAMMABLE_INTERRUPT_FLAG_BASE             KSEG1ADDR(0x190B8044)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union __vx185_dsp_to_cpe_programmable_flag_interrupt_register {
    struct _vx185_dsp_to_cpe_programmable_flag_interrupt_register {
        volatile unsigned int Reserved0:  7;    /*--- 31:25 ---*/
        volatile unsigned int DSP1_PF1INT:1;  /*--- 24 R/W Programmable Flag-1 Interrupt from DSP1 to CPE ---*/
        volatile unsigned int Reserved1:  7;    /*--- 17:23 ---*/
        volatile unsigned int DSP1_PF0INT:1;  /*--- 16 R/W Programmable Flag-0 Interrupt from DSP1 to CPE ---*/
        volatile unsigned int Reserved2:  7;    /*--- 9:15 ---*/
        volatile unsigned int DSP0_PF1INT:1;  /*--- 8 R/W Programmable Flag-1 Interrupt from DSP0 to CPE ---*/
        volatile unsigned int Reserved3:  7;    /*--- 1:7 ---*/
        volatile unsigned int DSP0_PF0INT:1;  /*--- 0 R/W Programmable Flag-0 Interrupt from DSP0 to CPE ---*/
    } Bits;
    volatile unsigned int Reg;
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
#define VX185_CONTROL_WDT_BASE             (volatile unsigned int *)KSEG1ADDR(0x19058028)
#define VX185_CONTROL_WDT_TIMER_X_ENABLE   (0x1 << 0)
#define VX185_CONTROL_WDT_INT_MASK         (0x1 << 1)
#define VX185_CONTROL_WDT_RESET_MASK       (0x1 << 2)

#define VX185_WDT_LOCKVALUE                (0x6BAD << 16)

#define VX185_PET_WDT_BASE                 (volatile unsigned int *)KSEG1ADDR(0x1905802C)
#define VX185_TOP_COUNT_BASE               (volatile unsigned int *)KSEG1ADDR(0x19058030)

#endif /*--- #ifndef _asm_mips_mach_ikan_mips_vx185_h_ ---*/
