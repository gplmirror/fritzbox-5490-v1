/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __ASM_MACH_MIPS_IRQ_H
#define __ASM_MACH_MIPS_IRQ_H

#if defined(CONFIG_FUSIV_VX185)
#define GIC_NUM_INTRS			(24 + NR_CPUS * 2)
#endif /* CONFIG_FUSIV_VX185 */

#if defined(CONFIG_FUSIV_VX585)
#define GIC_NUM_INTRS			128
#endif /* CONFIG_FUSIV_VX585 */

#define NR_IRQS 256

#include_next <irq.h>

#endif /* __ASM_MACH_MIPS_IRQ_H */
