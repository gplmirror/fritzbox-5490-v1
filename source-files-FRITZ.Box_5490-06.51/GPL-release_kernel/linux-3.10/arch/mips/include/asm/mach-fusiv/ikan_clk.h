/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _ikan_clk
#define _ikan_clk_h

#include <asm/mach_avm.h>

#if defined(CONFIG_FUSIV_VX185)
#include <vx185.h>
#else
#include <vx180.h>
#endif

unsigned int ikan_get_clock(enum _avm_clock_id clock_id);
#endif /*--- #ifndef _ikan_clk_h ---*/
