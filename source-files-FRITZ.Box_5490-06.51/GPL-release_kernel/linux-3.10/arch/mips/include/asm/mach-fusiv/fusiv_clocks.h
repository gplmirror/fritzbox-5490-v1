/**
 * Copyright (C) 2010-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef __VX185_CLOCKS_H__
#define __VX185_CLOCKS_H__

#define HOST_GW_PLL_DIV_MASK    0x00FF
#define HOST_SYS_AP_PD1_MASK    0x00FF
#define HOST_SPARE_PD1_MASK		0x003c
#define HOST_SPARE_PD2_MASK		0x0c00
#define HOST_PD1_MASK           0x000F
#define SYS_AP_PD1_MASK         0x00F0
#define HOST_SYS_AP_PD2_MASK    0x0F00
#define HOST_PD2_MASK           0x0003
#define SYS_AP_PD2_MASK         0x000C
#define CHIP_7185                               0x0
#define CHIP_7175                               0x1
#define CHIP_7183                               0x2
#define CHIP_7173                               0x3
#define CHIP_7185_SE                            0x4
#define CHIP_7185_HP                            0x6
#define BME_PLL_DIV_MASK		0xFF00
#define BME_PD1_MASK            0x000F
#define BME_PD2_MASK            0x0003
#define BME_FD_PD2_MASK         0x300000
#define BME_FD_PD1_MASK         0x0F000

#define cmips_cpu_clk_val		bmips_cpu_clk_val
/* 32-bits wide regs */
struct clock_values
{
	/* Current System Clock and Mips clock frequencies */
	u32 cpu_clk_val;
	u32 sys_clk_val;
    u32 bmips_cpu_clk_val;
    u32 uart_div_factor;
   	u32 ap_clk_val;
};
 void fusiv_mips_718x_get_clocks(void);

struct fusiv_clock_values
{
	/* Current System Clock, Mips clock and AP clock frequencies */
	u32 cpu_clk_val;
	u32 sys_clk_val;
   	u32 ap_clk_val;
};
void fusiv_mips_75xx_get_clocks(void);
#endif
