/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _IKAN6850_H_
#define _IKAN6850_H_
#ifdef CONFIG_FUSIV_VX180

/* MIPS HW interrupt line number used by periodic timer */
#define PERIODIC_TIMER_INT_PRIORITY 5
#define MIPS_CORE_CLK_FREQ      500*1000*1000

/* Interrupt controller registers */
typedef struct {
	unsigned long l;
	unsigned long h;
} int_stat_t;
typedef struct {
        unsigned long   ipc_id;         /* Interrupt controller ID */
        unsigned long   pad1[0x3f];     /* Reserved */
        unsigned long   cpe_prio[10];   /* CPE Interrupt Priority Registers */
        int_stat_t      cpe_stat[6];   /* CPE Interrupt Status Registers*/
        int_stat_t      emac_stat[3];   /* EMAC AP Interrupt Status Registers */
        int_stat_t      bmu_stat;       /* BMU AP Interrupt Status Registers */
        int_stat_t      wap_stat;      /* WAP AP Interrupt Register */
        int_stat_t      spa_stat;      /* SPA AP Interrupt Register */
        int_stat_t      pci_stat;       /* PCI Interrupt Registers */
        unsigned long   pad3[0x5a];     /* Reserved */
        unsigned long   cpe_per;        /* CPE Peripheral Interrupt */
} ipc_t;
/* Define Interrupt numbers. */
#define DMA_INT         0
#define ARB_INT         1
#define SCU_INT         2
#define MIPS_TIMER_INT       3
#define GPIO2_INT       4
#define DBUSA_ARB_INT        5
#define UART1_INT       6
#define SPI_INT         7
#define GPIO_INT        8
#define GPT1_INT        9
#define GPT2_INT        10
#define GPT3_INT        11
#define DBUSB_INT        12
#define GIGE2_INT       13
#define GIGE1_INT       14
#define VDSP_IDMA2_INT       15
#define VDSP_IDMA1_INT       16
#define DSP0_FL0_INT    17
#define DSP0_FL1_INT    18
#define DSP0_PF0_INT    19
#define DSP0_PF1_INT    20
#define DSP1_FL0_INT    21
#define DSP1_FL1_INT    22
#define DSP1_PF0_INT    23
#define DSP1_PF1_INT    24
#define PCI_INT         25
#define VDSL_PHY_INT         26
#define DBUSC_INT        27
#define SPA_INT       28
#define UART2           29
#define WAP_GIGE_INT        30
#define BMU_GIGE_INT   31
#define AP_DMA_INT      32
#define MIPS_SW0_INT        33
#define MIPS_SW1_INT       34
#define USBH_INT         35
#define EMAC3_INT        36
#define VDSL_AP_INT       36
#define MIPS_PC_INT       37
#define ADI_6843_MAX_INTS       38

//DDR AHB Arbiter Controller Register configurations
#define ARB_AMBA                *(volatile UINT32*)0xb9250080   //DDR AHD Arbiter Controller Register

#define ARB_STPRI2_DBUSA        (1 << 8)  //Bit 8- Enables Static Priority 2 for DBUS-A
#define ARB_STPRI2_DBUSB        (1 << 7)  //Bit 7- Enables Static Priority 2 for DBUS-B
#define ARB_STPRI2_DBUSC        (1 << 6)  //Bit 6- Enables Static Priority 2 for DBUS-C
#define ARB_STPRI2_xDSL         (1 << 5)  //Bit 5- Enables Static Priority 2 for xDSL-DBUS
#define ARB_STPRI2_MIPS         (1 << 4)  //Bit 4- Enables Static Priority 2 for MIPS-DBUS

#define ARB_STPRI3_DBUSA        (1 << 16)  //Bit 16- Enables Static Priority 3 for DBUS-A
#define ARB_STPRI3_DBUSB        (1 << 15)  //Bit 15- Enables Static Priority 3 for DBUS-B
#define ARB_STPRI3_DBUSC        (1 << 14)  //Bit 14- Enables Static Priority 3 for DBUS-C
#define ARB_STPRI3_xDSL         (1 << 13)  //Bit 13- Enables Static Priority 3 for xDSL-DBUS
#define ARB_STPRI3_MIPS         (1 << 12)  //Bit 12- Enables Static Priority 3 for MIPS-DBUS

#if CONFIG_FUSIV_AT300
typedef struct {
        unsigned long   ipc_id;         /* Interrupt controller ID */
        unsigned long   pad1[0x3f];     /* Reserved */
        unsigned long   cpe_prio[10];   /* CPE Interrupt Priority Registers */
        int_stat_t      cpe_stat[10];   /* CPE Interrupt Status Registers*/
        int_stat_t      emac_stat[3];   /* EMAC AP Interrupt Status Registers */
        int_stat_t      bmu_stat;       /* BMU AP Interrupt Status Registers */
        int_stat_t      peri_stat;      /* Peripheral AP Interrupt Register */
        int_stat_t      pci_stat;       /* PCI Interrupt Registers */
        unsigned long   pad3[0x54];     /* Reserved */
        unsigned long   cpe_per;        /* CPE Peripheral Interrupt */
} ipc_t;
/* Define Interrupt numbers. */
#define DMA_INT         0
#define ARB_INT         1
#define SCU_INT         2
#define USB_INT         3
#define WIU2_INT	4
#define WIU1_INT	5
#define UART1_INT       6
#define SPI_INT         7
#define GPIO_INT        8
#define GPT1_INT        9
#define GPT2_INT        10
#define GPT3_INT        11
#define ARB2_INT        12
#define EMAC2_INT       13
#define EMAC1_INT       14
#define IDMA2_INT       15
#define IDMA1_INT       16
#define DSP0_FL0_INT    17
#define DSP0_FL1_INT    18
#define DSP0_PF0_INT    19
#define DSP0_PF1_INT    20
#define DSP1_FL0_INT    21
#define DSP1_FL1_INT    22
#define DSP1_PF0_INT    23
#define DSP1_PF1_INT    24
#define PCI_INT         25
#define EMAC3_INT       26
#define ARB3_INT        27
#define SPORT2_RX_INT   28
#define UART2           29
#define SPA_INT		30
#define BMU_INT         31
#define AP_DMA_INT      32
#define SPORT2_TX_INT   34

#define ADI_6843_MAX_INTS       36
#endif // AT300
#endif
#endif /* _IKAN_6850_H_ */
