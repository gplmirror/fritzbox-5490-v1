/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 *
 * Info : Fusiv - NAND flash controller platfrom_device info
 */

/* struct fusiv_nand_platform
 *
 * define a interface between platfrom board specific code and
 * VX185 NFC driver.
 *
 * nr_partitions = number of partitions pointed to be partitoons (or zero)
 * partitions	 = mtd partition list
 */
#ifndef __FUSIV_NAND_H_
#define __FUSIV_NAND_H_

#include <linux/mtd/nand.h>

#ifndef CONFIG_FUSIV_NFC_DEBUG
#define CONFIG_FUSIV_NFC_DEBUG 0
#endif

/*
 * Dx() are mainly used for debugging messages, they must go away and be
 * superseded by nicer dbg_xxx() macros...
 */
#if CONFIG_FUSIV_NFC_DEBUG > 0
#define D(x) x
#else
#define D(x)
#endif

//#define FUSIV_NAND_ECC_DEBUG

/*
 * Data structures for fusiv nand flash controller driver
 */

/* fusiv nand info */
struct fusiv_nand_info {
	/* mtd info */
	struct nand_hw_control		controller;
	struct mtd_info			mtd;
	struct nand_chip		chip;

	/* platform info */
	struct fusiv_nand_platform	*platform;

	/* device info */
	struct device			*device;

	/* DMA stuff */
	struct completion		dma_completion;
};

struct fusiv_nand_platform {
	/* NAND chip information */
	unsigned short		page_size;
	unsigned short		data_width;

	/* RD/WR strobe delay timing information, all times in SCLK cycles */
	unsigned short		rd_dly;
	unsigned short		wr_dly;

	/* NAND MTD partition information */
	int                     nr_partitions;
	struct mtd_partition    *partitions;
};

/* KSEG1 uncached address of NFC NAND controller */
#define NFC_BASEADDR                0xB9090000

#define fusiv_nfc_writel(addr, datum) \
	*(volatile unsigned int *)(NFC_BASEADDR+addr) = (datum)

#define fusiv_nfc_readl(addr) \
	*(volatile unsigned int *)(NFC_BASEADDR+addr)

/* Register offsets for NFC NAND controller */
#define NFC_DATA                    0x0000
#define NFC_COMMAND                 0x8000
#define NFC_INDEX                   0x8004
#define OOB_START_OFFSET	    0x800
#define NFC_STATUS_0                0x8008
#define NFC_UNUSED_0                0x800C
#define NFC_ID_0                    0x8010
#define NFC_UNUSED_1                0x8014
#define NFC_ID_2                    0x8018
#define NFC_UNUSED_2                0x801C
#define NFC_ID_4                    0x8020
#define NFC_UNUSED_3                0x8024
#define NFC_ID_6                    0x8028
#define NFC_UNUSED_4                0x802C
#define NFC_INT_ENABLE              0x8030
#define INTE_ECCE		    (1 << 0)
#define INTE_RD	 		    (1 << 1)
#define INTE_RSEW		    (1 << 2)
#define INTE_ECCD		    (1 << 4)
#define NFC_INT_STATUS              0x8034
#define INTS_ECCE		    (1 << 0)
#define INTS_RD	 		    (1 << 1)
#define INTS_RSEW		    (1 << 2)
#define NFC_INT                     0x8038
#define NFC_GENERIC_CMD             0x803C
#define NFC_GENERIC_STATUS          0x8040
#define NFC_GENERIC_DATA            0x8044
#define NFC_EXT_INDEX               0x8048
#define NFC_TIMING_0                0x804C
#define NFC_TIMING_1                0x8050
#define NFC_CONFIG                  0x8054
#define NFC_DMA_ADDR                0x8058
#define NFC_BCH_STATUS              0x807C

/* Commands specific to Fusiv VX185 NFC */
#define NAND_CMD_CALC_ECC	0x23
#define NAND_CMD_CE_OFF		0xEE

#if defined(CONFIG_FUSIV_VX185)
#define NFC_SECTx_SYNDROMEy(x, y)   (NFC_SECT0_SYNDROME12 + ((x) * 0x10) + ((y) * 4))

#define NR_SYNDROMES                    (8)
#elif defined(CONFIG_FUSIV_VX585)
#define NFC_SECTx_SYNDROMEy(x, y)   (NFC_SECT0_SYNDROME12 + ((x) * 0x20) + ((y) * 4))

#define NR_SYNDROMES                    (16)
#endif
#define MAX_FIXABLE_ERRS                (NR_SYNDROMES / 2)

#define BCH_SECTION_SIZE	512

#define NAND_ECC_BYTE4          4
#define NAND_ECC_BYTE5          5
#define NAND_MAX_COUNT_ZERO     12
#define NAND_MIN_COUNT_ZERO     8 

#define NFC_SECT0_SYNDROME12        0x8080
#define NFC_SECT0_SYNDROME34        0x8084
#define NFC_SECT0_SYNDROME56        0x8088
#define NFC_SECT0_SYNDROME78        0x808C

#define NFC_SECT1_SYNDROME12        0x8090
#define NFC_SECT1_SYNDROME34        0x8094
#define NFC_SECT1_SYNDROME56        0x8098
#define NFC_SECT1_SYNDROME78        0x809C

#define NFC_SECT2_SYNDROME12        0x80A0
#define NFC_SECT2_SYNDROME34        0x80A4
#define NFC_SECT2_SYNDROME56        0x80A8

#define NFC_SECT3_SYNDROME12        0x80B0
#define NFC_SECT3_SYNDROME34        0x80B4
#define NFC_SECT3_SYNDROME56        0x80B8
#define NFC_SECT3_SYNDROME78        0x80BC

#define NFC_SECT4_SYNDROME12        0x80C0
#define NFC_SECT4_SYNDROME34        0x80C4
#define NFC_SECT4_SYNDROME56        0x80C8
#define NFC_SECT4_SYNDROME78        0x80CC

#define NFC_SECT5_SYNDROME12        0x80D0
#define NFC_SECT5_SYNDROME34        0x80D4
#define NFC_SECT5_SYNDROME56        0x80D8
#define NFC_SECT5_SYNDROME78        0x80DC

#define NFC_SECT6_SYNDROME12        0x80E0
#define NFC_SECT6_SYNDROME34        0x80E4
#define NFC_SECT6_SYNDROME56        0x80E8
#define NFC_SECT6_SYNDROME78        0x80EC

#define NFC_SECT7_SYNDROME12        0x80F0
#define NFC_SECT7_SYNDROME34        0x80F4
#define NFC_SECT7_SYNDROME56        0x80F8
#define NFC_SECT7_SYNDROME78        0x80FC

/* Bit fields for NFC_TIMING_0 register */
#define T0_TS(x) (((x) & 0xff) << 0)
#define T0_TWP(x) (((x) & 0xff) << 8)
#define T0_TRP(x) (((x) & 0xff) << 16)
#define T0_TH(x) (((x) & 0xff) << 24)

/* Bit fields for NFC_TIMING_1 register */
#define T1_TWH(x) (((x) & 0xff) << 0)
#define T1_TRH(x) (((x) & 0xff) << 8)
#define T1_TRR(x) (((x) & 0xff) << 16)
#define T1_TWB(x) (((x) & 0xff) << 24)

/* Bit fields for NFC_CONFIG register */
#define C_CSZ(x) (((x) & 0x7) << 0)
#define C_BNK(x) (((x) & 0x3) << 6)
#define C_PSZ(x) (((x) & 0x3) << 8)
#define C_WPR(x) (((x) & 0x1) << 10)
#define C_RAC(x) (((x) & 0x1) << 12)

#define NAND_PAGE_SIZE_4KB	4096
#define NAND_ECC_64          	64
#define NAND_ECC_128          	128
#define NAND_PAGE_SHIFT_4KB	13
#define NAND_PAGE_SIZE_2KB	2048
#define NAND_ECC_32		32
#define NAND_PAGE_SHIFT_2KB	12

#define NAND_OOB_224		224
#define NAND_BLOCKSIZE_512KB	524288
#endif
