/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_LOG_MGR_H__

#define __IKN_LOG_MGR_H__
#include "ikn_types.h"
#define   IKN_SPI_API        1
#define   IKN_SPI_HW_MOD     2
#define   IKN_GPIO_HW_MOD    3


typedef enum
{
    IKN_LOG_MSG_DEBUG,
    IKN_LOG_MSG_ERROR,
    IKN_LOG_MSG_BUG,
    IKN_LOG_MSG_WARN,
    IKN_LOG_MSG_INFO
}eLogLevel_t;


typedef struct iknLogMsgString_s
{
    eLogLevel_t eLogLevel;
    CHAR * pcLogLevelStr;

}iknLogMsgString_t;

typedef struct iknLogRegModules_s
{
    UINT32 uiModId;
    CHAR *pcModuleString;
}iknLogRegModules_t;

VOID iknLogMessage(UINT32 uiModId,eLogLevel_t eLogLevel, CHAR *pcLogMsg,...);

#endif //__IKN_LOG_MGR_H__
