/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_SCU_H__

#define __IKN_SCU_H__
#include "ikn_types.h"

UINT32 iknGetSysClock(VOID);
#endif //__IKN_SCU_H__
