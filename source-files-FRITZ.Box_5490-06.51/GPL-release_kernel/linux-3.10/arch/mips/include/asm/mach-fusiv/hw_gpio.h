/*------------------------------------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------------------------------------*/
#ifndef _hw_gpio_h_
#define _hw_gpio_h_


#define GPIO_BITS               32

#if defined(CONFIG_FUSIV_VX180)
#define GPIO_BIT_SPI_ERR        0   /*--- SPI Master mode input  ---*/
#define GPIO_BIT_SPI_CS1_OUT    1   /*--- SPI Master mode ---*/
#define GPIO_BIT_SPI_CS2_OUT    2   /*--- SPI Master mode ---*/
#define GPIO_BIT_UART2_TXD      3 
#define GPIO_BIT_UART2_RXD      4 
#define GPIO_BIT_UART2_CTS      5 
#define GPIO_BIT_UART2_RTS      6 
#define GPIO_BIT_DSP1_PF2       7   /*--- NOTE: GPIO 7 to GPIO 10 are connected to PF flags. ---*/
#define GPIO_BIT_DSP1_PF3       8 
#define GPIO_BIT_DSP2_PF2       9 
#define GPIO_BIT_DSP2_PF3       10
#define GPIO_BIT_SPI_CS2        11
#define GPIO_BIT_SPI_CS3        12
#define GPIO_BIT_DYING_GASP     13  /*--- NOTE: GPIO 13 is reserved for Dying Gasp function. ---*/
#define GPIO_BIT_14             14
#define GPIO_BIT_15             15  /* Ethernet switch */
#define GPIO_BIT_16             16  /*--- / VDSL PHY GPIO 0 ---*/
#define GPIO_BIT_17             17  /*--- / VDSL PHY GPIO 1 ---*/
#define GPIO_BIT_18             18  /*--- / VDSL PHY GPIO 2 ---*/
#define GPIO_BIT_19             19  /*--- / VDSL PHY GPIO 3 ---*/
#define GPIO_BIT_20             20
#define GPIO_BIT_21             21
#define GPIO_BIT_22             22
#define GPIO_BIT_23             23
#define GPIO_BIT_24             24
#define GPIO_BIT_25             25
#define GPIO_BIT_26             26
#define GPIO_BIT_27             27
#define GPIO_BIT_28             28
#define GPIO_BIT_29             29
#define GPIO_BIT_30             30
#define GPIO_BIT_31             31


#define GPIO_MASK_SPI_ERR       (1 << 0)
#define GPIO_MASK_SPI_CS1_OUT   (1 << 1 )
#define GPIO_MASK_SPI_CS2_OUT   (1 << 2 )
#define GPIO_MASK_UART2_TXD     (1 << 3 )
#define GPIO_MASK_UART2_RXD     (1 << 4 )
#define GPIO_MASK_UART2_CTS     (1 << 5 )
#define GPIO_MASK_UART2_RTS     (1 << 6 )
#define GPIO_MASK_DSP1_PF2      (1 << 7 )
#define GPIO_MASK_DSP1_PF3      (1 << 8 )
#define GPIO_MASK_DSP2_PF2      (1 << 9 )
#define GPIO_MASK_DSP2_PF3      (1 << 10)
#define GPIO_MASK_SPI_CS2       (1 << 11)
#define GPIO_MASK_SPI_CS3       (1 << 12)
#define GPIO_MASK_DYING_GASP    (1 << 13)
#define GPIO_MASK_14            (1 << 14)
#define GPIO_MASK_15            (1 << 15)
#define GPIO_MASK_16            (1 << 16)
#define GPIO_MASK_17            (1 << 17)
#define GPIO_MASK_18            (1 << 18)
#define GPIO_MASK_19            (1 << 19)
#define GPIO_MASK_20            (1 << 20)
#define GPIO_MASK_21            (1 << 21)
#define GPIO_MASK_22            (1 << 22)
#define GPIO_MASK_23            (1 << 23)
#define GPIO_MASK_24            (1 << 24)
#define GPIO_MASK_25            (1 << 25)
#define GPIO_MASK_26            (1 << 26)
#define GPIO_MASK_27            (1 << 27)
#define GPIO_MASK_28            (1 << 28)
#define GPIO_MASK_29            (1 << 29)
#define GPIO_MASK_30            (1 << 30)
#define GPIO_MASK_31            (1 << 31)

#elif defined(CONFIG_FUSIV_VX185)

#define GPIO_BIT_SPI0_CS_FLASH  0
#define GPIO_BIT_USB1_PWR_EN    1
#define GPIO_BIT_SPI0_WP_FLASH  2
#define GPIO_BIT_VOIP_RST       3
#define GPIO_BIT_USB1_OC_FAULT  4
#define GPIO_BIT_SPI1_CS1_DECT  5
#define GPIO_BIT_SPI1_CS2_VOIP  6
#define GPIO_BIT_USB0_PWR_EN    7
#define GPIO_BIT_USB0_OC_FAULT  8
#define GPIO_BIT_DECT_RST       9
#define GPIO_BIT_ETH_RESET      10
#define GPIO_BIT_UART2_TXD      11
#define GPIO_BIT_UART2_RXD      12
#define GPIO_BIT_TP_13          13
#define GPIO_BIT_TP_14          14
#define GPIO_BIT_PCIE_RESET     15
#define GPIO_BIT_16             16
#define GPIO_BIT_17             17
#define GPIO_BIT_18             18
#define GPIO_BIT_19             19
#define GPIO_BIT_20             20
#define GPIO_BIT_21             21
#define GPIO_BIT_22             22
#define GPIO_BIT_23             23
#define GPIO_BIT_24             24
#define GPIO_BIT_PERIPH_RESET   25
#define GPIO_BIT_ETH_IRQ        26
#define GPIO_BIT_DECT_IRQ       27
#define GPIO_BIT_DECT_BUTTON    28
#define GPIO_BIT_WLAN_BUTTON    29
#define GPIO_BIT_DYING_GASP     30
#define GPIO_BIT_VOIP_IRQ       31

#define GPIO_MASK_SPI0_CS_FLASH  (1 << 0)
#define GPIO_MASK_USB1_PWR_EN    (1 << 1)
#define GPIO_MASK_SPI0_WP_FLASH  (1 << 2)
#define GPIO_MASK_VOIP_RST       (1 << 3)
#define GPIO_MASK_USB1_OC_FAULT  (1 << 4)
#define GPIO_MASK_SPI1_CS1_DECT  (1 << 5)
#define GPIO_MASK_SPI1_CS2_VOIP  (1 << 6)
#define GPIO_MASK_USB0_PWR_EN    (1 << 7)
#define GPIO_MASK_USB0_OC_FAULT  (1 << 8)
#define GPIO_MASK_DECT_RST       (1 << 9)
#define GPIO_MASK_ETH_RESET      (1 << 10)
#define GPIO_MASK_UART2_TXD      (1 << 11)
#define GPIO_MASK_UART2_RXD      (1 << 12)
#define GPIO_MASK_TP_13          (1 << 13)
#define GPIO_MASK_TP_14          (1 << 14)
#define GPIO_MASK_PCIE_RESET     (1 << 15)
#define GPIO_MASK_16             (1 << 16)
#define GPIO_MASK_17             (1 << 17)
#define GPIO_MASK_18             (1 << 18)
#define GPIO_MASK_19             (1 << 19)
#define GPIO_MASK_20             (1 << 20)
#define GPIO_MASK_21             (1 << 21)
#define GPIO_MASK_22             (1 << 22)
#define GPIO_MASK_23             (1 << 23)
#define GPIO_MASK_24             (1 << 24)
#define GPIO_MASK_PERIPH_RESET   (1 << 25)
#define GPIO_MASK_ETH_IRQ        (1 << 26)
#define GPIO_MASK_DECT_IRQ       (1 << 27)
#define GPIO_MASK_DECT_BUTTON    (1 << 28)
#define GPIO_MASK_WLAN_BUTTON    (1 << 29)
#define GPIO_MASK_DYING_GASP     (1 << 30)
#define GPIO_MASK_VOIP_IRQ       (1 << 31)

#define GPIO_MODE_MASK           (0x3)
#define GPIO_PIN_SHIFT(pin)      (2 * (pin))
#define GPIO_MODE_VAL(pin, mode)    (((mode) & GPIO_MODE_MASK) << GPIO_PIN_SHIFT(pin))
#define GPIO_COUNT                32
#endif // if defined(CONFIG_FUSIV_VX180) elif defined(CONFIG_FUSIV_VX185)

enum _hw_gpio_mode {
    GPIO_MODE_INPUT  = 0x00,
    GPIO_MODE_OUTPUT = 0x01,
    GPIO_MODE_OD     = 0x02,
    GPIO_MODE_OS     = 0x03,
    GPIO_MODE_LAST   = GPIO_MODE_OS,
};

enum _hw_gpio_direction {
    GPIO_OUTPUT_PIN = 0,
    GPIO_INPUT_PIN  = 1,
};

enum _hw_gpio_function {
    FUNCTION_PIN = 0,
    GPIO_PIN     = 1,
};

enum _hw_gpio_polarity {
    GPIO_ACTIVE_HIGH = 0,
    GPIO_ACTIVE_LOW  = 1
};

enum _hw_gpio_sensitivity {
    GPIO_LEVEL_SENSITIVE       = 0,
    GPIO_EDGE_SENSITIVE        = 1,
    GPIO_BOTH_EDGES_SENSITIVE  = 2
};


typedef enum _hw_gpio_direction     GPIO_DIR;
typedef enum _hw_gpio_function      GPIO_MODE;
typedef enum _hw_gpio_polarity      GPIO_POLAR;
typedef enum _hw_gpio_sensitivity   GPIO_SENSE;

struct _hw_gpio_irqhandle {
    int enabled;
    unsigned int mask;
    unsigned int is_edge, is_both_edges, is_active_low;
    int delayed_irq;
    int (*func)(unsigned int mask);
    struct _hw_gpio_irqhandle *next;
};

struct ikanos_gpio_config {
    unsigned int valid;
    unsigned int dir;
    unsigned int mode;
    unsigned int func_sel;
    unsigned int func_val;
};


#endif /*--- #ifndef _hw_gpio_h_ ---*/

