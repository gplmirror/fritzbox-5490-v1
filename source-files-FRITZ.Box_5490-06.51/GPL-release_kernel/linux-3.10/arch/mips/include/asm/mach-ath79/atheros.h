/*
 * vim: tabstop=8 : noexpandtab
 */
#ifndef _ATHEROS_H
#define _ATHEROS_H

#ifndef __DTS__

#include <linux/compiler.h>
#include <linux/types.h>
#include <asm/addrspace.h>
#include <asm/barrier.h>
#include <asm/mach-ath79/ath79.h>

#endif /* __DTS__ */

#include "ath_reg_compatibility.h"

#include <asm/mach-ath79/ar71xx_regs.h>

#ifndef __DTS__
#ifndef __ASSEMBLY__

#define ATH_CONSOLE_BAUD	115200

#define ath_arch_init_irq() /* nothing */

typedef unsigned int ath_reg_t;

#define ath_reg_rd(_phys)	(*(volatile ath_reg_t *)KSEG1ADDR(_phys))

#define ath_reg_wr_nf(_phys, _val) \
	((*(volatile ath_reg_t *)KSEG1ADDR(_phys)) = (_val)); wmb() 

#define ath_reg_wr(_phys, _val) do {	\
	ath_reg_wr_nf(_phys, _val);	\
	ath_reg_rd(_phys);		\
} while(0)

#define ath_reg_rmw_set(_reg, _mask)	do {			\
	ath_reg_wr((_reg), (ath_reg_rd((_reg)) | (_mask)));	\
	ath_reg_rd((_reg));					\
} while(0)

#define ath_reg_rmw_clear(_reg, _mask) do {			\
	ath_reg_wr((_reg), (ath_reg_rd((_reg)) & ~(_mask)));	\
	ath_reg_rd((_reg));					\
} while(0)

#define UART_READ(y)		ath_reg_rd((ATH_UART_BASE+y))
#define UART_WRITE(x, z)	ath_reg_wr((ATH_UART_BASE+x), z)

#endif /* __ASSEMBLY__ */
#endif /* __DTS__ */

#define REG_OFFSET		4

#define OFS_RCV_BUFFER		(0 * REG_OFFSET)
#define OFS_TRANS_HOLD		(0 * REG_OFFSET)
#define OFS_SEND_BUFFER		(0 * REG_OFFSET)
#define OFS_INTR_ENABLE		(1 * REG_OFFSET)
#define OFS_INTR_ID		    (2 * REG_OFFSET)
#define OFS_DATA_FORMAT		(3 * REG_OFFSET)
#define OFS_LINE_CONTROL	(3 * REG_OFFSET)
#define OFS_MODEM_CONTROL	(4 * REG_OFFSET)
#define OFS_RS232_OUTPUT	(4 * REG_OFFSET)
#define OFS_LINE_STATUS		(5 * REG_OFFSET)
#define OFS_MODEM_STATUS	(6 * REG_OFFSET)
#define OFS_RS232_INPUT		(6 * REG_OFFSET)
#define OFS_SCRATCH_PAD		(7 * REG_OFFSET)

#define OFS_DIVISOR_LSB		(0 * REG_OFFSET)
#define OFS_DIVISOR_MSB		(1 * REG_OFFSET)

#define RST_GENERAL_BASE		0xb8060000
#define DDR_CONFIG_BASE			0xb8000000
#define DDRMON_CTL_OFFSET		0x00e8
#define DDRMON_GNT_OFFSET		0x00f8

#if defined(CONFIG_SOC_AR724X)
#	include <724x.h>
#elif defined(CONFIG_SOC_AR933X)
#	include <933x.h>
#elif defined(CONFIG_SOC_AR934X)
#	include <934x.h>
#elif defined(CONFIG_SOC_QCA955X)
#	ifndef CONFIG_CPU_HAS_DSP_ASE
#	error "Cannot build for QCA 955x without DSP ASE support"
#	endif
#	include <955x.h>

#elif defined(CONFIG_SOC_QCA953X)  // [GJu] support for HoneyBee/Dragonfly
#	include <953x.h>
#elif defined(CONFIG_SOC_QCA956X)
#	include <956x.h>
#else
#	error "Building atheros BSP for unknown device"
#endif

#ifndef __DTS__
#ifndef __ASSEMBLY__

/*
 * GPIO Access & Control
 */
void ath_gpio_init(void);
void ath_gpio_down(void);
void ath_gpio_up(void);

void ath_gpio_irq_init(int);
/*
 * GPIO Helper Functions
 */
void ath_gpio_enable_slic(void);

/* enable UART block, takes away GPIO 10 and 9 */
void ath_gpio_enable_uart(void);

/* enable STEREO block, takes away GPIO 11,8,7, and 6 */
void ath_gpio_enable_stereo(void);

/* allow CS0/CS1 to be controlled via SPI register, takes away GPIO0/GPIO1 */
void ath_gpio_enable_spi_cs1_cs0(void);

/* allow GPIO0/GPIO1 to be used as SCL/SDA for software based i2c */
void ath_gpio_enable_i2c_on_gpio_0_1(void);

/*
 * GPIO General Functions
 */
void ath_gpio_drive_low(unsigned int mask);
void ath_gpio_drive_high(unsigned int mask);

unsigned int ath_gpio_float_high_test(unsigned int mask);

void ath_gpio_config_output(int gpio);
void ath_gpio_config_input(int gpio);
void ath_gpio_set_fn(int gpio, int fn);

/*
 * Software support of i2c on gpio 0/1
 */
int ath_i2c_raw_write_bytes_to_addr(int addr, unsigned char *buffer, int count);
int ath_i2c_raw_read_bytes_from_addr(int addr, unsigned char *buffer, int count);


/* Low-level routines */
void ath_dma_addr_wr(int chan, unsigned int val);
void ath_dma_config_wr(int chan, unsigned int val);
void ath_dma_update_wr(int chan, unsigned int val);

unsigned int ath_dma_addr_rd(int chan);
unsigned int ath_dma_config_rd(int chan);

/* Use this routine to configure DMA access. Example:
 *
 * ath_dma_config_buffer( ATH_DMA_CHAN_SLIC0_TX,
 *				< address of buffer >,
 *				ATH_DMA_BUF_SIZE_512X2
 */
void ath_dma_config_buffer(int chan, void *buffer, int sizeCfg);

/*
 * SLIC Helper Functions
 */
unsigned int ath_slic_status_rd(void);
unsigned int ath_slic_cntrl_rd(void);

void ath_slic_cntrl_wr(unsigned int val);
void ath_slic_0_slot_pos_wr(unsigned int val);
void ath_slic_1_slot_pos_wr(unsigned int val);
void ath_slic_freq_div_wr(unsigned int val);
void ath_slic_sample_pos_wr(unsigned int val);

void ath_slic_setup(int _sam, int _s0n, int _s1n);

#ifdef CONFIG_AVM_AUDIO
/*
 * Audio data is little endian so 16b values must be swapped in the DMA buffers.
 */
static inline int
ath_stereo_sample_16b_cvt(unsigned int _v)
{
	return (((_v<<8)&0xff00)|((_v>>8)&0xff)) & 0xffff;
}

/* Low level read/write of configuration */
void ath_stereo_config_wr(unsigned int val);
void ath_stereo_volume_wr(unsigned int val);

unsigned int ath_stereo_config_rd(void);
unsigned int ath_stereo_volume_rd(void);

/* Routine sets up STEREO block for use. Use one of the predefined
 * configurations. Example:
 *
 * ath_stereo_config_setup(
 *	ATH_STEREO_CFG_MASTER_STEREO_FS32_48KHZ(ATH_STEREO_WS_16B))
 *
 */
void ath_stereo_config_setup(unsigned int cfg);

/* 48 kHz, 16 bit data & i2s 32fs */
static inline void
ath_setup_for_stereo_master(int ws)
{
	ath_stereo_config_setup(ATH_STEREO_CFG_MASTER_STEREO_FS32_48KHZ(ws));
}

/* 48 kHz, 16 bit data & 32fs i2s */
static inline void
ath_setup_for_stereo_slave(int ws)
{
	ath_stereo_config_setup(ATH_STEREO_CFG_SLAVE_STEREO_FS32_48KHZ(ws));
}
#endif

void ath_reset(unsigned int mask);
int ath_local_read_config(int bus, int where, int size, u32 *value);
int ath_local_write_config(int bus, int where, int size, u32 value);
int ath_check_error(int verbose);
unsigned char __ath_readb(const volatile void __iomem *p);
unsigned short __ath_readw(const volatile void __iomem *p);
void ap_usb_led_on(void);
void ap_usb_led_off(void);

#ifdef CONFIG_PCI
void ath_pci_irq_init(int);
#endif

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
struct _nmi_workaround_func {
    void (*cb_ath_workaround_nmi_suspendresume)(unsigned int suspend);
    void (*cb_ath_workaround_nmi_info)(const char *prefix);
    void (*cb_ath_workaround_nmi_check_cpugrant)(void);
    void (*cb_ath_workaround_nmi_suspend)(void);
    void (*cb_ath_workaround_watchdog)(unsigned int trigger_sec);
    int  (*cb_ath_workaround_nmi_link)(char *name);
    void (*cb_ath_workaround_nmi_unlink)(int handle);
    unsigned int (*cb_ath_workaround_nmi_link_settimer)(int handle, unsigned int timecount);
    unsigned int (*cb_ath_workaround_nmi_wastetime)(void);
};
extern struct _nmi_workaround_func nmi_workaround_func;
#define ath_workaround_nmi_suspendresume(a)  if(nmi_workaround_func.cb_ath_workaround_nmi_suspendresume)  nmi_workaround_func.cb_ath_workaround_nmi_suspendresume(a)
#define ath_workaround_nmi_info(a)	     if(nmi_workaround_func.cb_ath_workaround_nmi_info)		  nmi_workaround_func.cb_ath_workaround_nmi_info(a)
#define ath_workaround_nmi_check_cpugrant()  if(nmi_workaround_func.cb_ath_workaround_nmi_check_cpugrant) nmi_workaround_func.cb_ath_workaround_nmi_check_cpugrant()
#define ath_workaround_nmi_stop()	     if(nmi_workaround_func.cb_ath_workaround_nmi_suspend)	  nmi_workaround_func.cb_ath_workaround_nmi_suspend()
#define ath_workaround_watchdog(a)	     if(nmi_workaround_func.cb_ath_workaround_watchdog)		  nmi_workaround_func.cb_ath_workaround_watchdog(a)
#define ath_workaround_nmi_link(a)	     nmi_workaround_func.cb_ath_workaround_nmi_link		? nmi_workaround_func.cb_ath_workaround_nmi_link(a) : -1
#define ath_workaround_nmi_unlink(a)	     if(nmi_workaround_func.cb_ath_workaround_nmi_unlink)         nmi_workaround_func.cb_ath_workaround_nmi_unlink(a)
#define ath_workaround_nmi_link_settimer(a,b) (nmi_workaround_func.cb_ath_workaround_nmi_link_settimer) ? nmi_workaround_func.cb_ath_workaround_nmi_link_settimer(a,b) : 0
#define ath_workaround_nmi_wastetime()        (nmi_workaround_func.cb_ath_workaround_nmi_wastetime)	? nmi_workaround_func.cb_ath_workaround_nmi_wastetime() : 0

#define ath_workaround_nmi_installed()	    nmi_workaround_func.cb_ath_workaround_nmi_suspendresume ? 1 : 0
#define	ATH_NMI_TRIGGER			160     /*--- 40 MHZ-cycles (1 us == 40)  ---*/
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

#endif /* __ASSEMBLY__ */

extern void ath_restart(char *command);

#endif /* __DTS__ */

#endif /* _ATHEROS_H */
