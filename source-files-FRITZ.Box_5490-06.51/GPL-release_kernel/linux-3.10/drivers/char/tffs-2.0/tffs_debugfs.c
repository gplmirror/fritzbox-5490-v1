#include <linux/debugfs.h>
#include <linux/tffs.h>
#include "tffs_local.h"

u32 tffs_debug_mtd_reread_after_write = 0;
u32 tffs_debug_auto_enable_ssc_fifo_debug = 0;

struct tffs_debug_info {
	const char *name;
	struct file_operations fops;
	struct dentry *dentry;
	union {
		void *data;
		int (*seq_show_func)(struct seq_file *, void *);
	};
};

static struct tffs_debug_info tffs_debug_info[];
static struct dentry *_tffs_debug;

static int _index_show(struct seq_file *s, void *data __maybe_unused)
{
	size_t index;

	for (index = 0; index < FLASH_FS_ID_LAST; index++) {
		union tul {
			struct _TFFS_Entry *t;
			unsigned long ul;
		};
		const union tul free = { .ul = FLASH_FS_ID_FREE, };
		union tul entry;

		if (TFFS_Global_Index[index] == free.t)
			continue;

		entry.t = TFFS_Global_Index[index];

		seq_printf(s, "[index=0x%05x] = 0x%lx\n", index, entry.ul);
	}

	return 0;
}

static int _seq_open_data(struct inode *inode, struct file *file)
{
	int (*show)(struct seq_file *, void *);

	show = inode->i_private;

	return single_open(file, show, NULL);
}

static struct tffs_debug_info tffs_debug_info[] = {
	{
		.name = "index",
		.fops = {
			.open = _seq_open_data,
			.llseek = seq_lseek,
			.read = seq_read,
			.release = single_release,
		},
		.seq_show_func = _index_show,
	},
	{ }
};

int tffs_debugfs_init(void)
{
	struct tffs_debug_info *tdi;

	if (!tffs_debugfs_is_enabled)
		return 0;

	_tffs_debug = debugfs_create_dir("tffs", NULL);
	if (!_tffs_debug || (PTR_ERR(_tffs_debug) == -ENODEV)) {
		pr_err("%s: <debugfs>/tffs: Unable to create\n", __func__);
		return -ENODEV;
	}

	for (tdi = tffs_debug_info; tdi->name; tdi++) {
		umode_t mode = 0;
		mode |= (tdi->fops.read) ? 0444 : 0;
		mode |= (tdi->fops.write) ? 0222 : 0;

		tdi->dentry = debugfs_create_file(tdi->name, mode, _tffs_debug,
						  tdi->data, &tdi->fops);

		if (!tdi->dentry) {
			pr_err("%s: <debugfs>/tffs/%s: Unable to create "
			       "(error: %lu)\n",
			       __func__, tdi->name, PTR_ERR(tdi->dentry));
			tdi->dentry = NULL;
		}
	}

	if (!debugfs_create_bool("reread-after-mtd_write", 0666, _tffs_debug,
				 &tffs_debug_mtd_reread_after_write))
		pr_err("%s: <debugfs>/tffs/reread-after-mtd_write: Unable to "
		       "create\n", __func__);

	if (!debugfs_create_bool("auto-enable-ssc-fifo-debug", 0666,
				 _tffs_debug,
				 &tffs_debug_auto_enable_ssc_fifo_debug))
		pr_err("%s: <debugfs>/tffs/auto-enable-ssc-fifo-debug: Unable "
		       "to create\n", __func__);

	return 0;
}
