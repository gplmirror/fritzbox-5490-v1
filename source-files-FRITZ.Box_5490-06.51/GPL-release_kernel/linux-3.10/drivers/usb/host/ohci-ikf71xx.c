/*
 * OHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2002 David Brownell <dbrownell@users.sourceforge.net>
 * (C) Copyright 2002 Hewlett-Packard Company
 *
 * Bus Glue for AMD Alchemy Au1xxx
 * Bus Glue for IKANOS's IKF68XX
 *
 * Written by Christopher Hoover <ch@hpl.hp.com>
 * Based on fragments of previous driver by Rusell King et al.
 *
 * Modified for LH7A404 from ohci-sa1111.c
 *  by Durgesh Pattamatta <pattamattad@sharpsec.com>
 * Modified for AMD Alchemy Au1xxx
 *  by Matt Porter <mporter@kernel.crashing.org>
 * Modified for IKANOS's IKF68XX 
 *  by Vivek Dharmadhikari<vivek.dharmadhikari@analog.com>
 * Modified for IKANOS's IKF71XX 
 *  by Kiran Kumar C.S.K <kcsk@ikanos.com>
 *
 * This file is licenced under the GPL.
 */

/**
 * Some part of this file is modified by Ikanos Communications. 
 *
 * Copyright (C) 2013-2014 Ikanos Communications.
 */

#include <linux/platform_device.h>
#include <fusiv_usb.h>
#include <vx185_scu.h>

#define USBH_ENABLE_BE (1<<0)
#define USBH_ENABLE_C  (1<<1)
#define USBH_ENABLE_E  (1<<2)
#define USBH_ENABLE_CE (1<<3)
#define USBH_ENABLE_RD (1<<4)

#ifdef __LITTLE_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C)
#elif __BIG_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C | USBH_ENABLE_BE)
#else
#error not byte order defined
#endif

extern int usb_disabled(void);

static unsigned char fusiv_usb_enable;

/* USB PHY Reset sequence, UTMI configuration */
static void do_phy_reset (void) {

	/*
         * UTMI PHY port 0 & 1 reset, POR and register reset
	 * Enable big-endian for transactions data , INCR4/8/16 xfers,
         * start on burst boundaries, AP_START_CLOCK is on
         */
	*USB_GNL_REG_1 = 0x2FFFF;
  
	udelay(1000);

	/* Deassert PHY Reset */
	*USB_GNL_REG_1 = 0x2FFFE;

	udelay(1000);

	/* Deassert POR & port resets */
	*USB_GNL_REG_1 = 0x2FFF0;

	udelay(1000);
}

//*-------------------------------------------------------------------------*/
static void ikf71xx_start_ohci_hc(struct platform_device *dev)
{
        /* address of Clock Gate Control Register  */
        volatile int regval = 0;

	printk(KERN_DEBUG __FILE__
		": starting IKF71XX OHCI USB Host Controller\n");

	/* Should not issue reset if EHCI driver has already done it */
	if ((fusiv_usb_enable & FUSIV_USB_EHCI_ON) == 0) {

		printk("VX185 OHCI: Enabling USB clocks and USB PHY reset\n");

		/* Enable USB clock */
		regval = scu_regs->clk_gate_ctl;
		if (!(regval & (0x1 << USB_CLK_GATE))) {
			regval |= (0x1 << USB_CLK_GATE);
			scu_regs->clk_gate_ctl = regval;
		} 
	
		/* Enable USB clock PLL Post Div 3 Register */
		regval = scu_regs->pll_pd3_ctl;
		if (!(regval & (0x1 << USB_CLK_PLL_PDIV3))) {
			regval |= (0x1 << USB_CLK_PLL_PDIV3);
			scu_regs->pll_pd3_ctl = regval;
		}

		/* Unmask USB reset */
		regval = scu_regs->rst_mask;
		if (regval & (0x1 << USB_RESET))
		{
			regval &= ~(0x1 << USB_RESET);
			scu_regs->rst_mask = regval;
		} 

		/* Assert USB reset */
		regval = scu_regs->rst_vec;
		if (!(regval & (0x1 << USB_RESET))) {
			regval |= (0x1 << USB_RESET);
			scu_regs->rst_vec = regval;
		} 

		udelay(1000);

		/* Mask USB reset back */
		regval = scu_regs->rst_mask;
		if (!(regval & (0x1 << USB_RESET)))
		{
			regval |= (0x1 << USB_RESET);
			scu_regs->rst_mask = regval;
		} 

		/* UTMI PHY reset */
		do_phy_reset();
	}

	printk(KERN_DEBUG __FILE__ ": Clock to USB host has been enabled \n");

}

static void ikf71xx_stop_ohci_hc(struct platform_device *dev)
{
	volatile uint regval = 0;

	pr_debug(__FILE__ ": stopping IKF71XX OHCI USB Controller\n");

	if ((fusiv_usb_enable & FUSIV_USB_EHCI_ON) == 0) {
		/* Disable USB clock PLL Post Div 3 Register */
		regval = scu_regs->pll_pd3_ctl;
		if (regval & (0x1 << USB_CLK_PLL_PDIV3)) {
			regval &= ~(0x1 << USB_CLK_PLL_PDIV3);
			scu_regs->pll_pd3_ctl = regval;
		} 

		/* Disable USB clock */
		regval = scu_regs->clk_gate_ctl;
		if (regval & (0x1 << USB_CLK_GATE)) {
			regval &= ~(0x1 << USB_CLK_GATE);
			scu_regs->clk_gate_ctl = regval;
		}
	} 

}

/*-------------------------------------------------------------------------*/

/* configure so an HC device and id are always provided */
/* always called with process context; sleeping is OK */


/**
 * usb_hcd_ikf71xx_ohci_probe - initialize ikf71xx-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int usb_hcd_ikf71xx_ohci_probe (const struct hc_driver *driver,
			  struct platform_device *dev)
{
	int retval;
	struct usb_hcd *hcd;
	struct ohci_hcd *ohci;

	printk(KERN_DEBUG __FILE__": Probing USB OHCI  \n");

        /* offset 1 contains IRQ info */
	if(dev->resource[1].flags != IORESOURCE_IRQ) {
		pr_debug ("resource[1] is not IORESOURCE_IRQ");
		return -ENOMEM;
	}

	hcd = usb_create_hcd(driver, &dev->dev, "ikf71xx ohci");
	if (!hcd)
		return -ENOMEM;

        /* offset 0 contains OHCI info */
	hcd->rsrc_start = dev->resource[0].start;
	hcd->rsrc_len = dev->resource[0].end - dev->resource[0].start + 1;

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		pr_debug("request_mem_region failed");
		retval = -EBUSY;
		goto err1;
	}

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		pr_debug("ioremap failed");
		retval = -ENOMEM;
		goto err2;
	}

        ikf71xx_start_ohci_hc(dev);

	ohci_hcd_init(hcd_to_ohci(hcd));

	ohci = hcd_to_ohci(hcd);
	ohci->flags |= OHCI_QUIRK_FRAME_NO;

#if (defined(CONFIG_FUSIV_VX185)  || defined(CONFIG_FUSIV_VX585)) &&  defined (CONFIG_CPU_MIPSR2_IRQ_VI)
 	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_SHARED | IRQF_DISABLED );
#else
	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_DISABLED );
#endif

	if (retval == 0) {
		fusiv_usb_enable |= FUSIV_USB_OHCI_ON;
		return retval;
	}

	ikf71xx_stop_ohci_hc(dev);
	iounmap(hcd->regs);
	
 err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
 err1:
	usb_put_hcd(hcd);
	return retval;
}


/* may be called without controller electrically present */
/* may be called with controller, bus, and devices active */

/**
 * usb_hcd_ikf71xx_remove - shutdown processing for ikf71xx-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_hcd_ikf71xx_ohci_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void usb_hcd_ikf71xx_remove (struct usb_hcd *hcd, struct platform_device *dev)
{
	usb_remove_hcd(hcd);
	pr_debug("stopping IKF71XX USB OHCI Controller\n");
	ikf71xx_stop_ohci_hc(dev);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
	fusiv_usb_enable &= ~FUSIV_USB_OHCI_ON;

}

/*-------------------------------------------------------------------------*/

static int 
ohci_ikf71xx_start (struct usb_hcd *hcd)
{
	struct ohci_hcd	*ohci = hcd_to_ohci (hcd);
	int		ret;

	ohci_dbg (ohci, "ohci_ikf71xx_start, ohci:%p", ohci);

	if ((ret = ohci_init (ohci)) < 0)
		return ret;

	if ((ret = ohci_run (ohci)) < 0) {
		//err ("can't start %s", hcd->self.bus_name); commented for 3.10.28
		ohci_stop (hcd);
		return ret;
	}

	return 0;
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ohci_ikf71xx_hc_driver = {
	.description =		hcd_name,
	.product_desc =		"Ikanos On-Chip OHCI Host Controller",
	.hcd_priv_size =	sizeof(struct ohci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq =			ohci_irq,
	.flags =		HCD_USB11 | HCD_MEMORY,

	/*
	 * basic lifecycle operations
	 */
	.start =		ohci_ikf71xx_start,
#ifdef	CONFIG_PM
	.bus_suspend=		ohci_bus_suspend,  
	.bus_resume=		ohci_bus_resume,  
#endif /*CONFIG_PM*/
	.stop =			ohci_stop,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue =		ohci_urb_enqueue,
	.urb_dequeue =		ohci_urb_dequeue,
	.endpoint_disable =	ohci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number =	ohci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data =	ohci_hub_status_data,
	.hub_control =		ohci_hub_control,
};

/*-------------------------------------------------------------------------*/

static int ohci_hcd_ikf71xx_drv_probe(struct platform_device *pdev)
{
	int ret;

	pr_debug ("In ohci_hcd_ikf71xx_drv_probe");

	if (usb_disabled())
		return -ENODEV;

	ret = usb_hcd_ikf71xx_ohci_probe(&ohci_ikf71xx_hc_driver, pdev);

	return ret;
}

static int ohci_hcd_ikf71xx_drv_remove(struct platform_device *pdev)
{
	struct usb_hcd *hcd = platform_get_drvdata(pdev);

	pr_debug ("In ohci_hcd_ikf71xx_drv_remove");

	usb_hcd_ikf71xx_remove(hcd, pdev);

	return 0;
}

MODULE_ALIAS("fusiv-ohci-hcd");

static struct platform_driver ohci_hcd_ikf71xx_driver = {
	.probe		= ohci_hcd_ikf71xx_drv_probe,
	.remove		= ohci_hcd_ikf71xx_drv_remove,
	.driver		= {
		.name	= "fusiv-ohci-hcd",
		.bus	= &platform_bus_type,
		.owner	= THIS_MODULE,
	},
};
