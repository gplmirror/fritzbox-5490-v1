
#include <features.h>

#ifndef __UCLIBC_SUSV3_LEGACY__

#define index(a,b) strchr(a,b)
#define bzero(a,b) memset(a,0,b)

#endif

