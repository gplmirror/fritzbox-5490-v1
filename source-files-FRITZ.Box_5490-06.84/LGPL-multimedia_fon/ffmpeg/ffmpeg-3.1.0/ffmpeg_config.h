#ifndef FFMPEG_CONFIG_H
#define FFMPEG_CONFIG_H

#define av_restrict
/*--- #define attribute_deprecated ---*/
#define FFMPEG_LICENSE "LGPL version 2.1 or later"
#define FFMPEG_CONFIGURATION "--disable-yasm"
#define HAVE_ALIGNED_STACK 1
#define HAVE_FAST_UNALIGNED 0
#if __BYTE_ORDER==__BIG_ENDIAN
#define HAVE_BIGENDIAN  1
#else
#define HAVE_BIGENDIAN  0
#endif

/* we dont want threads during decoding of mp3 */
#define HAVE_ATOMICS_NATIVE 0
#define HAVE_ATOMICS_GCC 0
#define HAVE_PTHREADS 0
#define HAVE_THREADS 0

/* mathematical config stuff */
#define HAVE_CBRT       1
#define HAVE_CBRTF      1
#define HAVE_COPYSIGN   1
#define HAVE_ERF        1
#define HAVE_ISNAN      1
#define HAVE_ISFINITE   1
#define HAVE_HYPOT      1
#define HAVE_RINT       1
#define HAVE_LRINT      1
#define HAVE_LRINTF     1
#define HAVE_ROUND      1
#define HAVE_ROUNDF     1
#define HAVE_TRUNC      1
#define HAVE_TRUNCF     1

/* lib functions */
#define HAVE_GMTIME_R   1
#define HAVE_LOCALTIME_R    1
#define HAVE_GETTIMEOFDAY   1

/* system architecture */
#define ARCH_AARCH64 0
#define ARCH_ARM 0
#define ARCH_PPC 0
#define ARCH_X86 0
#define HAVE_MIPSFPU 0
#define HAVE_MIPSDSP 0

/* config flags of ffmpeg */
#define CONFIG_MEMORY_POISONING     0
#define CONFIG_FRAME_THREAD_ENCODER 0
#define CONFIG_ME_CMP               0
#define CONFIG_GRAY                 0
#define CONFIG_SMALL                0
#define CONFIG_FTRAPV               0
#define CONFIG_MDCT                 1
#define CONFIG_MP3_DECODER          1
#define CONFIG_SAVE_BITSTREAM_READER 0

#endif /* FFMPEG_CONFIG_H */
