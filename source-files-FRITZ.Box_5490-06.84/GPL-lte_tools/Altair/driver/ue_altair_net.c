/*
 * UE Altair Semiconductor FourGee-3100
 *
 * 2011 Bruno Caballero / AVM <b.caballero@avm.de>
 *
 * This driver is based on the ue_cdc driver by Altair and the cdc_ether
 * driver from the Linux kernel code. The hardware, although identifying
 * as CDC ECM class, requires a TTY interface for control.
 * The code is also "inspired" by: usb-serial, usb-skeleton, cdc-acm, and more
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the linux kernel you work with; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/usb.h>
#include <linux/version.h>
#include <linux/ctype.h>
#include <linux/ip.h>
#include <linux/if_vlan.h>
#include <linux/etherdevice.h>
#include "ue_altair.h"

#ifdef CONFIG_AVM_PA
#include <linux/avm_pa.h>
#endif

/* for each rx packet we prepend this header and setup the last two bytes based on the protocol. */
const u8 s_rxhdr[] = {0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0x44, 0x00, 0xaa, 0xbb, 0xcc, 0xdd, 0x64, 0x08, 0x00};
void ue_defer_kevent (struct ue_device *ue, int work);
static void ue_unlink_urbs(struct ue_device *ue, struct usb_anchor *usb_anchor);

/********************************************************
 * TX Path                                              *
 ********************************************************/

static int copy_skb_into_urb(struct ue_net *uenet, struct sk_buff *skb, struct txurb *txurb)
{
    struct ue_hdr hdr;
    int hdrlen;

#ifdef UE_ALTAIR_VLAN
    {
        int isVlan;
        isVlan = ((skb->len > 14)  && skb->data[12] == 0x81 && skb->data[13] == 0);
        if (isVlan) {
            hdr.vlan_id = skb->data[15];
            hdrlen = VLAN_ETH_HLEN;
        } else {
            hdr.vlan_id = 0;
            hdrlen = ETH_HLEN;
        }
    }
#else
    hdr.vlan_id = uenet->vlan_id;
    hdrlen = ETH_HLEN;
#endif

#ifdef UE_ALTAIR_ZEROCOPY
    hdr.signature = 0;
    hdr.offset = sizeof(hdr);
    memset(hdr.spacer, '\0', 11);
#else
    hdr.reserved = 0;
#endif

    hdr.len = cpu_to_be16(skb->len - hdrlen);

    /* Aligment 4 Bytes for each skb */
    txurb->len = ALIGN(txurb->len, 4);

    /* Copy Altair USB Header */
    (* (struct ue_hdr *) (txurb->buf + txurb->len)) = hdr;

    /* Copy IP Packet (no Ethernet header) */
    memcpy(txurb->buf + txurb->len + sizeof(hdr), skb->data + hdrlen, skb->len - hdrlen);
    txurb->len += skb->len - hdrlen + sizeof(hdr);

    list_add_tail((struct list_head*)skb, &txurb->skbs);

    txurb->nskbs++;

    return TXURB_OK;

}


//URB Completion Routine: called from interrupt context.
static void tx_complete (struct urb *urb)
{
    struct txurb *txurb = (struct txurb *) urb->context;
    struct ue_device *ue = txurb->ue;
    struct ue_net *uenet = txurb->uenet;
    struct sk_buff *skb;
    unsigned long flags;

    BUG_ON(list_empty(&txurb->skbs));

    if(urb->status == 0){
        uenet->stats.tx_packets += txurb->nskbs;
        uenet->stats.tx_bytes += urb->actual_length;
    } else {
        uenet->stats.tx_errors += txurb->nskbs;
        switch (urb->status) {
        case -EPIPE:
            //here we halt all usb traffic. both TX and RX. run some error handler in a process context
            ue_defer_kevent (ue, UE_EVENT_TX_HALT);
            dev_err (ue->dev, "tx_complete: status %d (-PIPE)\n", urb->status);
            break;

        // like rx, tx gets controller i/o faults during khubd delays
        // and so it uses the same throttling mechanism.
        case -EPROTO:
        case -ETIME:
        case -EILSEQ:
            //we stop the netif queue for a while(throttle). set a timer to restart it
            if (!timer_pending (&ue->delay_tx)) {
                mod_timer (&ue->delay_tx, jiffies + THROTTLE_JIFFIES);
                dev_err (ue->dev, "tx throttle urb->status %d\n", urb->status);
            }
            dev_err(ue->dev, "tx_complete: status %d (-EPROTO, -ETIME, -EILSEQ)\n", urb->status);
            uenet->my_stats.backpressure_status++;
            netif_stop_queue (uenet->net);
            break;
        default:
            //if (netif_msg_tx_err (dev))
                dev_err (ue->dev, "tx err %d\n", urb->status);
        /* software-driven interface shutdown */
        case -ECONNRESET:       // async unlink
        case -ESHUTDOWN:        // hardware gone
            break;
        }
    }

    //cleanup all skbs,
    while(!list_empty(&txurb->skbs)) {
        skb = (struct sk_buff*)(txurb->skbs.next);
        list_del((struct list_head*) skb);
        dev_kfree_skb(skb);
    }

    //bring the txurb to a proper state
    txurb->nskbs = 0;
    txurb->len = 0;
    txurb->uenet = uenet;
    txurb->ue = ue;
    INIT_LIST_HEAD(&txurb->skbs);

    //return it to the "ready pool"
    spin_lock_irqsave(&uenet->tx_urbs_ready_lock, flags);
    list_add_tail(&txurb->list, &uenet->tx_urbs_ready);
    spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);
   
    ue_do_tx((unsigned long)ue);
}

//TX Tasklet
void ue_do_tx(unsigned long param)
{
    struct ue_device *ue = (struct ue_device *)param;
    struct ue_net *uenet;
    struct sk_buff *skb;
    struct txurb *txurb;
    unsigned long flags, flags2;
    int skb_len, skb_len_stats;
    int i, max_loops = 1, retval;

    //Loop all interfaces
    for(i = UE_NET_INTERFACES-1; i >=0; i--) {
        uenet = ue->uenet[i];
        do {
            //lock the free urbs pool and the skb queue
            spin_lock_irqsave(&uenet->tx_urbs_ready_lock, flags);

            //verify that we have at least one urb and one skb in order to continue
            if(uenet->skb_list.qlen == 0 || list_empty(&uenet->tx_urbs_ready)) { //nothing to do
                spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);
                break;
            }

            //detach the first urb and the first skb
            txurb = list_first_entry(&uenet->tx_urbs_ready, struct txurb, list);

            list_del(&txurb->list);

            spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);

            txurb->nskbs = 0;

            do {
                //Pick a SKB
                spin_lock_irqsave(&uenet->skb_list.lock, flags2);

                if(!uenet->skb_list.qlen){
                    spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);
                    break;
                }

                skb = skb_peek(&uenet->skb_list);
                if(skb == NULL) {
                    spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);
                    break;
                }

                //Length of the skb
                skb_len = skb->len - ETH_HLEN + sizeof(struct ue_hdr);

                if((ALIGN(txurb->len,4) + skb_len) <= ue->tx_size) {
                    skb = __skb_dequeue(&uenet->skb_list);
                    if(skb == NULL){
                        //No more SKBs on the list
                        spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);
                        break;
                    }else{
                        uenet->queued_data_len -= skb->len;
                    }
                }else{
                    //size exceeded, continuing to checkout
                    spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);
                    break;
                }

                spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);

                //Copy the Payload
                if(copy_skb_into_urb(uenet, skb, txurb) == TXURB_NOMEM) {
                    //Should never happen
                    dev_err(ue->dev, "copy_skb_into_urb: TXURB_NOMEM");
                    break;
                }

            } while(txurb->nskbs < MAX_SKB_URB);

            if (txurb->nskbs == 0) {

                //bring the txurb to a proper state
                txurb->len = 0;
                txurb->uenet = uenet;
                txurb->ue = ue;
                INIT_LIST_HEAD(&txurb->skbs);

                //return it to the "ready pool"
                spin_lock_irqsave(&uenet->tx_urbs_ready_lock, flags);
                list_add_tail(&txurb->list, &uenet->tx_urbs_ready);
                spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);

                break;
            }

            /* We do no padding at the end of the buffer */
            usb_fill_bulk_urb (txurb->urb, ue->usb,
                                usb_sndbulkpipe (ue->usb, ue->out_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK),
                                txurb->buf, txurb->len, tx_complete, txurb);

            usb_anchor_urb(txurb->urb, &ue->tx_urb_anchor);

            switch ((retval = usb_submit_urb(txurb->urb, GFP_ATOMIC))) {
            case -EPIPE:
                netif_stop_queue (uenet->net);
                ue_defer_kevent (ue, UE_EVENT_TX_HALT);
            default:
                dev_dbg (ue->dev, "tx: submit urb err %d", retval);
                usb_unanchor_urb(txurb->urb);
                
                txurb->len = 0;
                txurb->uenet = uenet;
                txurb->ue = ue;
                INIT_LIST_HEAD(&txurb->skbs);

                spin_lock_irqsave(&uenet->tx_urbs_ready_lock, flags);
                list_add_tail(&txurb->list, &uenet->tx_urbs_ready);
                spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);

                break;
            case 0:
                uenet->net->trans_start = jiffies;
            }

            skb_len_stats = (txurb->nskbs <= STATS_MAX_SKB_URBS) ? (txurb->nskbs -1) : (STATS_MAX_SKB_URBS -1);
            uenet->my_stats.tx_num_skbs[skb_len_stats]++;

        }while(--max_loops);

        spin_lock_irqsave(&uenet->skb_list.lock, flags2);
        if( !BACKPRESSURE_COND && netif_queue_stopped(uenet->net) && (uenet->my_stats.qf_status == 0)){
            netif_wake_queue (uenet->net);
            uenet->my_stats.backpressure_status--;
        }
        spin_unlock_irqrestore(&uenet->skb_list.lock, flags2);

    }
    return;
}



#ifdef UE_ALTAIR_FASTSKB
static int tx_send_skb (struct ue_net *uenet, struct txurb *txurb, struct sk_buff *skb)
{

    struct ue_hdr hdr;
    struct ue_device *ue = uenet->parent;
    int ret;

    skb = skb_unshare(skb, GFP_ATOMIC);
#ifdef UE_ALTAIR_VLAN
    {
        int isVlan;
        isVlan = ((skb->len > 14)  && skb->data[12] == 0x81 && skb->data[13] == 0);
        if (isVlan) {
            hdr.vlan_id = skb->data[15];
            skb_pull(skb, VLAN_ETH_HLEN);
        } else {
            hdr.vlan_id = 0;
            skb_pull(skb, ETH_HLEN);
        }
    }
#else //!UE_ALTAIR_VLAN
    hdr.vlan_id = uenet->vlan_id;
    skb_pull(skb, ETH_HLEN);
#endif

    /* Altair Header */
    hdr.reserved = 0;
    hdr.len = cpu_to_be16(skb->len);

    skb_push(skb, sizeof(struct ue_hdr));

    memcpy(skb->data, &hdr, sizeof(struct ue_hdr));

    txurb->nskbs = 1;
    txurb->len = skb->len;
    txurb->uenet = uenet;

    list_add_tail((struct list_head*)skb, &txurb->skbs);

    usb_fill_bulk_urb (txurb->urb, ue->usb,
                        usb_sndbulkpipe (ue->usb, ue->out_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK),
                        skb->data, txurb->len, tx_complete, txurb);

    usb_anchor_urb(txurb->urb, &ue->tx_urb_anchor);

    ret = usb_submit_urb(txurb->urb, GFP_ATOMIC);

    uenet->my_stats.tx_onthefly++;

    return ret;
}
#endif

//Transmit a packet (called by the kernel)
int uenet_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
    struct ue_net *uenet = netdev_priv(dev);
    struct ue_device *ue = uenet->parent;
    unsigned long flags;
    int retval = NET_XMIT_SUCCESS;

    //AVM/BC 20130514: Mac Addresse wird korrekt gesetzt.
    memcpy(uenet->rx_ethhdr ,skb->data+6, 6);

#ifdef CONFIG_AVM_PA
    avm_pa_dev_snoop_transmit(AVM_PA_DEVINFO(dev), skb);
#endif

#ifdef UE_ALTAIR_FASTSKB

    spin_lock_irqsave(&uenet->tx_urbs_ready_lock, flags);

    if(!list_empty(&uenet->tx_urbs_ready)) {
        struct txurb *txurb;

        txurb = list_first_entry(&uenet->tx_urbs_ready, struct txurb, list);
        list_del(&txurb->list);

        spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);

        tx_send_skb(uenet, txurb, skb);

    } else {

        spin_unlock_irqrestore(&uenet->tx_urbs_ready_lock, flags);
#else
    {
#endif

        spin_lock_irqsave(&uenet->skb_list.lock, flags);

        __skb_queue_tail (&uenet->skb_list, skb);

        uenet->queued_data_len += skb->len;

        spin_unlock_irqrestore(&uenet->skb_list.lock, flags);

        if((BACKPRESSURE_COND) && (!netif_queue_stopped(uenet->net))){
            uenet->my_stats.backpressure++;
            uenet->my_stats.backpressure_status++;
            netif_stop_queue (uenet->net);
        }

        ue_do_tx((unsigned long)ue);

    }
    dev->trans_start = jiffies;

    return retval;
}

/*
 * Deal with a transmit timeout.
 */
void uenet_tx_timeout (struct net_device *dev)
{
    struct ue_net *uenet = netdev_priv(dev);
    struct ue_device *ue = uenet->parent;

    dev_err(ue->dev, "Transmit timeout at %ld, latency %ld\n", jiffies, jiffies - dev->trans_start);

    ue_unlink_urbs (ue, &ue->tx_urb_anchor);
    ue_do_tx((unsigned long)ue);

    // FIXME: device recovery -- reset?

    return;
}

#ifdef CONFIG_AVM_PA
static void lte_pa_dev_transmit(void *arg, struct sk_buff *skb)
{
    skb->dev = (struct net_device *) arg;
    dev_queue_xmit(skb);
}
#endif

/********************************************************
 * RX Path                                              *
 ********************************************************/

#ifdef UE_ALTAIR_ZEROCOPY

static int rx_process_sub(struct ue_device *ue, struct sk_buff *skb)
{
    struct ue_net *uenet = ue->uenet[0];
    struct ue_hdr *hdr;
    struct iphdr *ip;
    u8 *mac;
    int skblen;

    hdr = (struct ue_hdr *) skb->data;
    skblen = be16_to_cpu(hdr->len);

    if (skblen <= 0 ||  skblen > skb->len) {
        dev_err(ue->dev, "Error processing SKB, hdr->len = %d\n",skblen);
        uenet->stats.rx_errors++;
        uenet->my_stats.device_errors++;
        return -1;
    }

#ifdef UE_ALTAIR_VLAN
    vid = hdr->vlan_id;
#else
    if (hdr->vlan_id > 0 || hdr->vlan_id <= UE_NET_INTERFACES)
        //vlan_id = 1 o 0 => ETH0 /// vlan_id 2 => ETH1 /// vlan_id 3 => ETH2
        uenet = hdr->vlan_id > 1 ? ue->uenet[hdr->vlan_id -1] : ue->uenet[0];
    else
        uenet = ue->uenet[0];
#endif

    // skip usb header
    skb_pull(skb, sizeof(*hdr));

    //skip the rest of the packet
    skb_trim(skb, skblen);

    ip = (struct iphdr *)skb->data;

#ifdef UE_ALTAIR_VLAN
    skb_push(skb, VLAN_ETH_HLEN);
    memcpy(skb->data, uenet->rx_vlan_ethhdr, VLAN_ETH_HLEN);
    skb->data[12] = 0x81;
    skb->data[14] = 0;
    skb->data[15] = vid;
    if(ip->version == 6){
        skb->data[16] = 0x86;
        skb->data[17] = 0xdd;
    }
#else
    skb_push(skb, ETH_HLEN);
    memcpy(skb->data, uenet->rx_ethhdr, ETH_HLEN);
    if(ip->version == 6){
        skb->data[12] = 0x86;
        skb->data[13] = 0xdd;
    }
#endif

    //broadcast IP
    if (unlikely(ip->daddr == 0xffffffff)) {
        mac = skb->data;
        mac[0] = 0xff;
        mac[1] = 0xff;
        mac[2] = 0xff;
        mac[3] = 0xff;
        mac[4] = 0xff;
        mac[5] = 0xff;
    }

    // submit the skb to the ip stack.
#ifdef CONFIG_AVM_PA
    if (avm_pa_dev_receive(AVM_PA_DEVINFO(uenet->net), skb) != 0) {
#endif
        skb->dev = uenet->net;
        skb->protocol = eth_type_trans (skb, uenet->net);
        skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
        /* skb not accelerated */
        netif_rx(skb);

        //Statistics
        uenet->stats.rx_packets++;
        uenet->stats.rx_bytes += skblen;
#ifdef CONFIG_AVM_PA
    }
#endif

    return 0;
}

int rx_process (struct rxurb *rxurb)
{
    struct ue_device *ue = (struct ue_device *)rxurb->ue;
    struct ue_net *uenet = ue->uenet[0];
    struct sk_buff *skb2;
    struct ue_hdr *hdr;
    int skblen, offset, num_skb_rx = 0, skb_len_stats;

    if (unlikely(rxurb->urb->status != 0)) {
        return -1;
    }

    /* peripheral may have batched packets to us... */
    if (unlikely(rxurb->skb->len < sizeof(struct ue_hdr))) {//we got a bad skb
        dev_err(ue->dev, "ERROR: buflen %d < sizeof(struct ue_hdr)\n", rxurb->skb->len);
        //FIXME skb_free
        uenet->stats.rx_errors++;
        return -1;
    }

    hdr = (struct ue_hdr *) rxurb->skb->data;

    skblen = be16_to_cpu(hdr->len);
    offset = be16_to_cpu(hdr->offset);

    while(ALIGN(skblen + offset, 4) < rxurb->skb->len) {

        skb2 = skb_clone(rxurb->skb, GFP_ATOMIC | GFP_DMA);

        rx_process_sub(ue, skb2);

        num_skb_rx++;

        skb_pull(rxurb->skb, ALIGN(skblen + offset, 4));

        if (rxurb->skb->len > sizeof(struct ue_hdr *)) {

            hdr = (struct ue_hdr *) rxurb->skb->data;
            skblen = be16_to_cpu(hdr->len);
            offset = be16_to_cpu(hdr->offset);
        } else {
            break;
        }
    }

    rx_process_sub(ue, rxurb->skb);

    num_skb_rx++;

    skb_len_stats = (num_skb_rx <= STATS_MAX_SKB_URBS) ? (num_skb_rx -1) : (STATS_MAX_SKB_URBS -1);
    uenet->my_stats.rx_num_skbs[skb_len_stats]++;

    return 0;
}

#else //UE_ALTAIR_ZEROCOPY

static int rx_process (struct rxurb *rxurb)
{
    struct ue_device *ue = (struct ue_device *)rxurb->ue;
    struct ue_net *uenet = ue->uenet[0];
    struct sk_buff *skb;
    struct ue_hdr* hdr;
    struct iphdr *ip;
    u8 *p = rxurb->buf, *mac;
    int buflen = rxurb->urb->actual_length;
    int skblen, num_skb_rx = 0, skb_len_stats, vid, len;

    if (unlikely(rxurb->urb->status != 0)) {
        return -1;
    }


    /* peripheral may have batched packets to us... */
    if (unlikely(buflen < sizeof(struct ue_hdr))) {//we got a bad skb
        dev_err(ue->dev, "ERROR: buflen %d < sizeof(struct ue_hdr)\n", buflen);
        uenet->stats.rx_errors++;
        return -1;
    }

    while (likely(buflen > sizeof(struct ue_hdr))) {

        hdr = (struct ue_hdr *) p;
        skblen = be16_to_cpu(hdr->len);

#ifdef UE_ALTAIR_VLAN
        vid = hdr->vlan_id;
#else
        if (hdr->vlan_id < 0 || hdr->vlan_id > UE_NET_INTERFACES)
            uenet = ue->uenet[0];
        else
            //FIXME
            //vlan_id = 1 o 0 => ETH0 /// vlan_id 2 => ETH1 /// vlan_id 3 => ETH2
            uenet = hdr->vlan_id > 1 ? ue->uenet[hdr->vlan_id -1] : ue->uenet[0];
        vid = 0;
#endif

        if (skblen <= 0 || skblen > buflen ){
            dev_err(ue->dev, "Error p=%p, buflen=%d, skblen=%d\n", p, buflen, skblen);
            uenet->stats.rx_errors++;
            uenet->my_stats.device_errors++;
            break;
        }

        len = skblen + NET_IP_ALIGN + (vid ? VLAN_ETH_HLEN : ETH_HLEN);
        if ((skb =  dev_alloc_skb (len)) == NULL) {
            dev_err(ue->dev, "rx_process: dev_alloc_skb failed\n");
            return 0;
        }

        skb_reserve (skb, NET_IP_ALIGN);

        p += sizeof(struct ue_hdr);
        buflen -= sizeof(struct ue_hdr);

        mac = skb->data;
        ip = (struct iphdr *)p;

        /*== AVM/BC 20110608 Driver with VLAN Support (Branch 3.0) ==*/
        if (vid) {
            memcpy(skb->data, uenet->rx_vlan_ethhdr, VLAN_ETH_HLEN);
            skb->data[12] = 0x81;
            skb->data[14] = 0;
            skb->data[15] = vid;

            if(ip->version == 6){
                skb->data[16] = 0x86;
                skb->data[17] = 0xdd;
            }
            skb_put(skb,VLAN_ETH_HLEN);

        } else {
            memcpy(skb->data, uenet->rx_ethhdr, ETH_HLEN);

            if(ip->version == 6){
                skb->data[12] = 0x86;
                skb->data[13] = 0xdd;
            }
            skb_put(skb, ETH_HLEN);
        }

        if (unlikely(ip->daddr == 0xffffffff)) {//this is a broadcast IP packet we need an ethernet broadcast dest.
            mac[0] = 0xff;
            mac[1] = 0xff;
            mac[2] = 0xff;
            mac[3] = 0xff;
            mac[4] = 0xff;
            mac[5] = 0xff;
        }

        //copy ip payload
        memcpy(skb_tail_pointer(skb), p, skblen);
        skb_put(skb, skblen);

        // submit the skb to the ip stack.
#ifdef CONFIG_AVM_PA
        if (avm_pa_dev_receive(AVM_PA_DEVINFO(uenet->net), skb) != 0) {
#endif
            skb->dev = uenet->net;
            skb->protocol = eth_type_trans (skb, uenet->net);
            skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
            /* skb not accelerated */
            netif_rx(skb);

            //Statistics
            uenet->stats.rx_packets++;
            uenet->stats.rx_bytes += skblen;
#ifdef CONFIG_AVM_PA
        }
#endif

        num_skb_rx++;

        buflen -= ALIGN(skblen, 4);
        p += ALIGN(skblen, 4);

        if (buflen <= 4) {
             /* buflen too small, going out */
            break;
        }

    }

    skb_len_stats = (num_skb_rx <= STATS_MAX_SKB_URBS) ? (num_skb_rx -1) : (STATS_MAX_SKB_URBS -1);
    uenet->my_stats.rx_num_skbs[skb_len_stats]++;

    return 0;
}

#endif //UE_ALTAIR_ZEROCOPY


//URB Completion Routine: called from interrupt context.
static void rx_complete (struct urb *urb)
{
    struct rxurb *rxurb = (struct rxurb *) urb->context;
    struct ue_device *ue = (struct ue_device *)rxurb->ue;

#ifdef UE_ALTAIR_ZEROCOPY
    skb_put(rxurb->skb, urb->actual_length);
#endif //UE_ALTAIR_ZEROCOPY

    switch (urb->status) {
        /* success */
        case 0:
            if (urb->actual_length < ue->uenet[0]->net->hard_header_len) {
                ue->uenet[0]->stats.rx_errors++;
                ue->uenet[0]->stats.rx_length_errors++;
                dev_err(ue->dev, "rx length %d\n", urb->actual_length);
            }
            rx_process(rxurb);
            break;

        /* stalls need manual reset. this is rare ... except that
         * when going through USB 2.0 TTs, unplug appears this way.
         * we avoid the highspeed version of the ETIMEDOUT/EILSEQ
         * storm, recovering as needed.
         */
        case -EPIPE:
            ue->uenet[0]->stats.rx_errors++;
            ue_defer_kevent (ue, UE_EVENT_RX_HALT);
            // FALLTHROUGH

        /* software-driven interface shutdown */
        case -ECONNRESET:       /* async unlink */
        case -ESHUTDOWN:        /* hardware gone */
            dev_err(ue->dev, "rx shutdown, code %d\n", urb->status);
            goto block;

        /* we get controller i/o faults during khubd disconnect() delays.
         * throttle down resubmits, to avoid log floods; just temporarily,
         * so we still recover when the fault isn't a khubd delay.
         */
        case -EPROTO:
        case -ETIME:
        case -EILSEQ:
            ue->uenet[0]->stats.rx_errors++;
            //if (!timer_pending (&dev->delay)) {
            //  mod_timer (&dev->delay, jiffies + THROTTLE_JIFFIES);
            //  if (netif_msg_link (dev))
            //      devdbg (dev, "rx throttle %d", urb_status);
            //}
    block:
            break;

        /* data overrun ... flush fifo? */
        case -EOVERFLOW:
            ue->uenet[0]->stats.rx_over_errors++;
            // FALLTHROUGH

        default:

            ue->uenet[0]->stats.rx_errors++;
            dev_err(ue->dev, "rx status %d\n", urb->status);
            break;
    }
    rx_submit(ue, rxurb);
}


void rx_submit (struct ue_device *ue, struct rxurb *rxurb)
{
    int retval, i;

#ifdef UE_ALTAIR_ZEROCOPY
    if ((rxurb->skb = alloc_skb (ue->rx_size, GFP_ATOMIC)) == NULL) {
        printk(KERN_INFO "Alloc SKB Error\n");
        return;
    }
    usb_fill_bulk_urb (rxurb->urb, ue->usb,
                        usb_rcvbulkpipe (ue->usb, ue->in_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK),
                        rxurb->skb->data, ue->rx_size, rx_complete, rxurb);
#else
    usb_fill_bulk_urb (rxurb->urb, ue->usb,
                        usb_rcvbulkpipe (ue->usb, ue->in_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK),
                        rxurb->buf, ue->rx_size, rx_complete, rxurb);
#endif

    usb_anchor_urb(rxurb->urb, &ue->rx_urb_anchor);

    switch (retval = usb_submit_urb (rxurb->urb, GFP_ATOMIC)) {

        case -EPIPE:
            ue_defer_kevent (ue, UE_EVENT_RX_HALT);
            break;
        case -ENOMEM:
            ue_defer_kevent (ue, UE_EVENT_RX_MEMORY);
            break;
        case -ENODEV:
            dev_err(ue->dev, "rx_submit: usb_submit_urb failed, device gone\n");
            for (i = 0; i < UE_NET_INTERFACES; i++) netif_device_detach (ue->uenet[i]->net);
            break;
        default:
            dev_err(ue->dev, "rx submit, %d\n", retval);
            break;
        case 0:
            //OK
            break;
    }
}


/********************************************************
 * Help                                                 *
 ********************************************************/

static void ue_unlink_urbs(struct ue_device *ue, struct usb_anchor *usb_anchor)
{
    usb_unlink_anchored_urbs(usb_anchor);
}


void kevent (struct work_struct *work)
{
    struct ue_device *ue = container_of(work, struct ue_device, kevent);
    int status, i;

    /* usb_clear_halt() needs a thread context */
    if (test_bit (UE_EVENT_TX_HALT, &ue->flags)) {
        dev_info(ue->dev, "UE_EVENT_TX_HALT\n");
        ue_unlink_urbs (ue, &ue->tx_urb_anchor);
        status = usb_clear_halt (ue->usb, usb_sndbulkpipe (ue->usb, ue->out_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK));
        if (status < 0
                && status != -EPIPE
                && status != -ESHUTDOWN) {
            dev_err(ue->dev, "can't clear tx halt, status %d", status);
        } else {
            dev_info(ue->dev, "usb_clear_halt\n");
            clear_bit (UE_EVENT_TX_HALT, &ue->flags);
            if (status != -ESHUTDOWN)
                for (i = 0; i < UE_NET_INTERFACES; i++)
                    netif_wake_queue (ue->uenet[i]->net);
        }
    }
    if (test_bit (UE_EVENT_RX_HALT, &ue->flags)) {
        dev_info(ue->dev, "UE_EVENT_RX_HALT\n");
        ue_unlink_urbs (ue, &ue->rx_urb_anchor);
        status = usb_clear_halt (ue->usb, usb_rcvbulkpipe (ue->usb, ue->in_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK));
        if (status < 0
                && status != -EPIPE
                && status != -ESHUTDOWN) {
            dev_err(ue->dev, "can't clear rx halt, status %d", status);
        } else {
            clear_bit (UE_EVENT_RX_HALT, &ue->flags);
        }
    }

    /* tasklet could resubmit itself forever if memory is tight */
    if (test_bit (UE_EVENT_RX_MEMORY, &ue->flags)) {
        dev_info(ue->dev, "UE_EVENT_RX_MEMORY\n");
        /*struct urb    *urb = NULL;

        if (netif_running (dev->net))
            urb = usb_alloc_urb (0, GFP_KERNEL);
        else
            clear_bit (EVENT_RX_MEMORY, &dev->flags);
        if (urb != NULL) {
            clear_bit (EVENT_RX_MEMORY, &dev->flags);
            rx_submit (dev, urb, GFP_KERNEL);
            tasklet_schedule (&dev->bh);
        }*/
    }

}


void ue_defer_kevent (struct ue_device *ue, int work)
{
    set_bit(work, &ue->flags);
    if (!schedule_work (&ue->kevent))
        dev_err(ue->dev, "kevent %d may have been dropped", work);
    else {
        dev_dbg(ue->dev, "kevent %d scheduled", work);
    }
}


static struct net_device_stats *uenet_stats(struct net_device *dev)
{
    struct ue_net *uenet = netdev_priv(dev);
    return &uenet->stats;
}


static int uenet_set_mac_addr(struct net_device *net, void *p)
{
    struct sockaddr *addr = p;

    struct ue_net *uenet = netdev_priv(net);

    if (netif_running(net))
        return -EBUSY;

    if (!is_valid_ether_addr(addr->sa_data))
        return -EADDRNOTAVAIL;

    memcpy(net->dev_addr, addr->sa_data, ETH_ALEN);

    memcpy(uenet->rx_ethhdr, addr->sa_data, ETH_ALEN);
    memcpy(uenet->rx_vlan_ethhdr,uenet->rx_ethhdr,12);
    uenet->rx_vlan_ethhdr[12]=0x81;
    uenet->rx_vlan_ethhdr[13]=0x00;
    uenet->rx_vlan_ethhdr[14]=0x00;
    uenet->rx_vlan_ethhdr[15]=0x00;
    memcpy(uenet->rx_vlan_ethhdr+16,uenet->rx_ethhdr+12, 2);

    return 0;
}


/********************************************************
 * Open / Close                                         *
 ********************************************************/

int uenet_open(struct net_device *dev)
{
    struct ue_net *uenet = netdev_priv(dev);
    int retval = 0;

    if(sscanf(dev->name, INTERFACE_NAME, &uenet->vlan_id) != 1) {
        dev_err(uenet->parent->dev, "Interface does not have a valid name: %s\n", dev->name);
        return -1;
    }

    //vlan_id lte0 = 1, vlan_id lte1 = 2
    uenet->vlan_id++;

    netif_start_queue(dev);

    return retval;
}

int uenet_stop(struct net_device *dev)
{
    /* release ports, irq and such -- like fops->close */

    netif_stop_queue(dev); /* can't transmit any more */
    return 0;
}


/********************************************************
 * Init / Cleanup                                       *
 ********************************************************/
#ifdef CONFIG_LANTIQ
static struct net_device_ops uenet_netdev_ops = {
#else
static const struct net_device_ops uenet_netdev_ops = {
#endif
    .ndo_open               = uenet_open,
    .ndo_stop               = uenet_stop,
    .ndo_start_xmit         = uenet_start_xmit,
    .ndo_get_stats          = uenet_stats,
    .ndo_set_mac_address    = uenet_set_mac_addr,
    .ndo_tx_timeout         = uenet_tx_timeout
};

/*
 * The init function (sometimes called probe).
 * It is invoked by register_netdev()
 */
void uenet_init(struct net_device *dev)
{
    struct ue_net *uenet = netdev_priv(dev);

    /*
     * Assign other fields in dev, using ether_setup() and some
     * hand assignments
     */
    ether_setup(dev); /* assign some of the fields */

    dev->netdev_ops = &uenet_netdev_ops;
    dev->watchdog_timeo = TX_TIMEOUT_JIFFIES;

    /* keep the default flags, just add NOARP */
    dev->flags           |= IFF_NOARP & (~IFF_BROADCAST & ~IFF_MULTICAST);

    /*== AVM/BC 20110608 Driver with VLAN Support (Branch 3.0) ==*/
    dev->features        |= NETIF_F_VLAN_CHALLENGED;

    skb_queue_head_init (&uenet->skb_list);

    /* 2 "Fake" Ethernet headers, with and without VLAN */
    uenet->rx_ethhdr = kzalloc(ETH_HLEN, GFP_KERNEL);
    uenet->rx_vlan_ethhdr = kzalloc(VLAN_ETH_HLEN, GFP_KERNEL);

    memcpy(uenet->rx_ethhdr, s_rxhdr, ETH_HLEN);
    memcpy(uenet->rx_vlan_ethhdr,uenet->rx_ethhdr,12);
    uenet->rx_vlan_ethhdr[12]=0x81;
    uenet->rx_vlan_ethhdr[13]=0;
    uenet->rx_vlan_ethhdr[14]=0;
    uenet->rx_vlan_ethhdr[15]=0;
    memcpy(uenet->rx_vlan_ethhdr+16,uenet->rx_ethhdr+12, 2);
}

int uenet_init_module(struct ue_device *ue)
{
    int result, ret = -ENOMEM;
    struct net_device *net;
    int i, j;

    for (i = 0; i < UE_NET_INTERFACES; i++) {

        net = alloc_netdev(sizeof(struct ue_net), INTERFACE_NAME, uenet_init);

        ue->uenet[i] = netdev_priv(net);
        ue->uenet[i]->parent = ue;
        ue->uenet[i]->net = net;

        netif_carrier_off(ue->uenet[i]->net);
        netif_dormant_on(ue->uenet[i]->net);

        if(ue->uenet[i] == NULL)
            goto out;

        ret = -ENODEV;
        if ((result = register_netdev(net)))
            dev_err(ue->dev, "error %i registering device \"%s\"",
                    result, net->name);
        else
            ret = 0;

#ifdef CONFIG_AVM_PA
        {
            struct avm_pa_pid_cfg cfg;
            snprintf(cfg.name, sizeof(cfg.name), "%s", net->name);
            cfg.framing = avm_pa_framing_ether;
            cfg.default_mtu = net->mtu;
            cfg.tx_func = lte_pa_dev_transmit;
            cfg.tx_arg = net;
            if(avm_pa_dev_pid_register(AVM_PA_DEVINFO(net), &cfg) < 0)
                printk(KERN_ERR "%s: failed to register PA PID\n", cfg.name);
        }
#endif

        //init statistics
        ue->uenet[i]->my_stats.backpressure = 0;
        ue->uenet[i]->my_stats.backpressure_status = 0;
        ue->uenet[i]->my_stats.device_errors = 0;
        ue->uenet[i]->my_stats.tx_onthefly = 0;
        ue->uenet[i]->my_stats.qf_status = 0;
        ue->uenet[i]->my_stats.qf_count = 0;

        for (j=0; j < STATS_MAX_SKB_URBS; j++) {
            ue->uenet[i]->my_stats.rx_num_skbs[j] = 0;
            ue->uenet[i]->my_stats.tx_num_skbs[j] = 0;
        }

    }
out:
    if (ret)
        uenet_cleanup(ue);
    return ret;
}

void uenet_cleanup(struct ue_device *ue)
{
    int i;

    for (i = 0; i < UE_NET_INTERFACES; i++) {

        if(ue->uenet[i]) {
            // skb_queue_purge is smp safe (takes skb_list.lock)
            skb_queue_purge(&ue->uenet[i]->skb_list);
            kfree(ue->uenet[i]->rx_ethhdr);
            kfree(ue->uenet[i]->rx_vlan_ethhdr);
            if (ue->uenet[i]->net) {
                unregister_netdev(ue->uenet[i]->net);
                free_netdev(ue->uenet[i]->net);
                ue->uenet[i]->net = NULL;
            }
        }
    }

    return;
}

