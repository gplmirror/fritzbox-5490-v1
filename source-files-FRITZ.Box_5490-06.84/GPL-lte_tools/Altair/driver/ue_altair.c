/*
 * UE Altair Semiconductor FourGee-3100
 *
 * 2011 Bruno Caballero / AVM <b.caballero@avm.de>
 *
 * This driver is based on the ue_cdc driver by Altair and the cdc_ether
 * driver from the Linux kernel code. The hardware, although identifying
 * as CDC ECM class, requires a TTY interface for control.
 * The code is also "inspired" by: usb-serial, usb-skeleton, cdc-acm, and more
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the linux kernel you work with; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/usb/cdc.h>
#include <linux/version.h>
#include <linux/netdevice.h>

#include "ue_altair.h"

/* driver info */
static const char driver_name[] = "ue_altair";

/* one instance for each inserted device */
struct ue_device *uedevs_table[UE_DEVS];

/* Driver parameters */
static int txsize = TX_URB_BUFSIZE;
static int rxsize = RX_URB_BUFSIZE;
static int txurbs = TX_NUM_URBS;
static int rxurbs = RX_NUM_URBS;
static int bus = 0;
static int hub_port = 0;

static struct ue_device *dev_to_ue(struct device *dev)
{
    int i;
    for (i=0;i<UE_DEVS;i++) {
        if (uedevs_table[i])
            if (&uedevs_table[i]->usb->dev == dev)
                return uedevs_table[i];
    }
    return NULL;
}

/********************************************************
 * Sysfs debug infraestructur                           *
 ********************************************************/

static ssize_t ue_stats_show(struct device *dev, 
                                struct device_attribute *attr,
                                char *buf) 
{
    ssize_t result = -EINVAL;
    struct ue_device *ue = dev_to_ue(dev);
    struct ue_net *uenet, *uenet2;
    int *rx, *tx, *rx2, *tx2;
    char *backpressure_status_str1, *backpressure_status_str2, 
            *qf_status_str1, *qf_status_str2;

    if(!ue)
        return -ENODEV;

    uenet = ue->uenet[0];
    uenet2 = ue->uenet[1];
    rx = uenet->my_stats.rx_num_skbs;
    tx = uenet->my_stats.tx_num_skbs;
    rx2 = uenet2->my_stats.rx_num_skbs;
    tx2 = uenet2->my_stats.tx_num_skbs;
    backpressure_status_str1 = (uenet->my_stats.backpressure_status == 0) ? "OFF (queue active)" : "ON (stop SKB queue)";
    backpressure_status_str2 = (uenet2->my_stats.backpressure_status == 0) ? "OFF (queue active)" : "ON (stop SKB queue)";
    qf_status_str1 =    (uenet->my_stats.qf_status == 0) ? "OFF (queue active)" : "ON (stop URB queue)";
    qf_status_str2 =    (uenet2->my_stats.qf_status == 0) ? "OFF (queue active)" : "ON (stop URB queue)";
    result = sprintf(buf, "USB:\n----\nRX fixup error %d\n\n"
            "LTE0\n"
            "-----\n"
            "USB Stats:\n"
            "RX %d %d %d %d %d\n"
            "TX %d %d %d %d %d\n"
#ifdef UE_ALTAIR_FASTSKB
            "Tx OntheFly %d\n"
#endif
            "Net stats:\n"
            "RX packets %ld errors %ld\n"
            "TX packets %ld errors %ld\n"
            "Queue Stats:\n"
            "driver_backpressure_counter %d\n"
            "driver_backpressure_status %s\n"
            "modem_queue_full_notify_counter %d\n"
            "modem_queue_full_notify_status %s\n\n"
            "LTE1\n"
            "-----\n"
            "USB Stats:\n"
            "RX %d %d %d %d %d\n"
            "TX %d %d %d %d %d\n"
#ifdef UE_ALTAIR_FASTSKB
            "Tx OntheFly %d\n"
#endif
            "Net stats:\n"
            "RX packets %ld errors %ld\n"
            "TX packets %ld errors %ld\n"
            "Queue Stats:\n"
            "driver_backpressure_counter %d\n"
            "driver_backpressure_status %s\n"
            "modem_queue_full_notify_counter %d\n"
            "modem_queue_full_notify_status %s\n",
            uenet->my_stats.device_errors,
            rx[0],rx[1],rx[2],rx[3],rx[4],
            tx[0],tx[1],tx[2],tx[3],tx[4],
#ifdef UE_ALTAIR_FASTSKB
            uenet->my_stats.tx_onthefly,
#endif
            uenet->stats.rx_packets, uenet->stats.rx_errors, 
            uenet->stats.tx_packets, uenet->stats.tx_errors, 
            uenet->my_stats.backpressure, 
            backpressure_status_str1,
            uenet->my_stats.qf_count,
            qf_status_str1,
            rx2[0],rx2[1],rx2[2],rx2[3],rx2[4],
            tx2[0],tx2[1],tx2[2],tx2[3],tx2[4],
#ifdef UE_ALTAIR_FASTSKB
            uenet2->my_stats.tx_onthefly,
#endif
            uenet2->stats.rx_packets, uenet2->stats.rx_errors, 
            uenet2->stats.tx_packets, uenet2->stats.tx_errors,
            uenet2->my_stats.backpressure, 
            backpressure_status_str2,
            uenet2->my_stats.qf_count,
            qf_status_str2);
    
    return result;
}

static DEVICE_ATTR(ue_stats, S_IRUGO | S_IWUSR,
                   ue_stats_show, NULL);

const struct attribute *ue_dev_attrs = &dev_attr_ue_stats.attr;

/********************************************************
 * Status Interrupt Handling                            *
 ********************************************************/

/* Interrupt handler: INT ep control interface. */
static void ue_status_handler(struct ue_device *ue, struct urb *urb)
{
    struct usb_cdc_notification *event;
    int i, cid, queue;

    if (urb->actual_length < sizeof *event)
        return;

    event = urb->transfer_buffer;

    switch (event->bNotificationType) {
    case USB_CDC_NOTIFY_NETWORK_CONNECTION:
        if (event->wValue){
            dev_info(ue->dev, "LTE: Network Connection active!\n");
            ue->carrier_on = 1;
            for (i = 0; i < UE_NET_INTERFACES; i++) {
                netif_carrier_on(ue->uenet[i]->net);
                netif_dormant_off(ue->uenet[i]->net);
            }
        } else {
            dev_info(ue->dev, "LTE: Network disconnected\n");
            ue->carrier_on = 0;
            for (i = 0; i < UE_NET_INTERFACES; i++) {
                netif_carrier_off(ue->uenet[i]->net);
                netif_dormant_on(ue->uenet[i]->net);
            }
        }
        break;
    case USB_CDC_NOTIFY_SPEED_CHANGE:
        //We should not receive this.
        dev_info(ue->dev, "LTE: speed change (len %d)\n", urb->actual_length);
        break;
    case USB_CDC_NOTIFY_RESPONSE_AVAILABLE:
        schedule_work_on(0, &ue->ueserial->readwork);
        break;
    case ALTAIR_NOTIFY_QUEUE:
        cid = __le16_to_cpu(event->wIndex);
        if (cid < 1 || cid > UE_NET_INTERFACES)
            break;
        else
            queue = cid - 1; //First cid is 1

        if (__le16_to_cpu(event->wValue) == ALTAIR_NOTIFY_QUEUE_TH) {
            ue->uenet[queue]->my_stats.qf_status = 1;
            if (!netif_queue_stopped(ue->uenet[queue]->net))
                netif_stop_queue (ue->uenet[queue]->net);
        } else if (__le16_to_cpu(event->wValue) == ALTAIR_NOTIFY_QUEUE_TL) {
            ue->uenet[queue]->my_stats.qf_status = 0;
            if(netif_queue_stopped(ue->uenet[queue]->net)) {
                netif_wake_queue (ue->uenet[queue]->net);
                ue->uenet[queue]->my_stats.qf_count++;
            }
        }

        //dev_info(ue->dev, "LTE Event bNotificationType %x, wValue %x, wIndex %x\n", 
        //       event->bNotificationType, event->wValue, event->wIndex);
        break;
    default:
        dev_info(ue->dev, "LTE: unexpected notification %02x!\n",
                 event->bNotificationType);
        break;
    }
}

static void ue_status_complete (struct urb *urb)
{
    struct ue_device *ue = urb->context;
    int status = urb->status;

    switch (status) {
    case 0:     /* success */
        ue_status_handler(ue, urb);
        break;

    /* software-driven interface shutdown */
    case -ENOENT:           /* urb killed */
    case -ESHUTDOWN:        /* hardware gone */
        dev_err(ue->dev, "%s: intr status %d\n", __FUNCTION__, status);
        return;

    /* NOTE:  not throttling like RX/TX, since this endpoint
    * already polls infrequently
    */
    default:
        dev_err(ue->dev, "%s: intr status %d\n", __FUNCTION__, status);
        break;
    }

    //AVM/BC 20131125: We don't check network, we need the URB for AT
    /* If first Interface is not running, don't try again */
    /*if (!netif_running (ue->uenet[0]->net))
        return;
    */

    memset(urb->transfer_buffer, 0, urb->transfer_buffer_length);
    status = usb_submit_urb (urb, GFP_ATOMIC);
    if (status != 0) {
        dev_err(ue->dev, "%s: usb_submit_urb fails %d\n", __FUNCTION__, status);
    }
}

static int ue_status_init (struct ue_device *ue)
{
    unsigned        pipe = 0, maxp, period;

    pipe = usb_rcvintpipe (ue->usb, ue->intr_endp->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK);
    maxp = usb_maxpacket (ue->usb, pipe, usb_pipeout(pipe));
    period = UE_INTERRRUPT_INTERVAL;

    ue->intr_buf = kmalloc (maxp, GFP_KERNEL);
    if(!ue->intr_buf) {
        return -ENOMEM;
    } else {
        ue->intr_urb = usb_alloc_urb (0, GFP_KERNEL);
        if (!ue->intr_urb) {
            kfree (ue->intr_buf);
            return -ENOMEM;
        } else {
            usb_fill_int_urb(ue->intr_urb, ue->usb, pipe,
                                ue->intr_buf, maxp, ue_status_complete, ue, period);
        }
        ue->intr_urb_active = 0;
    }
    return 0;
}


/********************************************************
 * Init / Exit                                          *
 ********************************************************/

static int init_tx_urbs(struct ue_device *ue, int nurbs)
{
    struct ue_net *uenet;
    int i, j;

    for(j = 0; j < UE_NET_INTERFACES; j++) {

        uenet = ue->uenet[j];

        INIT_LIST_HEAD(&uenet->tx_urbs_ready);
        spin_lock_init(&uenet->tx_urbs_ready_lock);

        uenet->txurbs_pool = kzalloc(nurbs * sizeof(struct txurb), GFP_KERNEL);
        if(!uenet->txurbs_pool)
            return -ENOMEM;

        for(i = 0; i < nurbs; i++) {

            uenet->txurbs_pool[i].urb = usb_alloc_urb(0, GFP_KERNEL);
            if(!uenet->txurbs_pool[i].urb)
                return -ENOMEM;
            uenet->txurbs_pool[i].urb->transfer_flags |= URB_ZERO_PACKET;
            uenet->txurbs_pool[i].ue = ue;
            uenet->txurbs_pool[i].uenet = uenet;
            INIT_LIST_HEAD(&uenet->txurbs_pool[i].list);
            INIT_LIST_HEAD(&uenet->txurbs_pool[i].skbs);
            uenet->txurbs_pool[i].nskbs = 0;
            uenet->txurbs_pool[i].len = 0;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
            uenet->txurbs_pool[i].buf = usb_alloc_coherent(ue->usb, ue->tx_size, GFP_KERNEL, &uenet->txurbs_pool[i].buf_dma);
#else
            uenet->txurbs_pool[i].buf = usb_buffer_alloc(ue->usb, ue->tx_size, GFP_KERNEL, &uenet->txurbs_pool[i].buf_dma);
#endif
            if(!uenet->txurbs_pool[i].buf)
                return -ENOMEM;
            list_add_tail(&uenet->txurbs_pool[i].list, &uenet->tx_urbs_ready);
        }
    }
    return 0;
}


static int init_rx_urbs(struct ue_device *ue, int nurbs)
{
    int i;

    ue->rxurbs_pool = kzalloc(nurbs * sizeof(struct rxurb), GFP_KERNEL);
    if(!ue->rxurbs_pool)
        return -ENOMEM;

    for(i = 0; i < nurbs; i++) {

        ue->rxurbs_pool[i].urb = usb_alloc_urb(0, GFP_KERNEL);
        if(!ue->rxurbs_pool[i].urb)
            return -ENOMEM;
        ue->rxurbs_pool[i].urb->transfer_flags |= URB_ZERO_PACKET;
        ue->rxurbs_pool[i].ue = ue;
        INIT_LIST_HEAD(&ue->rxurbs_pool[i].list);
        ue->rxurbs_pool[i].len = 0;
#ifndef UE_ALTAIR_ZEROCOPY
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
        ue->rxurbs_pool[i].buf = usb_alloc_coherent(ue->usb, ue->rx_size, GFP_KERNEL, &ue->rxurbs_pool[i].buf_dma);
#else
        ue->rxurbs_pool[i].buf = usb_buffer_alloc(ue->usb, ue->rx_size, GFP_KERNEL, &ue->rxurbs_pool[i].buf_dma);
#endif
        if(!ue->rxurbs_pool[i].buf)
            return -ENOMEM;
#else
        ue->rxurbs_pool[i].skb = NULL;
#endif //UE_ALTAIR_ZEROCOPY
        rx_submit(ue, &ue->rxurbs_pool[i]);
    }

    return 0;

}


static void destroy_tx_urbs(struct ue_device *ue, int nurbs)
{
    struct ue_net *uenet;
    struct sk_buff *skb;
    int i, j;

    for(j = 0; j < UE_NET_INTERFACES; j++) {

        uenet = ue->uenet[j];

        for(i = 0; i < nurbs; i++){
            usb_kill_urb(uenet->txurbs_pool[i].urb);
            if(uenet->txurbs_pool[i].buf)
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
            usb_free_coherent(ue->usb, ue->tx_size, uenet->txurbs_pool[i].buf, uenet->txurbs_pool[i].buf_dma);
#else
            usb_buffer_free(ue->usb, ue->tx_size, uenet->txurbs_pool[i].buf, uenet->txurbs_pool[i].buf_dma);
#endif          
            if(uenet->txurbs_pool[i].urb)
                usb_free_urb(uenet->txurbs_pool[i].urb);
            while(!list_empty(&uenet->txurbs_pool[i].skbs)) {
                skb = (struct sk_buff*)(&uenet->txurbs_pool[i].skbs.next);
                list_del((struct list_head*) skb);
                dev_kfree_skb(skb);
            }
        }
        kfree(uenet->txurbs_pool);

        uenet->txurbs_pool = NULL;
        INIT_LIST_HEAD(&uenet->tx_urbs_ready);

    }
}


static void destroy_rx_urbs(struct ue_device *ue, int nurbs)
{
    int i;
    for(i = 0; i < nurbs; i++){
        usb_kill_urb(ue->rxurbs_pool[i].urb);
#ifndef UE_ALTAIR_ZEROCOPY
        if(ue->rxurbs_pool[i].buf)
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
            usb_free_coherent(ue->usb, ue->rx_size, ue->rxurbs_pool[i].buf, ue->rxurbs_pool[i].buf_dma);
#else
            usb_buffer_free(ue->usb, ue->rx_size, ue->rxurbs_pool[i].buf, ue->rxurbs_pool[i].buf_dma);
#endif
            
#else 
        if(ue->rxurbs_pool[i].skb)
            dev_kfree_skb(ue->rxurbs_pool[i].skb);
#endif //UE_ALTAIR_ZEROCOPY
        if(ue->rxurbs_pool[i].urb)
            usb_free_urb(ue->rxurbs_pool[i].urb);
    }
    kfree(ue->rxurbs_pool);

    ue->rxurbs_pool = NULL;
}


#ifdef UE_ALTAIR_ZEROCOPY
static int ue_concat_format_change(struct ue_device *ue)
{
    int retval = 0, ifnum;
    struct usb_device *udev;
    __be32 command = be32_to_cpu(UE_CMD_CONCAT_FORMAT_CHANGE);
    int count = sizeof(command);

    if(!ue || !ue->comm_interface)
        return -ENODEV;

    udev = interface_to_usbdev (ue->comm_interface);
    ifnum = ue->comm_interface->cur_altsetting->desc.bInterfaceNumber;

    // send down the encapuslated command.
    retval = usb_control_msg(
        udev, usb_sndctrlpipe(udev, 0),
        USB_CDC_SEND_ENCAPSULATED_COMMAND,
        USB_TYPE_CLASS | USB_RECIP_INTERFACE,
        0, ifnum, (u8*)&command, count,
        UE_CONTROL_TIMEOUT_MS);

    printk(KERN_INFO "Send concat format change command: %d chars, control msg returned %d\n", count, retval);

    if(retval < 0)
        return retval;
    return count;
}
#endif

/* Get the endpoint ! */
static struct usb_endpoint_descriptor *ue_get_ep(struct usb_interface *intf, int type, int dir)
{
    int i, tmp;
    struct usb_host_interface *alt;
    struct usb_endpoint_descriptor *endp;

    for (tmp = 0; tmp < intf->num_altsetting; tmp++) {

        alt = intf->altsetting + tmp;

        for (i = 0; i < alt->desc.bNumEndpoints; i++) {
            endp = &alt->endpoint[i].desc;
            if (((endp->bEndpointAddress & USB_ENDPOINT_DIR_MASK) == dir) &&
                ((endp->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == type))
                return endp;
        }
    }
    return NULL;
}

/* Creates a ue device */
static struct ue_device *ue_create_device(struct usb_interface *intf)
{
    struct ue_device *ue;
    struct usb_driver *driver = to_usb_driver(intf->dev.driver);
    int status;

    ue = kzalloc(sizeof(*ue), GFP_ATOMIC);
    if (!ue)
        return NULL;

    ue->usb = interface_to_usbdev(intf);
    ue->comm_interface = intf;
    ue->dev = &intf->dev;

    /* Find the slave interface */
    ue->data_interface = usb_ifnum_to_if(ue->usb, USB_INTF_NET_DATA);

    status = usb_driver_claim_interface(driver, ue->data_interface, ue);
    if (status < 0)
        goto error;

    /* status endpoint */
    ue->intr_endp = ue_get_ep(ue->comm_interface, USB_ENDPOINT_XFER_INT, USB_DIR_IN);
    if (!ue->intr_endp) {
        dev_err(ue->dev, "Can't find INT IN endpoint");
        goto error;
    }

    /* Data endpoints -> Altsetting */
    ue->in_endp = ue_get_ep(ue->data_interface, USB_ENDPOINT_XFER_BULK, USB_DIR_IN);
    if (!ue->in_endp) {
        dev_err(ue->dev, "Can't find BULK IN endpoint");
        goto error;
    }
    ue->out_endp = ue_get_ep(ue->data_interface, USB_ENDPOINT_XFER_BULK, USB_DIR_OUT);
    if (!ue->out_endp) {
        dev_err(ue->dev, "Can't find BULK OUT endpoint");
        goto error;
    }

    ue->disconnect_counter = 0;

    kref_init(&ue->ref);
    mutex_init(&ue->mutex);

    usb_set_intfdata(intf, ue);

    usb_get_dev(ue->usb);

    usb_set_interface(ue->usb, 1, 1);

    INIT_WORK (&ue->kevent, kevent);

    return ue;
error:
    kfree(ue);
    return NULL;
}


static struct ue_serial *ue_create_serial_device(struct ue_device *ue)
{
    struct ue_serial *ue_serial;

    ue_serial = kzalloc(sizeof(*ue_serial), GFP_ATOMIC);
    if (!ue_serial) {
        return NULL;
    }

    ue_serial->ttybuf = kzalloc(UE_CONTROL_MSG_SIZE, GFP_KERNEL);
    if(!ue_serial->ttybuf) {
        if(ue_serial)
            kfree(ue_serial);
        return NULL;
    }

    ue->ueserial = ue_serial;
    ue_serial->parent = ue;

    ue_serial->minor = 0;

    uetty_bind(ue_serial);

    return ue_serial;
}


static struct ue_device *ue_create_net_device(struct ue_device *ue)
{

    if(uenet_init_module(ue) != 0) {
        return NULL;
    }

    return ue;
}

static void ue_destroy_net_device(struct ue_device *ue)
{
    uenet_cleanup(ue);
}

/* called once for each interface upon device insertion */
static int ue_probe(struct usb_interface *intf,
             const struct usb_device_id *id)
{
    struct ue_device *ue = NULL;
    struct ue_serial *ueserial = NULL;
    struct usb_device *udev = interface_to_usbdev(intf);
    int status;

    /* Check if bus is supported */
    if((bus != 0) && (bus != udev->bus->busnum)){
        printk(KERN_ERR "ueservice: device ignored (bus %d)\n", udev->bus->busnum);
        return -ENODEV;
    }

    /* Check if port of internal hub is supported (6841). Level must be 2 */
    if(hub_port != 0) {
        if ((udev->level != 2) || (hub_port != udev->portnum)){
            printk(KERN_ERR "ueservice: device ignored (port %d level %d)\n", udev->portnum, udev->level);
            return -ENODEV;
        }
    }

    ue = ue_create_device(intf);
    if (!ue)
        return -ENODEV;
    
    ueserial = ue_create_serial_device(ue);
    if (!ueserial)
        return -ENODEV;

    ue_create_net_device(ue);

    /* Init URBs */
    ue->tx_urbs = txurbs;
    ue->rx_urbs = rxurbs;
    ue->tx_size = txsize;
    ue->rx_size = rxsize;

    init_usb_anchor(&ue->tx_urb_anchor);
    init_usb_anchor(&ue->rx_urb_anchor);

    status = init_tx_urbs(ue, ue->tx_urbs);
    if(status != 0) {
        goto err_tx_urbs;
    }
    status = init_rx_urbs(ue, ue->rx_urbs);
    if(status != 0) {
        goto err_rx_urbs;
    }

    /* TX Delay Function */
    init_timer(&ue->delay_tx);
    setup_timer(&ue->delay_tx, ue_do_tx, (unsigned long) ue );

    /* Status Interrupt initiation */
    status = ue_status_init(ue);
    if(status < 0) {
        printk(KERN_ERR "ue_status_init failed: %d\n", status);
        goto err;
    }

#ifdef UE_ALTAIR_ZEROCOPY
    /* Switch to Zero Copy Modus */
    ue_concat_format_change(ue);
#endif

    status = sysfs_create_file(&ue->usb->dev.kobj, ue_dev_attrs);
    if (status < 0) {
        printk(KERN_ERR "cannot setup sysfs: %d", status);
        goto err;
    }

    return 0;

err:
    destroy_rx_urbs(ue, ue->rx_urbs);
err_rx_urbs:
    destroy_tx_urbs(ue, ue->tx_urbs);
err_tx_urbs:
    return status;
}

/* device removed, cleaning up */
static void ue_disconnect(struct usb_interface *intf)
{
    struct ue_device *ue = NULL;

    ue = usb_get_intfdata(intf);
    usb_set_intfdata(intf, NULL);
    if(!ue) {
        BUG_ON("ue must never be NULL");
        return;
    }

    /* AVM/BC 19.12.2013: disconnect called twice because 2 Interfaces are used */
    mutex_lock(&ue->mutex);
    ue->disconnect_counter++;
    if (ue->disconnect_counter < 2) {
        mutex_unlock(&ue->mutex);
        return;
    }
    mutex_unlock(&ue->mutex);

    sysfs_remove_file(&ue->usb->dev.kobj, ue_dev_attrs);

    uetty_unbind(ue->ueserial);

    destroy_rx_urbs(ue, ue->rx_urbs);
    destroy_tx_urbs(ue, ue->tx_urbs);

    del_timer_sync(&ue->delay_tx);

    ue_destroy_net_device(ue);

    /* decrement our usage count */
    usb_put_dev(ue->usb);

    kfree(ue->intr_buf);
    kfree(ue->ueserial->ttybuf);
    kfree(ue->ueserial);
    kfree(ue);

    return;
}

/* called by kernel when we need to suspend device */
static int ue_suspend(struct usb_interface *iface, pm_message_t message)
{
    //FIXME: Implement it.
    return 0;
}

/* called by kernel when we need to resume device */
static int ue_resume(struct usb_interface *iface)
{
    //FIXME: Implement it.
    return 0;
}


static const struct usb_device_id ue_ids[] = {
    {
        USB_DEVICE_AND_INTERFACE_INFO(0x216f, 0x0041, USB_CLASS_COMM,
                                      0xff, 0x00)
    },
    { },// END
};
MODULE_DEVICE_TABLE(usb, ue_ids);


static struct usb_driver ue_driver = {
    .name = driver_name,
    .probe = ue_probe,
    .disconnect = ue_disconnect,
    .id_table = ue_ids,
    .suspend = ue_suspend,
    .resume = ue_resume,
};

static int __init ue_init(void)
{
    int result;

    /* register tty driver */
    result = uetty_init();
    if (result) {
        printk(KERN_ERR "Could not register TTY driver. Error: %d\n", result);
        return result;
    }

    /* register the usb driver */
    result = usb_register(&ue_driver);
    if (result) {
        printk(KERN_ERR "Could not register usb driver. Error: %d", result);
        return result;
    }

    return 0;
}


static void __exit ue_exit(void)
{
    /* deregister this driver with the USB subsystem */
    usb_deregister(&ue_driver);

    /* deregister tty driver */
    uetty_exit();
}


/* Module definitions */
module_init(ue_init);
module_exit(ue_exit);

MODULE_AUTHOR(MOD_AUTHOR);
MODULE_DESCRIPTION(MOD_DESCRIPTION);
MODULE_LICENSE(MOD_LICENSE);
MODULE_INFO(Version, DRIVER_VERSION);

MODULE_PARM_DESC(txsize, "tx buffer size");
module_param(txsize, int, S_IRUGO | S_IWUSR);

MODULE_PARM_DESC(rxsize, "rx buffer size");
module_param(rxsize, int, S_IRUGO | S_IWUSR);

MODULE_PARM_DESC(txurbs, "number of tx URBs");
module_param(txurbs, int, S_IRUGO | S_IWUSR);

MODULE_PARM_DESC(rxurbs, "number of rx URBs");
module_param(rxurbs, int, S_IRUGO | S_IWUSR);

MODULE_PARM_DESC(bus, "USB Bus supported (0: all, 1: Only bus 1, 2: Only Bus 2)");
module_param(bus, int, S_IRUGO | S_IWUSR);

MODULE_PARM_DESC(hub_port, "when internal hub, which port is supported");
module_param(hub_port, int, S_IRUGO | S_IWUSR);

