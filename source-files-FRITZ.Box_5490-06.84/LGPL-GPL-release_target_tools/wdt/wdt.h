
#define WDT_OK                   0
#define WDT_REQUEST              1
#define WDT_ALREADY_REGISTERED  -1
#define WDT_UNKNOWN_HANDLE      -2
#define WDT_INVALID_TIMEOUT     -3
#define WDT_RESOURCE_ERROR      -4

/*-------------------------------------------------------------------------------------*\
 * K�ndigt das Starten einer Applikation an.
 *
 * name: null-terminierter Name des Prozesses (mu� eindeutig sein)
 * timeout: Intervall in Sekunden, innerhalb dessen sich die Applikation mit demselben Namen
 *          registrieren mu�.
 *
 * returns: WDT_OK, wenn erfolgreich
\*-------------------------------------------------------------------------------------*/
int watchdog_announce(char* name, int timeout);

/*-------------------------------------------------------------------------------------*\
 * Registriert einen Proze� beim Watchdog
 * Der Default Timeout betr�gt 20s, nach 10s wird die Applikation benachrichtigt (optional)
 *
 * name: null-terminierter Name des Prozesses (mu� eindeutig sein)
 *
 * returns:    > 0: handle f�r registrierten Proze�, sonst Fehler
\*-------------------------------------------------------------------------------------*/
int watchdog_register(char* name);

/*-------------------------------------------------------------------------------------*\
 * Liefert den Filehandle zum Watchdog-Handle zur�ck
 *
 * handle: Beim Register erhaltener Handle
 * returns: file handle
\*-------------------------------------------------------------------------------------*/
int watchdog_get_fd(int handle);

/*-------------------------------------------------------------------------------------*\
 * Deregistriert einen Proze� beim Watchdog
 *
 * handle: Beim Register erhaltener Handle
 *
 * returns: WDT_OK, wenn erfolgreich, sonst Fehler
 *
 * NOTE: Ein Proze� mu� sich per watchdog_release() abmelden, bevor er sich beendet,
 *       anderenfalls wird ein Reboot ausgel�st! Dies gilt auch f�r den Fall, da� der
 *       Proze� von einem anderen Proze� (gewollt) beendet wird. Hierf�r ist ein
 *       watchdog_release() im Signal-Handler vorzusehen.
\*-------------------------------------------------------------------------------------*/
int watchdog_release(int handle);

/*-------------------------------------------------------------------------------------*\
 * �ndert den default Timeout f�r den registrierten Proze�
 *
 * handle:  Beim Register erhaltener Handle
 * timeout: neuer Timeout in Sekunden ab sofort (Maximum 40s)
 *
 * returns: WDT_OK, sonst Fehler
 *
 * NOTE: watchdog_settimeout() dient gleichzeitig als Trigger f�r den Watchdog
\*-------------------------------------------------------------------------------------*/
int watchdog_settimeout(int handle, int timeout);

/*-------------------------------------------------------------------------------------*\
 * Triggert den Watchdog f�r den angegebenen Proze�
 *
 * handle:  Beim Register erhaltener Handle
 *
 * returns: WDT_OK, sonst Fehler
\*-------------------------------------------------------------------------------------*/
int watchdog_trigger(int handle);

/*-------------------------------------------------------------------------------------*\
 * Blockiert, bis Watchdog neu getriggert werden mu�
 * Nach R�ckkehr aus der Funktion hat die Applikation noch die H�fte des gesetzten Timeouts
 * Zeit zum Zur�cksetzen des Watchdogs.
 *
 * handle:  Beim Register erhaltener Handle
 *
 * returns: WDT_OK, sonst Fehler
\*-------------------------------------------------------------------------------------*/
int watchdog_wait(int handle);

/*-------------------------------------------------------------------------------------*\
 * Teilt mit, ob der Watchdog neu getriggert werden mu�
 * Die Funktion blockiert nicht.
 * 
 * handle:  Beim Register erhaltener Handle
 *
 * returns: WDT_OK vor Ablauf der H�lfte des gesetzten Timeouts, ungleich WDT_OK danach
\*-------------------------------------------------------------------------------------*/
int watchdog_poll(int handle);

