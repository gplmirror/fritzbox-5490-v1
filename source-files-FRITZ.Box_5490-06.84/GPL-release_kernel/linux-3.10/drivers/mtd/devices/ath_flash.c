/*
 * This file contains glue for Atheros ath spi flash interface
 * Primitives are ath_spi_*
 * mtd flash implements are ath_flash_*
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <asm/delay.h>
#include <asm/cacheflush.h>
#include <asm/r4kcache.h>
#include <asm/io.h>
#include <asm/div64.h>

#include <atheros.h>
#include <ar71xx_regs.h>
#include "ath_flash.h"

#include "../mtdcore.h"

#define MTD_AVM_PARTITION_PARSER

/*
 * statics
 */
static void ath_spi_write_enable(void);
static void ath_spi_poll(unsigned int locked);

static int ath_flash_probe(struct platform_device* pdev);
static int ath_flash_remove(struct platform_device* pdev);

#if !defined(ATH_SST_FLASH)
static void ath_spi_write_page(uint32_t addr, uint8_t * data, int len);
#endif
static void ath_spi_sector_erase(uint32_t addr);

static const char *part_probes[] __initdata = { "find_jffs2", "find_squashfs", "cmdlinepart", "RedBoot", NULL };

#define ATH_FLASH_SIZE_2MB          ( 2*1024*1024)
#define ATH_FLASH_SIZE_4MB          ( 4*1024*1024)
#define ATH_FLASH_SIZE_8MB          ( 8*1024*1024)
#define ATH_FLASH_SIZE_16MB         (16*1024*1024)
#define ATH_FLASH_SECTOR_SIZE_64KB  (64*1024)
#define ATH_FLASH_PG_SIZE_256B       256
#define ATH_FLASH_NAME               "ath-nor"

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
static DEFINE_SPINLOCK(nmi_lock);

/*--- #define FLASH_ACCESS_STATISTIC ---*/
#if defined(FLASH_ACCESS_STATISTIC)
struct _generic_stat {
    unsigned long long avg;
    unsigned long cnt; 
    unsigned long min;
    unsigned long max;
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void init_generic_stat(struct _generic_stat *pgstat) {
    pgstat->min = ULONG_MAX;
    pgstat->max = 0;
    pgstat->cnt = 0;
    pgstat->avg = 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void generic_stat(struct _generic_stat *pgstat, signed long val) {
    if(pgstat->cnt == 0) {
        init_generic_stat(pgstat);
    }
    if(val > pgstat->max) pgstat->max = val;
    if(val < pgstat->min) pgstat->min = val;
    pgstat->avg += (unsigned long long) val;
    pgstat->cnt++;
}
#define CYCLE_MHZ  280
#define CLK_TO_USEC(a) ((a) / CYCLE_MHZ)
/*--------------------------------------------------------------------------------*\
 * timebase: 0 unveraendert, 1: clk ->  usec
 * reset: Statistik ruecksetzen
\*--------------------------------------------------------------------------------*/
static void display_generic_stat(char *prefix, struct _generic_stat *pgstat, unsigned int timebase, unsigned int reset) {
    struct _generic_stat gstat;
    unsigned long flags, tmp;
    signed long cnt = pgstat->cnt;
    spin_lock_irqsave(&nmi_lock, flags);
    if(cnt == 0) {
        spin_unlock_irqrestore(&nmi_lock, flags);
        return;
    }
    memcpy(&gstat,pgstat, sizeof(gstat));
    if(reset) {
        pgstat->cnt = 0;
    }
    spin_unlock_irqrestore(&nmi_lock, flags);
    tmp = cnt;
    while(gstat.avg > 0xFFFFFFFFLU) {
        gstat.avg  >>= 1;
        tmp        >>= 1;
    }
    switch(timebase) {
        case 0: 
            printk(KERN_ERR "%s[%lu] min=%lu max=%lu avg=%lu\n", prefix, cnt, gstat.min, gstat.max, tmp ? (long)gstat.avg / tmp : 0);
            break;
        case 1: 
            printk(KERN_ERR "%s[%lu] min=%lu max=%lu avg=%lu usec\n", prefix, cnt, CLK_TO_USEC(gstat.min), CLK_TO_USEC(gstat.max), tmp ? CLK_TO_USEC((long)gstat.avg / tmp) : 0);
            break;
    }
}
static struct _generic_stat erase_stat, write_stat, read_stat;
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void display_periodical_statistic(void) {
    static unsigned int last;
    if((jiffies - last) >  10 * HZ) {   
        last = jiffies;
        /*--- display_generic_stat("flash:read", &read_stat, 1, 1); ---*/
        display_generic_stat("flash:write", &write_stat, 1, 1);
        display_generic_stat("flash:erase", &erase_stat, 1, 1);
    }
}
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/

#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

static struct platform_driver ath_flash_driver = {
    .probe  = ath_flash_probe,
    .remove = ath_flash_remove,
    .driver = {
                .name = ATH_FLASH_NAME,              
              },
};

/*------------------------------------------------------------------------------------------*\
 * bank geometry
\*------------------------------------------------------------------------------------------*/
typedef struct ath_flash_geom {
	uint32_t size;
	uint32_t sector_size;
	uint32_t nsectors;
	uint32_t pgsize;
} ath_flash_geom_t;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
ath_flash_geom_t flash_geom_tbl[ATH_FLASH_MAX_BANKS] = {
	{
		.size		    = ATH_FLASH_SIZE_16MB,
		.sector_size	= ATH_FLASH_SECTOR_SIZE_64KB,
		.pgsize		    = ATH_FLASH_PG_SIZE_256B
	}
};

static DEFINE_SEMAPHORE(ath_flash_sem);

/*------------------------------------------------------------------------------------------*\
 * Locking API
\*------------------------------------------------------------------------------------------*/
void ath_flash_spi_down(void) {
	down(&ath_flash_sem);
}

void ath_flash_spi_up(void) {
	up(&ath_flash_sem);
}

EXPORT_SYMBOL(ath_flash_spi_down);
EXPORT_SYMBOL(ath_flash_spi_up);

#if defined(ATH_SST_FLASH)
void
ath_spi_flash_unblock(void)
{
	ath_spi_write_enable();
	ath_spi_bit_banger(ATH_SPI_CMD_WRITE_SR);
	ath_spi_bit_banger(0x0);
	ath_spi_go();
	ath_spi_poll(0);
}
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ath_flash_erase(struct mtd_info *mtd, struct erase_info *instr) {
	int nsect, s_curr, s_last;
	uint64_t  res;

	if (instr->addr + instr->len > mtd->size)
		return (-EINVAL);

	ath_flash_spi_down();

	res = instr->len;
	do_div(res, mtd->erasesize);
	nsect = res;

	if (((uint32_t)instr->len) % mtd->erasesize)
		nsect ++;

	res = instr->addr;
	do_div(res,mtd->erasesize);
	s_curr = res;

	s_last  = s_curr + nsect;

	do {
		ath_spi_sector_erase(s_curr * ATH_SPI_SECTOR_SIZE);
	} while (++s_curr < s_last);
#if !defined(CONFIG_NMI_ARBITER_WORKAROUND)
	ath_spi_done();
#endif/*--- #if !defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

	ath_flash_spi_up();

	if (instr->callback) {
		instr->state |= MTD_ERASE_DONE;
		instr->callback(instr);
	}
#if defined(FLASH_ACCESS_STATISTIC)
    display_periodical_statistic();
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/

	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _nmi_vector_gap {
    unsigned int in_use;
    unsigned long long start;
    unsigned long long gap_size;
    unsigned long long end;
};

struct _nmi_vector_gap nmi_vector_gap;

void set_nmi_vetor_gap(unsigned int start, unsigned int firmware_size, unsigned int gap_size) {
    nmi_vector_gap.start     = (unsigned long long)start & ((16ULL << 20) - 1ULL);   /*--- funktioniert bis zur Flashgröße von 16 MByte ---*/
    nmi_vector_gap.gap_size  = gap_size;
    nmi_vector_gap.end       = (unsigned long long)firmware_size; 
    nmi_vector_gap.in_use    = 1;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void local_memcpy(uint8_t *to, uint8_t *from, size_t len) {
    uint32_t addr = (uint32_t)from | 0xbf000000;
    flush_icache_range((uint32_t)to, (uint32_t)to + len);
    memcpy(to, (void *)addr, len);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ath_flash_read(struct mtd_info *mtd, loff_t from, size_t len, size_t *retlen, u_char *buf) {

	/*--- uint32_t addr = from | 0x9f000000; ---*/
	uint32_t addr = from;

	if (!len) return (0);
	if (from + len > mtd->size) {
        printk("%s:error: from=%llx len=%d\n", __func__, from, len);
		return (-EINVAL);
    }

	ath_flash_spi_down();

#if 0
	memcpy(buf, (uint8_t *)(addr), len);
#else
    /*--- wir sind jenseites/höher als der nmi vector gap aber unterhalb des JFFS ---*/
    if (nmi_vector_gap.in_use && (addr > nmi_vector_gap.start) && (addr < nmi_vector_gap.end)) {
        /*--- printk(KERN_ERR "[%s] addr 0x%x (groesser gap start, kleiner end: %llx)\n", __FUNCTION__, addr, nmi_vector_gap.end); ---*/
	    local_memcpy(buf, (uint8_t *)((unsigned long)addr + (unsigned long)nmi_vector_gap.gap_size), len);

    /*--- wir sind unterhalb des nmi vector gaps, die länge kann aber hineinreichen ---*/
    } else if (nmi_vector_gap.in_use && (addr <= nmi_vector_gap.start)) {

        /*--- eine Aufteilung ist nötig ---*/
        if(addr + len > nmi_vector_gap.start) {
            unsigned int len_part = nmi_vector_gap.start - addr;
            /*--- printk(KERN_ERR "[%s] addr 0x%x (kleiner gap start, aufteilung noetig, part 0x%x und 0x%x)\n", ---*/ 
                    /*--- __FUNCTION__, addr, len_part, len - len_part); ---*/
            if(len_part)
	            local_memcpy(buf, (uint8_t *)addr, len_part);
	        local_memcpy(buf + len_part, (uint8_t *)addr + nmi_vector_gap.gap_size + len_part, len - len_part);

        /*--- eine Aufteilung ist nicht nötig ---*/
        } else {
            /*--- printk(KERN_ERR "[%s] addr 0x%x (kleiner gap start)\n", __FUNCTION__, addr); ---*/ 
	        local_memcpy(buf, (uint8_t *)addr, len);
        }
    } else {
        /*--- printk(KERN_ERR "[%s] addr 0x%x (groesser end 0x%llx)\n", __FUNCTION__, addr, nmi_vector_gap.end); ---*/
	    local_memcpy(buf, (uint8_t *)addr, len);
    }
#endif
	*retlen = len;

	ath_flash_spi_up();

#if defined(FLASH_ACCESS_STATISTIC)
    /*--- generic_stat(&read_stat, get_cycles() - cycles); ---*/
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/

	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(ATH_SST_FLASH)
static int ath_flash_write(struct mtd_info *mtd, loff_t dst, size_t len, size_t * retlen, const u_char * src) {

	uint32_t val;

	//printk("write len: %lu dst: 0x%x src: %p\n", len, dst, src);

	*retlen = len;

	for (; len; len--, dst++, src++) {
		ath_spi_write_enable();	// dont move this above 'for'
		ath_spi_bit_banger(ATH_SPI_CMD_PAGE_PROG);
		ath_spi_send_addr(dst);

		val = *src & 0xff;
		ath_spi_bit_banger(val);

		ath_spi_go();
		ath_spi_poll(0);
	}
	/*
	 * Disable the Function Select
	 * Without this we can't re-read the written data
	 */
	ath_reg_wr(ATH_SPI_FS, 0);

	if (len) {
		*retlen -= len;
		return -EIO;
	}
	return 0;
}
#else
static int ath_flash_write(struct mtd_info *mtd, loff_t to, size_t len, size_t *retlen, const u_char *buf) {

	int total = 0, len_this_lp, bytes_this_page;
	uint32_t addr = 0;
	u_char *mem;

	ath_flash_spi_down();

    /*--- printk("spi: SPI_CONTROL %08x: %08x divider=%d+1\n", ATH_SPI_CLOCK, ath_reg_rd(ATH_SPI_CLOCK), (ath_reg_rd(ATH_SPI_CLOCK) & ~(0x1 << 5))); ---*/

	while (total < len) {
		mem                 = (u_char *) (buf + total);
		addr                = to + total;
		bytes_this_page     = ATH_SPI_PAGE_SIZE - (addr % ATH_SPI_PAGE_SIZE);
		len_this_lp         = min(((int)len - total), bytes_this_page);

		ath_spi_write_page(addr, mem, len_this_lp);
		total += len_this_lp;
	}
#if !defined(CONFIG_NMI_ARBITER_WORKAROUND)
	ath_spi_done();
#endif/*--- #if !defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

	ath_flash_spi_up();

#if defined(FLASH_ACCESS_STATISTIC)
    display_periodical_statistic();
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/

	*retlen = len;
	return 0;
}
#endif

/*------------------------------------------------------------------------------------------*\
 * sets up flash_info and returns size of FLASH (bytes)
\*------------------------------------------------------------------------------------------*/
static int ath_flash_probe(struct platform_device* pdev) {

	struct mtd_partition *mtd_parts = NULL;     /*--- part-pointer wird von jffs2-parser gefuellt ---*/
	int np;
	ath_flash_geom_t *geom;
	struct mtd_info *mtd;
	uint8_t index = 0;

	sema_init(&ath_flash_sem, 1);

#if defined(ATH_SST_FLASH)
	ath_reg_wr_nf(ATH_SPI_CLOCK, 0x3);
	ath_spi_flash_unblock();
	ath_reg_wr(ATH_SPI_FS, 0);
#else
#if !defined CONFIG_SOC_AR934X && !defined CONFIG_SOC_QCA953X
	ath_reg_wr_nf(ATH_SPI_CLOCK, 0x43);
#endif
#endif

    geom = &flash_geom_tbl[index];

    mtd = kmalloc(sizeof(struct mtd_info), GFP_KERNEL);
    if (!mtd) {
        printk("Cant allocate mtd stuff\n");
        return -1;
    }
    memset(mtd, 0, sizeof(struct mtd_info));

    mtd->name		        = ATH_FLASH_NAME;
    mtd->type		        = MTD_NORFLASH;
    mtd->flags		        = MTD_CAP_NORFLASH | MTD_WRITEABLE;
    mtd->size		        = geom->size;
    mtd->erasesize		    = geom->sector_size;
    mtd->numeraseregions	= 0;
    mtd->eraseregions	    = NULL;
    mtd->owner		        = THIS_MODULE;
    mtd->_erase		        = ath_flash_erase;
    mtd->_read		        = ath_flash_read;
    mtd->_write		        = ath_flash_write;
    mtd->writesize		    = 1;

    np = parse_mtd_partitions(mtd, part_probes, &mtd_parts, 0);

#ifdef MTD_AVM_PARTITION_PARSER
    if (np > 0) {
        /*-----------------------------------------------------------------*\
         * Partitionenen gefunden, add_mtd_partions traegt diese als
         * mtds in 'mtd_table' ein
        \*-----------------------------------------------------------------*/
        add_mtd_partitions(mtd, mtd_parts, np);
        return 0;
    } else {
        printk("[%s] No partitions flash found\n", __func__);
    }
#endif

	/*-----------------------------------------------------------------*\
	 * keine Partitionenen gefunen, add_mtd_device 
	 * traegt das gesamte mtd 'mtd_table' ein
	\*-----------------------------------------------------------------*/
	add_mtd_device(mtd);
	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ath_flash_remove(struct platform_device* pdev __attribute__((unused))) {
	/*
	 * nothing to do
	 */

    return 0;
}

/*------------------------------------------------------------------------------------------*\
 * Primitives to implement flash operations
\*------------------------------------------------------------------------------------------*/
static void ath_spi_write_enable() {

    do {
        ath_reg_wr_nf(ATH_SPI_FS, 1);
    } while(ath_reg_rd(ATH_SPI_FS) != 1);

	ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CS_DIS);
	ath_spi_bit_banger(ATH_SPI_CMD_WREN);
	ath_spi_go();
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void ath_spi_poll(unsigned int locked) {

	int rd;

    /*--------------------------------------------------------------------------------*\
     * ausgemessen: 5 Durchlaeufe pro 10 usec - es werden mindestens 500 us benoetigt
    \*--------------------------------------------------------------------------------*/
    do {
        ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CS_DIS);     
        ath_spi_bit_banger(ATH_SPI_CMD_RD_STATUS);        
        ath_spi_delay_8();
        rd = (ath_reg_rd(ATH_SPI_RD_STATUS) & 1);               
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
        if(locked == 0) {
            unsigned long flags;
            /*--------------------------------------------------------------------------------*\
             * wegen NMI-Workaround
             * wir sind im Erase-Modus: geschuetzt warten, um CPU-Zugriff zu entspannen
             * da kein Preempt kommen aktuell sowieso nur Sw-Irqs/Irqs ran
            \*--------------------------------------------------------------------------------*/
            spin_lock_irqsave(&nmi_lock, flags);
            udelay(100);
            spin_unlock_irqrestore(&nmi_lock, flags);
        }
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
	} while (rd);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void ath_spi_write_page(uint32_t addr, uint8_t *data, int len) {

	int i;
	uint8_t ch;
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    unsigned long flags;
    unsigned long cycles __attribute__((unused));
    spin_lock_irqsave(&nmi_lock, flags);
    cycles = get_cycles();
    ath_workaround_nmi_suspendresume(0x1 | 0x2);
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

	ath_spi_write_enable();
	ath_spi_bit_banger(ATH_SPI_CMD_PAGE_PROG);
	ath_spi_send_addr(addr);

	for (i = 0; i < len; i++) {
		ch = *(data + i);
		ath_spi_bit_banger(ch);
	}

	ath_spi_go();
	ath_spi_poll(1);

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    ath_spi_done();

    dma_cache_wback_inv((unsigned int)data, len);
    ath_workaround_nmi_suspendresume(0x0 | 0x2);
#if defined(FLASH_ACCESS_STATISTIC)
    generic_stat(&write_stat, get_cycles() - cycles);
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/
    spin_unlock_irqrestore(&nmi_lock, flags);
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void ath_spi_sector_erase(uint32_t addr) {

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    unsigned long cycles __attribute__((unused));
    cycles = get_cycles();
    ath_workaround_nmi_suspendresume(0x1 | 0x4);
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
	ath_spi_write_enable();
	ath_spi_bit_banger(ATH_SPI_CMD_SECTOR_ERASE);
	ath_spi_send_addr(addr);
	ath_spi_go();
#if 0
	/*
	 * Do not touch the GPIO's unnecessarily. Might conflict
	 * with customer's settings.
	 */
	display(0x7d);
#endif
	ath_spi_poll(0);

#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    ath_spi_done();
    ath_workaround_nmi_suspendresume(0x0 | 0x4);
#if defined(FLASH_ACCESS_STATISTIC)
    generic_stat(&erase_stat, get_cycles() - cycles);
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/

}

/*------------------------------------------------------------------------------------------*\
 * Module Functions
\*------------------------------------------------------------------------------------------*/
static int __init ath_flash_init (void) {
    printk(KERN_INFO "Registering ATH-flash-driver...\n");

#if defined(FLASH_ACCESS_STATISTIC)
    init_generic_stat(&erase_stat);
    init_generic_stat(&write_stat);
    init_generic_stat(&read_stat);
#endif/*--- #if defined(FLASH_ACCESS_STATISTIC) ---*/

    return platform_driver_register(&ath_flash_driver);
}

static void __exit ath_flash_exit(void) {
    platform_driver_unregister(&ath_flash_driver);
}

module_init(ath_flash_init);
module_exit(ath_flash_exit);
