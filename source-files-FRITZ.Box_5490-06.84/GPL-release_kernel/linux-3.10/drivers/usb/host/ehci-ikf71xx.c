/*
 * EHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2002 David Brownell <dbrownell@users.sourceforge.net>
 * (C) Copyright 2002 Hewlett-Packard Company
 *
 * Bus Glue for Ikanos IKF71XX
 *
 * Written by Christopher Hoover <ch@hpl.hp.com>
 * Based on fragments of previous driver by Rusell King et al.
 *
 * Modified for LH7A404 from ohci-sa1111.c
 *  by Durgesh Pattamatta <pattamattad@sharpsec.com>
 * Modified for AMD Alchemy Au1xxx
 *  by Matt Porter <mporter@kernel.crashing.org>
 * Modified for IKANOS IKF71XX
 *  by Kiran Kumar CSK <kcsk@ikanos.com>
 *
 * This file is licenced under the GPL.
 */

/**
 * Some part of this file is modified by Ikanos Communications. 
 *
 * Copyright (C) 2013-2014 Ikanos Communications.
 */

#include <linux/platform_device.h>
#include <fusiv_usb.h>
#include <vx185_scu.h>

extern int usb_disabled(void);

static unsigned char fusiv_usb_enable;

/* USB PHY Reset sequence, UTMI configuration */
static void do_phy_reset (void) {

	/*
         * UTMI PHY port 0 & 1 reset, POR and register reset
	 * Enable big-endian for transactions data , INCR4/8/16 xfers,
         * start on burst boundaries, AP_START_CLOCK is on
         */
	*USB_GNL_REG_1 = 0x2FFFF;
  
	udelay(1000);

	/* Deassert PHY Reset */
	*USB_GNL_REG_1 = 0x2FFFE;

	udelay(1000);

	/* Deassert POR & port resets */
	*USB_GNL_REG_1 = 0x2FFF0;
	
	udelay(1000);
}

/* Bring up the UTMI PHY, host controller */
static void ikf71xx_start_ehci_hc(struct platform_device *dev)
{
	volatile uint regval = 0;

	pr_debug(__FILE__ ": starting VX185 EHCI USB Controller\n");

	/* Should not issue reset if OHCI driver has already done it */
	if ((fusiv_usb_enable & FUSIV_USB_OHCI_ON) == 0) {

		printk("EHCI: Enabling USB clocks and USB PHY reset\n");

		/* Enable USB clock */
		regval = scu_regs->clk_gate_ctl;
		if (!(regval & (0x1 << USB_CLK_GATE))) {
			regval |= (0x1 << USB_CLK_GATE);
			scu_regs->clk_gate_ctl = regval;
		} 
	
		/* Enable USB clock PLL Post Div 3 Register */
		regval = scu_regs->pll_pd3_ctl;
		if (!(regval & (0x1 << USB_CLK_PLL_PDIV3))) {
			regval |= (0x1 << USB_CLK_PLL_PDIV3);
			scu_regs->pll_pd3_ctl = regval;
		} 

		/* Unmask USB reset */
		regval = scu_regs->rst_mask;
		if (regval & (0x1 << USB_RESET))
		{
			regval &= ~(0x1 << USB_RESET);
			scu_regs->rst_mask = regval;
		} 
	
		/* Assert USB reset */
		regval = scu_regs->rst_vec;
		if (!(regval & (0x1 << USB_RESET))) {
			regval |= (0x1 << USB_RESET);
			scu_regs->rst_vec = regval;
		} 

		udelay(1000);

		/* Mask USB reset back */
		regval = scu_regs->rst_mask;
		if (!(regval & (0x1 << USB_RESET)))
		{
			regval |= (0x1 << USB_RESET);
			scu_regs->rst_mask = regval;
		} 

		/* UTMI PHY reset */
		do_phy_reset();
	}

}

/* Disable host controller  */
static void ikf71xx_stop_ehci_hc(struct platform_device *dev)
{
	volatile uint regval = 0;

	pr_debug(__FILE__ ": stopping IKF71XX EHCI USB Controller\n");

	if ((fusiv_usb_enable & FUSIV_USB_OHCI_ON) == 0) {
		/* Disable USB clock PLL Post Div 3 Register */
		regval = scu_regs->pll_pd3_ctl;
		if (regval & (0x1 << USB_CLK_PLL_PDIV3)) {
			regval &= ~(0x1 << USB_CLK_PLL_PDIV3);
			scu_regs->pll_pd3_ctl = regval;
		} 

		/* Disable USB clock */
		regval = scu_regs->clk_gate_ctl;
		if (regval & (0x1 << USB_CLK_GATE)) {
			regval &= ~(0x1 << USB_CLK_GATE);
			scu_regs->clk_gate_ctl = regval;
		}
	} 
}


/*-------------------------------------------------------------------------*/

/* configure so an HC device and id are always provided */
/* always called with process context; sleeping is OK */


/**
 * usb_hcd_ikf71xx_probe - initialize Au1xxx-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int usb_hcd_ikf71xx_ehci_probe (const struct hc_driver *driver,
			  struct platform_device *dev)
{
	int retval = 0;
	struct usb_hcd *hcd;
	struct ehci_hcd *ehci;

	pr_debug(__FILE__ ": Probing USB EHCI  \n");

        ikf71xx_start_ehci_hc(dev);

        /* offset 1 contains IRQ info */
	if(dev->resource[1].flags != IORESOURCE_IRQ) {
		pr_debug ("resource[2] is not IORESOURCE_IRQ");
		return -ENOMEM;
	}

	hcd = usb_create_hcd(driver, &dev->dev, "ikf71xx ehci");
	if (!hcd)
		return -ENOMEM;

        /* offset 0 contains EHCI info */
	hcd->rsrc_start = dev->resource[0].start;
	hcd->rsrc_len = dev->resource[0].end - dev->resource[0].start + 1;

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		pr_debug("request_mem_region failed");
		retval = -EBUSY;
		goto err1;
	}

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		pr_debug("ioremap failed");
		retval = -ENOMEM;
		goto err2;
	}

	ehci = hcd_to_ehci(hcd);
	ehci->caps = hcd->regs;
	ehci->regs = hcd->regs + HC_LENGTH(ehci,readl(&ehci->caps->hc_capbase));

	/* cache this readonly data; minimize chip reads */
	ehci->hcs_params = readl(&ehci->caps->hcs_params);
	
	/* Convert QH/QTD descriptors to big-endian */
	ehci->big_endian_desc = 1;

#if (defined (CONFIG_FUSIV_VX185) || defined(CONFIG_FUSIV_VX585)) &&  defined (CONFIG_CPU_MIPSR2_IRQ_VI)
	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_SHARED | IRQF_DISABLED );
#else
	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_DISABLED | IRQF_SAMPLE_RANDOM);
#endif
	if (retval == 0) {
		fusiv_usb_enable |= FUSIV_USB_EHCI_ON;
		return retval;
	}

        ikf71xx_stop_ehci_hc(dev);
	iounmap(hcd->regs);
 err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
 err1:
	usb_put_hcd(hcd);
	return retval;
}


/* may be called without controller electrically present */
/* may be called with controller, bus, and devices active */

/**
 * usb_hcd_au1xxx_remove - shutdown processing for Au1xxx-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_hcd_ikf71xx_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void usb_hcd_ikf71xx_echi_remove (struct usb_hcd *hcd, struct platform_device *dev)
{
	usb_remove_hcd(hcd);
        pr_debug("stopping IKF71XX EHCI USB Controller\n");
	ikf71xx_stop_ehci_hc(dev);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
	fusiv_usb_enable &= ~FUSIV_USB_EHCI_ON;
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ehci_ikf71xx_hc_driver = {
	.description = hcd_name,
	.product_desc = "Ikanos On-Chip EHCI Host Controller",
	.hcd_priv_size = sizeof(struct ehci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq = ehci_irq,
	.flags = HCD_MEMORY | HCD_USB2,

	/*
	 * basic lifecycle operations
	 */
	.reset = ehci_init,
	.start = ehci_run,
	.stop = ehci_stop,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue = ehci_urb_enqueue,
	.urb_dequeue = ehci_urb_dequeue,
	.endpoint_disable = ehci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number = ehci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data = ehci_hub_status_data,
	.hub_control = ehci_hub_control,
#ifdef	CONFIG_PM
	.bus_suspend = ehci_bus_suspend,
	.bus_resume = ehci_bus_resume,
#endif
};


/*-------------------------------------------------------------------------*/
static int ehci_hcd_ikf71xx_drv_probe(struct platform_device *pdev)
{
	pr_debug("In ehci_hcd_ikf71xx_drv_probe\n");

	if (usb_disabled())
		return -ENODEV;

	return usb_hcd_ikf71xx_ehci_probe(&ehci_ikf71xx_hc_driver, pdev);
}

static int ehci_hcd_ikf71xx_drv_remove(struct platform_device *pdev)
{
	struct usb_hcd *hcd = platform_get_drvdata(pdev);

	pr_debug ("In ehci_hcd_ikf71xx_drv_remove\n");

	usb_hcd_ikf71xx_echi_remove(hcd, pdev);

	return 0;
}

MODULE_ALIAS("ikf71xx-ehci-hcd");

static struct platform_driver ehci_hcd_ikf71xx_driver = {
	.probe		= ehci_hcd_ikf71xx_drv_probe,
	.remove		= ehci_hcd_ikf71xx_drv_remove,
	.driver = {
		.name		= "fusiv-ehci-hcd",
		.bus		= &platform_bus_type,
	}
};
