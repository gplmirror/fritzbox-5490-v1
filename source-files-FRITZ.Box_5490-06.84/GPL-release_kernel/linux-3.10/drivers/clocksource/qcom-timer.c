/*
 *
 * Copyright (C) 2007 Google, Inc.
 * Copyright (c) 2009-2012,2014, The Linux Foundation. All rights reserved.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/clocksource.h>
#include <linux/clockchips.h>
#include <linux/cpu.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/delay.h>

#include <asm/sched_clock.h>

#define TIMER_MATCH_VAL			0x0000
#define TIMER_COUNT_VAL			0x0004
#define TIMER_ENABLE			0x0008
#define TIMER_ENABLE_CLR_ON_MATCH_EN	BIT(1)
#define TIMER_ENABLE_EN			BIT(0)
#define TIMER_CLEAR			0x000C
#define DGT_CLK_CTL			0x10
#define DGT_CLK_CTL_DIV_4		0x3

#if defined(CONFIG_AVM_ENHANCED)
#define DGT_CLK_CTL_DIV_1		0x0
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

#define TIMER_STS_GPT0_CLR_PEND		BIT(10)

#define GPT_HZ 32768

#define MSM_DGT_SHIFT 5

#define BASE_EVENT_OFFSET  0x04 /* APCS_GPT_MTCH (an interrupt will be generated, when this value is reached) */
#define BASE_STS_OFFSET    0x88
#define DGT_SOURCE_OFFSET 0x24


/*--------------------------------------------------------------------------------*\
 * avm-comment:  event_base-access is depending on cpu and map address on cpuX to cpuX_base
 * so it's possible to use event_base for percpu-timer-irq/set_next_event
\*--------------------------------------------------------------------------------*/
static inline void __iomem *event_base( struct clocksource *cs ){
       return cs->archdata.base + BASE_EVENT_OFFSET;
}

static inline void __iomem *dgt_base( struct clocksource *cs ){
       return cs->archdata.cpu0_base + DGT_SOURCE_OFFSET;
}

static inline void __iomem *sts_base( struct clocksource *cs ){
       return cs->archdata.base + BASE_STS_OFFSET;
}

#if defined(CONFIG_DEBUG_FS)
static struct debugfs_reg32 debugfs_kpss_timer_regs[] = {
	{ "apcs_tmrsecure", 0x0000 },

	{ "apcs_gpt0_mtch", 0x0004 },
	{ "apcs_gpt0_cnt", 0x0008 },
	{ "apcs_gpt0_en", 0x000c },
	{ "apcs_gpt0_clr", 0x0010 },

	{ "apcs_gpt1_mtch", 0x0014 },
	{ "apcs_gpt1_cnt", 0x0018 },
	{ "apcs_gpt1_en", 0x001c },
	{ "apcs_gpt1_clr", 0x0020 },

	{ "apcs_dgt_mtch", 0x0024 },
	{ "apcs_dgt_cnt", 0x0028 },
	{ "apcs_dgt_en", 0x002c },
	{ "apcs_dgt_clr", 0x0030 },
	{ "apcs_dgt_clk_ctl", 0x0034 },

	{ "apcs_wdt0_rst", 0x0038 },
	{ "apcs_wdt0_frz", 0x003c },
	{ "apcs_wdt0_en", 0x0040 },
	{ "apcs_wdt0_sts", 0x0044 },
	{ "apcs_wdt0_int_width", 0x0048 },
	{ "apcs_wdt0_bark_time", 0x004c },
	{ "apcs_wdt0_tst_ld_sts", 0x0050 },
	{ "apcs_wdt0_tst_ld", 0x0054 },
	{ "apcs_wdt0_tst", 0x0058 },
	{ "apcs_wdt0_bite_time", 0x005c },

	{ "apcs_wdt1_rst", 0x0060 },
	{ "apcs_wdt1_frz", 0x0064 },
	{ "apcs_wdt1_en", 0x0068 },
	{ "apcs_wdt1_sts", 0x006c },
	{ "apcs_wdt1_int_width", 0x0070 },
	{ "apcs_wdt1_bark_time", 0x0074 },
	{ "apcs_wdt1_tst_ld_sts", 0x0078 },
	{ "apcs_wdt1_tst_ld", 0x007c },
	{ "apcs_wdt1_tst", 0x0080 },
	{ "apcs_wdt1_bite_time", 0x0084 },
	
	{ "apcs_tmr_sts", 0x0088 }
};
#endif /* defined(CONFIG_DEBUG_FS) */

static notrace cycle_t msm_read_timer_count(struct clocksource *cs);

static struct clocksource msm_clocksource = {
	.name	= "dg_timer",
	.rating	= 300,
	.read	= msm_read_timer_count,
	.mask	= CLOCKSOURCE_MASK(32),
	.flags	= CLOCK_SOURCE_IS_CONTINUOUS | CLOCK_SOURCE_VALID_FOR_HRES,
};


static irqreturn_t msm_timer_interrupt(int irq, void *dev_id)
{
	struct clock_event_device *evt = dev_id;
	/* Stop the timer tick */
	if (evt->mode == CLOCK_EVT_MODE_ONESHOT) {
		u32 ctrl = readl_relaxed(event_base(&msm_clocksource) + TIMER_ENABLE);
		ctrl &= ~TIMER_ENABLE_EN;
		writel_relaxed(ctrl, event_base(&msm_clocksource) + TIMER_ENABLE);
	}
	evt->event_handler(evt);
	return IRQ_HANDLED;
}

static int msm_timer_set_next_event(unsigned long cycles,
				    struct clock_event_device *evt)
{
	u32 ctrl = readl_relaxed(event_base(&msm_clocksource) + TIMER_ENABLE);

	ctrl &= ~TIMER_ENABLE_EN;
	writel_relaxed(ctrl, event_base(&msm_clocksource) + TIMER_ENABLE);

	writel_relaxed(ctrl, event_base(&msm_clocksource) + TIMER_CLEAR);
	writel_relaxed(cycles, event_base(&msm_clocksource) + TIMER_MATCH_VAL);

	if (sts_base(&msm_clocksource))
		while (readl_relaxed(sts_base(&msm_clocksource)) & TIMER_STS_GPT0_CLR_PEND)
			cpu_relax();

	writel_relaxed(ctrl | TIMER_ENABLE_EN, event_base(&msm_clocksource) + TIMER_ENABLE);
	return 0;
}

static void msm_timer_set_mode(enum clock_event_mode mode,
			      struct clock_event_device *evt)
{
	u32 ctrl;

	ctrl = readl_relaxed(event_base(&msm_clocksource) + TIMER_ENABLE);
	ctrl &= ~(TIMER_ENABLE_EN | TIMER_ENABLE_CLR_ON_MATCH_EN);

	switch (mode) {
	case CLOCK_EVT_MODE_RESUME:
	case CLOCK_EVT_MODE_PERIODIC:
		break;
	case CLOCK_EVT_MODE_ONESHOT:
		/* Timer is enabled in set_next_event */
		break;
	case CLOCK_EVT_MODE_UNUSED:
	case CLOCK_EVT_MODE_SHUTDOWN:
		break;
	}
	writel_relaxed(ctrl, event_base(&msm_clocksource) + TIMER_ENABLE);
}

static struct clock_event_device __percpu *msm_evt;


static notrace cycle_t msm_read_timer_count(struct clocksource *cs)
{
	return readl_relaxed( dgt_base(cs) + TIMER_COUNT_VAL);
}


#if defined(CONFIG_AVM_ENHANCED)
static struct delay_timer msm_delay_timer;
/*--------------------------------------------------------------------------------*\
 * mbahr: aktuell (4080): 25 MHz (40 ns - 25 counts/us)
\*--------------------------------------------------------------------------------*/
unsigned long msm_get_timer_count(void){
	struct clocksource *cs = &msm_clocksource;
	if(cs->archdata.cpu0_base) {
		return msm_read_timer_count(cs);
	}
	return 0;
}
EXPORT_SYMBOL(msm_get_timer_count);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
unsigned long msm_get_timer_freq(void){
	return msm_delay_timer.freq;
}
EXPORT_SYMBOL(msm_get_timer_freq);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

static int msm_timer_irq;
static int msm_timer_has_ppi;

static int msm_local_timer_setup(struct clock_event_device *evt)
{
	int cpu = smp_processor_id();
	int err;

	evt->irq = msm_timer_irq;
	evt->name = "msm_timer";
	evt->features = CLOCK_EVT_FEAT_ONESHOT;
	evt->rating = 200;
	evt->set_mode = msm_timer_set_mode;
	evt->set_next_event = msm_timer_set_next_event;
	evt->cpumask = cpumask_of(cpu);

	clockevents_config_and_register(evt, GPT_HZ, 4, 0xffffffff);

	if (msm_timer_has_ppi) {
		enable_percpu_irq(evt->irq, IRQ_TYPE_EDGE_RISING);
	} else {
		err = request_irq(evt->irq, msm_timer_interrupt,
				IRQF_TIMER | IRQF_NOBALANCING |
				IRQF_TRIGGER_RISING, "gp_timer", evt);
		if (err)
			pr_err("request_irq failed\n");
	}

	return 0;
}

static void msm_local_timer_stop(struct clock_event_device *evt)
{
	evt->set_mode(CLOCK_EVT_MODE_UNUSED, evt);
	disable_percpu_irq(evt->irq);
}

static int msm_timer_cpu_notify(struct notifier_block *self,
					   unsigned long action, void *hcpu)
{
	/*
	 * Grab cpu pointer in each case to avoid spurious
	 * preemptible warnings
	 */
	switch (action & ~CPU_TASKS_FROZEN) {
	case CPU_STARTING:
		msm_local_timer_setup(this_cpu_ptr(msm_evt));
		break;
	case CPU_DYING:
		msm_local_timer_stop(this_cpu_ptr(msm_evt));
		break;
	}

	return NOTIFY_OK;
}

static struct notifier_block msm_timer_cpu_nb = {
	.notifier_call = msm_timer_cpu_notify,
};

static u32 notrace msm_sched_clock_read(void)
{
	return msm_clocksource.read(&msm_clocksource);
}

static void __init msm_timer_init(u32 dgt_hz, int sched_bits, int irq,
				  bool percpu)
{
	struct clocksource *cs = &msm_clocksource;
	int res = 0;

	msm_timer_irq = irq;
	msm_timer_has_ppi = percpu;

	printk(KERN_ERR"msm_timer_init: %d, %d, %d, %d\n", dgt_hz, sched_bits, irq,
	       percpu);

	msm_evt = alloc_percpu(struct clock_event_device);
	if (!msm_evt) {
		pr_err("memory allocation failed for clockevents\n");
		goto err;
	}

	if (percpu)
		res = request_percpu_irq(irq, msm_timer_interrupt,
					 "gp_timer", msm_evt);

	if (res) {
		pr_err("request_percpu_irq failed\n");
	} else {
		res = register_cpu_notifier(&msm_timer_cpu_nb);
		if (res) {
			free_percpu_irq(irq, msm_evt);
			goto err;
		}

		/* Immediately configure the timer on the boot CPU */
		msm_local_timer_setup(__this_cpu_ptr(msm_evt));
	}

err:
	writel_relaxed(TIMER_ENABLE_EN, dgt_base(cs) + TIMER_ENABLE);
	res = clocksource_register_hz(cs, dgt_hz);
	if (res)
		pr_err("clocksource_register failed\n");
	setup_sched_clock(msm_sched_clock_read, sched_bits, dgt_hz);
#if defined(CONFIG_AVM_ENHANCED)
	/*--------------------------------------------------------------------------------*\
	 * an accurate timer necessary for Bogo-Mips calibration, that calculates loops_per_jiffies), 
	 * (initial cpufreq not on maximum value)
	 * (mdelay/udelay() based on loops_per_jiffies)
	\*--------------------------------------------------------------------------------*/
	msm_delay_timer.freq = dgt_hz;
	msm_delay_timer.read_current_timer = msm_get_timer_count;
	register_current_timer_delay(&msm_delay_timer);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
}


#if defined(CONFIG_DEBUG_FS)

static uint32_t debugfs_write_fn( void *context, uint32_t addr, uint32_t val){
	writel_relaxed( val, context + addr);
	return 0;
}

static uint32_t debugfs_read_fn( void *context, uint32_t addr ){
	return readl_relaxed( context + addr);
}

static void init_debugfs_cs_regset( const char *name, void __iomem *base) {
	struct debugfs_regset32 *regset;

	regset = kzalloc(sizeof( struct debugfs_regset32), GFP_KERNEL);
	if (!regset)
		return;

	regset->regs = debugfs_kpss_timer_regs ;
	regset->nregs = ARRAY_SIZE( debugfs_kpss_timer_regs );
	regset->base = NULL;
	regset->reg_read_fn = debugfs_read_fn;
	regset->reg_write_fn = debugfs_write_fn;
	regset->rw_context = base;
	debugfs_create_regset32( name , 0444, NULL, regset );
}


static int init_debugfs( void ) {
	struct clocksource *cs  = &msm_clocksource;
	init_debugfs_cs_regset( "qcom_timer_cpu0", cs->archdata.cpu0_base ); 
	init_debugfs_cs_regset( "qcom_timer_cpu1", cs->archdata.cpu1_base ); 
	init_debugfs_cs_regset( "qcom_timer", cs->archdata.base ); 
	return 0;
}

late_initcall ( init_debugfs );


#endif

#ifdef CONFIG_ARCH_QCOM
static void __init msm_dt_timer_init(struct device_node *np)
{
	u32 freq;
	int irq;
	struct resource res;
	u32 percpu_offset;
	struct clocksource *cs  = &msm_clocksource;

	// mapping timerreg: 0x200a000
	cs->archdata.base = of_iomap(np, 0);  
	if (!cs->archdata.base) {
		pr_err("Failed to map event base\n");
		return;
	}

	pr_err("event base %#x mapped successfully\n", (unsigned
							int)cs->archdata.base);

	/* We use GPT0 for the clockevent */
	irq = irq_of_parse_and_map(np, 1);
	if (irq <= 0) {
		pr_err("Can't get irq\n");
		return;
	}

	/* We use CPU0's DGT for the clocksource */
	if (of_property_read_u32(np, "cpu-offset", &percpu_offset))
		percpu_offset = 0;

	if (of_address_to_resource(np, 0, &res)) {
		pr_err("Failed to parse DGT resource\n");
		return;
	}
	
	//mapping timerreg: 0x200a000 + 0x80000
	cs->archdata.cpu0_base = ioremap(res.start + percpu_offset, resource_size(&res));
	if (!cs->archdata.cpu0_base) {
		pr_err("Failed to map source base\n");
		return;
	}
	
	//mapping timerreg: 0x200a000 + 0x90000
	cs->archdata.cpu1_base = ioremap(res.start + percpu_offset + 0x10000, resource_size(&res));
	if (!cs->archdata.cpu1_base) {
		pr_err("Failed to map source base\n");
		return;
	}

	if (of_property_read_u32(np, "clock-frequency", &freq)) {
		pr_err("Unknown frequency\n");
		return;
	}
#if defined(CONFIG_AVM_ENHANCED)
	/*--- avm: accurate timer is a better timer: 25 MHz (40 ns) ---*/
	writel_relaxed(DGT_CLK_CTL_DIV_1, dgt_base(cs) + DGT_CLK_CTL);
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
	freq /= 4;
	writel_relaxed(DGT_CLK_CTL_DIV_4, dgt_base(cs) + DGT_CLK_CTL);
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

	msm_timer_init(freq, 32, irq, !!percpu_offset);
}
CLOCKSOURCE_OF_DECLARE(kpss_timer, "qcom,kpss-timer", msm_dt_timer_init);
CLOCKSOURCE_OF_DECLARE(scss_timer, "qcom,scss-timer", msm_dt_timer_init);
#else
#error AVM: we support only device tree mode
#endif
