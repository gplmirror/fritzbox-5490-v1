#ifndef _linux_avm_proc_h_
#define _linux_avm_proc_h_

/* Reference for /proc/avm/; use it e.g. via
 *
 *	struct proc_dir_entry *proc_avm __attribute__((weak));
 *
 *	void func(void)
 *	{
 *		if (!proc_avm)
 *			proc_avm = proc_mkdir("avm", NULL);
 *	}
 */
extern struct proc_dir_entry *proc_avm;

#endif
