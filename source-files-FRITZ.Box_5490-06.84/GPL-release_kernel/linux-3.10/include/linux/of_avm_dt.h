#ifndef _OF_AVM_DT_H_
#define _OF_AVM_DT_H_

int avm_generate_hw_config_table_from_device_tree(void);
/*
 * Reads Interrupt Information from the Device Tree
 * Finds the Interrupt specified by irq_name and stores the information in cpu_used, cpu_mask and int_num
 * @param int_name: the name of the interrupt to find
 * @param cpu_used: pointer to store the cpu_used information
 * @param cpu_mask: pointer to store the cpu_mask information
 * @param int_num: pointer to store the int_num information
 */
int get_interrupt_info_from_device_tree(const char *int_name,
					int32_t * cpu_used, int32_t * cpu_mask,
					int32_t * int_num);


#endif
