#ifndef __ASM_MACH_GENERIC_KMALLOC_H
#define __ASM_MACH_GENERIC_KMALLOC_H


#ifndef CONFIG_DMA_COHERENT
#if defined(CONFIG_AVM_ENHANCED)
#define ARCH_DMA_MINALIGN	 64
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
/*
 * Total overkill for most systems but need as a safe default.
 * Set this one if any device in the system might do non-coherent DMA.
 */
#define ARCH_DMA_MINALIGN	128
#endif/*--- #else ---*//*--- #if defined() ---*/
#endif

#endif /* __ASM_MACH_GENERIC_KMALLOC_H */
