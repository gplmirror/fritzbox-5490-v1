/*
 *  Atheros AR71XX/AR724X/AR913X SoC register definitions
 *
 *  Copyright (C) 2010-2011 Jaiganesh Narayanan <jnarayanan@atheros.com>
 *  Copyright (C) 2008-2010 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  Parts of this file are based on Atheros' 2.6.15/2.6.31 BSP
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#ifndef __ASM_MACH_AR71XX_REGS_H
#define __ASM_MACH_AR71XX_REGS_H

// [GJu] Neue SOCs (QCA953X/QCA956X/..) kriegen eigene Headerdateien
#if (defined CONFIG_SOC_AR71XX || defined CONFIG_SOC_AR724X || defined CONFIG_SOC_AR913X || defined CONFIG_SOC_AR933X || defined CONFIG_SOC_AR934X || defined CONFIG_SOC_QCA955X)

#include <linux/types.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/bitops.h>

#define AR71XX_APB_BASE		0x18000000u
#define AR71XX_GE0_BASE		0x19000000u
#define AR71XX_GE1_BASE		0x1a000000u
#define AR71XX_EHCI_BASE	0x1b000000u
#define AR71XX_EHCI_SIZE	0x1000
#define AR71XX_OHCI_BASE	0x1c000000u
#define AR71XX_OHCI_SIZE	0x1000
#define AR71XX_SPI_BASE		0x1f000000u
#define AR71XX_SPI_SIZE		0x01000000u

#define AR71XX_DDR_CTRL_BASE	(AR71XX_APB_BASE + 0x00000000)
#define AR71XX_DDR_CTRL_SIZE	0x100
#define AR71XX_UART_BASE	    (AR71XX_APB_BASE + 0x00020000)
#define AR71XX_UART_SIZE	    0x100
#define AR71XX_USB_CTRL_BASE	(AR71XX_APB_BASE + 0x00030000)
#define AR71XX_USB_CTRL_SIZE	0x100
#define AR71XX_GPIO_BASE	    (AR71XX_APB_BASE + 0x00040000)
#define AR71XX_GPIO_SIZE	    0x100
#define AR71XX_PLL_BASE		    (AR71XX_APB_BASE + 0x00050000)
#define AR71XX_PLL_SIZE		    0x100
#define AR71XX_RESET_BASE	    (AR71XX_APB_BASE + 0x00060000)
#define AR71XX_RESET_SIZE	    0x100
#define AR71XX_DMA_BASE         (AR71XX_APB_BASE + 0x000A0000)
#define AR71XX_SLIC_BASE        (AR71XX_APB_BASE + 0x000A9000)
#define AR71XX_STEREO_BASE      (AR71XX_APB_BASE + 0x000B0000)
#define AR71XX_OTP_BASE         (AR71XX_APB_BASE + 0x00130000)

#define AR71XX_PCI_MEM_BASE	0x10000000u
#define AR71XX_PCI_MEM_SIZE	0x07000000u

#define AR71XX_PCI_WIN0_OFFS	0x10000000
#define AR71XX_PCI_WIN1_OFFS	0x11000000
#define AR71XX_PCI_WIN2_OFFS	0x12000000
#define AR71XX_PCI_WIN3_OFFS	0x13000000
#define AR71XX_PCI_WIN4_OFFS	0x14000000
#define AR71XX_PCI_WIN5_OFFS	0x15000000
#define AR71XX_PCI_WIN6_OFFS	0x16000000
#define AR71XX_PCI_WIN7_OFFS	0x07000000

#define AR71XX_PCI_CFG_BASE	\
	(AR71XX_PCI_MEM_BASE + AR71XX_PCI_WIN7_OFFS + 0x10000)
#define AR71XX_PCI_CFG_SIZE	0x100

#define AR7240_USB_CTRL_BASE	(AR71XX_APB_BASE + 0x00030000)
#define AR7240_USB_CTRL_SIZE	0x100
#define AR7240_OHCI_BASE	0x1b000000u
#define AR7240_OHCI_SIZE	0x1000

#define AR724X_PCI_MEM_BASE	0x10000000u
#define AR724X_PCI_MEM_SIZE	0x04000000u

#define AR724X_PCI_CFG_BASE	0x14000000u
#define AR724X_PCI_CFG_SIZE	0x1000
#define AR724X_PCI_CRP_BASE	(AR71XX_APB_BASE + 0x000c0000)
#define AR724X_PCI_CRP_SIZE	0x1000
#define AR724X_PCI_CTRL_BASE	(AR71XX_APB_BASE + 0x000f0000)
#define AR724X_PCI_CTRL_SIZE	0x100

#define AR724X_EHCI_BASE	0x1b000000u
#define AR724X_EHCI_SIZE	0x1000

#define AR913X_EHCI_BASE	0x1b000000u
#define AR913X_EHCI_SIZE	0x1000
#define AR913X_WMAC_BASE	(AR71XX_APB_BASE + 0x000C0000)
#define AR913X_WMAC_SIZE	0x30000

#define AR933X_UART_BASE	(AR71XX_APB_BASE + 0x00020000)
#define AR933X_UART_SIZE	0x14
#define AR933X_WMAC_BASE	(AR71XX_APB_BASE + 0x00100000)
#define AR933X_WMAC_SIZE	0x20000
#define AR933X_EHCI_BASE	0x1b000000u
#define AR933X_EHCI_SIZE	0x1000

#define AR934X_WMAC_BASE	(AR71XX_APB_BASE + 0x00100000)
#define AR934X_WMAC_SIZE	0x20000
#define AR934X_EHCI_BASE	0x1b000000u
#define AR934X_EHCI_SIZE	0x200
#define AR934X_SRIF_BASE	(AR71XX_APB_BASE + 0x00116000)
#define AR934X_SRIF_SIZE	0x1000

#define QCA955X_I2C_BASE        (AR71XX_APB_BASE+0x00018000)

#define QCA955X_PCI_MEM_BASE0	0x10000000u
#define QCA955X_PCI_MEM_BASE1	0x12000000u
#define QCA955X_PCI_MEM_SIZE	0x02000000
#define QCA955X_PCI_CFG_BASE0	0x14000000u
#define QCA955X_PCI_CFG_BASE1	0x16000000u
#define QCA955X_PCI_CFG_SIZE	0x1000
#define QCA955X_PCI_CRP_BASE0	(AR71XX_APB_BASE + 0x000c0000)
#define QCA955X_PCI_CRP_BASE1	(AR71XX_APB_BASE + 0x00250000)
#define QCA955X_PCI_CRP_SIZE	0x1000
#define QCA955X_PCI_CTRL_BASE0	(AR71XX_APB_BASE + 0x000f0000)
#define QCA955X_PCI_CTRL_BASE1	(AR71XX_APB_BASE + 0x00280000)
#define QCA955X_PCI_CTRL_SIZE	0x100

#define QCA955X_WMAC_BASE	(AR71XX_APB_BASE + 0x00100000)
#define QCA955X_WMAC_SIZE	0x20000
#define QCA955X_EHCI0_BASE	0x1b000000u
#define QCA955X_EHCI1_BASE	0x1b400000u
#define QCA955X_EHCI_SIZE	0x1000
#define QCA955X_SRAM_BASE	0x1d000000u

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define ATH_APB_BASE			AR71XX_APB_BASE
#define ATH_GE0_BASE			AR71XX_GE0_BASE
#define ATH_GE1_BASE			AR71XX_GE1_BASE
#define ATH_USB_OHCI_BASE		AR71XX_OHCI_BASE
#define ATH_USB_EHCI_BASE		AR71XX_EHCI_BASE

#define ATH_SPI_BASE			AR71XX_SPI_BASE
#define ATH_SPI_SIZE            AR71XX_SPI_SIZE

/*------------------------------------------------------------------------------------------*\
 * APB block
\*------------------------------------------------------------------------------------------*/
#define ATH_DDR_CTL_BASE		AR71XX_DDR_CTRL_BASE
#define ATH_DDR_CTL_SIZE		AR71XX_DDR_CTRL_SIZE

#define ATH_UART_BASE			AR71XX_UART_BASE

#define ATH_USB_CONFIG_BASE		AR71XX_USB_CTRL_BASE

#define ATH_GPIO_BASE			AR71XX_GPIO_BASE

#define ATH_PLL_BASE			AR71XX_PLL_BASE
#define ATH_PLL_SIZE			AR71XX_PLL_SIZE

#define ATH_RESET_BASE			AR71XX_RESET_BASE
#define ATH_RESET_SIZE          AR71XX_RESET_SIZE

#define ATH_DMA_BASE			AR71XX_DMA_BASE

#define ATH_SLIC_BASE			AR71XX_SLIC_BASE

#define ATH_STEREO_BASE			AR71XX_STEREO_BASE

#define ATH_OTP_BASE			AR71XX_OTP_BASE


/*------------------------------------------------------------------------------------------*\
 * DDR_CTRL block
\*------------------------------------------------------------------------------------------*/
#define ATH_DDR_CONFIG			    (AR71XX_DDR_CTRL_BASE + 0)
#define ATH_DDR_CONFIG2			    (AR71XX_DDR_CTRL_BASE + 4)
#define ATH_DDR_MODE			    (AR71XX_DDR_CTRL_BASE + 0x08)
#define ATH_DDR_EXT_MODE		    (AR71XX_DDR_CTRL_BASE + 0x0c)
#define ATH_DDR_CONTROL			    (AR71XX_DDR_CTRL_BASE + 0x10)
#define ATH_DDR_REFRESH			    (AR71XX_DDR_CTRL_BASE + 0x14)
#define ATH_DDR_RD_DATA_THIS_CYCLE	(AR71XX_DDR_CTRL_BASE + 0x18)
#define ATH_DDR_TAP_CONTROL0		(AR71XX_DDR_CTRL_BASE + 0x1c)
#define ATH_DDR_TAP_CONTROL1		(AR71XX_DDR_CTRL_BASE + 0x20)
#define ATH_DDR_TAP_CONTROL2		(AR71XX_DDR_CTRL_BASE + 0x24)
#define ATH_DDR_TAP_CONTROL3		(AR71XX_DDR_CTRL_BASE + 0x28)

/*------------------------------------------------------------------------------------------*\
 * DDR Config values
\*------------------------------------------------------------------------------------------*/
#define ATH_DDR_CONFIG_16BIT		    (1 << 31)
#define ATH_DDR_CONFIG_PAGE_OPEN	    (1 << 30)
#define ATH_DDR_CONFIG_CAS_LAT_SHIFT	27
#define ATH_DDR_CONFIG_TMRD_SHIFT	    23
#define ATH_DDR_CONFIG_TRFC_SHIFT	    17
#define ATH_DDR_CONFIG_TRRD_SHIFT	    13
#define ATH_DDR_CONFIG_TRP_SHIFT	     9
#define ATH_DDR_CONFIG_TRCD_SHIFT	     5
#define ATH_DDR_CONFIG_TRAS_SHIFT	     0

#define ATH_DDR_CONFIG2_BL2		        (2 << 0)
#define ATH_DDR_CONFIG2_BL4		        (4 << 0)
#define ATH_DDR_CONFIG2_BL8		        (8 << 0)

#define ATH_DDR_CONFIG2_BT_IL		    (1 << 4)
#define ATH_DDR_CONFIG2_CNTL_OE_EN	    (1 << 5)
#define ATH_DDR_CONFIG2_PHASE_SEL	    (1 << 6)
#define ATH_DDR_CONFIG2_DRAM_CKE	    (1 << 7)
#define ATH_DDR_CONFIG2_TWR_SHIFT	          8
#define ATH_DDR_CONFIG2_TRTW_SHIFT	         12
#define ATH_DDR_CONFIG2_TRTP_SHIFT	         17
#define ATH_DDR_CONFIG2_TWTR_SHIFT	         21
#define ATH_DDR_CONFIG2_HALF_WIDTH_L	(1 << 31)

#define ATH_DDR_TAP_DEFAULT		0x18

/*------------------------------------------------------------------------------------------*\
 * DDR block, gmac flushing
\*------------------------------------------------------------------------------------------*/
#define AR71XX_DDR_REG_PCI_WIN0		0x7c
#define AR71XX_DDR_REG_PCI_WIN1		0x80
#define AR71XX_DDR_REG_PCI_WIN2		0x84
#define AR71XX_DDR_REG_PCI_WIN3		0x88
#define AR71XX_DDR_REG_PCI_WIN4		0x8c
#define AR71XX_DDR_REG_PCI_WIN5		0x90
#define AR71XX_DDR_REG_PCI_WIN6		0x94
#define AR71XX_DDR_REG_PCI_WIN7		0x98
#define AR71XX_DDR_REG_FLUSH_GE0	0x9c
#define AR71XX_DDR_REG_FLUSH_GE1	0xa0
#define AR71XX_DDR_REG_FLUSH_USB	0xa4
#define AR71XX_DDR_REG_FLUSH_PCI	0xa8

#define AR724X_DDR_REG_FLUSH_GE0	0x7c
#define AR724X_DDR_REG_FLUSH_GE1	0x80
#define AR724X_DDR_REG_FLUSH_USB	0x84
#define AR724X_DDR_REG_FLUSH_PCIE	0x88

#define AR913X_DDR_REG_FLUSH_GE0	0x7c
#define AR913X_DDR_REG_FLUSH_GE1	0x80
#define AR913X_DDR_REG_FLUSH_USB	0x84
#define AR913X_DDR_REG_FLUSH_WMAC	0x88

#define AR933X_DDR_REG_FLUSH_GE0	0x7c
#define AR933X_DDR_REG_FLUSH_GE1	0x80
#define AR933X_DDR_REG_FLUSH_USB	0x84
#define AR933X_DDR_REG_FLUSH_WMAC	0x88

#define AR934X_DDR_REG_FLUSH_GE0	0x9c
#define AR934X_DDR_REG_FLUSH_GE1	0xa0
#define AR934X_DDR_REG_FLUSH_USB	0xa4
#define AR934X_DDR_REG_FLUSH_PCIE	0xa8
#define AR934X_DDR_REG_FLUSH_WMAC	0xac

#define QCA955X_DDR_REG_FLUSH_GE0	0x9c
#define QCA955X_DDR_REG_FLUSH_GE1	0xa0
#define QCA955X_DDR_REG_FLUSH_USB	0xa4
#define QCA955X_DDR_REG_FLUSH_PCIE	0xa8

#define ATH_DDR_WB_FLUSH_SRC1		(AR71XX_DDR_CTRL_BASE + 0xb0)
#define ATH_DDR_WB_FLUSH_SRC2		(AR71XX_DDR_CTRL_BASE + 0xb4)	/*--- checksum engine ---*/

/*
 * DDR block, counters
 */
/*
    DDRMON_CTL:
	12:11 => Bank Sel
	10:8  => Client Sel
	1     => Clear Max Latency Counters
	2     => Latency disable refresh
	0 		=> Clear Counters

    By Default Client is CPU;
    Client_sel = 0 => CPU
    Client_sel = 3 => USB/MBOX(SLIC/I2S/SPDIF)
*/
#define ATH_DDR_MON_CTL			    (AR71XX_DDR_CTRL_BASE+0xe8)
#define ATH_DDR_MON_ALL_GNT		    (AR71XX_DDR_CTRL_BASE+0xec)       /*--- Total Number of Grants - All clients, all banks ---*/
#define ATH_DDR_MON_ALL_REQ_DUR_L	(AR71XX_DDR_CTRL_BASE+0xf0)       /*--- Total Request Assertion Duration(Low/High) - How long the request has been asserted. ---*/
#define ATH_DDR_MON_ALL_REQ_DUR_H	(AR71XX_DDR_CTRL_BASE+0xf4)
#define ATH_DDR_MON_GNT			    (AR71XX_DDR_CTRL_BASE+0xf8)       /*--- All Bank Total Number of Grants for the Selected Client ---*/
#define ATH_DDR_MON_DUR_GNT_L		(AR71XX_DDR_CTRL_BASE+0xfc)       /*--- All Bank Total Number of Grants for the Selected Client Request assertion duration for a seleceted client. ---*/
#define ATH_DDR_MON_DUR_GNT_H		(AR71XX_DDR_CTRL_BASE+0x100)
#define ATH_DDR_MON_MAX_LATENCY		(AR71XX_DDR_CTRL_BASE+0x104)      /*--- Maximum Grant Latency for client C ---*/


/*
 * PLL block
 */
#define AR71XX_PLL_REG_CPU_CONFIG	0x00
#define AR71XX_PLL_REG_SEC_CONFIG	0x04
#define AR71XX_PLL_REG_ETH0_INT_CLOCK	0x10
#define AR71XX_PLL_REG_ETH1_INT_CLOCK	0x14

#define AR71XX_PLL_DIV_SHIFT		3
#define AR71XX_PLL_DIV_MASK		0x1f
#define AR71XX_CPU_DIV_SHIFT		16
#define AR71XX_CPU_DIV_MASK		0x3
#define AR71XX_DDR_DIV_SHIFT		18
#define AR71XX_DDR_DIV_MASK		0x3
#define AR71XX_AHB_DIV_SHIFT		20
#define AR71XX_AHB_DIV_MASK		0x7

#define AR724X_PLL_REG_CPU_CONFIG	0x00
#define AR724X_PLL_REG_PCIE_CONFIG	0x18

#define AR724X_PLL_CPU_CONFIG_REG		           0x00
#define AR724X_PLL_ETH_USB_CONFIG_REG	           0x04
#define AR724X_PLL_CLK_CTRL_REG		               0x08
#define AR724X_PLL_PCIE_PLL_CONFIG                 0x10
#define AR724X_PLL_PCIE_PLL_DITHER_DIV_MAX         0x14
#define AR724X_PLL_PCIE_PLL_DITHER_DIV_MIN         0x18
#define AR724X_PLL_PCIE_PLL_DITHER_STEP            0x1c
#define AR724X_PLL_LDO_POWER_CONTROL               0x20
#define AR724X_PLL_SWITCH_CLOCK_SPARE              0x24
#define AR724X_PLL_CURRENT_PCIE_PLL_DITHER         0x28
#define AR724X_PLL_ETH_XMII                        0x2c
#define AR724X_PLL_AUDIO_PLL_CONFIG                0x30
#define AR724X_PLL_AUDIO_PLL_MODULATION            0x34
#define AR724X_PLL_AUDIO_PLL_MOD_STEP              0x38
#define AR724X_PLL_CURRENT_AUDIO_PLL_MODULATION    0x3c


#define AR724X_PLL_DIV_SHIFT		0
#define AR724X_PLL_DIV_MASK		0x3ff
#define AR724X_PLL_REF_DIV_SHIFT	10
#define AR724X_PLL_REF_DIV_MASK		0xf
#define AR724X_AHB_DIV_SHIFT		19
#define AR724X_AHB_DIV_MASK		0x1
#define AR724X_DDR_DIV_SHIFT		22
#define AR724X_DDR_DIV_MASK		0x1

#define AR913X_PLL_REG_CPU_CONFIG	0x00
#define AR913X_PLL_REG_ETH_CONFIG	0x04
#define AR913X_PLL_REG_ETH0_INT_CLOCK	0x14
#define AR913X_PLL_REG_ETH1_INT_CLOCK	0x18

#define AR913X_PLL_DIV_SHIFT		0
#define AR913X_PLL_DIV_MASK		0x3ff
#define AR913X_DDR_DIV_SHIFT		22
#define AR913X_DDR_DIV_MASK		0x3
#define AR913X_AHB_DIV_SHIFT		19
#define AR913X_AHB_DIV_MASK		0x1

#define AR933X_PLL_CPU_CONFIG_REG	0x00
#define AR933X_PLL_CLOCK_CTRL_REG	0x08

#define AR933X_PLL_CPU_CONFIG_NINT_SHIFT	10
#define AR933X_PLL_CPU_CONFIG_NINT_MASK		0x3f
#define AR933X_PLL_CPU_CONFIG_REFDIV_SHIFT	16
#define AR933X_PLL_CPU_CONFIG_REFDIV_MASK	0x1f
#define AR933X_PLL_CPU_CONFIG_OUTDIV_SHIFT	23
#define AR933X_PLL_CPU_CONFIG_OUTDIV_MASK	0x7

#define AR933X_PLL_CLOCK_CTRL_BYPASS		BIT(2)
#define AR933X_PLL_CLOCK_CTRL_CPU_DIV_SHIFT	5
#define AR933X_PLL_CLOCK_CTRL_CPU_DIV_MASK	0x3
#define AR933X_PLL_CLOCK_CTRL_DDR_DIV_SHIFT	10
#define AR933X_PLL_CLOCK_CTRL_DDR_DIV_MASK	0x3
#define AR933X_PLL_CLOCK_CTRL_AHB_DIV_SHIFT	15
#define AR933X_PLL_CLOCK_CTRL_AHB_DIV_MASK	0x7

#define AR934X_PLL_CPU_CONFIG_REG		    0x00
#define AR934X_PLL_DDR_CONFIG_REG		    0x04
#define AR934X_PLL_CPU_DDR_CLK_CTRL_REG		0x08
#define AR934X_PLL_PCI_PLL_CONFIG_REG	    0x10
#define AR934X_PLL_PCI_DITHER_DIV_MAX_REG   0x14
#define AR934X_PLL_PCI_DITHER_DIV_MIN_REG   0x18
#define AR934X_PLL_DITHER_STEP_REG          0x1c
#define AR934X_PLL_SWITCH_CLOCK_CTRL_REG    0x24
#define AR934X_PLL_CURRENT_PCIE_DITHER_REG  0x28
#define AR934X_PLL_ETH_XMII_CTRL_REG        0x2c

#define ATH_ETH_XMII_CONFIG             (AR71XX_PLL_BASE + AR934X_PLL_ETH_XMII_CTRL_REG)
#define ATH_PLL_ETH_XMII  ATH_ETH_XMII_CONFIG  // [GJu]

#define AR934X_PLL_CPU_CONFIG_NFRAC_SHIFT	0
#define AR934X_PLL_CPU_CONFIG_NFRAC_MASK	0x3f
#define AR934X_PLL_CPU_CONFIG_NINT_SHIFT	6
#define AR934X_PLL_CPU_CONFIG_NINT_MASK		0x3f
#define AR934X_PLL_CPU_CONFIG_REFDIV_SHIFT	12
#define AR934X_PLL_CPU_CONFIG_REFDIV_MASK	0x1f
#define AR934X_PLL_CPU_CONFIG_OUTDIV_SHIFT	19
#define AR934X_PLL_CPU_CONFIG_OUTDIV_MASK	0x3

#define AR934X_PLL_DDR_CONFIG_NFRAC_SHIFT	0
#define AR934X_PLL_DDR_CONFIG_NFRAC_MASK	0x3ff
#define AR934X_PLL_DDR_CONFIG_NINT_SHIFT	10
#define AR934X_PLL_DDR_CONFIG_NINT_MASK		0x3f
#define AR934X_PLL_DDR_CONFIG_REFDIV_SHIFT	16
#define AR934X_PLL_DDR_CONFIG_REFDIV_MASK	0x1f
#define AR934X_PLL_DDR_CONFIG_OUTDIV_SHIFT	23
#define AR934X_PLL_DDR_CONFIG_OUTDIV_MASK	0x7

#define AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_PLL_BYPASS	BIT(2)
#define AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_PLL_BYPASS	BIT(3)
#define AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_PLL_BYPASS	BIT(4)
#define AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_POST_DIV_SHIFT	5
#define AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_POST_DIV_MASK	0x1f
#define AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_POST_DIV_SHIFT	10
#define AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_POST_DIV_MASK	0x1f
#define AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_POST_DIV_SHIFT	15
#define AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_POST_DIV_MASK	0x1f
#define AR934X_PLL_CPU_DDR_CLK_CTRL_CPUCLK_FROM_CPUPLL	BIT(20)
#define AR934X_PLL_CPU_DDR_CLK_CTRL_DDRCLK_FROM_DDRPLL	BIT(21)
#define AR934X_PLL_CPU_DDR_CLK_CTRL_AHBCLK_FROM_DDRPLL	BIT(24)

#define QCA955X_PLL_CPU_CONFIG_REG		            0x00
#define QCA955X_PLL_DDR_CONFIG_REG		            0x04
#define QCA955X_PLL_CLK_CTRL_REG		            0x08
#define QCA955X_PLL_PCIE_PLL_CONFIG                 0x0c
#define QCA955X_PLL_PCIE_PLL_DITHER_DIV_MAX         0x10
#define QCA955X_PLL_PCIE_PLL_DITHER_DIV_MIN         0x14
#define QCA955X_PLL_PCIE_PLL_DITHER_STEP            0x18
#define QCA955X_PLL_LDO_POWER_CONTROL               0x1c
#define QCA955X_PLL_SWITCH_CLOCK_SPARE              0x20
#define QCA955X_PLL_CURRENT_PCIE_PLL_DITHER         0x24
#define QCA955X_PLL_ETH_XMII                        0x28
#define QCA955X_PLL_AUDIO_PLL_CONFIG                0x2c
#define QCA955X_PLL_AUDIO_PLL_MODULATION            0x30
#define QCA955X_PLL_AUDIO_PLL_MOD_STEP              0x34
#define QCA955X_PLL_CURRENT_AUDIO_PLL_MODULATION    0x38
#define QCA955X_PLL_BB_PLL_CONFIG                   0x3c
#define QCA955X_PLL_DDR_PLL_DITHER                  0x40
#define QCA955X_PLL_CPU_PLL_DITHER                  0x44


#define QCA955X_PLL_CPU_CONFIG_NFRAC_SHIFT	0
#define QCA955X_PLL_CPU_CONFIG_NFRAC_MASK	0x3f
#define QCA955X_PLL_CPU_CONFIG_NINT_SHIFT	6
#define QCA955X_PLL_CPU_CONFIG_NINT_MASK	0x3f
#define QCA955X_PLL_CPU_CONFIG_REFDIV_SHIFT	12
#define QCA955X_PLL_CPU_CONFIG_REFDIV_MASK	0x1f
#define QCA955X_PLL_CPU_CONFIG_OUTDIV_SHIFT	19
#define QCA955X_PLL_CPU_CONFIG_OUTDIV_MASK	0x3

#define QCA955X_PLL_DDR_CONFIG_NFRAC_SHIFT	0
#define QCA955X_PLL_DDR_CONFIG_NFRAC_MASK	0x3ff
#define QCA955X_PLL_DDR_CONFIG_NINT_SHIFT	10
#define QCA955X_PLL_DDR_CONFIG_NINT_MASK	0x3f
#define QCA955X_PLL_DDR_CONFIG_REFDIV_SHIFT	16
#define QCA955X_PLL_DDR_CONFIG_REFDIV_MASK	0x1f
#define QCA955X_PLL_DDR_CONFIG_OUTDIV_SHIFT	23
#define QCA955X_PLL_DDR_CONFIG_OUTDIV_MASK	0x7

#define QCA955X_PLL_CLK_CTRL_CPU_PLL_BYPASS		BIT(2)
#define QCA955X_PLL_CLK_CTRL_DDR_PLL_BYPASS		BIT(3)
#define QCA955X_PLL_CLK_CTRL_AHB_PLL_BYPASS		BIT(4)
#define QCA955X_PLL_CLK_CTRL_CPU_POST_DIV_SHIFT		5
#define QCA955X_PLL_CLK_CTRL_CPU_POST_DIV_MASK		0x1f
#define QCA955X_PLL_CLK_CTRL_DDR_POST_DIV_SHIFT		10
#define QCA955X_PLL_CLK_CTRL_DDR_POST_DIV_MASK		0x1f
#define QCA955X_PLL_CLK_CTRL_AHB_POST_DIV_SHIFT		15
#define QCA955X_PLL_CLK_CTRL_AHB_POST_DIV_MASK		0x1f
#define QCA955X_PLL_CLK_CTRL_CPUCLK_FROM_CPUPLL		BIT(20)
#define QCA955X_PLL_CLK_CTRL_DDRCLK_FROM_DDRPLL		BIT(21)
#define QCA955X_PLL_CLK_CTRL_AHBCLK_FROM_DDRPLL		BIT(24)

// [GJu] some substitutions for general code compatibility
#define ATH_PLL_ETH_XMII_GIGE_SET(x)          ETH_XMII_GIGE_SET(x)
#define ATH_PLL_ETH_XMII_TX_DELAY_SET(x)      ETH_XMII_TX_DELAY_SET(x)
#define ATH_PLL_ETH_XMII_RX_DELAY_SET(x)      ETH_XMII_RX_DELAY_SET(x)
#define ATH_PLL_ETH_XMII_PHASE0_COUNT_SET(x)  ETH_XMII_PHASE0_COUNT_SET(x)
#define ATH_PLL_ETH_XMII_PHASE1_COUNT_SET(x)  ETH_XMII_PHASE1_COUNT_SET(x)
//

/*------------------------------------------------------------------------------------------*\
 * USB_CONFIG block
\*------------------------------------------------------------------------------------------*/
#define AR71XX_USB_CTRL_REG_FLADJ	0x00
#define AR71XX_USB_CTRL_REG_CONFIG	0x04

#define ATH_USB_FLADJ_VAL		    (AR71XX_USB_CTRL_BASE + 0x0)
#define ATH_USB_CONFIG			    (AR71XX_USB_CTRL_BASE + 0x4)
#define ATH_USB_WINDOW			    0x10000
#define ATH_USB_MODE			    (AR71XX_EHCI_BASE + 0x1a8)
#define ATH_USB_PORTSCX			    (AR71XX_EHCI_BASE + 0x184)
#define ATH_USB_STS                	(AR71XX_EHCI_BASE + 0x144)
#define ATH_USB_SET_SERIAL_MODE		(1<<29) /* This bit will enable the serial mode */
#define ATH_USB_STS_SOF             (1<<7)
#define ATH_USB2_STS_SOF            (1<<7)

#define ATH_USB2_MODE               (QCA955X_EHCI1_BASE + 0x1a8)
#define ATH_USB2_PORTSCX		    (QCA955X_EHCI1_BASE + 0x184)
#define ATH_USB2_STS               	(QCA955X_EHCI1_BASE + 0x144)

/*------------------------------------------------------------------------------------------*\
 * RESET block
\*------------------------------------------------------------------------------------------*/

#define AR71XX_RESET_REG_TIMER			    0x00
#define AR71XX_RESET_REG_TIMER_RELOAD		0x04
#define AR71XX_RESET_REG_WDOG_CTRL		    0x08
#define AR71XX_RESET_REG_WDOG			    0x0c

#define ATH_WD_ACT_MASK			3u
#define ATH_WD_ACT_NONE			0u /* No Action */
#define ATH_WD_ACT_GP_INTR		1u /* General purpose intr */
#define ATH_WD_ACT_NMI			2u /* NMI */
#define ATH_WD_ACT_RESET		3u /* Full Chip Reset */

#define ATH_WD_LAST_SHIFT		31
#define ATH_WD_LAST_MASK		((uint32_t)(1 << ATH_WD_LAST_SHIFT))


#define AR71XX_RESET_REG_MISC_INT_STATUS	0x10
#define AR71XX_RESET_REG_MISC_INT_ENABLE	0x14
#define AR71XX_RESET_REG_GLOBAL_INT_STATUS	0x18
#define AR71XX_RESET_REG_RESET_MODULE		0x1c
#define AR71XX_RESET_REG_BOOTSTRAP          0xb0

#define AR71XX_RESET_REG_PERFC_CTRL		    0x2c
#define AR71XX_RESET_REG_PERFC0			    0x30
#define AR71XX_RESET_REG_PERFC1			    0x34
#define AR71XX_RESET_REG_REV_ID			    0x90
#define AR71XX_STICKY_REG			        0xB8

/*--- #define AR71XX_RESET_REG_PCI_INT_STATUS		0x18 ---*/
/*--- #define AR71XX_RESET_REG_PCI_INT_ENABLE		0x1c ---*/

#define AR913X_RESET_REG_GLOBAL_INT_STATUS      0x18
#define AR913X_RESET_REG_RESET_MODULE	        0x1c
#define AR913X_RESET_REG_PERF_CTRL		        0x20
#define AR913X_RESET_REG_PERFC0			        0x24
#define AR913X_RESET_REG_PERFC1			        0x28

#define AR724X_RESET_REG_RESET_MODULE	        0x1c

#define AR933X_RESET_REG_RESET_MODULE	        0x1c
#define AR933X_RESET_REG_BOOTSTRAP		        0xac

#define AR934X_RESET_REG_RESET_MODULE	        0x1c
#define AR934X_RESET_REG_BOOTSTRAP		        0xb0
#define AR934X_RESET_REG_PCIE_WMAC_INT_STATUS	0xac

#define QCA955X_RESET_REG_RESET_MODULE		    0x1c
#define QCA955X_RESET_REG_BOOTSTRAP		        0xb0
#define QCA955X_RESET_REG_EXT_INT_STATUS	    0xac

// [GJu] General defines supporting general code

#define ATH_RESET_REG_WDOG_CTRL              AR71XX_RESET_REG_WDOG_CTRL
#define ATH_RESET_REG_MISC_INTERRUPT_STATUS  AR71XX_RESET_REG_MISC_INT_STATUS
#define ATH_RESET_REG_MISC_INTERRUPT_ENABLE  AR71XX_RESET_REG_MISC_INT_ENABLE	

#define ATH_RESET_REG_REVISION_ID            AR71XX_RESET_REG_REV_ID

#if defined CONFIG_SOC_AR71XX
#define ATH_RESET_REG_RESET_MODULE           AR71XX_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              AR71XX_RESET_REG_BOOTSTRAP
#elif defined CONFIG_SOC_AR724X
#define ATH_RESET_REG_RESET_MODULE           AR724X_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              AR71XX_RESET_REG_BOOTSTRAP
#elif defined CONFIG_SOC_AR913X
#define ATH_RESET_REG_RESET_MODULE           AR913X_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              AR71XX_RESET_REG_BOOTSTRAP
#elif defined CONFIG_SOC_AR933X
#define ATH_RESET_REG_RESET_MODULE           AR933X_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              AR933X_RESET_REG_BOOTSTRAP
#elif defined CONFIG_SOC_AR934X
#define ATH_RESET_REG_RESET_MODULE           AR934X_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              AR934X_RESET_REG_BOOTSTRAP
#elif defined CONFIG_SOC_QCA955X
#define ATH_RESET_REG_RESET_MODULE           QCA955X_RESET_REG_RESET_MODULE 
#define ATH_RESET_REG_BOOTSTRAP              QCA955X_RESET_REG_BOOTSTRAP
#endif

#define ATH_RESET_REG_STICKY                 AR71XX_STICKY_REG

//

#define AR71XX_RESET_EXTERNAL		    BIT(28)
#define AR71XX_RESET_FULL_CHIP		    BIT(24)
#define AR71XX_RESET_CPU_NMI		    BIT(21)
#define AR71XX_RESET_CPU_COLD		    BIT(20)
#define AR71XX_RESET_DMA		        BIT(19)
#define AR71XX_RESET_SLIC		        BIT(18)
#define AR71XX_RESET_STEREO		        BIT(17)
#define AR71XX_RESET_UART1		        BIT(17)
#define AR71XX_RESET_DDR		        BIT(16)
#define AR71XX_RESET_GE1_MAC		    BIT(13)
#define AR71XX_RESET_GE1_PHY		    BIT(12)
#define AR71XX_RESET_USBSUS_OVERRIDE	BIT(10)
#define AR71XX_RESET_GE0_MAC		    BIT(9)
#define AR71XX_RESET_GE0_PHY		    BIT(8)
#define AR71XX_RESET_USB_OHCI_DLL	    BIT(6)
#define AR71XX_RESET_USB_HOST		    BIT(5)
#define AR71XX_RESET_USB_PHY		    BIT(4)
#define AR71XX_RESET_LUT	            BIT(2)
#define AR71XX_RESET_MBOX	            BIT(1)
#define AR71XX_RESET_PCI_BUS		    BIT(1)
#define AR71XX_RESET_I2C	            BIT(0)
#define AR71XX_RESET_PCI_CORE		    BIT(0)

#define AR7240_RESET_USB_HOST		    BIT(5)
#define AR7240_RESET_OHCI_DLL		    BIT(3)

#define AR724X_RESET_GE1_MDIO		    BIT(23)
#define AR724X_RESET_GE0_MDIO		    BIT(22)
#define AR724X_RESET_PCIE_PHY_SERIAL	BIT(10)
#define AR724X_RESET_PCIE_PHY		    BIT(7)
#define AR724X_RESET_PCIE		        BIT(6)
#define AR724X_RESET_USB_HOST		    BIT(5)
#define AR724X_RESET_USB_PHY		    BIT(4)
#define AR724X_RESET_USBSUS_OVERRIDE	BIT(3)

#define AR913X_RESET_AMBA2WMAC		    BIT(22)
#define AR913X_RESET_USBSUS_OVERRIDE	BIT(10)
#define AR913X_RESET_USB_HOST		    BIT(5)
#define AR913X_RESET_USB_PHY		    BIT(4)

#define AR933X_RESET_WMAC		        BIT(11)
#define AR933X_RESET_USB_HOST		    BIT(5)
#define AR933X_RESET_USB_PHY		    BIT(4)
#define AR933X_RESET_USBSUS_OVERRIDE	BIT(3)

#define AR934X_RESET_UART1		        BIT(17)
#define AR934X_RESET_USB_PHY_ANALOG	    BIT(11)
#define AR934X_RESET_USB_HOST		    BIT(5)
#define AR934X_RESET_USB_PHY		    BIT(4)
#define AR934X_RESET_USBSUS_OVERRIDE	BIT(3)
#define AR934X_RESET_LUT	            BIT(2)
#define AR934X_RESET_MBOX	            BIT(1)
#define AR934X_RESET_I2C	            BIT(0)

#define ATH_RESET_GE1_MDIO          AR724X_RESET_GE1_MDIO
#define ATH_RESET_GE0_MDIO          AR724X_RESET_GE0_MDIO
#define ATH_RESET_GE1_PHY           AR71XX_RESET_GE1_PHY
#define ATH_RESET_GE0_PHY           AR71XX_RESET_GE0_PHY
#define ATH_RESET_GE1_MAC		    AR71XX_RESET_GE1_MAC
#define ATH_RESET_GE0_MAC		    AR71XX_RESET_GE0_MAC
#define ATH_RESET_FULL_CHIP         AR71XX_RESET_FULL_CHIP
#define ATH_RESET_DDR               AR71XX_RESET_DDR
#define ATH_RESET_UART1             AR71XX_RESET_UART1

#define ATH_GENERAL_TMR			        (AR71XX_RESET_BASE + 0)
#define ATH_GENERAL_TMR2		        (AR71XX_RESET_BASE + 0x94)
#define ATH_GENERAL_TMR3		        (AR71XX_RESET_BASE + 0x9c)
#define ATH_GENERAL_TMR4		        (AR71XX_RESET_BASE + 0xa4)
#define ATH_GENERAL_TMR_RELOAD		    (AR71XX_RESET_BASE + 4)
#define ATH_GENERAL_TMR2_RELOAD		    (AR71XX_RESET_BASE + 0x98)
#define ATH_GENERAL_TMR3_RELOAD		    (AR71XX_RESET_BASE + 0xa0)
#define ATH_GENERAL_TMR4_RELOAD		    (AR71XX_RESET_BASE + 0xa8)
#define ATH_WATCHDOG_TMR_CONTROL	    (AR71XX_RESET_BASE + 8)
#define ATH_WATCHDOG_TMR			    (AR71XX_RESET_BASE + 0xc)
#define ATH_RST_MISC_INTERRUPT_STATUS	(AR71XX_RESET_BASE + ATH_RESET_REG_MISC_INT_STATUS)
#define ATH_RST_MISC_INTERRUPT_MASK	    (AR71XX_RESET_BASE + ATH_RESET_REG_MISC_INT_ENABLE)
#define ATH_RST_RESET_MODULE            (AR71XX_RESET_BASE + ATH_RESET_REG_RESET_MODULE)
// for backward compatibiliy
#define ATH_RESET                       ATH_RST_RESET_MODULE

#define ATH_RST_REVISION_ID	            (AR71XX_RESET_BASE + ATH_RESET_REG_REV_ID)
#define ATH_RST_BOOTSTRAP		        (AR71XX_RESET_BASE + ATH_RESET_REG_BOOTSTRAP)
#define ATH_SPARE_STICKY		        (AR71XX_RESET_BASE + 0xb8)

#define AR933X_BOOTSTRAP_REF_CLK_40	BIT(0)

#define AR934X_BOOTSTRAP_SW_OPTION8	    BIT(23)
#define AR934X_BOOTSTRAP_SW_OPTION7	    BIT(22)
#define AR934X_BOOTSTRAP_SW_OPTION6	    BIT(21)
#define AR934X_BOOTSTRAP_SW_OPTION5	    BIT(20)
#define AR934X_BOOTSTRAP_SW_OPTION4	    BIT(19)
#define AR934X_BOOTSTRAP_SW_OPTION3	    BIT(18)
#define AR934X_BOOTSTRAP_SW_OPTION2	    BIT(17)
#define AR934X_BOOTSTRAP_SW_OPTION1	    BIT(16)
#define AR934X_BOOTSTRAP_USB_MODE_DEVICE BIT(7)
#define AR934X_BOOTSTRAP_PCIE_RC	    BIT(6)
#define AR934X_BOOTSTRAP_EJTAG_MODE	    BIT(5)
#define AR934X_BOOTSTRAP_REF_CLK_40	    BIT(4)
#define AR934X_BOOTSTRAP_BOOT_FROM_SPI	BIT(2)
#define AR934X_BOOTSTRAP_SDRAM_DISABLED BIT(1)
#define AR934X_BOOTSTRAP_DDR1		    BIT(0)

#define QCA955X_BOOTSTRAP_REF_CLK_40	BIT(4)

#define AR934X_PCIE_WMAC_INT_WMAC_MISC		BIT(0)
#define AR934X_PCIE_WMAC_INT_WMAC_TX		BIT(1)
#define AR934X_PCIE_WMAC_INT_WMAC_RXLP		BIT(2)
#define AR934X_PCIE_WMAC_INT_WMAC_RXHP		BIT(3)
#define AR934X_PCIE_WMAC_INT_PCIE_RC		BIT(4)
#define AR934X_PCIE_WMAC_INT_PCIE_RC0		BIT(5)
#define AR934X_PCIE_WMAC_INT_PCIE_RC1		BIT(6)
#define AR934X_PCIE_WMAC_INT_PCIE_RC2		BIT(7)
#define AR934X_PCIE_WMAC_INT_PCIE_RC3		BIT(8)
#define AR934X_PCIE_WMAC_INT_WMAC_ALL \
	(AR934X_PCIE_WMAC_INT_WMAC_MISC | AR934X_PCIE_WMAC_INT_WMAC_TX | \
	 AR934X_PCIE_WMAC_INT_WMAC_RXLP | AR934X_PCIE_WMAC_INT_WMAC_RXHP)

#define AR934X_PCIE_WMAC_INT_PCIE_ALL \
	(AR934X_PCIE_WMAC_INT_PCIE_RC | AR934X_PCIE_WMAC_INT_PCIE_RC0 | \
	 AR934X_PCIE_WMAC_INT_PCIE_RC1 | AR934X_PCIE_WMAC_INT_PCIE_RC2 | \
	 AR934X_PCIE_WMAC_INT_PCIE_RC3)

#define QCA955X_EXT_INT_WMAC_MISC		    BIT(0)
#define QCA955X_EXT_INT_WMAC_TX			    BIT(1)
#define QCA955X_EXT_INT_WMAC_RXLP		    BIT(2)
#define QCA955X_EXT_INT_WMAC_RXHP		    BIT(3)
#define QCA955X_EXT_INT_PCIE_RC1		    BIT(4)
#define QCA955X_EXT_INT_PCIE_RC1_INT0		BIT(5)
#define QCA955X_EXT_INT_PCIE_RC1_INT1		BIT(6)
#define QCA955X_EXT_INT_PCIE_RC1_INT2		BIT(7)
#define QCA955X_EXT_INT_PCIE_RC1_INT3		BIT(8)
#define QCA955X_EXT_INT_PCIE_RC2		    BIT(12)
#define QCA955X_EXT_INT_PCIE_RC2_INT0		BIT(13)
#define QCA955X_EXT_INT_PCIE_RC2_INT1		BIT(14)
#define QCA955X_EXT_INT_PCIE_RC2_INT2		BIT(15)
#define QCA955X_EXT_INT_PCIE_RC2_INT3		BIT(16)
#define QCA955X_EXT_INT_USB1			    BIT(24)
#define QCA955X_EXT_INT_USB2			    BIT(28)

#define QCA955X_EXT_INT_WMAC_ALL \
	(QCA955X_EXT_INT_WMAC_MISC | QCA955X_EXT_INT_WMAC_TX | \
	 QCA955X_EXT_INT_WMAC_RXLP | QCA955X_EXT_INT_WMAC_RXHP)

#define QCA955X_EXT_INT_PCIE_RC1_ALL \
	(QCA955X_EXT_INT_PCIE_RC1 | QCA955X_EXT_INT_PCIE_RC1_INT0 | \
	 QCA955X_EXT_INT_PCIE_RC1_INT1 | QCA955X_EXT_INT_PCIE_RC1_INT2 | \
	 QCA955X_EXT_INT_PCIE_RC1_INT3)

#define QCA955X_EXT_INT_PCIE_RC2_ALL \
	(QCA955X_EXT_INT_PCIE_RC2 | QCA955X_EXT_INT_PCIE_RC2_INT0 | \
	 QCA955X_EXT_INT_PCIE_RC2_INT1 | QCA955X_EXT_INT_PCIE_RC2_INT2 | \
	 QCA955X_EXT_INT_PCIE_RC2_INT3)

#define REV_ID_MAJOR_MASK		0xfff0
#define REV_ID_MAJOR_AR71XX		0x00a0
#define REV_ID_MAJOR_AR913X		0x00b0
#define REV_ID_MAJOR_AR7240		0x00c0
#define REV_ID_MAJOR_AR7241		0x0100
#define REV_ID_MAJOR_AR7242		0x1100
#define REV_ID_MAJOR_AR9330		0x0110
#define REV_ID_MAJOR_AR9331		0x1110
#define REV_ID_MAJOR_AR9341		0x0120
#define REV_ID_MAJOR_AR9342		0x1120
#define REV_ID_MAJOR_AR9344		0x2120
#define REV_ID_MAJOR_QCA9556		0x0130
#define REV_ID_MAJOR_QCA9558		0x1130

#define AR71XX_REV_ID_MINOR_MASK	0x3
#define AR71XX_REV_ID_MINOR_AR7130	0x0
#define AR71XX_REV_ID_MINOR_AR7141	0x1
#define AR71XX_REV_ID_MINOR_AR7161	0x2
#define AR71XX_REV_ID_REVISION_MASK	0x3
#define AR71XX_REV_ID_REVISION_SHIFT	2

#define AR913X_REV_ID_MINOR_MASK	0x3
#define AR913X_REV_ID_MINOR_AR9130	0x0
#define AR913X_REV_ID_MINOR_AR9132	0x1
#define AR913X_REV_ID_REVISION_MASK	0x3
#define AR913X_REV_ID_REVISION_SHIFT	2

#define AR933X_REV_ID_REVISION_MASK	0x3

#define AR724X_REV_ID_REVISION_MASK	0x3

#define AR934X_REV_ID_REVISION_MASK	0xf

#define QCA955X_REV_ID_REVISION_MASK 0xf


/*------------------------------------------------------------------------------------------*\
 * SPI block
\*------------------------------------------------------------------------------------------*/
#define AR71XX_SPI_REG_FS	0x00	/* Function Select */
#define AR71XX_SPI_REG_CTRL	0x04	/* SPI Control */
#define AR71XX_SPI_REG_IOC	0x08	/* SPI I/O Control */
#define AR71XX_SPI_REG_RDS	0x0c	/* Read Data Shift */

#define AR71XX_SPI_FS_GPIO	    BIT(0)	/* Enable GPIO mode */

#define AR71XX_SPI_CTRL_RD	    BIT(6)	/* Remap Disable */
#define AR71XX_SPI_CTRL_DIV_MASK 0x3f

#define AR71XX_SPI_IOC_DO	    BIT(0)	/* Data Out pin */
#define AR71XX_SPI_IOC_CLK	    BIT(8)	/* CLK pin */
#define AR71XX_SPI_IOC_CS(n)	BIT(16 + (n))
#define AR71XX_SPI_IOC_CS0	AR71XX_SPI_IOC_CS(0)
#define AR71XX_SPI_IOC_CS1	AR71XX_SPI_IOC_CS(1)
#define AR71XX_SPI_IOC_CS2	AR71XX_SPI_IOC_CS(2)
#define AR71XX_SPI_IOC_CS_ALL	(AR71XX_SPI_IOC_CS0 | AR71XX_SPI_IOC_CS1 | AR71XX_SPI_IOC_CS2)

#define ATH_SPI_FS		        (AR71XX_SPI_BASE + AR71XX_SPI_REG_FS)
#define ATH_SPI_READ		    (AR71XX_SPI_BASE + AR71XX_SPI_REG_FS)
#define ATH_SPI_CLOCK		    (AR71XX_SPI_BASE + AR71XX_SPI_REG_CTRL)
#define ATH_SPI_WRITE		    (AR71XX_SPI_BASE + AR71XX_SPI_REG_IOC)
#define ATH_SPI_RD_STATUS	    (AR71XX_SPI_BASE + AR71XX_SPI_REG_RDS)
#define ATH_SPI_SHIFT_DO	    (AR71XX_SPI_BASE + 0x10)
#define ATH_SPI_SHIFT_CNT	    (AR71XX_SPI_BASE + 0x14)
#define ATH_SPI_SHIFT_DI	    (AR71XX_SPI_BASE + 0x18)

#define ATH_SPI_IOC_CLK          AR71XX_SPI_IOC_CLK
#define ATH_SPI_IOC_CS_ENABLE(i) (AR71XX_SPI_IOC_CS_ALL & ~AR71XX_SPI_IOC_CS (i))

#define ATH_SPI_IOC_CS_ALL       AR71XX_SPI_IOC_CS_ALL

/*------------------------------------------------------------------------------------------*\
 * GPIO block
\*------------------------------------------------------------------------------------------*/
#define AR71XX_GPIO_REG_OE		    0x00
#define AR71XX_GPIO_REG_IN		    0x04
#define AR71XX_GPIO_REG_OUT		    0x08
#define AR71XX_GPIO_REG_SET		    0x0c
#define AR71XX_GPIO_REG_CLEAR		0x10
#define AR71XX_GPIO_REG_INT_MODE	0x14  // [GJu] FIXME ???
#define AR71XX_GPIO_REG_INT_ENABLE	0x14
#define AR71XX_GPIO_REG_INT_TYPE	0x18
#define AR71XX_GPIO_REG_INT_POLARITY	0x1c
#define AR71XX_GPIO_REG_INT_PENDING	0x20
//#define AR71XX_GPIO_REG_INT_ENABLE	0x24  [GJu] FIXME
#define AR71XX_GPIO_REG_FUNC		0x28

#define AR934X_GPIO_REG_FUNC		0x6c

#define AR71XX_GPIO_COUNT		16
#define AR7240_GPIO_COUNT		18
#define AR7241_GPIO_COUNT		20
#define AR913X_GPIO_COUNT		22
#define AR933X_GPIO_COUNT		30
#define AR934X_GPIO_COUNT		23
#define QCA955X_GPIO_COUNT		24

#define ATH_GPIO_OE			        (AR71XX_GPIO_BASE + 0x0)
#define ATH_GPIO_IN			        (AR71XX_GPIO_BASE + 0x4)
#define ATH_GPIO_OUT			    (AR71XX_GPIO_BASE + 0x8)
#define ATH_GPIO_SET			    (AR71XX_GPIO_BASE + 0xc)
#define ATH_GPIO_CLEAR			    (AR71XX_GPIO_BASE + 0x10)
#define ATH_GPIO_INT_ENABLE		    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_ENABLE)
#define ATH_GPIO_INT_TYPE		    (AR71XX_GPIO_BASE + 0x18)
#define ATH_GPIO_INT_POLARITY	    (AR71XX_GPIO_BASE + 0x1c)
#define ATH_GPIO_INT_PENDING	    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_PENDING)
#define ATH_GPIO_INT_MASK		    (AR71XX_GPIO_BASE + 0x24)
#define ATH_GPIO_IN_ETH_SWITCH_LED	(AR71XX_GPIO_BASE + 0x28)
#define ATH_GPIO_OUT_FUNCTION0		(AR71XX_GPIO_BASE + 0x2c)
#define ATH_GPIO_OUT_FUNCTION1		(AR71XX_GPIO_BASE + 0x30)
#define ATH_GPIO_OUT_FUNCTION2		(AR71XX_GPIO_BASE + 0x34)
#define ATH_GPIO_OUT_FUNCTION3		(AR71XX_GPIO_BASE + 0x38)
#define ATH_GPIO_OUT_FUNCTION4		(AR71XX_GPIO_BASE + 0x3c)
#define ATH_GPIO_OUT_FUNCTION5		(AR71XX_GPIO_BASE + 0x40)
#define ATH_GPIO_IN_ENABLE0		    (AR71XX_GPIO_BASE + 0x44)
#define ATH_GPIO_IN_ENABLE1		    (AR71XX_GPIO_BASE + 0x48)
#define ATH_GPIO_IN_ENABLE2		    (AR71XX_GPIO_BASE + 0x4c)
#define ATH_GPIO_IN_ENABLE3		    (AR71XX_GPIO_BASE + 0x50)
#define ATH_GPIO_IN_ENABLE4		    (AR71XX_GPIO_BASE + 0x54)
#define ATH_GPIO_IN_ENABLE5		    (AR71XX_GPIO_BASE + 0x58)
#define ATH_GPIO_IN_ENABLE6		    (AR71XX_GPIO_BASE + 0x5c)
#define ATH_GPIO_IN_ENABLE7		    (AR71XX_GPIO_BASE + 0x60)
#define ATH_GPIO_IN_ENABLE8		    (AR71XX_GPIO_BASE + 0x64)
#define ATH_GPIO_IN_ENABLE9		    (AR71XX_GPIO_BASE + 0x68)
#define ATH_GPIO_FUNCTIONS		    (AR71XX_GPIO_BASE + 0x6c)
#define ATH_GPIO_FUNCTION_2		    (AR71XX_GPIO_BASE + 0x30)

/*
 * GPIO Function Enables
 */
#define ATH_GPIO_FUNCTION_STEREO_EN			        (1<<17)
#define ATH_GPIO_FUNCTION_SLIC_EN			        (1<<16)

#define ATH_GPIO_FUNCTION_OVERCURRENT_EN		    (1<< 4)
#define ATH_GPIO_FUNCTION_USB_CLK_CORE_EN		    (1<< 0)
#define ATH_GPIO_FUNCTION_WMAC_LED			        (1<<22)
#define ATH_GPIO_FUNCTION_STEREO_EN			        (1<<17)
#define ATH_GPIO_FUNCTION_SLIC_EN			        (1<<16)
#define ATH_GPIO_FUNCTION_SPDIF2TCK_EN			    (1<<31)
#define ATH_GPIO_FUNCTION_SPDIF_EN			        (1<<30)
#define ATH_GPIO_FUNCTION_I2S_GPIO_18_22_EN		    (1<<29)
#define ATH_GPIO_FUNCTION_I2S_REFCLKEN			    (1<<28)
#define ATH_GPIO_FUNCTION_I2S_MCKEN			        (1<<27)
#define ATH_GPIO_FUNCTION_I2S0_EN			        (1<<26)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED_DUPL_EN	(1<<25)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED_COLL		(1<<24)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED_ACTV		(1<<23)
#define ATH_GPIO_FUNCTION_PLL_SHIFT_EN			    (1<<22)
#define ATH_GPIO_FUNCTION_EXT_MDIO_SEL			    (1<<21)
#define ATH_GPIO_FUNCTION_CLK_OBS6_ENABLE		    (1<<20)
#define ATH_GPIO_FUNCTION_CLK_OBS0_ENABLE		    (1<<19)
#define ATH_GPIO_FUNCTION_SPI_EN			        (1<<18)
#define ATH_GPIO_FUNCTION_DDR_DQOE_EN			    (1<<17)
#define ATH_GPIO_FUNCTION_PCIEPHY_TST_EN		    (1<<16)
#define ATH_GPIO_FUNCTION_S26_UART_DISABLE		    (1<<15)
#define ATH_GPIO_FUNCTION_SPI_CS_1_EN			    (1<<14)
#define ATH_GPIO_FUNCTION_SPI_CS_0_EN			    (1<<13)
#define ATH_GPIO_FUNCTION_CLK_OBS5_ENABLE		    (1<<12)
#define ATH_GPIO_FUNCTION_CLK_OBS4_ENABLE		    (1<<11)
#define ATH_GPIO_FUNCTION_CLK_OBS3_ENABLE		    (1<<10)
#define ATH_GPIO_FUNCTION_CLK_OBS2_ENABLE		    (1<< 9)
#define ATH_GPIO_FUNCTION_CLK_OBS1_ENABLE		    (1<< 8)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED4_EN	    (1<< 7)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED3_EN	    (1<< 6)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED2_EN	    (1<< 5)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED1_EN	    (1<< 4)
#define ATH_GPIO_FUNCTION_ETH_SWITCH_LED0_EN	    (1<< 3)
#define ATH_GPIO_FUNCTION_UART_RTS_CTS_EN		    (1<< 2)
#define ATH_GPIO_FUNCTION_UART_EN			        (1<< 1)
#define ATH_GPIO_FUNCTION_2_EN_I2WS_ON_0		    (1<< 4)
#define ATH_GPIO_FUNCTION_2_EN_I2SCK_ON_1		    (1<< 3)
#define ATH_GPIO_FUNCTION_2_I2S_ON_LED			    (1<< 1)
#define ATH_GPIO_FUNCTION_SRIF_ENABLE			    (1<< 0)
#define ATH_GPIO_FUNCTION_JTAG_DISABLE			    (1<< 1)
#define ATH_GPIO_FUNCTION_USB_LED			        (1<< 4)
#define ATH_GPIO_FUNCTION_JTAG_DISABLE			    (1<< 1)

/*------------------------------------------------------------------------------------------*\
 * SRIF block
\*------------------------------------------------------------------------------------------*/
#define AR934X_SRIF_CPU_DPLL1_REG	0x1c0
#define AR934X_SRIF_CPU_DPLL2_REG	0x1c4
#define AR934X_SRIF_CPU_DPLL3_REG	0x1c8

#define AR934X_SRIF_DDR_DPLL1_REG	0x240
#define AR934X_SRIF_DDR_DPLL2_REG	0x244
#define AR934X_SRIF_DDR_DPLL3_REG	0x248

#define AR934X_SRIF_DPLL1_REFDIV_SHIFT	27
#define AR934X_SRIF_DPLL1_REFDIV_MASK	0x1f
#define AR934X_SRIF_DPLL1_NINT_SHIFT	18
#define AR934X_SRIF_DPLL1_NINT_MASK	0x1ff
#define AR934X_SRIF_DPLL1_NFRAC_MASK	0x0003ffff

#define AR934X_SRIF_DPLL2_LOCAL_PLL	BIT(30)
#define AR934X_SRIF_DPLL2_OUTDIV_SHIFT	13
#define AR934X_SRIF_DPLL2_OUTDIV_MASK	0x7

/*------------------------------------------------------------------------------------------*\
 * High Speed Uart
\*------------------------------------------------------------------------------------------*/
#define ATH_HS_UART_BASE        0x18500000
#define ATH_HS_UART_CS			(ATH_HS_UART_BASE + 0x04)
#define ATH_HS_UART_CLK			(ATH_HS_UART_BASE + 0x08)
#define ATH_HS_UART_INT_STATUS	(ATH_HS_UART_BASE + 0x0c)
#define ATH_HS_UART_INT_EN		(ATH_HS_UART_BASE + 0x10)

#define ATH_HS_UART_TD			0x12
#define ATH_HS_UART_RTS			0x13

#define ATH_HS_UART_TD_GPIO		    23
#define ATH_HS_UART_RD_GPIO		    22
#define ATH_HS_UART_RTS_GPIO		21
#define ATH_HS_UART_CTS_GPIO		18

#define ATH_HS_UART_BAUD		115200
//#define ATH_HS_UART_BAUD		3000000

#ifdef CONFIG_ATH_EMULATION
#	define ATH_HS_SERIAL_CLOCK	    80
#else
#	define ATH_HS_SERIAL_CLOCK	    100
#endif /* CONFIG_ATH_EMULATION */

#endif /* (defined CONFIG_SOC_AR71XX || defined CONFIG_SOC_AR724X || defined CONFIG_SOC_AR913X || defined CONFIG_SOC_AR933X || defined CONFIG_SOC_AR934X || defined CONFIG_SOC_QCA955X) */
#endif /* __ASM_MACH_AR71XX_REGS_H */

