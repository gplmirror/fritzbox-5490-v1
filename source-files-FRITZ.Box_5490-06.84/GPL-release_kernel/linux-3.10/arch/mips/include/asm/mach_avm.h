#ifndef _mach_avm_h_
#define _mach_avm_h_

#include <linux/kconfig.h>
#include <linux/avm_reboot_status.h>

struct pt_regs;
extern void show_registers(const struct pt_regs *regs);

#if defined(CONFIG_LANTIQ)
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_fpi        = 0x02, /*--- On-Chip Bus ---*/
    avm_clock_id_usb        = 0x04,
    avm_clock_id_pci        = 0x08,
    avm_clock_id_ephy       = 0x10, /*--- Ethernet CLKO ---*/
    avm_clock_id_ddr        = 0x20, /*--- DDR ---*/
    avm_clock_id_pp32       = 0x40, /*--- Paketprocessor ---*/
};

extern unsigned int lantiq_get_clock(enum _avm_clock_id clock_id);

#define avm_get_clock               lantiq_get_clock

#include <asm/hw_gpio.h>

#endif/*--- #if defined(CONFIG_LANTIQ) ---*/


/*------------------------------------------------------------------------------------------*\
 * VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN VIRIAN 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_SOC_AR724X) || defined(CONFIG_SOC_AR934X) || defined(CONFIG_SOC_QCA955X) || defined(CONFIG_SOC_QCA953X) || defined(CONFIG_SOC_QCA956X)

#include <atheros.h>
#include <atheros_gpio.h>
#define avm_gpio_init           ath_avm_gpio_init			/*--- init ath_avm_gpio_init(void) ---*/ 
#define avm_gpio_ctrl           ath_avm_gpio_ctrl			/*--- int ath_avm_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir) ---*/
#define avm_gpio_out_bit        ath_avm_gpio_out_bit		/*--- int ath_avm_gpio_out_bit(unsigned int gpio_pin, int value)  ---*/
#define avm_gpio_in_bit         ath_avm_gpio_in_bit		    /*--- int ath_avm_gpio_in_bit(unsigned int gpio_pin)  ---*/
#define avm_gpio_in_value       ath_avm_gpio_in_value		/*--- unsigned int ath_avm_gpio_in_value(void)  ---*/
#define avm_gpio_set_bitmask    ath_avm_gpio_set_bitmask	/*--- void ath_avm_gpio_set_bitmask(unsigned int mask, unsigned int value)  ---*/
#define avm_gpio_request_irq    ath_avm_gpio_request_irq
#define avm_gpio_enable_irq     ath_avm_gpio_enable_irq
#define avm_gpio_disable_irq    ath_avm_gpio_disable_irq

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_ddr        = 0x02,
    avm_clock_id_ahb        = 0x04,
    avm_clock_id_ref        = 0x08,
    avm_clock_id_peripheral = 0x10  /*--- Uart ---*/
};
extern unsigned int ath_get_clock(enum _avm_clock_id id);
#define avm_get_clock           ath_get_clock

extern void ath_avm_gpio_dump_registers(const char *prefix);
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

#endif /*--- #if defined(CONFIG_SOC_AR724X) || defined(CONFIG_SOC_AR934X) || defined(CONFIG_SOC_QCA955X) ---*/


/*------------------------------------------------------------------------------------------*\
 * IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS IKANOS
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MACH_FUSIV)

#define default_ikan_lan_xtal    25000000
#define default_ikan_vdsl_xtal   16328000


enum _avm_clock_id {
    avm_clock_id_non        =  0x00,
    avm_clock_id_cpu        =  0x01,  /*--- default: 500.000 MHz ---*/
    avm_clock_id_system     =  0x02,  /*--- default: 166.000 MHz ---*/
    avm_clock_id_usb        =  0x04,  /*--- default:  12.000 MHz ---*/
    avm_clock_id_usb2       =  0x08,  /*--- default: 480.000 MHz ---*/
    avm_clock_id_21xx       =  0x10,  /*--- default: 100.000 MHz ??? ---*/
    avm_clock_id_ddr        =  0x20,  /*--- default: 333.000 MHz ---*/
    avm_clock_id_ephy       =  0x80,  /*--- default: 125.000 MHz ---*/
    avm_clock_id_pci        = 0x100,  /*--- default:  33.000 MHz ---*/
    avm_clock_id_pci66      = 0x200,  /*--- default:  63.000 MHz ---*/
    avm_clock_id_ap         = 0x400,  /*--- default: 400.000 MHz ??? ---*/
    avm_clock_id_bme        = 0x800,  /*--- default: 400.000 MHz ---*/
    avm_clock_id_fd         = 0x1000,
};

#include <ikan_clk.h>

#define avm_get_clock               ikan_get_clock

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_ENV_ENTRY               256
#define FLASH_ENV_ENTRY_SIZE        64

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-fusiv/hw_gpio.h>
#include <ikan_gpio.h>

#define avm_gpio_init        ikan_gpio_init
#define avm_gpio_ctrl        ikan_gpio_ctrl
#define avm_gpio_request_irq ikan_gpio_request_irq
#define avm_gpio_enable_irq  ikan_gpio_enable_irq
#define avm_gpio_disable_irq ikan_gpio_disable_irq
#define avm_read_irq_gpio               ikan_read_irq_gpio
#define avm_gpio_set_delayed_irq_mode   ikan_gpio_set_delayed_irq_mode
#define avm_gpio_out_bit     ikan_gpio_out_bit
#define avm_gpio_in_bit      ikan_gpio_in_bit
#define avm_gpio_in_value    ikan_gpio_in_value
#define avm_gpio_set_bitmask ikan_gpio_set_bitmask

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mips-boards/ur8_power.h> ---*/

/*--- #define avm_power_down_init               ur8_power_down_init ---*/
/*--- #define avm_put_device_into_power_down    ur8_put_device_into_power_down ---*/
/*--- #define avm_take_device_out_of_power_down ur8_take_device_out_of_power_down ---*/
/*--- #define avm_register_power_down_gpio      ur8_register_power_down_gpio ---*/
/*--- #define avm_release_power_down_gpio       ur8_release_power_down_gpio ---*/
/*--- #define avm_power_down_status             ur8_power_down_status ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mach-ur8/hw_reset.h> ---*/
/*--- #include <asm/mips-boards/ur8_reset.h> ---*/

/*--- #define avm_reset_init                      ur8_reset_init ---*/
/*--- #define avm_take_device_out_of_reset        ur8_take_device_out_of_reset ---*/
/*--- #define avm_put_device_into_reset           ur8_put_device_into_reset ---*/
/*--- #define avm_reset_device                    ur8_reset_device ---*/
/*--- #define avm_reset_status                    ur8_reset_status ---*/
/*--- #define avm_register_reset_gpio               ur8_register_reset_gpio ---*/
/*--- #define avm_release_reset_gpio                ur8_release_reset_gpio ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #include <asm/mips-boards/ur8int.h> ---*/

/*--- #define avm_int_init                        ur8int_init ---*/
/*--- #define avm_int_set_type                    ur8int_set_type ---*/
/*--- #define avm_int_ctrl_irq_pacing_setup       ur8int_ctrl_irq_pacing_setup ---*/
/*--- #define avm_int_ctrl_irq_pacing_register    ur8int_ctrl_irq_pacing_register ---*/
/*--- #define avm_int_ctrl_irq_pacing_unregister  ur8int_ctrl_irq_pacing_unregister ---*/
/*--- #define avm_int_ctrl_irq_pacing_set         ur8int_ctrl_irq_pacing_set ---*/
/*--- #define avm_nsec_timer                      ur8_nsec_timer ---*/


#endif /*--- #if defined(CONFIG_MACH_FUSIV) ---*/

/*------------------------------------------------------------------------------------------*\
 * zusaetzlich folgende:
 *
 * 207 char	Compaq ProLiant health feature indicate
 * 220 char	Myricom Myrinet "GM" board
 * 224 char	A2232 serial card
 * 225 char	A2232 serial card (alternate devices)
 * 227 char	IBM 3270 terminal Unix tty access
 * 228 char	IBM 3270 terminal block-mode access
 * 229 char	IBM iSeries virtual console
 * 230 char	IBM iSeries virtual tape
 *
\*------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
#define avm_get_cycles()             get_cycles()
#define avm_get_cyclefreq()          (avm_get_clock(avm_clock_id_cpu) / 2 )
#define avm_cycles_cpuclock_depend() (1)

#endif /*--- #ifndef _mach_avm_h_ ---*/
