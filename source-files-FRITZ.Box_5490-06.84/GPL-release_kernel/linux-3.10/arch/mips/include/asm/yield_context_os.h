/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   author: mbahr@avm.de
 *   description: yield-thread-interface mips34k
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef __yield_context_os_h__
#define __yield_context_os_h__

#include <linux/bug.h>
#include <asm/mipsmtregs.h>

#define YIELD_MAX_TC                                2
/*--- #define YIELD_MASK_LINUX_OS                       0x3        ---*//*--- ersten 2 Tc's fuer Linux-OS reserviert ---*/
/*--- #define YIELD_FIRST_TC                            0x2        ---*//*--- ersten 2 Tc's fuer Linux-OS reserviert ---*/

/*--------------------------------------------------------------------------------*\
 * performante Ermittlung ob im Linux-Kontext
\*--------------------------------------------------------------------------------*/
static inline int yield_is_linux_context(void){
/*--- 	unsigned int act_tc = (read_c0_tcbind() & TCBIND_CURTC) >> TCBIND_CURTC_SHIFT; ---*/
/*--- 	return ((1 << act_tc) & YIELD_MASK_LINUX_OS) ? 1 : 0; ---*/ /*--- 7 Assembler-Befehle ---*/
/*--- 	return act_tc < YIELD_FIRST_TC;  ---*//*--- 5 Assembler-Befehle ---*/
    return !test_thread_flag(TIF_YIELDCONTEXT); /*--- 3 Assembler-Befehle ---*/
}
/*--------------------------------------------------------------------------------*\
 * tell me if we in any yield instead linux context
 * ret: 0 no yield-context
 *      <any> - 1: thread-number
\*--------------------------------------------------------------------------------*/
extern int is_yield_context(void);


#define BUG_ON_YIELDTHREAD_NOT_ALLOWED() BUG_ON(!yield_is_linux_context())

#endif/*--- #ifndef __yield_context_debug_h__ ---*/
