/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _FUSIV_ALIGN_H
#define _FUSIV_ALIGN_H

#if (defined(CONFIG_FUSIV_VX185) || defined(CONFIG_FUSIV_VX585)) && defined(CONFIG_FUSIV_ALIGNMENT_FIXUPS)
#define RD16(x) (((unsigned long)*(unsigned short *)&(x) << 16) | (unsigned long)*((unsigned short *)&(x) + 1))
#define WR16(a, d) {*(unsigned short *)&(a) = (unsigned short)((d) >> 16); *((unsigned short *)&(a) + 1) = (unsigned short)(d);}
#define TCP_DOFF(h) (*((unsigned char *)h + 12) >> 4)
#define TCP_FIN(h) (*((unsigned char *)h + 13) & 1)
#define TCP_SYN(h) ((*((unsigned char *)h + 13) & 2) >> 1)
#define TCP_RST(h) ((*((unsigned char *)h + 13) & 4) >> 2)
#define TCP_ACK(h) ((*((unsigned char *)h + 13) & 0x10) >> 4)
#define TCP_URG(h) ((*((unsigned char *)h + 13) & 0x20) >> 5)
#define TCP_ECE(h) ((*((unsigned char *)h + 13) & 0x40) >> 6)
#define TCP_FLAGS(h) *((unsigned char *)h + 13)
#define TCP_FLAGS_WORD(h) (RD16(*((unsigned char *)h + 12)))

#define IP_IHL(h) (*(unsigned char *)h & 0xf)

#else

#define RD16(x) (x)
#define WR16(a, d) {a = d;}
#define TCP_DOFF(h) ((h)->doff)
#define TCP_FIN(h) ((h)->fin)
#define TCP_SYN(h) ((h)->syn)
#define TCP_RST(h) ((h)->rst)
#define TCP_ACK(h) ((h)->ack)
#define TCP_URG(h) ((h)->urg)
#define TCP_ECE(h) ((h)->ece)
#define IP_IHL(h) ((h)->ihl)

#endif

#endif
