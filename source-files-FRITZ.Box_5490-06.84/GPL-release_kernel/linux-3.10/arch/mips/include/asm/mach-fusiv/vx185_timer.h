/**
 * Copyright (C) 2010-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef __VX185_TIMER_H__
#define __VX185_TIMER_H__

#define VX185_TIMER_BASE_ADDR KSEG1ADDR(0xB9050000)

/* 32-bits wide regs */
struct timer_regs
{
	/* GPT configuration */
	u32 gpt0_top_cnt;
	u32 gpt0_ctrl;
	u32 gpt1_top_cnt;
	u32 gpt1_ctrl;
	u32 gpt2_top_cnt;
	u32 gpt2_ctrl;
	u32 gpt3_top_cnt;
	u32 gpt3_ctrl;
	u32 prescalar0;
	u32 prescalar1;

	/* WDT */
	u32 wdt_ctrl;
	u32 wdt_pet;
	u32 wdt_top_cnt;
	u32 wdt_value;
	
	/* GPT interrupt control and status */
	u32 gpt_int_ctrl;
	u32 gpt_int_status;
};

#endif
