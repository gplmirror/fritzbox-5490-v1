/*
 *
 * BRIEF MODULE DESCRIPTION
 *	Include file for Alchemy Semiconductor's Au1k CPU.
 *	modified for Ikanos's FUSIV SoCs.
 *
 * Copyright 2000,2001 MontaVista Software Inc.
 * Author: MontaVista Software, Inc.
 *         	ppopov@mvista.com or source@mvista.com
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  THIS  SOFTWARE  IS PROVIDED   ``AS  IS'' AND   ANY  EXPRESS OR IMPLIED
 *  WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 *  NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 *  USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  You should have received a copy of the  GNU General Public License along
 *  with this program; if not, write  to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

 /*
  * some definitions add by takuzo@sm.sony.co.jp and sato@sm.sony.co.jp
  */

#ifndef _FUSIV_USB_H_
#define _FUSIV_USB_H_

#include <generated/autoconf.h>

#include <linux/delay.h>
#include <asm/io.h>

/* Extension to EHCI registers added by Synopsis */
#define USB_EHCI_MICR_FR_BAS_VAL	((volatile uint *)(0xb9120090))
#define USB_EHCI_PKT_BUF_OI_THRSHLD	((volatile uint *)(0xb9120094))
#define USB_EHCI_PKT_BUF_DEPTH		((volatile uint *)(0xb9120098))
#define USB_EHCI_BRK_MEM_TRNSFR		((volatile uint *)(0xb912009c))
#define USB_EHCI_DBG			((volatile uint *)(0xb91200a0))
#define USB_EHCI_UTMI_CSR		((volatile uint *)(0xb91200a4))

#define FUSIV_USB_EHCI_ON	0x1
#define FUSIV_USB_OHCI_ON	0x2

#if defined(CONFIG_FUSIV_VX185) || defined(CONFIG_FUSIV_VX585)

/* USB PHY/UTMI specific registers in VX185 */
#define USB_GNL_REG_1 			((volatile uint *)(0xb9128000))
#define USB_GNL_REG_2 			((volatile uint *)(0xb9128004))
#define USB_GNL_REG_3 			((volatile uint *)(0xb9128008))
#define USB_GNL_REG_4 			((volatile uint *)(0xb912800C))
#define USB_GNL_REG_5 			((volatile uint *)(0xb9128010))
#define USB_GNL_STS 			((volatile uint *)(0xb9128014))
#define USB_GNL_REG_6 			((volatile uint *)(0xb9128018))

/* IRQ for USB host controller on VX585 */
#define FUSIV_USB_XHCI_HOST_INT  57

/* IRQ for USB host controller on VX185 */
#if defined (CONFIG_CPU_MIPSR2_IRQ_VI)
#define FUSIV_USB_OHCI_HOST_INT  FUSIV_MIPS_INT_USBO
#define FUSIV_USB_EHCI_HOST_INT  FUSIV_MIPS_INT_USBE
#else
#define FUSIV_USB_OHCI_HOST_INT  34
#define FUSIV_USB_EHCI_HOST_INT  35
#endif

/* Physical Base address of OHCI operational registers in VX185 */
#define FUSIV_USB_OHCI_BASE             0x19110000
/* Total size in bytes of all OHCI operational registers in VX185 */
#define FUSIV_USB_OHCI_LEN              0x0000005C

/* Physical Base address of EHCI capability  registers in VX185 */
/* The location of EHCI operational registers is derived from ehci caplen reg */
#define FUSIV_USB_EHCI_BASE             0x19120000

/* Total size in bytes of all EHCI capabilty registers in VX185 */
#define FUSIV_USB_EHCI_LEN              0x0000005C

/* Physical Base address of XHCI capability  registers in ikf75xx */
/* The location of XHCI operational registers is derived from xhci caplen reg */
#define FUSIV_USB_XHCI_BASE             0x19800000


/* Total size in bytes of all XHCI capabilty registers in ikf75xx */
#define FUSIV_USB_XHCI_LEN              0x00000430

#elif defined(CONFIG_FUSIV_VX180)

/* IRQ for USB host controller on IKF68XX */
#define FUSIV_USB_EHCI_HOST_INT  35
#define FUSIV_USB_OHCI_HOST_INT  35

/* Physical Base address of OHCI operational registers in ik68xx */
#define FUSIV_USB_OHCI_BASE             0x19240800
/* Total size in bytes of all OHCI operational registers in ik68xx */
#define FUSIV_USB_OHCI_LEN              0x0000005C

/* Physical Base address of EHCI capability  registers in ik68xx */
/* The location of EHCI operational registers is derived from ehci caplen reg */
#define FUSIV_USB_EHCI_BASE             0x19230000

/* Total size in bytes of all EHCI capabilty registers in ik68xx */
#define FUSIV_USB_EHCI_LEN              0x0000005C

/* Physical Base address of XHCI capability  registers in ikf75xx */
/* The location of XHCI operational registers is derived from xhci caplen reg */
#define FUSIV_USB_XHCI_BASE             0x19800000


/* Total size in bytes of all XHCI capabilty registers in ikf75xx */
#define FUSIV_USB_XHCI_LEN              0x00000430

#else
#error Fusiv processor undefined
#endif

#endif
