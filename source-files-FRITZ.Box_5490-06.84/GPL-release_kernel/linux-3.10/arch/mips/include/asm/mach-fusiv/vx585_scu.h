/**
 * Copyright (C) 2013-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 *
 * Info :  IKF75XX SCU Definitions
 */

#ifndef _IKF75XX_SCU_DEF
#define _IKF75XX_SCU_DEF

typedef struct scu_reg_s {

	volatile unsigned int 	analog_ip_write_protect; 	 			//0x0000
	volatile unsigned int 	refclk_plldll_program; 	 				//0x0004
	volatile unsigned int 	plldllx2_program; 	 					//0x0008
	volatile unsigned int 	hadc_program; 	 						//0x000c
	volatile unsigned int 	refclk_plldll_control_and_status; 		//0x0010
	volatile unsigned int 	refclk_plldll_pll0_phase_rotator_con; 	//0x0014
	volatile unsigned int 	reserved_00; 	 						//0x0018
	volatile unsigned int 	refclk_plldll_test_control; 	 		//0x001c
	volatile unsigned int 	plldllx2_control_and_status; 	 		//0x0020
	volatile unsigned int 	plldllx2_pll0_phase_rotator_control; 	//0x0024
	volatile unsigned int 	plldllx2_pll1_phase_rotator_control; 	//0x0028
	volatile unsigned int 	plldll_test_control; 	 				//0x002c
	volatile unsigned int 	swreg_control_status[4]; 	 			//0x0030 - 0x003c
	volatile unsigned int 	por_control_status; 	 				//0x0040
	volatile unsigned int 	reserved_01; 							//0x0044
	volatile unsigned int 	reserved_02; 	 						//0x0048
	volatile unsigned int 	reserved_03; 	 						//0x004c
	volatile unsigned int 	hadc_control[2]; 	 					//0x0050 - 0x0054
	volatile unsigned int 	hadc_status; 	 						//0x0058
	volatile unsigned int 	hadc_test_control; 	 					//0x005c
	volatile unsigned int 	reserved_04; 	 						//0x0060
	volatile unsigned int 	reserved_05; 	 						//0x0064
	volatile unsigned int 	reserved_06; 	 						//0x0068
	volatile unsigned int 	reserved_07; 	 						//0x006c
	volatile unsigned int 	serdes_control[2]; 	 					//0x0070 - 0x0074
	volatile unsigned int 	usb2_phy_control; 	 					//0x0078
	volatile unsigned int 	reserved_08; 	 						//0x007c
	volatile unsigned int 	base_address_control; 	 				//0x0080
	volatile unsigned int 	reserved_09; 							//0x0084
	volatile unsigned int 	reserved_10; 	 						//0x0088
	volatile unsigned int 	reserved_11; 	 						//0x008c
	volatile unsigned int 	ring_oscillator_voltage_adjust_contr; 	//0x0090
	volatile unsigned int 	usb2_phy_abist_control; 	 			//0x0094
	volatile unsigned int 	reserved_12; 	 						//0x0098
	volatile unsigned int 	reserved_13; 	 						//0x009c
	volatile unsigned int 	usb2_phy_configuration[2]; 	 			//0x00a0 - 0x00a4
	volatile unsigned int 	reserved_14; 	 						//0x00a8
	volatile unsigned int 	reserved_15[21];  						//0x00ac - 0x00fc
	volatile unsigned int 	clock_gate_control_0_mask; 	 			//0x0100
	volatile unsigned int 	clock_gate_control_0; 	 				//0x0104
	volatile unsigned int 	clock_gate_control_1_mask; 	 			//0x0108
	volatile unsigned int 	clock_gate_control_1; 	 				//0x010c
	volatile unsigned int 	clock_gate_control_2_mask; 	 			//0x0110
	volatile unsigned int 	clock_gate_control_2; 	 				//0x0114
	volatile unsigned int 	reserved_16; 	 						//0x0118
	volatile unsigned int 	reserved_17; 	 						//0x011c
	volatile unsigned int 	clock_control_write_protect; 	 		//0x0120
	volatile unsigned int 	clock_control[6]; 	 					//0x0124 - 0x013c
	volatile unsigned int 	rrm_write_protect; 	 					//0x0140
	volatile unsigned int 	apu_ahb_rrm_control; 	 				//0x0144
	volatile unsigned int 	hmips_rrm_control; 	 					//0x0148
	volatile unsigned int 	ddr_rrm_control; 	 					//0x014c
	volatile unsigned int 	bme_rrm_control; 	 					//0x0150
	volatile unsigned int 	bmips_rrm_control; 	 					//0x0154
	volatile unsigned int 	bme_rrm_control_1; 	 					//0x0158
	volatile unsigned int 	reserved_18[10]; 	 					//0x015c
	volatile unsigned int 	timing_recovery_dll_control; 	 		//0x0180
	volatile unsigned int 	timing_recovery_delta_frequency; 		//0x0184
	volatile unsigned int 	timing_recovery_frequency_offset; 		//0x0188
	volatile unsigned int 	reserved_19; 	 						//0x018c
	volatile unsigned int 	ddr_ssc_control; 	 					//0x0190
	volatile unsigned int 	com_ssc_control; 	 					//0x0194
	volatile unsigned int 	reserved_20; 	 						//0x0198
	volatile unsigned int 	reserved_21[25];  						//0x019c
	volatile unsigned int 	reset_control_0_mask; 	 				//0x0200
	volatile unsigned int 	reset_control_0; 	 					//0x0204
	volatile unsigned int 	reset_control_1_mask; 	 				//0x0208
	volatile unsigned int 	reset_control_1; 	 					//0x020c
	volatile unsigned int 	reset_control_2_mask; 	 				//0x0210
	volatile unsigned int 	reset_control_2; 	 					//0x0214
	volatile unsigned int 	rst_cause;	 	 						//0x0208
	volatile unsigned int 	reserved_22; 	 						//0x021c
	volatile unsigned int 	latch_on_reset[3]; 	 					//0x0220 - 0x0228
	volatile unsigned int 	reserved_23[5];  						//0x022c
	volatile unsigned int 	otp_user_general_purpose[4]; 	 		//0x0240 - 0x024c
	volatile unsigned int 	otp_additional_configuration; 	 		//0x0250
	volatile unsigned int 	otp_internal_general_purpose[2]; 		//0x0254 - 0x0258
	volatile unsigned int 	reserved_24[41];  						//0x025c - 0x02fc
	volatile unsigned int 	ecid[4]; 	 							//0x0300 - 0x030c
	volatile unsigned int 	revision_id[2]; 	 					//0x0310 - 0x0314
	volatile unsigned int 	chip_mode[2]; 	 						//0x0318 - 0x031c
	volatile unsigned int 	keys_read_protect; 	 					//0x0320
	volatile unsigned int 	reserved_25; 	 						//0x0324
	volatile unsigned int 	reserved_26; 	 						//0x0328
	volatile unsigned int 	reserved_27; 	 						//0x032c
	volatile unsigned int 	chip_control_mask; 	 					//0x0330
	volatile unsigned int 	chip_control; 	 						//0x0334
	volatile unsigned int 	reserved_28; 	 						//0x0338
	volatile unsigned int 	reserved_29; 	 						//0x033c
	volatile unsigned int 	hmips_control[2]; 	 					//0x0340 - 0x0344
	volatile unsigned int 	hmips_status[2]; 	 					//0x0348 - 0x034c
	volatile unsigned int 	hmips_axi_error_control_and_status_0; 	//0x0350
	volatile unsigned int 	hmips_axi_error_address_0; 	 			//0x0354
	volatile unsigned int 	hmips_axi_error_control_and_status_1; 	//0x0358
	volatile unsigned int 	hmips_axi_error_address_1; 	 			//0x035c
	volatile unsigned int 	reserved_30; 	 						//0x0360
	volatile unsigned int 	reserved_31; 	 						//0x0364
	volatile unsigned int 	reserved_32; 	 						//0x0368
	volatile unsigned int 	reserved_33; 	 						//0x036c
	volatile unsigned int 	bme_mask0;	 	 						//0x0370
	volatile unsigned int 	bme_map0; 		 						//0x0374
	volatile unsigned int 	bme_mask1;	 	 						//0x0378
	volatile unsigned int 	bme_map1;	 	 						//0x037c
	volatile unsigned int 	bmips_axi_error_control_and_status_0; 	//0x0380
	volatile unsigned int 	bmips_axi_error_address_0; 	 			//0x0384
	volatile unsigned int 	bmips_axi_error_control_and_status_1; 	//0x0388
	volatile unsigned int 	bmips_axi_error_address_1; 	 			//0x038c
	volatile unsigned int 	bmips_exceptional_base; 	 			//0x0390
	volatile unsigned int 	reserved_34; 	 						//0x0394
	volatile unsigned int 	reserved_35; 	 						//0x0398
	volatile unsigned int 	reserved_36; 	 						//0x039c
	volatile unsigned int 	gige_control; 	 						//0x03a0
	volatile unsigned int 	nand_status; 	 						//0x03a4
	volatile unsigned int 	rslic_control_and_status; 	 			//0x03a8
	volatile unsigned int 	pcie_control; 	 						//0x03ac
	volatile unsigned int 	classifier_buffer_control; 	 			//0x03b0
	volatile unsigned int 	ft_pad_control; 	 					//0x03b4
	volatile unsigned int 	reserved_37; 	 						//0x03b8
	volatile unsigned int 	reserved_38; 	 						//0x03bc
	volatile unsigned int 	reserved_39; 	 						//0x03c0
	volatile unsigned int 	reserved_40; 	 						//0x03c4
	volatile unsigned int 	reserved_41; 	 						//0x03c8
	volatile unsigned int 	noc_timeout_control; 	 				//0x03cc
	volatile unsigned int 	noc_control_status[4]; 					//0x03d0 - 0x03dc
	volatile unsigned int 	qspi_boot_control[6]; 	 				//0x03e0 - 0x03f4
	volatile unsigned int 	qspi_control_status; 	 				//0x03f8
	volatile unsigned int 	reserved_42; 	 						//0x03fc
	volatile unsigned int 	aes_decryption_key[4]; 	 				//0x0400 - 0x040c
	volatile unsigned int 	reserved_43; 	 						//0x0410
	volatile unsigned int 	reserved_44; 	 						//0x0414
	volatile unsigned int 	reserved_45; 	 						//0x0418
	volatile unsigned int 	reserved_46; 	 						//0x041c
	volatile unsigned int 	public_hash_0_key[8]; 	 				//0x0420 - 0x043c
	volatile unsigned int	reserved_46_1[48];						//0x0440 - 0x04fc
	volatile unsigned int 	tod_control_and_status; 	 			//0x0500
	volatile unsigned int 	tod_co_phase; 	 						//0x0504
	volatile unsigned int 	reserved_47; 	 						//0x0508
	volatile unsigned int 	reserved_48; 	 						//0x050c
	volatile unsigned int 	tod_loop_filter_control; 	 			//0x0510
	volatile unsigned int 	tod_timer_rate_configuration; 	 		//0x0514
	volatile unsigned int 	tod_loop_filter_deltaf_seed; 	 		//0x0518
	volatile unsigned int 	tod_loop_filter_deltaf_read; 	 		//0x051c
	volatile unsigned int 	tod_timer_set; 	 						//0x0520
	volatile unsigned int 	tod_phase_count; 	 					//0x0524
	volatile unsigned int 	tod_timer_out; 	 						//0x0528
	volatile unsigned int 	reserved_49; 	 						//0x052c
	volatile unsigned int 	tod_time_stamp[3]; 	 					//0x0530 - 0x0538
	volatile unsigned int 	tod_time_stamp_reserved; 	 			//0x053c
	volatile unsigned int 	ntr_control_and_status; 	 			//0x0540
	volatile unsigned int 	ntr_value; 	 							//0x0544
	volatile unsigned int 	reserved_50; 	 						//0x0548
	volatile unsigned int 	reserved_51; 	 						//0x054c
	volatile unsigned int 	ntr_loop_filter_control; 	 			//0x0550
	volatile unsigned int 	ntr_loop_filter_coefficient; 	 		//0x0554
	volatile unsigned int 	ntr_loop_filter_deltaf_seed; 	 		//0x0558
	volatile unsigned int 	ntr_loop_filter_deltaf_read; 	 		//0x055c
	volatile unsigned int	reserved_51_1[40];						//0x0560 - 0x05fc
	volatile unsigned int 	pll_drift_control_reset; 	 			//0x0600
	volatile unsigned int 	rck_plldll_drift_wait_reset; 	 		//0x0604
	volatile unsigned int 	plldll_pll0_drift_wait_reset; 	 		//0x0608
	volatile unsigned int 	plldll_pll1_drift_wait_reset; 	 		//0x060c
	volatile unsigned int 	pll_drift_control_interrupt; 	 		//0x0610
	volatile unsigned int 	rck_plldll_drift_wait_interrupt; 		//0x0614
	volatile unsigned int 	plldll_pll0_drift_wait_interrupt; 		//0x0618
	volatile unsigned int 	plldll_pll1_drift_wait_interrupt; 		//0x061c
	volatile unsigned int 	usb_control_and_status[7]; 	 			//0x0620 - 0x0638
	volatile unsigned int 	reserved_52[49];  						//0x063c - 0x06fc
	volatile unsigned int 	h2b_mb_int_set;				 	 		//0x0700
	volatile unsigned int 	h2b_mb_int_clr; 						//0x0704
	volatile unsigned int 	h2b_mb_int_en; 							//0x0708
	volatile unsigned int 	reserved_53; 	 						//0x070c
	volatile unsigned int 	h2b_mb[8]; 	 							//0x0710 - 0x72c
	volatile unsigned int 	b2h_mb_int_set; 	 					//0x0730
	volatile unsigned int 	b2h_mb_int_clr;					 		//0x0734
	volatile unsigned int 	b2h_mb_int_en; 							//0x0738
	volatile unsigned int 	reserved_54; 	 						//0x073c
	volatile unsigned int 	b2h_mb[8]; 	 							//0x0740 - 0x075c
	volatile unsigned int 	hmips_scu_interrupt_enable; 			//0x0760
	volatile unsigned int 	hmips_scu_interrupt; 	 				//0x0764
	volatile unsigned int 	hmips_mips_interrupt_enable; 	 		//0x0768
	volatile unsigned int 	hmips_mips_interrupt; 	 				//0x076c
	volatile unsigned int 	hmips_miscellaneous_interrupt_enable; 	//0x0770
	volatile unsigned int 	hmips_miscellaneous_interrupt; 	 		//0x0774
	volatile unsigned int 	hmips_noc_slave_error_interrupt_enab; 	//0x0778
	volatile unsigned int 	hmips_noc_slave_error_interrupt; 		//0x077c
	volatile unsigned int 	hmips_noc_decode_error_interrupt_ena; 	//0x0780
	volatile unsigned int 	hmips_noc_decode_error_interrupt; 		//0x0784
	volatile unsigned int 	reserved_55; 	 						//0x0788
	volatile unsigned int 	reserved_56; 	 						//0x078c
	volatile unsigned int 	bmips_scu_interrupt_enable;  			//0x0790
	volatile unsigned int 	bmips_scu_interrupt; 					//0x0794
	volatile unsigned int 	bmips_mips_interrupt_enable; 	 		//0x0798
	volatile unsigned int 	bmips_mips_interrupt; 	 				//0x079c
	volatile unsigned int 	bmips_miscellaneous_interrupt_enable; 	//0x07a0
	volatile unsigned int 	bmips_miscellaneous_interrupt; 	 		//0x07a4
	volatile unsigned int 	bmips_noc_slave_error_interrupt_enab; 	//0x07a8
	volatile unsigned int 	bmips_noc_slave_error_interrupt; 		//0x07ac
	volatile unsigned int 	bmips_noc_decode_error_interrupt_ena; 	//0x07b0
	volatile unsigned int 	bmips_noc_decode_error_interrupt; 		//0x07b4
	volatile unsigned int 	reserved_57; 	 						//0x07b8
	volatile unsigned int 	reserved_58[17]; 	 					//0x07bc - 0x07fc
	volatile unsigned int 	debug_control_write_protect; 	 		//0x0800
	volatile unsigned int 	apu_debug_control; 	 					//0x0804
	volatile unsigned int 	tod_pad_control; 	 					//0x0808
	volatile unsigned int 	eth_phy_pad_control; 	 				//0x080c
	volatile unsigned int 	dsl_debug_control; 	 					//0x0810
	volatile unsigned int 	control_and_observe_clocks_pad_contr; 	//0x0814
	volatile unsigned int 	hmips_ejtag_control; 	 				//0x0818
	volatile unsigned int 	reserved_59; 	 						//0x081c
	volatile unsigned int 	reserved_60; 	 						//0x0820
	volatile unsigned int 	reserved_61; 	 						//0x0824
	volatile unsigned int 	reserved_62; 	 						//0x0828
	volatile unsigned int 	reserved_63; 	 						//0x082c
	volatile unsigned int 	reserved_64; 	 						//0x0830
	volatile unsigned int 	reserved_65; 	 						//0x0834
	volatile unsigned int 	reserved_66; 	 						//0x0838
	volatile unsigned int 	reserved_67; 	 						//0x083c
	volatile unsigned int 	opt_lor_debug[2]; 	 					//0x0840 - 0x0844
	volatile unsigned int 	opt_qspi_debug[4]; 	 					//0x0848 - 0x0854
	volatile unsigned int 	opt_additional_config_debug[2]; 		//0x0858 - 0x085c
	volatile unsigned int 	bond_debug; 	 						//0x0860
	volatile unsigned int 	lor_debug[3]; 	 						//0x0864 - 0x086c
	volatile unsigned int 	otp_hadc_debug[2]; 	 					//0x0870 - 0x0874
	volatile unsigned int 	reserved_68; 	 						//0x0878
	volatile unsigned int 	reserved_69; 	 						//0x087c
	volatile unsigned int 	test_clock_select; 	 					//0x0860
	volatile unsigned int 	extended_test_clock_select; 	 		//0x0884
	volatile unsigned int 	reserved_70; 	 						//0x0888
	volatile unsigned int 	reserved_71; 	 						//0x088c
	volatile unsigned int 	frequency_measurement_control; 	 		//0x0890
	volatile unsigned int 	frequency_measurement_counter[8]; 		//0x0894 - 0x08b0
	volatile unsigned int 	reserved_72; 	 						//0x08b4
	volatile unsigned int 	reserved_73; 	 						//0x08b8
	volatile unsigned int 	reserved_74[17];  						//0x08bc - 0x08fc
	volatile unsigned int 	mbist_control; 	 						//0x0900
	volatile unsigned int 	mbist_power_down; 	 					//0x0904
	volatile unsigned int 	mbist_safe_reconfiguration_register; 	//0x0908
	volatile unsigned int 	mbist_sms_ready; 	 					//0x090c
	volatile unsigned int 	mbist_sms_fail; 	 					//0x0910
	volatile unsigned int 	mbist_sms_current_error; 	 			//0x0914
	volatile unsigned int 	mbist_udr; 	 							//0x0918
	volatile unsigned int 	reserved_75; 	 						//0x091c
	volatile unsigned int 	otp_control; 	 						//0x0920
	volatile unsigned int 	reserved_76; 	 						//0x0924
	volatile unsigned int 	reserved_77; 	 						//0x0928
	volatile unsigned int 	reserved_78[181]; 						//0x092c - 0x0bfc
	volatile unsigned int 	software_revocation_control; 	 		//0x0c00
	volatile unsigned int 	reserved_79; 	 						//0x0c04
	volatile unsigned int 	reserved_80; 	 						//0x0c08
	volatile unsigned int 	reserved_81; 	 						//0x0c0c
	volatile unsigned int 	scratch_register[8]; 	 				//0x0c10 - 0x0c2c
	volatile unsigned int	reserved_82;							//0x0c30
	volatile unsigned int	reserved_83;							//0x0c34
	volatile unsigned int	reserved_84;							//0x0c38
	volatile unsigned int	reserved_85;							//0x0c3c
	volatile unsigned int 	spare_output_register[3]; 	 			//0x0c40 - 0x0c48
	volatile unsigned int 	reserved_90; 	 						//0x0c4c
	volatile unsigned int 	spare_input_register[3]; 	 			//0x0c50 - 0x0c58
	volatile unsigned int 	reserved_91; 	 						//0x0c5c
 
}scu_reg_t;


#define SCU_BASE_ADDRESS	0xb9000000

#define scu_regs ((scu_reg_t *) SCU_BASE_ADDRESS)

/* Bit definitions for above register 0xb9000010 */
#define REFCLK_PLL0_DIGTUNE_FAILFAST		19 //RO
#define REFCLK_PLL0_DIGTUNE_FAILSLOW		18 //RO
#define REFCLK_PLL0_DIGTUNE_OK				17 //RO
#define REFCLK_PLL0_LOSS_OF_LOCK			16 //RO
#define REFCLK_RESETN						15 //RW

/* Bit definitions for above register 0xb9000020 */
#define PLLDLLX2_PLL1_DIGTUNE_FAILFAST		27 //RO
#define PLLDLLX2_PLL1_DIGTUNE_FAILSLOW		26 //RO
#define PLLDLLX2_PLL1_DIGTUNE_OK			25 //RO
#define PLLDLLX2_PLL1_LOSS_OF_LOCK			24 //RO
#define PLLDLLX2_PLL0_DIGTUNE_FAILFAST		19 //RO
#define PLLDLLX2_PLL0_DIGTUNE_FAILSLOW		18 //RO
#define PLLDLLX2_PLL0_DIGTUNE_OK			17 //RO
#define PLLDLLX2_PLL0_LOSS_OF_LOCK			16 //RO
#define PLLDLLX2_RESETN						15 //RW

/*=============================================================
*    Bit definitions for Reset Control 1 0xb900020c
*=============================================================*/

//#define	Reserved						31-22
#define	CLASS_23_APU_RSTN					21
#define	CLASS_23_AHB_RSTN					20
//#define	Reserved						19-18
#define	CLASS_01_APU_RSTN					17
#define	CLASS_01_AHB_RSTN					16
#define	SPU_APU_RSTN						15
#define	SPU_AHB_RSTN						14
#define	BMU_APU_RSTN						13
#define	BMU_AHB_RSTN						12
#define	HAP_APU_RSTN						11
#define	HAP_AHB_RSTN						10
#define	VAP_APU_RSTN						9
#define	VAP_AHB_RSTN						8
//#define	Reserved						7
#define	GIGE2_APU_RSTN						5
#define	GIGE2_AHB_RSTN						4
#define	GIGE1_APU_RSTN						3
#define	GIGE1_AHB_RSTN						2
#define	GIGE0_APU_RSTN						1
#define	GIGE0_AHB_RSTN						0


/*=============================================================
*    Bit definitions for Reset Control 2 0xb9000210
*=============================================================*/

#define RSTN_OUT                            31
#define SELF_RSTN                           30
/* Bits 29-24  Reserved*/
#define PLLDLLX2_PLL1_DRIFT_ RST_DIS        23
#define PLLDLLX2_PLL0_DRIFT_ RST_DIS        22
#define RCK_PLLDLL_DRIFT_ RST_DIS           21
#define SYS_WDOG_SYS_RST_DIS                20
#define HMIPS_WDOG_SYS_RST_DIS              12  //19-12 bits
/* Bit 11 Reserved */
#define SW_HMIPS_PWR_RSTN                   10
#define SW_HMIPS_RSTN                       9
#define SYS_WDOG_HMIPS_RST_DIS              8
#define HMIPS_WDOG_HMIPS_RST_DIS            0   //7-0 bits

/*=============================================================
*    Bit definitions for clock gate Control0 Mask
*    SCU Register address - 0xb900_0100
*=============================================================*/
#define PCIE1_SLAVE_AHB_CLK_GATE_MASK       31
#define PCIE1_MASTER_AHB_CLK_GATE_MASK      30
#define PCIE1_AUX_CLK_GATE_MASK             29
#define PCIE1_AHB_CLK_GATE_MASK             28
#define PCIE0_SLAVE_AHB_CLK_GATE_MASK       27
#define PCIE0_MASTER_AHB_CLK_GATE_MASK      26
#define PCIE0_AUX_CLK_GATE_MASK             25
#define PCIE0_AHB_CLK_GATE_MASK             24
//#define Reserved                          23-21
#define USB_AHB_CLK_GATE_MASK               20
//#define Reserved                          19-17
#define SATA_AHB_CLK_GATE_MASK              16
//#define Reserved                          15  
#define AFE_TEST_CLK_GATE_MASK              14
#define TEST_60M_CLK_GATE_MASK              13
#define TEST_100M_CLK_GATE_MASK             12
#define TEST_250M_CLK_GATE_MASK             11
#define TEST_300M_CLK_GATE_MASK             10
#define DSP_AFE_CLK_GATE_MASK               9
#define QSPI_AHB_CLK_GATE_MASK              8
#define USB2_PHY_CLK_GATE_MASK              7
#define NTR_CLK_GATE_MASK                   6
#define DPS_CLK_GATE_MASK                   5
#define ETH_50M_CLK_GATE_MASK               4
#define ETH_125M_CLK_GATE_MASK              3
#define HMIPS_CLK_GATE_MASK                 2
#define DDR_CLK_GATE_MASK                   1
#define APU_CLK_GATE_MASK                   0
        
/*=============================================================
*    Bit definitions for clock gate Control0 
*    SCU Register address - 0xb900_0104
*=============================================================*/
#define PCIE1_SLAVE_AHB_CLK_GATE        31
#define PCIE1_MASTER_AHB_CLK_GATE       30
#define PCIE1_AUX_CLK_GATE              29
#define PCIE1_AHB_CLK_GATE              28
#define PCIE0_SLAVE_AHB_CLK_GATE        27
#define PCIE0_MASTER_AHB_CLK_GATE       26
#define PCIE0_AUX_CLK_GATE              25
#define PCIE0_AHB_CLK_GATE              24
//#define Reserved                      23-21
#define USB_AHB_CLK_GATE                20
//#define Reserved                      19-17
#define SATA_AHB_CLK_GATE               16
//#define Reserved                      15
#define AFE_TEST_CLK_GATE               14
#define TEST_60M_CLK_GATE               13
#define TEST_100M_CLK_GATE              12
#define TEST_250M_CLK_GATE              11
#define TEST_300M_CLK_GATE              10
#define DSP_AFE_CLK_GATE                9
#define QSPI_AHB_CLK_GATE               8
#define USB2_PHY_CLK_GATE               7
#define NTR_CLK_GATE                    6
#define DPS_CLK_GATE                    5
#define ETH_50M_CLK_GATE                4
#define ETH_125M_CLK_GATE               3
#define HMIPS_CLK_GATE                  2
#define DDR_CLK_GATE                    1
#define APU_CLK_GATE                    0
        
/*=============================================================
*    Bit definitions for clock gate Control1 mask
*    SCU Register address - 0xb900_0108
*=============================================================*/

//#define   Reserved                    31-24
#define ETH_PHY2_CLK_GATE_MASK          26
#define ETH_PHY1_CLK_GATE_MASK          25
#define ETH_PHY0_CLK_GATE_MASK          24
#define GIGE2_RMII_CLK_GATE_MASK        23
#define GIGE2_MAC_2P5M_CLK_GATE_MASK    22
#define GIGE2_MAC_25M_CLK_GATE_MASK     21
#define GIGE2_MAC_125M_CLK_GATE_MASK    20
//#define   Reserved                    19
#define GIGE2_MDC_CLK_GATE_MASK         18
#define GIGE2_APU_CLK_GATE_MASK         17
#define GIGE2_AHB_CLK_GATE_MASK         16
#define GIGE1_RMII_CLK_GATE_MASK        15
#define GIGE1_MAC_2P5M_CLK_GATE_MASK    14
#define GIGE1_MAC_25M_CLK_GATE_MASK     13
#define GIGE1_MAC_125M_CLK_GATE_MASK    12
//#define   Reserved                    11
#define GIGE1_MDC_CLK_GATE_MASK         10
#define GIGE1_APU_CLK_GATE_MASK         9
#define GIGE1_AHB_CLK_GATE_MASK         8
#define GIGE0_RMII_CLK_GATE_MASK        7
#define GIGE0_MAC_2P5M_CLK_GATE_MASK    6
#define GIGE0_MAC_25M_CLK_GATE_MASK     5
#define GIGE0_MAC_125M_CLK_GATE_MASK    4
//#define   Reserved                    3
#define GIGE0_MDC_CLK_GATE_MASK         2
#define GIGE0_APU_CLK_GATE_MASK         1
#define GIGE0_AHB_CLK_GATE_MASK         0
        
/*=============================================================
*    Bit definitions for clock gate Control1
*    SCU Register address - 0xb900_010C
*=============================================================*/

//#define   Reserved                    31
#define ETH_PHY2_CLK_GATE               26
#define ETH_PHY1_CLK_GATE               25
#define ETH_PHY0_CLK_GATE               24
#define GIGE2_RMII_CLK_GATE             23
#define GIGE2_MAC_2P5M_CLK_GATE         22
#define GIGE2_MAC_25M_CLK_GATE          21
#define GIGE2_MAC_125M_CLK_GATE         20
//#define   Reserved                    19
#define GIGE2_MDC_CLK_GATE              18
#define GIGE2_APU_CLK_GATE              17
#define GIGE2_AHB_CLK_GATE              16
#define GIGE1_RMII_CLK_GATE             15
#define GIGE1_MAC_2P5M_CLK_GATE         14
#define GIGE1_MAC_25M_CLK_GATE          13
#define GIGE1_MAC_125M_CLK_GATE         12
//#define   Reserved                    11
#define GIGE1_MDC_CLK_GATE              10
#define GIGE1_APU_CLK_GATE              9
#define GIGE1_AHB_CLK_GATE              8
#define GIGE0_RMII_CLK_GATE             7
#define GIGE0_MAC_2P5M_CLK_GATE         6
#define GIGE0_MAC_25M_CLK_GATE          5
#define GIGE0_MAC_125M_CLK_GATE         4
//#define   Reserved                    3
#define GIGE0_MDC_CLK_GATE              2
#define GIGE0_APU_CLK_GATE              1
#define GIGE0_AHB_CLK_GATE              0

/*=============================================================
*    Bit definitions for clock gate Control2 mask
*    SCU Register address - 0xb900_0110
*=============================================================*/

//#define   Reserved                    31-24
#define CLA3_APU_CLK_GATE_MASK          23
#define CLA3_AHB_CLK_GATE_MASK          22
#define CLA2_APU_CLK_GATE_MASK          21
#define CLA2_AHB_CLK_GATE_MASK          20
#define CLA1_APU_CLK_GATE_MASK          19
#define CLA1_AHB_CLK_GATE_MASK          18
#define CLA0_APU_CLK_GATE_MASK          17
#define CLA0_AHB_CLK_GATE_MASK          16
#define SPA_APU_CLK_GATE_MASK           15
#define SPA_AHB_CLK_GATE_MASK           14
#define BMU_APU_CLK_GATE_MASK           13
#define BMU_AHB_CLK_GATE_MASK           12
#define HAP_APU_CLK_GATE_MASK           11
#define HAP_AHB_CLK_GATE_MASK           10
#define VAP_APU_CLK_GATE_MASK           9
#define VAP_AHB_CLK_GATE_MASK           8
#define VAP_RMII_CLK_GATE_MASK          7
#define VAP_MAC_2P5M_CLK_GATE_MASK      6
#define VAP_MAC_25M_CLK_GATE_MASK       5
#define VAP_MAC_125M_CLK_GATE_MASK      4
//#define   Reserved                    3
#define BMIPS_CLK_GATE_MASK             2
#define BME_CLK_GATE_MASK               1
#define AFE_CLK_GATE_MASK               0

/*=============================================================
*    Bit definitions for clock gate Control2
*    SCU Register address - 0xb900_0114
*=============================================================*/

//#define   Reserved                    31-24
#define CLA3_APU_CLK_GATE               23
#define CLA3_AHB_CLK_GATE               22
#define CLA2_APU_CLK_GATE               21
#define CLA2_AHB_CLK_GATE               20
#define CLA1_APU_CLK_GATE               19
#define CLA1_AHB_CLK_GATE               18
#define CLA0_APU_CLK_GATE               17
#define CLA0_AHB_CLK_GATE               16
#define SPA_APU_CLK_GATE                15
#define SPA_AHB_CLK_GATE                14
#define BMU_APU_CLK_GATE                13
#define BMU_AHB_CLK_GATE                12
#define HAP_APU_CLK_GATE                11
#define HAP_AHB_CLK_GATE                10
#define VAP_APU_CLK_GATE                9
#define VAP_AHB_CLK_GATE                8
#define VAP_RMII_CLK_GATE               7
#define VAP_MAC_2P5M_CLK_GATE           6
#define VAP_MAC_25M_CLK_GATE            5
#define VAP_MAC_125M_CLK_GATE           4
//#define   Reserved                    3
#define BMIPS_CLK_GATE                  2
#define BME_CLK_GATE                    1
#define AFE_CLK_GATE                    0

/*=============================================================
*    Bit definitions for clock Control write protect 
*    SCU Register address - 0xb900_0120
*=============================================================*/

//bits 28 - 31 are reserved
#define WP_CLK_CTRL_6                       24  //bits 24 - 27
#define WP_CLK_CTRL_5                       20  //bits 20 - 23
#define WP_CLK_CTRL_4                       16  //bits 16 - 19
#define WP_CLK_CTRL_3                       12  //bits 15 - 12
#define WP_CLK_CTRL_2                       8   //bits 11 - 8
#define WP_CLK_CTRL_1                       4   //bits 4 - 7
#define WP_CLK_CTRL_0                       0   //bits 0 - 3

/*=============================================================
*    Bit definitions for clock Control 0
*    SCU Register address - 0xb900_0124
*=============================================================*/

//bits 28 - 31 are reserved
#define HMIPS_DIV_SEL                       28  //bits 31 - 28, Select divider value 1 to 16 in steps size 1
#define HMIPS_CLK_SEL                       24  //bits 27 - 24, option to select clock source for HMIPS
#define DDR_DIV_SEL                         20  //bits 23 - 20, Select divider value 2 to 32 in steps size 2
#define DDR_CLK_SEL                         16  //bits 19 - 16, option to select clock source for DDR
/* Bits 15 - 8 Reserved */
#define APU_DIV_SEL                         4   //bits 7 - 4, Select divider value 1 to 16 in steps size 1
#define APU_CLK_SEL                         0   //bits 3 - 0, option to select clock source for APU

/*=============================================================
*    Bit definitions for Latch On Reset 1 Register
*    SCU Register address - 0xb900_0224
*=============================================================*/

#define NAND_RESET_1GB_4KB  				0x17    // 0b'10= 4KB  0b'111 = 8Gb - other bit status set to default
#define NAND_RESET_1GB_4KB_MASK 			0x17    // 0b'10= 4KB  0b'111 = 8Gb - other bit status set to default

/*=============================================================
*    Bit definitions for NAND Status Register
*    SCU Register address - 0xb900_03a4
*=============================================================*/

#define NAND_CHIP_BUSY          			(1 << 0)
#define NAND_PECC_BUSY          			(1 << 1)


#endif
