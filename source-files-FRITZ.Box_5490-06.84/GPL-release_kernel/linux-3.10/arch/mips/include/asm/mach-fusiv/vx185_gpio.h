/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _GPIO_DEF
#define _GPIO_DEF

typedef struct gpio_reg_s {

        volatile unsigned int gpio_val_reg;               //0x000
        volatile unsigned int gpio_set_reg;               //0x004
        volatile unsigned int gpio_clear_reg;             //0x008
        volatile unsigned int gpio_mode1_reg;             //0x00c
        volatile unsigned int gpio_mode2_reg;             //0x010
        volatile unsigned int alt_func_sel_reg;       //0x014
        volatile unsigned int alt_func_default_reg    //0x018
}gpio_reg_t;

 #define gpio_regs ((gpio_reg_t *) 0xb9030000)
#endif

