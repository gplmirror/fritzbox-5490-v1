/******************************************************************************
 * FILE NAME    : avm_cpuidle.c                                               *
 * COMPANY      : AVM                                                         *
 * AUTHOR       : Christoph Buettner                                          *
 * DESCRIPTION  : cpufreq interface                                           *
 *                                                                            *
 *****************************************************************************/
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/cpufreq.h>
#include <asm/mach_avm.h>
#include <common_routines.h>

/*-Defines----------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AVM_CPUFREQ_DESCRIPTION    "AVM cpufreq driver for AR9+VR9+AR10"
#define AVM_CPUFREQ_VERSION        "0.0.1"


static struct cpufreq_driver avm_cpufreq__cfreq_driver;

#if defined(CONFIG_AR9)
static struct cpufreq_frequency_table avm_cpufreq__frequency_table[] =
{
    { .frequency = 393215 , .index = 0 }, 
    { .frequency = 333333 , .index = 1 }, 
    { .frequency = 111111 , .index = 2 }, 
    { .frequency = CPUFREQ_TABLE_END }
};
#elif defined(CONFIG_VR9)
static struct cpufreq_frequency_table avm_cpufreq__frequency_table[] =
{
    { .frequency = 500000 , .index = 0 }, 
    { .frequency = 393215 , .index = 1 }, 
    { .frequency = 333333 , .index = 2 }, 
    { .frequency = 125000 , .index = 3 }, 
    { .frequency = CPUFREQ_TABLE_END }
};
#else /*--- AR10 ---*/
static struct cpufreq_frequency_table avm_cpufreq__frequency_table[] =
{
    { .frequency = 500000 , .index = 0 }, 
    { .frequency = 250000 , .index = 1 }, 
    { .frequency = 125000 , .index = 2 }, 
    { .frequency = CPUFREQ_TABLE_END }
};
#endif

static int __init avm_cpufreq__cpu_init(struct cpufreq_policy *policy) {
    if (policy->cpu != 0)
        return -EINVAL;
    /*--- policy->cpuinfo.transition_latency = CPUFREQ_ETERNAL; ---*/
    policy->cpuinfo.transition_latency = 500000; /* latency in ns = 500µs*/
    cpufreq_frequency_table_cpuinfo(policy, avm_cpufreq__frequency_table);
    return 0;
}

int avm_cpufreq__verify_speed(struct cpufreq_policy *policy) {
    return cpufreq_frequency_table_verify(policy, avm_cpufreq__frequency_table);
}

unsigned int avm_cpufreq__getspeed(unsigned int cpu) {
    if (cpu != 0){
        return -EINVAL;
    } else {
        return ifx_get_cpu_hz() / 1000;
    }
}

static struct cpufreq_driver avm_cpufreq__cfreq_driver = {
    .flags  = CPUFREQ_STICKY,
    .verify = avm_cpufreq__verify_speed,
    .target = avm_cpufreq__target,
    .get    = avm_cpufreq__getspeed,
    .init   = avm_cpufreq__cpu_init,
    .name   = "avm_cpufreq",
};


static int __init avm_cpufreq__register_cpufreq(void) {
    printk(KERN_ERR "Registering %s v%s\n", AVM_CPUFREQ_DESCRIPTION, AVM_CPUFREQ_VERSION);
    return cpufreq_register_driver(&avm_cpufreq__cfreq_driver);
}

late_initcall(avm_cpufreq__register_cpufreq);

