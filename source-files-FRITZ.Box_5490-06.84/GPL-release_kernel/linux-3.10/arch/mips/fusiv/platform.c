/**
 * Some part of this file is modified by Ikanos Communications. 
 *
 * Copyright (C) 2006-2014 Ikanos Communications.
 */

/*
 * Platform device support for Ikanos's FUSIV SoCs.
 *
 * Copyright 2004, Matt Porter <mporter@kernel.crashing.org>
 * Modified  2005, Vivek Dharmadhikari <mporter@kernel.crashing.org>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

/* TODO: Is above copyright correct -- VB */
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/resource.h>
#include <linux/mtd/partitions.h>

#include <fusiv_usb.h>
#include <ikn_spi.h>

#if defined(CONFIG_FUSIV_VX185)
#include <fusiv_nand.h>
#include <vx185_int.h>
#include <vx185_scu.h>
#endif

#if defined(CONFIG_FUSIV_VX585)
#include <fusiv_nand.h>
#include <vx185_int.h>		//To be changed to vx585_int.h
#include <vx585_scu.h>
#endif
#include <linux/i2c.h>
#include <linux/i2c/at24.h>
#include <linux/sizes.h>
#include <linux/interrupt.h>
#include "../../../../drivers/i2c/busses/i2c-designware-core.h"

#define  SPI_BASE_ADDRESS       (0xB9028000)	/* Base Address              */
#if defined(CONFIG_FUSIV_VX585)
#define  I2C_BASE_ADDRESS       (0xB9070000)
#endif


static struct resource fusiv_usb_ohci_hcd_resources[] = {
	[0] = {
	       .start = FUSIV_USB_OHCI_BASE,
	       .end = FUSIV_USB_OHCI_BASE + FUSIV_USB_OHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = FUSIV_USB_OHCI_HOST_INT,
	       .end = FUSIV_USB_OHCI_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

static struct resource fusiv_usb_ehci_hcd_resources[] = {
	[0] = {
	       .start = FUSIV_USB_EHCI_BASE,
	       .end = FUSIV_USB_EHCI_BASE + FUSIV_USB_EHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = FUSIV_USB_EHCI_HOST_INT,
	       .end = FUSIV_USB_EHCI_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

#if defined(CONFIG_FUSIV_VX585)
static struct resource fusiv_usb_xhci_hcd_resources[] = {
	[0] = {
	       .start = FUSIV_USB_XHCI_BASE,
	       .end = FUSIV_USB_XHCI_BASE + FUSIV_USB_XHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = FUSIV_USB_XHCI_HOST_INT,
	       .end = FUSIV_USB_XHCI_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};
#endif

// AVM/TKL: NAND resource setup moved to fusiv_mtd.c
#undef CONFIG_MTD_NAND
#if defined(CONFIG_MTD_NAND)
static struct mtd_partition nand_partition_info[] = {
	{
	 /* Level-1 bootloader uses the full first block */
	 .name = "Bootloader-stage-1(nand)",
	 .offset = 0,
#if defined(CONFIG_FUSIV_VX585)
	 .size = 1024 * 1024,
#else
	 .size = 128 * 1024,
#endif
	 },
	{
	 /* Fusiv partition. uboot, uboot environment, Kernel  & Filesytem */
	 .name = "FusivMTD(nand)",
	 .offset = MTDPART_OFS_APPEND,	/* 0xC0000 */
	 .size = MTDPART_SIZ_FULL,
	 },
};

static struct mtd_partition nand_partition_info_8Gb[] = {
	{
	 /* Level-1 bootloader uses the full first block */
	 .name = "Bootloader-stage-1(nand)",
	 .offset = 0,
#if defined(CONFIG_FUSIV_VX585)
	 .size = 1024 * 1024,
#else
	 .size = 512 * 1024,
#endif
	 },
	{
	 /* Fusiv partition. uboot, uboot environment, Kernel  & Filesytem */
	 .name = "FusivMTD(nand)",
	 .offset = MTDPART_OFS_APPEND,	/* 0xC0000 */
	 .size = MTDPART_SIZ_FULL,
	 },

};

static struct fusiv_nand_platform nand_platform_data = {
	.partitions = nand_partition_info,
	.nr_partitions = ARRAY_SIZE(nand_partition_info),
};

static struct fusiv_nand_platform nand_platform_data_8Gb = {
	.partitions = nand_partition_info_8Gb,
	.nr_partitions = ARRAY_SIZE(nand_partition_info_8Gb),
};

static struct resource fusiv_nand_resources[] = {
	[0] = {
	       .start = NFC_BASEADDR + NFC_DATA,
	       .end = NFC_BASEADDR + NFC_SECT7_SYNDROME78,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = NAND_FLASH_INT,
	       .end = NAND_FLASH_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

#endif

static u64 fusiv_dmamask = ~(u32) 0;

static struct platform_device fusiv_usb_ohci_hcd_device = {
	.name = "fusiv-ohci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &fusiv_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(fusiv_usb_ohci_hcd_resources),
	.resource = fusiv_usb_ohci_hcd_resources,
};

static struct platform_device fusiv_usb_ehci_hcd_device = {
	.name = "fusiv-ehci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &fusiv_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(fusiv_usb_ehci_hcd_resources),
	.resource = fusiv_usb_ehci_hcd_resources,
};

#if defined(CONFIG_FUSIV_VX585)
static struct platform_device fusiv_usb_xhci_hcd_device = {
	.name = "fusiv-xhci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &fusiv_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(fusiv_usb_xhci_hcd_resources),
	.resource = fusiv_usb_xhci_hcd_resources,
};
#endif

#if defined(CONFIG_MTD_NAND)
static struct platform_device fusiv_nand_device = {
	.name = "fusiv-nand",
	.id = 0,
	.dev = {
		.platform_data = &nand_platform_data,
		},
	.num_resources = ARRAY_SIZE(fusiv_nand_resources),
	.resource = fusiv_nand_resources,
};

static struct platform_device fusiv_nand_device_8Gb = {
	.name = "fusiv-nand",
	.id = 0,
	.dev = {
		.platform_data = &nand_platform_data_8Gb,
		},
	.num_resources = ARRAY_SIZE(fusiv_nand_resources),
	.resource = fusiv_nand_resources,
};

#endif

#if defined(CONFIG_FUSIV_VX585)
/* Vx585 SATA device */
static struct resource vx585_ahci_resources[] = {
	[0] = {
	       .start = 0x19150000,
	       .end = 0x19150000 + 0xffff,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = 54,
	       .end = 54,
	       .flags = IORESOURCE_IRQ,
	       }
};

static int vx585_ahci_data = 0;
static struct platform_device vx585_ahci_device = {
	.name = "ahci_vx585",
	.id = -1,
	.dev = {
		.platform_data = &vx585_ahci_data,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(vx585_ahci_resources),
	.resource = vx585_ahci_resources,
};
#else
/* Vx185 SATA device */
static struct resource vx185_ahci_resources[] = {
	[0] = {
	       .start = 0x19150000,
	       .end = 0x19150000 + 0xffff,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
#if (defined(CONFIG_FUSIV_VX185)) && defined (CONFIG_CPU_MIPSR2_IRQ_VI)
	       .start = FUSIV_MIPS_INT_SATA,
	       .end = FUSIV_MIPS_INT_SATA,
#else
	       .start = 31,
	       .end = 31,
#endif
	       .flags = IORESOURCE_IRQ,
	       }
};

static int vx185_ahci_data = 0;
static struct platform_device vx185_ahci_device = {
	.name = "ahci_vx185",
	.id = -1,
	.dev = {
		.platform_data = &vx185_ahci_data,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(vx185_ahci_resources),
	.resource = vx185_ahci_resources,
};

#endif

#if defined(CONFIG_SPI)
static struct resource fusiv_spi_resources[] = {
	[0] = {
	       .start = SPI_BASE_ADDRESS,
	       .end = SPI_BASE_ADDRESS + 0x1000,
	       .flags = IORESOURCE_MEM,
	       },
};

static spiConfig_t fusiv185_spi_data = {
	//TODO define a config for SPI mode
	.eSPIMode = eSPI_MODE_SLAVE,
	.eSPIInitMode = eSPI_INIT_MODE_BY_WRITE,
	.eSPISZState = eSPI_SZ_SEND_ZEROS,
	.eSPIGMState = eSPI_GM_DISCARD_INCOMING_DATA,
	.eSPIErrorInput = eSPI_PSSE_DISABLE,
	.eSPIEnableMISO = eSPI_EMISO_ENABLE,
	.eSPIWordSize = eSPI_WORD_SIZE_16,
	.eSPIDataFormat = eSPI_MSB_FIRST,
	.eSPIEnableCPHA = eSPI_CPHA_ENABLE,
	.eSPIEnableCPOL = eSPI_CPOL_DISABLE,
	.eSPIOpenDrain = eSPI_WOM_DISABLE,
	.eSPIModuleState = eSPI_MODULE_ENABLE,
//      .spiSlaveSelectBitMap,  //number of slaves to enable
//      .uiSPIBaudRate  = 62500000, // applicable only in Master Mode
//      .u32SPIMaxBufSize = 1000, //can be changed
};

static struct platform_device fusiv_spi_device = {
	.name = "fusiv_spi",
	.id = 1,
	.dev = {
		.platform_data = &fusiv185_spi_data,
		},
	.num_resources = ARRAY_SIZE(fusiv_spi_resources),
	.resource = fusiv_spi_resources,
};

#endif

#if defined(CONFIG_I2C) && (defined(CONFIG_FUSIV_VX585))
static struct resource fusiv_i2c_resources[] = {
	[0] = {
	       .start = I2C_BASE_ADDRESS,
	       .end = I2C_BASE_ADDRESS + 0x1000,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = 4,
	       .end = 4,
	       .flags = IORESOURCE_IRQ,
	       },
};

static struct dw_i2c_dev fusiv_i2c_data = {
	.tx_fifo_depth = 1,
	.rx_fifo_depth = 1,
};

static struct platform_device fusiv_i2c_device = {
	.name = "i2c_designware",
	.id = 0,
	.dev.platform_data = &fusiv_i2c_data,
	.num_resources = ARRAY_SIZE(fusiv_i2c_resources),
	.resource = fusiv_i2c_resources,
};
#endif
#if (defined(CONFIG_FUSIV_VX185) || defined(CONFIG_FUSIV_VX585)) && defined (CONFIG_CPU_MIPSR2_IRQ_VI)
static struct platform_device *fusiv_platform_devices8Gb[] __initdata = {
#if defined(CONFIG_FUSIV_VX585)
	&fusiv_usb_xhci_hcd_device,
#endif
	&fusiv_usb_ohci_hcd_device,
	&fusiv_usb_ehci_hcd_device,
#if defined(CONFIG_FUSIV_VX585)
	&vx585_ahci_device,
#else
	&vx185_ahci_device,
#endif
// AVM/TKL: check CONFIG_MTD_NAND
#if defined(CONFIG_MTD_NAND)
#if defined(CONFIG_FUSIV_VX185) || defined(CONFIG_FUSIV_VX585)
	&fusiv_nand_device_8Gb,
#else
	&fusiv_nand_device,
#endif
#endif
#if defined(CONFIG_SPI)
	&fusiv_spi_device,
#endif
#if defined(CONFIG_I2C) && (defined(CONFIG_FUSIV_VX585))
	&fusiv_i2c_device,
#endif
};
#endif

static struct platform_device *fusiv_platform_devices[] __initdata = {
#if defined(CONFIG_FUSIV_VX585)
	&fusiv_usb_xhci_hcd_device,
#endif
	&fusiv_usb_ohci_hcd_device,
	&fusiv_usb_ehci_hcd_device,
#if defined(CONFIG_FUSIV_VX585)
	&vx585_ahci_device,
#else
	&vx185_ahci_device,
#endif
#if defined(CONFIG_MTD_NAND)
	&fusiv_nand_device,
#endif
#if defined(CONFIG_SPI)
	&fusiv_spi_device,
#endif
#if defined(CONFIG_I2C) && (defined(CONFIG_FUSIV_VX585))
	&fusiv_i2c_device,
#endif
};

#if defined(CONFIG_FUSIV_VX585) && defined(CONFIG_I2C)
static struct at24_platform_data m24c01 = {
	.byte_len = SZ_1K / 8,
	.page_size = 16,
};

static struct i2c_board_info fusiv_i2c_info[] __initdata = {
	{
	 I2C_BOARD_INFO("24c01", 0x50),
	 .type = "24c01",
	 .platform_data = &m24c01,
	 }
};
#endif
int fusiv_platform_init(void)
{
// AVM/TKL: always needed
#if 1 //defined(CONFIG_MTD_NAND)
	unsigned int reset_strap_val;
#endif

#if defined(CONFIG_FUSIV_VX585) && defined(CONFIG_I2C)
	i2c_register_board_info(0, fusiv_i2c_info, ARRAY_SIZE(fusiv_i2c_info));
#endif

// AVM/TKL: always needed
#if 1 //defined(CONFIG_MTD_NAND)

#if defined(CONFIG_FUSIV_VX185)
	reset_strap_val = scu_regs->rst_strap;

	if ((reset_strap_val & NAND_RESET_1GB_4KB_MASK) == NAND_RESET_1GB_4KB)
		return platform_add_devices(fusiv_platform_devices8Gb,
					    ARRAY_SIZE
					    (fusiv_platform_devices8Gb));
	else
		return platform_add_devices(fusiv_platform_devices,
					    ARRAY_SIZE(fusiv_platform_devices));
#endif
#if defined(CONFIG_FUSIV_VX585)
	reset_strap_val = scu_regs->latch_on_reset[1];
	if (((fusiv_nfc_readl(NFC_CONFIG) >> 8) & 3) == 1) {
		return platform_add_devices(fusiv_platform_devices,
					    ARRAY_SIZE(fusiv_platform_devices));
	} else {
		return platform_add_devices(fusiv_platform_devices8Gb,
					    ARRAY_SIZE
					    (fusiv_platform_devices8Gb));
	}
#endif

#endif
}

arch_initcall(fusiv_platform_init);
