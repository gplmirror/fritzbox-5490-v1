/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config fuer FRITZ!Box 7369
 *
\*------------------------------------------------------------------------------------------*/

#undef AVM_HARDWARE_CONFIG
#define AVM_HARDWARE_CONFIG  avm_hardware_config_hw191

#define VX185_GPIO_MODE_INPUT   0x00
#define VX185_GPIO_MODE_OUTPUT  0x01
#define VX185_GPIO_MODE_OD      0x02
#define VX185_GPIO_MODE_OS      0x03

struct _avm_hw_config AVM_HARDWARE_CONFIG[] = {


    /****************************************************************************************\
     *
     * GPIO Config
     *
    \****************************************************************************************/


    /*------------------------------------------------------------------------------------------*\
     * LEDs / Taster
    \*------------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_led_power",
        .value  = 17,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_led_power_red",
        .value  = 19,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    {
        .name   = "gpio_avm_led_internet",
        .value  = 23,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    {
        .name   = "gpio_avm_led_festnetz",
        .value  = 22,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    {
        .name   = "gpio_avm_led_wlan",
        .value  = 24,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    {
        .name   = "gpio_avm_led_info",
        .value  = 20,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    {
        .name   = "gpio_avm_led_info_red",
        .value  = 21,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },

    /*--- DECT button connected to EXTIN 1 ---*/
    {
        .name   = "gpio_avm_button_dect",
        .value  = 28,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
            .mode   = VX185_GPIO_MODE_INPUT
        }
    },
    /*--- WLAN button connected to EXTIN 0 ---*/
    {
        .name   = "gpio_avm_button_wlan",
        .value  = 29,
        .param  = avm_hw_param_gpio_in_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_INPUT_PIN,
            .mode   = VX185_GPIO_MODE_INPUT

        }
    },

    /*--------------------------------------------------------------------------------------*\
     * SPI-Flash
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_spi_flash_cs",
        .value  = 0,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_spi_flash_hold",
        .value  = 1,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_spi_flash_wp",
        .value  = 2,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },

    /*--------------------------------------------------------------------------------------*\
     * DECT
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_dect_reset", 
        .value  = 9,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_dect_uart_rx",
        .value  = 12,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_INPUT_PIN,
            .mode      = VX185_GPIO_MODE_INPUT
        }
    },
    {
        .name   = "gpio_avm_dect_rd",
        .value  = 12,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_INPUT_PIN,
            .mode      = VX185_GPIO_MODE_INPUT
        }
    },
    {
        .name   = "gpio_avm_dect_uart_tx",
        .value  = 11,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },
    /*--------------------------------------------------------------------------------*\
     * SPI1-Slic
    \*--------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_spi_slic_cs2",
        .value  = 6,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .func_sel  = 1,
            .dir       = GPIO_OUTPUT_PIN,
            .mode      = VX185_GPIO_MODE_OUTPUT
        }
    },
    /*--------------------------------------------------------------------------------*\
     * Slic-Reset
    \*--------------------------------------------------------------------------------*/
    {
        .name   = "gpio_slic_reset",
        .value  = 3,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },
    /*--------------------------------------------------------------------------------------*\
     * ETHERNET
     * Which GPIO to use depends on the chosen hardware options
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_periph_reset",
        .value  = 25,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_extphy_reset",
        .value  = 10,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
            .dir    = GPIO_OUTPUT_PIN,
            .mode   = VX185_GPIO_MODE_OUTPUT
        }
    },
    {
        .name   = "gpio_avm_extphy_int",
        .value  = 26,
        .param  = avm_hw_param_no_param,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
           .dir    = GPIO_INPUT_PIN,
           .mode   = VX185_GPIO_MODE_INPUT

        }
    },
    {
        .name   = "gpio_avm_pcie_reset",
        .value  = 16,
        .param  = avm_hw_param_gpio_out_active_low,
        .manufactor_hw_config.manufactor_ikanos_gpio_config = {
           .dir    = GPIO_OUTPUT_PIN,
           .mode   = VX185_GPIO_MODE_OUTPUT

        }
    },

    {   .name   = NULL }
};
EXPORT_SYMBOL(AVM_HARDWARE_CONFIG);


