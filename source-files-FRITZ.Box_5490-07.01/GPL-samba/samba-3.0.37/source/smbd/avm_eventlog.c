/* 
   Unix SMB/CIFS implementation.

   FRITZ!OS Event Logging

   Copyright (C) 2016 AVM 
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifdef AVM_FRITZBOX

#include "includes.h"

#include "avm_eventlog.h"

void AccessEventLog(unsigned event_id, const char *name, const char *addr_str)
{
	char event_id_s[48] = { 0 };

	// assert (0 == strchr(addr_str, '\''));

	if ('@' == name[0]) return;

	snprintf(event_id_s, sizeof(event_id_s) - 1, "%u", event_id);
	avm_system_no_sh("/sbin/eventadd", event_id_s, name, addr_str, NULL, NULL, NULL);
}

char *GetAppDisplayName(const char *username)
{
	char buf[512];
	char *app_name = 0;
	FILE *fp = fopen("/var/tmp/apps.map", "r");
	
	if (fp) {
		while (!app_name && (buf == fgets(buf, sizeof(buf), fp))) {
			char *p;
			while((p = strrchr(buf, '\n')) || (p = strrchr(buf, '\r'))) {
				*p = '\0';
			}
			p = strchr(buf, '=');
			if (p) {
				*p = 0;
				if (0 == strcmp(buf, username)) {
					app_name = strdup(p + 1);
				}
			}
		}
		
		fclose(fp);
	}
	return app_name;
}

int avm_system_no_sh(const char *prg, const char * para1, const char * para2, const char * para3, const char * para4, const char * para5, const char * para6)
{
	char *argv[8];  // letzten Platz als Abschluss immer auf NULL setzen
	int status = -1;
	pid_t pid;

	if (!prg || !*prg)
		return -1;

	argv[0] = (char*)prg;
	argv[1] = (char*)para1;
	argv[2] = (char*)para2;
	argv[3] = (char*)para3;
	argv[4] = (char*)para4;
	argv[5] = (char*)para5;
	argv[6] = (char*)para6;
	argv[7] = NULL;  // Wenn keine Argumente mehr kommen, Ende / nach dem letzten Parameter immer auf NULL setzten!

	pid = fork();
	if (pid < 0) {
		syslog(LOG_MAKEPRI(LOG_DAEMON, LOG_ERR), "fork() failed");
		return -1;
	}
	if (pid == 0) { /* child */
		execvp(argv[0], argv);   // return -1 bei Fehler, Grund in errno
								 // hier nur bei einem Fehler, bei erfolgreicher Ausfuehrung von execvp kehrt er hier nicht zurueck, sondern ersetzt den Child-Prozess
		Log(("avm_system_no_sh(\"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"): execvp() failed, errno %d",
			prg, para1 ? para1 : "", para2 ? para2 : "", para3 ? para3 : "", para4 ? para4 : "", para4 ? para4 : "", para4 ? para4 : "", errno));
		exit(1); // Fehler
	}
	/* parent */
	if (waitpid(pid, &status, 0) < 0) {  // wenn WNOHANG mit angegeben wird, kann der Return-Wert auch 0 sein
		Log(("avm_system_no_sh waitpid(%d) failed", pid));
		return -1;
	}
	else {
		if (WIFEXITED(status)) {
			Log(("avm_system_no_sh waitpid child exited, status=%d", WEXITSTATUS(status)));
			return WEXITSTATUS(status); // child sauber beendet, Exit code zurueckgeben 0 OK, 1 Fehler beim execvp oder Fehlercode vom Programm
		}
		else if (WIFSIGNALED(status)) {
			Log(("avm_system_no_sh waitpid child killed by signal %d", WTERMSIG(status)));
			return -2;
		}
		else if (WIFSTOPPED(status)) {  // nur moeglich wenn beim Aufruf von waitpid WUNTRACED angegeben wird
			Log(("avm_system_no_sh waitpid child stopped by signal %d\n", WSTOPSIG(status)));
			return -3;
		}
		else if (WIFCONTINUED(status)) {
			Log(("avm_system_no_sh waitpid child continue"));
			return -4;
		}
	}

	return -1;
}


#endif /*  AVM_FRITZBOX */
