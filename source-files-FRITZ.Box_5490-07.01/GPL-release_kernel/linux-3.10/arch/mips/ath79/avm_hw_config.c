
#include <linux/kernel.h>

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);

/*------------------------------------------------------------------------------------------*\
  [GJu] new: fill the GPIO table defined literally before from DeviceTree now
\*------------------------------------------------------------------------------------------*/

#include <atheros_gpio.h>
#include <linux/of.h>

struct _gpio_function *g_gpio_table;
EXPORT_SYMBOL (g_gpio_table);

unsigned int g_gpio_table_size;
EXPORT_SYMBOL (g_gpio_table_size);

#define GPIO_TABLE_SIZE_MAX  32

static struct _gpio_function msGpios [GPIO_TABLE_SIZE_MAX];

int init_gpio_config(void) 
{
    const char* szOfCompatible = "avm,avm_gpio_specific";

    struct device_node *psNode;
    struct device_node *psNodeGpio;

    int igpio = 0;

    struct _gpio_function *psGpio = msGpios;

    for (
        psNode = of_find_compatible_node (NULL, NULL, szOfCompatible); 
        NULL != psNode; 
        psNode = of_find_compatible_node (psNode, NULL, szOfCompatible)) 
    {
        for_each_child_of_node (psNode, psNodeGpio) {
            char *szName;
            unsigned u;

            int len;

            if (GPIO_TABLE_SIZE_MAX <= igpio) 
                panic("[%s] Too many GPIO configs in Device Tree\n", __FUNCTION__);

            if (NULL == (szName = (char *) of_get_property (psNodeGpio, "name", &len)))
                panic ("[%s] Cannot find mandatory property 'name' in Device Tree\n", __FUNCTION__);

            // Ensure the correct order (name has to be 'gpio<nn>' where <nn> must match igpio)
            if ((szName [4] - '0') * 10 + (szName [5] - '0') != igpio) 
                panic ("[%s] Invalid GPIO name enumeration in Device Tree\n", __FUNCTION__);

            if (0 != of_property_read_u32 (psNodeGpio, "function", &u)) 
                panic ("[%s] Invalid or missing GPIO property 'function' in Device Tree\n", __FUNCTION__);
        
            psGpio->func = u;

            if (0 != of_property_read_u32 (psNodeGpio, "direction", &u)) 
                panic ("[%s] Invalid or missing GPIO property 'direction' in Device Tree\n", __FUNCTION__);
        
            psGpio->dir = u;

            ++igpio;
            ++psGpio;
        }
    }

    printk ("[DTB] read %d GPIO config entries from node '%s'\n", igpio, szOfCompatible);
      
    g_gpio_table_size = igpio;
    g_gpio_table      = msGpios;

    return 0;
}

