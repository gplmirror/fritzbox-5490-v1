/*
 *  Atheros AR71XX/AR724X/AR913X common routines
 *
 *  Copyright (C) 2010-2011 Jaiganesh Narayanan <jnarayanan@atheros.com>
 *  Copyright (C) 2011 Gabor Juhos <juhosg@openwrt.org>
 *
 *  Parts of this file are based on Atheros' 2.6.15/2.6.31 BSP
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/clk.h>

#include <asm/div64.h>

#include <asm/mach_avm.h>
#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include "common.h"

#define AR71XX_BASE_FREQ	(40 * 1000 * 1000)
#define AR724X_BASE_FREQ	AR71XX_BASE_FREQ
#define AR913X_BASE_FREQ	AR71XX_BASE_FREQ

static struct clk ath79_ref_clk;
static struct clk ath79_cpu_clk;
static struct clk ath79_ddr_clk;
static struct clk ath79_ahb_clk;
static struct clk ath79_wdt_clk;
static struct clk ath79_uart_clk;

#if defined CONFIG_SOC_AR71XX
static void __init ar71xx_clocks_init(void)
{
	u32 pll;
	u32 freq;
	u32 div;

	ath79_ref_clk.rate = AR71XX_BASE_FREQ;

	pll = ath79_pll_rr(AR71XX_PLL_REG_CPU_CONFIG);

	div = ((pll >> AR71XX_PLL_DIV_SHIFT) & AR71XX_PLL_DIV_MASK) + 1;
	freq = div * ath79_ref_clk.rate;

	div = ((pll >> AR71XX_CPU_DIV_SHIFT) & AR71XX_CPU_DIV_MASK) + 1;
	ath79_cpu_clk.rate = freq / div;

	div = ((pll >> AR71XX_DDR_DIV_SHIFT) & AR71XX_DDR_DIV_MASK) + 1;
	ath79_ddr_clk.rate = freq / div;

	div = (((pll >> AR71XX_AHB_DIV_SHIFT) & AR71XX_AHB_DIV_MASK) + 1) * 2;
	ath79_ahb_clk.rate = ath79_cpu_clk.rate / div;

	ath79_wdt_clk.rate = ath79_ahb_clk.rate;
	ath79_uart_clk.rate = ath79_ahb_clk.rate;
}
#endif

#if defined CONFIG_SOC_AR724X
static void __init ar724x_clocks_init(void)
{
	u32 pll;
	u32 freq;
	u32 div;

	ath79_ref_clk.rate = AR724X_BASE_FREQ;
	pll = ath79_pll_rr(AR724X_PLL_REG_CPU_CONFIG);

	div = ((pll >> AR724X_PLL_DIV_SHIFT) & AR724X_PLL_DIV_MASK);
	freq = div * ath79_ref_clk.rate >> 1;

	div = ((pll >> AR724X_PLL_REF_DIV_SHIFT) & AR724X_PLL_REF_DIV_MASK);
	freq /= div;

	ath79_cpu_clk.rate = freq;

	div = ((pll >> AR724X_DDR_DIV_SHIFT) & AR724X_DDR_DIV_MASK) + 1;
	ath79_ddr_clk.rate = freq / div;

	div = (((pll >> AR724X_AHB_DIV_SHIFT) & AR724X_AHB_DIV_MASK) + 1) * 2;
	ath79_ahb_clk.rate = ath79_cpu_clk.rate / div;

	ath79_wdt_clk.rate = ath79_ahb_clk.rate;
	ath79_uart_clk.rate = ath79_ahb_clk.rate;
}
#endif

#if defined CONFIG_SOC_AR913X
static void __init ar913x_clocks_init(void)
{
	u32 pll;
	u32 freq;
	u32 div;

	ath79_ref_clk.rate = AR913X_BASE_FREQ;
	pll = ath79_pll_rr(AR913X_PLL_REG_CPU_CONFIG);

	div = ((pll >> AR913X_PLL_DIV_SHIFT) & AR913X_PLL_DIV_MASK);
	freq = div * ath79_ref_clk.rate;

	ath79_cpu_clk.rate = freq;

	div = ((pll >> AR913X_DDR_DIV_SHIFT) & AR913X_DDR_DIV_MASK) + 1;
	ath79_ddr_clk.rate = freq / div;

	div = (((pll >> AR913X_AHB_DIV_SHIFT) & AR913X_AHB_DIV_MASK) + 1) * 2;
	ath79_ahb_clk.rate = ath79_cpu_clk.rate / div;

	ath79_wdt_clk.rate = ath79_ahb_clk.rate;
	ath79_uart_clk.rate = ath79_ahb_clk.rate;
}
#endif

#if defined CONFIG_SOC_AR933X
static void __init ar933x_clocks_init(void)
{
	u32 clock_ctrl;
	u32 cpu_config;
	u32 freq;
	u32 t;

	t = ath79_reset_rr(AR933X_RESET_REG_BOOTSTRAP);
	if (t & AR933X_BOOTSTRAP_REF_CLK_40)
		ath79_ref_clk.rate = (40 * 1000 * 1000);
	else
		ath79_ref_clk.rate = (25 * 1000 * 1000);

	clock_ctrl = ath79_pll_rr(AR933X_PLL_CLOCK_CTRL_REG);
	if (clock_ctrl & AR933X_PLL_CLOCK_CTRL_BYPASS) {
		ath79_cpu_clk.rate = ath79_ref_clk.rate;
		ath79_ahb_clk.rate = ath79_ref_clk.rate;
		ath79_ddr_clk.rate = ath79_ref_clk.rate;
	} else {
		cpu_config = ath79_pll_rr(AR933X_PLL_CPU_CONFIG_REG);

		t = (cpu_config >> AR933X_PLL_CPU_CONFIG_REFDIV_SHIFT) & AR933X_PLL_CPU_CONFIG_REFDIV_MASK;
		freq = ath79_ref_clk.rate / t;

		t = (cpu_config >> AR933X_PLL_CPU_CONFIG_NINT_SHIFT) & AR933X_PLL_CPU_CONFIG_NINT_MASK;
		freq *= t;

		t = (cpu_config >> AR933X_PLL_CPU_CONFIG_OUTDIV_SHIFT) & AR933X_PLL_CPU_CONFIG_OUTDIV_MASK;
		if (t == 0)
			t = 1;

		freq >>= t;

		t = ((clock_ctrl >> AR933X_PLL_CLOCK_CTRL_CPU_DIV_SHIFT) & AR933X_PLL_CLOCK_CTRL_CPU_DIV_MASK) + 1;
		ath79_cpu_clk.rate = freq / t;

		t = ((clock_ctrl >> AR933X_PLL_CLOCK_CTRL_DDR_DIV_SHIFT) & AR933X_PLL_CLOCK_CTRL_DDR_DIV_MASK) + 1;
		ath79_ddr_clk.rate = freq / t;

		t = ((clock_ctrl >> AR933X_PLL_CLOCK_CTRL_AHB_DIV_SHIFT) & AR933X_PLL_CLOCK_CTRL_AHB_DIV_MASK) + 1;
		ath79_ahb_clk.rate = freq / t;
	}

	ath79_wdt_clk.rate = ath79_ahb_clk.rate;
	ath79_uart_clk.rate = ath79_ref_clk.rate;
}
#endif

#if (defined CONFIG_SOC_AR934X || defined CONFIG_SOC_QCA953X || defined CONFIG_SOC_QCA955X || defined CONFIG_SOC_QCA956X)
 u32 __init ar9xxx_get_pll_freq(u32 ref, u32 ref_div, u32 nint, u32 nfrac,
				      u32 frac, u32 out_div)
{
	u64 t;
	u32 ret;

	t = ath79_ref_clk.rate;
	t *= nint;
	do_div(t, ref_div);
	ret = t;

	t = ath79_ref_clk.rate;
	t *= nfrac;
	do_div(t, ref_div * frac);
	ret += t;

	ret /= (1 << out_div);
	return ret;
}
#endif

#if defined CONFIG_SOC_AR934X
static void __init ar934x_clocks_init(void)
{
	u32 pll, out_div, ref_div, nint, nfrac, frac, clk_ctrl, postdiv;
	u32 cpu_pll, ddr_pll;
	u32 bootstrap;
	void __iomem *dpll_base;

	dpll_base = ioremap(AR934X_SRIF_BASE, AR934X_SRIF_SIZE);

	bootstrap = ath79_reset_rr(AR934X_RESET_REG_BOOTSTRAP);
	if (bootstrap & AR934X_BOOTSTRAP_REF_CLK_40)
		ath79_ref_clk.rate = 40 * 1000 * 1000;
	else
		ath79_ref_clk.rate = 25 * 1000 * 1000;

	pll = __raw_readl(dpll_base + AR934X_SRIF_CPU_DPLL2_REG);
	if (pll & AR934X_SRIF_DPLL2_LOCAL_PLL) {
		out_div = (pll >> AR934X_SRIF_DPLL2_OUTDIV_SHIFT) & AR934X_SRIF_DPLL2_OUTDIV_MASK;
		pll = __raw_readl(dpll_base + AR934X_SRIF_CPU_DPLL1_REG);
		nint = (pll >> AR934X_SRIF_DPLL1_NINT_SHIFT) & AR934X_SRIF_DPLL1_NINT_MASK;
		nfrac = pll & AR934X_SRIF_DPLL1_NFRAC_MASK;
		ref_div = (pll >> AR934X_SRIF_DPLL1_REFDIV_SHIFT) & AR934X_SRIF_DPLL1_REFDIV_MASK;
		frac = 1 << 18;
	} else {
		pll = ath79_pll_rr(AR934X_PLL_CPU_CONFIG_REG);
		out_div = (pll >> AR934X_PLL_CPU_CONFIG_OUTDIV_SHIFT) & AR934X_PLL_CPU_CONFIG_OUTDIV_MASK;
		ref_div = (pll >> AR934X_PLL_CPU_CONFIG_REFDIV_SHIFT) & AR934X_PLL_CPU_CONFIG_REFDIV_MASK;
		nint = (pll >> AR934X_PLL_CPU_CONFIG_NINT_SHIFT) & AR934X_PLL_CPU_CONFIG_NINT_MASK;
		nfrac = (pll >> AR934X_PLL_CPU_CONFIG_NFRAC_SHIFT) & AR934X_PLL_CPU_CONFIG_NFRAC_MASK;
		frac = 1 << 6;
	}

	cpu_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint, nfrac, frac, out_div);

	pll = __raw_readl(dpll_base + AR934X_SRIF_DDR_DPLL2_REG);
	if (pll & AR934X_SRIF_DPLL2_LOCAL_PLL) {
		out_div = (pll >> AR934X_SRIF_DPLL2_OUTDIV_SHIFT) & AR934X_SRIF_DPLL2_OUTDIV_MASK;
		pll = __raw_readl(dpll_base + AR934X_SRIF_DDR_DPLL1_REG);
		nint = (pll >> AR934X_SRIF_DPLL1_NINT_SHIFT) & AR934X_SRIF_DPLL1_NINT_MASK;
		nfrac = pll & AR934X_SRIF_DPLL1_NFRAC_MASK;
		ref_div = (pll >> AR934X_SRIF_DPLL1_REFDIV_SHIFT) & AR934X_SRIF_DPLL1_REFDIV_MASK;
		frac = 1 << 18;
	} else {
		pll = ath79_pll_rr(AR934X_PLL_DDR_CONFIG_REG);
		out_div = (pll >> AR934X_PLL_DDR_CONFIG_OUTDIV_SHIFT) & AR934X_PLL_DDR_CONFIG_OUTDIV_MASK;
		ref_div = (pll >> AR934X_PLL_DDR_CONFIG_REFDIV_SHIFT) & AR934X_PLL_DDR_CONFIG_REFDIV_MASK;
		nint = (pll >> AR934X_PLL_DDR_CONFIG_NINT_SHIFT) & AR934X_PLL_DDR_CONFIG_NINT_MASK;
		nfrac = (pll >> AR934X_PLL_DDR_CONFIG_NFRAC_SHIFT) & AR934X_PLL_DDR_CONFIG_NFRAC_MASK;
		frac = 1 << 10;
	}

	ddr_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint, nfrac, frac, out_div);

	clk_ctrl = ath79_pll_rr(AR934X_PLL_CPU_DDR_CLK_CTRL_REG);

	postdiv = (clk_ctrl >> AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_POST_DIV_SHIFT) &
		  AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_POST_DIV_MASK;

	if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_CPU_PLL_BYPASS)
		ath79_cpu_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_CPUCLK_FROM_CPUPLL)
		ath79_cpu_clk.rate = cpu_pll / (postdiv + 1);
	else
		ath79_cpu_clk.rate = ddr_pll / (postdiv + 1);

	postdiv = (clk_ctrl >> AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_POST_DIV_SHIFT) &
		  AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_POST_DIV_MASK;

	if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_DDR_PLL_BYPASS)
		ath79_ddr_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_DDRCLK_FROM_DDRPLL)
		ath79_ddr_clk.rate = ddr_pll / (postdiv + 1);
	else
		ath79_ddr_clk.rate = cpu_pll / (postdiv + 1);

	postdiv = (clk_ctrl >> AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_POST_DIV_SHIFT) &
		  AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_POST_DIV_MASK;

	if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_AHB_PLL_BYPASS)
		ath79_ahb_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & AR934X_PLL_CPU_DDR_CLK_CTRL_AHBCLK_FROM_DDRPLL)
		ath79_ahb_clk.rate = ddr_pll / (postdiv + 1);
	else
		ath79_ahb_clk.rate = cpu_pll / (postdiv + 1);

	ath79_wdt_clk.rate = ath79_ref_clk.rate;
	ath79_uart_clk.rate = ath79_ref_clk.rate;

	iounmap(dpll_base);
}
#endif

#if defined CONFIG_SOC_QCA953X 
static void __init qca953x_clocks_init(void)
{
	u32 v, out_div, ref_div, nint, nfrac, clk_ctrl, postdiv;
	u32 cpu_pll, ddr_pll;
    u32 bootstrap;

	bootstrap = ath79_reset_rr (QCA953X_REGOFS__RESET__RST_BOOTSTRAP);

	if (bootstrap &	BIT (QCA953X_SSB__RESET__RST_BOOTSTRAP__REF_CLK)) {
		ath79_ref_clk.rate = 40 * 1000 * 1000;
    }
	else {
		ath79_ref_clk.rate = 25 * 1000 * 1000;
    }

	v = ath79_pll_rr (QCA953X_REGOFS__PLL_CONTROL__CPU_PLL_CONFIG);

    out_div = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__OUTDIV,
        QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__OUTDIV);
	ref_div = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__REFDIV,
        QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__REFDIV);

    nint    = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__NINT,
        QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__NINT);
	nfrac   = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__NFRAC,
        QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__NFRAC);

	cpu_pll = ar9xxx_get_pll_freq (ath79_ref_clk.rate, ref_div, nint, nfrac, 1 << 6, out_div);

	v = ath79_pll_rr (QCA953X_REGOFS__PLL_CONTROL__DDR_PLL_CONFIG);

    out_div = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__OUTDIV,
        QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__OUTDIV);
	ref_div = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__REFDIV,
        QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__REFDIV);

    nint    = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__NINT  ,
        QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__NINT  );
	nfrac   = BITS_GET (v, 
        QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__NFRAC ,
        QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__NFRAC );

	ddr_pll = ar9xxx_get_pll_freq (ath79_ref_clk.rate, ref_div, nint, nfrac, 1 << 10, out_div);

	clk_ctrl = ath79_pll_rr (QCA953X_REGOFS__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL);

	if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_PLL_BYPASS)) {
		ath79_cpu_clk.rate = ath79_ref_clk.rate;
    }
	else {
        postdiv = BITS_GET (clk_ctrl, 
            QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_POST_DIV,
            QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_POST_DIV);

        if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPUCLK_FROM_CPUPLL)) {
	    	ath79_cpu_clk.rate = cpu_pll / (postdiv + 1); 
        }
    	else {
	    	ath79_cpu_clk.rate = ddr_pll / (postdiv + 1);
        }
    }

    if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_PLL_BYPASS)) {
		ath79_ddr_clk.rate = ath79_ref_clk.rate;
    }
	else {
    	postdiv = BITS_GET (clk_ctrl, 
            QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_POST_DIV,
            QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_POST_DIV);

        if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDRCLK_FROM_DDRPLL)) {
		    ath79_ddr_clk.rate = ddr_pll / (postdiv + 1);
        }
    	else {
	    	ath79_ddr_clk.rate = cpu_pll / (postdiv + 1);
        }
    }

	if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_PLL_BYPASS)) {
		ath79_ahb_clk.rate = ath79_ref_clk.rate;
    }
	else {
	    postdiv = BITS_GET (clk_ctrl, 
            QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_POST_DIV,
            QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_POST_DIV);

        if (BIT_GET (clk_ctrl, QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHBCLK_FROM_DDRPLL)) {
    		ath79_ahb_clk.rate = ddr_pll / (postdiv + 1);
        }
        else {
	    	ath79_ahb_clk.rate = cpu_pll / (postdiv + 1);
        }
    }

	ath79_wdt_clk.rate  = ath79_ref_clk.rate;
	ath79_uart_clk.rate = ath79_ref_clk.rate;
}
#endif

#if defined CONFIG_SOC_QCA955X
static void __init qca955x_clocks_init(void)
{
	u32 pll, out_div, ref_div, nint, nfrac, clk_ctrl, postdiv;
	u32 cpu_pll, ddr_pll;
	u32 bootstrap;

	bootstrap = ath79_reset_rr(QCA955X_RESET_REG_BOOTSTRAP);
	if (bootstrap &	QCA955X_BOOTSTRAP_REF_CLK_40)
		ath79_ref_clk.rate = 40 * 1000 * 1000;
	else
		ath79_ref_clk.rate = 25 * 1000 * 1000;

	pll = ath79_pll_rr(QCA955X_PLL_CPU_CONFIG_REG);
	out_div = (pll >> QCA955X_PLL_CPU_CONFIG_OUTDIV_SHIFT) & QCA955X_PLL_CPU_CONFIG_OUTDIV_MASK;
	ref_div = (pll >> QCA955X_PLL_CPU_CONFIG_REFDIV_SHIFT) & QCA955X_PLL_CPU_CONFIG_REFDIV_MASK;
	nint = (pll >> QCA955X_PLL_CPU_CONFIG_NINT_SHIFT) & QCA955X_PLL_CPU_CONFIG_NINT_MASK;
	nfrac = (pll >> QCA955X_PLL_CPU_CONFIG_NFRAC_SHIFT) & QCA955X_PLL_CPU_CONFIG_NFRAC_MASK;

	cpu_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint,
				      nfrac, 1 << 6, out_div);

	pll = ath79_pll_rr(QCA955X_PLL_DDR_CONFIG_REG);
	out_div = (pll >> QCA955X_PLL_DDR_CONFIG_OUTDIV_SHIFT) & QCA955X_PLL_DDR_CONFIG_OUTDIV_MASK;
	ref_div = (pll >> QCA955X_PLL_DDR_CONFIG_REFDIV_SHIFT) & QCA955X_PLL_DDR_CONFIG_REFDIV_MASK;
	nint = (pll >> QCA955X_PLL_DDR_CONFIG_NINT_SHIFT) & QCA955X_PLL_DDR_CONFIG_NINT_MASK;
	nfrac = (pll >> QCA955X_PLL_DDR_CONFIG_NFRAC_SHIFT) & QCA955X_PLL_DDR_CONFIG_NFRAC_MASK;

	ddr_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint,
				      nfrac, 1 << 10, out_div);

	clk_ctrl = ath79_pll_rr(QCA955X_PLL_CLK_CTRL_REG);

	postdiv = (clk_ctrl >> QCA955X_PLL_CLK_CTRL_CPU_POST_DIV_SHIFT) & QCA955X_PLL_CLK_CTRL_CPU_POST_DIV_MASK;

	if (clk_ctrl & QCA955X_PLL_CLK_CTRL_CPU_PLL_BYPASS)
		ath79_cpu_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & QCA955X_PLL_CLK_CTRL_CPUCLK_FROM_CPUPLL)
		ath79_cpu_clk.rate = cpu_pll / (postdiv + 1);
	else
		ath79_cpu_clk.rate = ddr_pll / (postdiv + 1);

	postdiv = (clk_ctrl >> QCA955X_PLL_CLK_CTRL_DDR_POST_DIV_SHIFT) & QCA955X_PLL_CLK_CTRL_DDR_POST_DIV_MASK;

	if (clk_ctrl & QCA955X_PLL_CLK_CTRL_DDR_PLL_BYPASS)
		ath79_ddr_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & QCA955X_PLL_CLK_CTRL_DDRCLK_FROM_DDRPLL)
		ath79_ddr_clk.rate = ddr_pll / (postdiv + 1);
	else
		ath79_ddr_clk.rate = cpu_pll / (postdiv + 1);

	postdiv = (clk_ctrl >> QCA955X_PLL_CLK_CTRL_AHB_POST_DIV_SHIFT) & QCA955X_PLL_CLK_CTRL_AHB_POST_DIV_MASK;

	if (clk_ctrl & QCA955X_PLL_CLK_CTRL_AHB_PLL_BYPASS)
		ath79_ahb_clk.rate = ath79_ref_clk.rate;
	else if (clk_ctrl & QCA955X_PLL_CLK_CTRL_AHBCLK_FROM_DDRPLL)
		ath79_ahb_clk.rate = ddr_pll / (postdiv + 1);
	else
		ath79_ahb_clk.rate = cpu_pll / (postdiv + 1);

	ath79_wdt_clk.rate = ath79_ref_clk.rate;
	ath79_uart_clk.rate = ath79_ref_clk.rate;
}
#endif

#if defined CONFIG_SOC_QCA956X 
static void __init qca956x_clocks_init(void)
{
	u32 v, out_div, ref_div, nint, nfrac, clk_ctrl, postdiv;
	u32 cpu_pll, ddr_pll;

	v = ath79_reset_rr (QCA956X_REGOFS__RESET__RST_BOOTSTRAP);

	if (v & BIT (QCA956X_SSB__RESET__RST_BOOTSTRAP__REF_CLK)) {
		ath79_ref_clk.rate = 40 * 1000 * 1000;
    }
	else {
		ath79_ref_clk.rate = 25 * 1000 * 1000;
    }

	v = ath79_pll_rr (QCA956X_REGOFS__PLL_CONTROL__CPU_PLL_CONFIG);

    out_div = QCA956X_GET__PLL_CONTROL__CPU_PLL_CONFIG__OUTDIV (v);
	ref_div = QCA956X_GET__PLL_CONTROL__CPU_PLL_CONFIG__REFDIV (v);

	v = ath79_pll_rr (QCA956X_REGOFS__PLL_CONTROL__CPU_PLL_CONFIG1);

    nint    = QCA956X_GET__PLL_CONTROL__CPU_PLL_CONFIG1__NINT  (v);
	nfrac   = QCA956X_GET__PLL_CONTROL__CPU_PLL_CONFIG1__NFRAC (v);

	cpu_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint, nfrac, 1 << 6, out_div);

	v = ath79_pll_rr(QCA956X_REGOFS__PLL_CONTROL__DDR_PLL_CONFIG);

    out_div = QCA956X_GET__PLL_CONTROL__DDR_PLL_CONFIG__OUTDIV (v);
	ref_div = QCA956X_GET__PLL_CONTROL__DDR_PLL_CONFIG__REFDIV (v);

	v = ath79_pll_rr(QCA956X_REGOFS__PLL_CONTROL__DDR_PLL_CONFIG1);

    nint    = QCA956X_GET__PLL_CONTROL__DDR_PLL_CONFIG1__NINT  (v);
	nfrac   = QCA956X_GET__PLL_CONTROL__DDR_PLL_CONFIG1__NFRAC (v);

	ddr_pll = ar9xxx_get_pll_freq(ath79_ref_clk.rate, ref_div, nint, nfrac, 1 << 10, out_div);

	clk_ctrl = ath79_pll_rr(QCA956X_REGOFS__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL);

	if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_PLL_BYPASS (clk_ctrl)) {
		ath79_cpu_clk.rate = ath79_ref_clk.rate;
    }
	else {
        postdiv = QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_POST_DIV (clk_ctrl);

        if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_DDR_CLK_FROM_CPUPLL (clk_ctrl)) {
	    	ath79_cpu_clk.rate = cpu_pll / (postdiv + 1); 
        }
    	else {
	    	ath79_cpu_clk.rate = ddr_pll / (postdiv + 1);
        }
    }

    if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_PLL_BYPASS (clk_ctrl)) {
		ath79_ddr_clk.rate = ath79_ref_clk.rate;
    }
	else {
    	postdiv = QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_POST_DIV (clk_ctrl);

        if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_DDR_CLK_FROM_DDRPLL (clk_ctrl)) {
		    ath79_ddr_clk.rate = ddr_pll / (postdiv + 1);
        }
    	else {
	    	ath79_ddr_clk.rate = cpu_pll / (postdiv + 1);
        }
    }

	if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_PLL_BYPASS (clk_ctrl)) {
		ath79_ahb_clk.rate = ath79_ref_clk.rate;
    }
	else {
	    postdiv = QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_POST_DIV (clk_ctrl);

        if (QCA956X_GET__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHBCLK_FROM_DDRPLL (clk_ctrl)) {
    		ath79_ahb_clk.rate = ddr_pll / (postdiv + 1);
        }
        else {
	    	ath79_ahb_clk.rate = cpu_pll / (postdiv + 1);
        }
    }

	ath79_wdt_clk.rate  = ath79_ref_clk.rate;
	ath79_uart_clk.rate = ath79_ref_clk.rate;
}
#endif

void __init ath79_clocks_init(void)
{
#if defined CONFIG_SOC_AR71XX
	if (soc_is_ar71xx()) {
		ar71xx_clocks_init();
    }
    else
#elif defined CONFIG_SOC_AR724X
	if (soc_is_ar724x()) {
		ar724x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_AR913X
	if (soc_is_ar913x()) {
		ar913x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_AR933X
	if (soc_is_ar933x()) {
		ar933x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_AR934X
	if (soc_is_ar934x()) {
		ar934x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_QCA953X
	if (soc_is_qca953x()) {
		qca953x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_QCA955X
	if (soc_is_qca955x()) {
		qca955x_clocks_init();
    }
    else
#elif defined CONFIG_SOC_QCA956X
	if (soc_is_qca956x()) {
		qca956x_clocks_init();
    }
    else
#else
#error "clocks_init () not called"
#endif
    BUG();

	pr_info("Clocks: CPU:%lu.%03luMHz, DDR:%lu.%03luMHz, AHB:%lu.%03luMHz, "
		"Ref:%lu.%03luMHz",
		ath79_cpu_clk.rate / 1000000,
		(ath79_cpu_clk.rate / 1000) % 1000,
		ath79_ddr_clk.rate / 1000000,
		(ath79_ddr_clk.rate / 1000) % 1000,
		ath79_ahb_clk.rate / 1000000,
		(ath79_ahb_clk.rate / 1000) % 1000,
		ath79_ref_clk.rate / 1000000,
		(ath79_ref_clk.rate / 1000) % 1000);
}

/*
 * Linux clock API
 */
struct clk *clk_get(struct device *dev, const char *id)
{
	if (!strcmp(id, "ref"))
		return &ath79_ref_clk;

	if (!strcmp(id, "cpu"))
		return &ath79_cpu_clk;

	if (!strcmp(id, "ddr"))
		return &ath79_ddr_clk;

	if (!strcmp(id, "ahb"))
		return &ath79_ahb_clk;

	if (!strcmp(id, "wdt"))
		return &ath79_wdt_clk;

	if (!strcmp(id, "uart"))
		return &ath79_uart_clk;

	return ERR_PTR(-ENOENT);
}
EXPORT_SYMBOL(clk_get);

int clk_enable(struct clk *clk)
{
	return 0;
}
EXPORT_SYMBOL(clk_enable);

void clk_disable(struct clk *clk)
{
}
EXPORT_SYMBOL(clk_disable);

unsigned long clk_get_rate(struct clk *clk)
{
	return clk->rate;
}
EXPORT_SYMBOL(clk_get_rate);

void clk_put(struct clk *clk)
{
}
EXPORT_SYMBOL(clk_put);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int ath_get_clock(enum _avm_clock_id id) {

    struct clk *clk;

    switch(id) {
        case avm_clock_id_cpu:
            clk = clk_get(NULL, "cpu");
            return clk->rate;
        case avm_clock_id_peripheral:
            clk = clk_get(NULL, "uart");
            return clk->rate;
        case avm_clock_id_ahb:
            clk = clk_get(NULL, "ahb");
            return clk->rate;
        case avm_clock_id_ddr:
            clk = clk_get(NULL, "ddr");
            return clk->rate;
        case avm_clock_id_ref:
            clk = clk_get(NULL, "ref");
            return clk->rate;
        default:
            break;
    }
    return 0;
}
EXPORT_SYMBOL(ath_get_clock);
