/*
 * Copyright (C) 2006 Texas Instruments.
 * Copyright (C) 2007 AVM GmbH
 *
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ----------------------------------------------------------------------------
 *
 */

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/env.h>
#include <linux/root_dev.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>

#include <asm/setup.h>
#include <asm/io.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/plat-ram.h>
#include <../fs/squashfs/squashfs_fs.h>
#include <linux/jffs2.h>

#include <asm/prom.h>

enum _flash_map_enum {
    MAP_UNKNOWN,
    MAP_RAM,
    MAP_NOR_FLASH,
    MAP_NAND_FLASH,
    MAP_SPI_FLASH
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #define ATH_MTD_DEBUG ---*/

#if defined(ATH_MTD_DEBUG)
    #define DEBUG_MTD(fmt, arg...) printk(KERN_ERR "[%d:%s/%d] " fmt "\n", smp_processor_id(), __func__, __LINE__, ##arg);
#else
    #define DEBUG_MTD(fmt, arg...)
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _nmi_vector_location {
    unsigned int firmware_length;
    unsigned int vector_gap;
    char vector_id[32];
};


struct _nmi_vector_location *nmi_vector_location = (struct _nmi_vector_location *)0xbfc00040;
extern void set_nmi_vetor_gap(unsigned int start, unsigned int firmware_size, unsigned int gap_size);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_FLASH_MTD   6
#define JFFS2_MIN_SIZE  6
#define JFFS2_MAX_SIZE  16

/*-------------------------------------------------------------------------------------*\
 * Zuerst wird das JFFS2 gesucht, dann das Squash-FS!
\*-------------------------------------------------------------------------------------*/
static const char *probes[] = { "find_jffs2", "find_squashfs" , NULL };

void ath_ram_mtd_set_rw(struct device *pdev, int);
extern int __init root_dev_setup(char *line);
static unsigned int flash_erase_block_size = 64 << 10;

/*------------------------------------------------------------------------------------------*\
 * NOR
\*------------------------------------------------------------------------------------------*/
static struct mtd_partition ath_partitions[MAX_FLASH_MTD];

static struct resource ath_flash_resource[1] = {
    {
        .start		= 0x1f000000,
        .end		= 0x1f000000 + (16 << 20) - 1,    /* 16 MB */
        .flags		= IORESOURCE_MEM,
    }
};

static struct physmap_flash_data ath_nor_flash = {
	.width		        = 1,
	.parts		        = ath_partitions,
	.nr_parts	        = ARRAY_SIZE(ath_partitions),
	.part_probe_types   = probes,
};


struct platform_device ath_flash_device = {
	.name		= "ath-nor",
	.id		    = 0,
	.dev		= {
		.platform_data	= &ath_nor_flash,
	},
	.num_resources	= 1,
	.resource	= &ath_flash_resource[0],
};


/*------------------------------------------------------------------------------------------*\
 * RAM
\*------------------------------------------------------------------------------------------*/
static struct mtd_partition ath_ram_partitions[2];

static struct resource ath_ram_resource[1] = {
    {   /* für ins RAM geladenes Filesystem */
        .start		= 0,
        .end		= 0 + (32 << 20),
        .flags		= IORESOURCE_MEM,
    }
};

static struct platdata_mtd_ram ath_ram_data = {
	.mapname       = "ram-filesystem",
	.bankwidth	   = 4,
	.partitions    = ath_ram_partitions,
	.nr_partitions = ARRAY_SIZE(ath_ram_partitions),
    .set_rw        = ath_ram_mtd_set_rw,
    .probes        = probes
};

struct platform_device ath_ram_device = {
	.name		= "mtd-ram",
	.id		    = -1,
	.dev		= {
		.platform_data	= &ath_ram_data,
	},
	.num_resources	= 1,
	.resource	= &ath_ram_resource[0],
};


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ath_ram_mtd_set_rw(struct device *pdev, int mode) {
    if(mode == PLATRAM_RO) {
        printk(KERN_ERR "[ath_ram_mtd_set_rw] PLATRAM_RO\n");
    } else if(mode == PLATRAM_RW) {
        printk(KERN_ERR "[ath_ram_mtd_set_rw] PLATRAM_RW\n");
    }
}


   /*-------------------------------------------------------------------------------------*\
    *
    *    +---+---------------------+-----------------------+--------------------+
    *    |   |     Kernel          |     SquashFS          |      JFFS2         |
    *    +---+---------------------+-----------------------+--------------------+
    *        A                     ^_pos                                        E
    *
    *    Zu Beginn ist das Layout obiges:
    *    start_offset = A
    *    MTD1 mit Kernel reicht von A bis E
    *    MTD5 für JFFS2 kann gesetzt sein, wenn JFFS2 Parser vorher schon was gefunden hat
    *
    *    Wenn SquashFS gefunden wird, wird MTD1 auf den Kernel verkleinert,
    *    MTD0 für das FS wird von pos bis E angelegt
    *    Wenn noch kein MTD5 mit JFFS2 existiert wird dieses innerhalb von MTD0 angelegt
    *
   \*-------------------------------------------------------------------------------------*/

static int ath_squashfs_parser_function(struct mtd_info *mtd, 
                                        struct mtd_partition **p_mtd_pat, 
                                        struct mtd_part_parser_data *parse_data) {

    enum _flash_map_enum maptype = MAP_UNKNOWN;
    unsigned count = 1, maxcount = 0;
    
    DEBUG_MTD("mtd->name %s mtd->index %u p_mtd_pat=0x%p", mtd->name, mtd->index, p_mtd_pat);
    
    if (!strcmp(mtd->name, "ram-filesystem")) {
        maptype = MAP_RAM;
    } else if (!strcmp(mtd->name, "ath-nor")) {
        maptype = MAP_NOR_FLASH;
        flash_erase_block_size = mtd->erasesize;
    } else {
        printk(KERN_WARNING "[%s] with unknown mtd type %s\n", __func__, mtd->name);
        return 0;
    }

    if (p_mtd_pat) {
        unsigned int magic = 0, readlen = 0;
        loff_t pos, start_offset; 

        switch (maptype) {
            case MAP_NOR_FLASH:
                *p_mtd_pat = ath_partitions;
                maxcount = ARRAY_SIZE(ath_partitions);
                break;
            case MAP_RAM:
                *p_mtd_pat = ath_ram_partitions;
                maxcount = ARRAY_SIZE(ath_ram_partitions);
                break;
            default:
                break;
        }

        DEBUG_MTD("try partition %s (offset 0x%lx len %lu blocksize=%x) read='%pF'", (*p_mtd_pat)[count].name,
                (unsigned long)((*p_mtd_pat)[count].offset), (unsigned long)((*p_mtd_pat)[count].size), mtd->erasesize, mtd->_read);

        *p_mtd_pat = kmemdup(*p_mtd_pat, maxcount * sizeof(struct mtd_partition), GFP_KERNEL);
        BUG_ON(!*p_mtd_pat);

        start_offset = pos = (*p_mtd_pat)[count].offset;

        DEBUG_MTD("mtd[%s]: start_offset := (*p_mtd_pat)[%d].offset = %lld\n", mtd->name, count, start_offset);

        // Starten mit einer 256-Byte aligned Adresse.
        // Begruendung:
        // Das Squashfs wird 256-Byte aligned. Der Kernel steht davor. Die Startadresse der MTD-RAM-Partition ist also nicht aligned.
        // Der Suchalgorythmus kann also nicht im schlimmsten Fall das Squashfs-Magic nicht finden.
        // pos wird als auf die ersten 256 Byte NACH dem Kernel-Start positioniert.
        if(maptype == MAP_RAM) {
            if((ath_ram_resource[0].start & ((1 << 8) - 1))) {
                pos = ((ath_ram_resource[0].start & ~((1 << 8) - 1)) + 256) - ath_ram_resource[0].start;
                printk(KERN_ERR "[%s:%d] Use offset of 0x%x to search squashfs magic.\n", __func__, __LINE__, (unsigned int)pos);
            }
        }

        while(pos < ((*p_mtd_pat)[1].offset + (*p_mtd_pat)[count].size)) {
            int ret;

            ret = mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&magic);
            if ((ret < 0) || (readlen < sizeof(unsigned int))) {
                printk(KERN_ERR "[%s] breaking due to incomplete or erronous mtd_read\n", __func__);
                return 0;
            }

			if(magic == 0x73717368) {

                const char* hw_revision;

                (*p_mtd_pat)[0].offset = pos;
                (*p_mtd_pat)[0].size	 = (u_int32_t)start_offset + (u_int32_t)(*p_mtd_pat)[1].size - (u_int32_t)pos;
                (*p_mtd_pat)[1].size	 = (u_int32_t)pos - (u_int32_t)start_offset;

#if 0
				if(maptype == MAP_RAM) {
					(*p_mtd_pat)[0].name	 = "rootfs_ram";
				} else {
					(*p_mtd_pat)[0].name	 = "rootfs";
				}
                (*p_mtd_pat)[1].name     = "kernel";
#else
				if(maptype != MAP_RAM) {
					(*p_mtd_pat)[0].name	 = "rootfs";
                    (*p_mtd_pat)[1].name     = "kernel";
                }
#endif
                printk("[%s] magic found @pos 0x%x\n", __func__, (unsigned int)pos);
                hw_revision = prom_getenv("HWRevision");
                if ((hw_revision != NULL) && (strcmp(hw_revision, "180") == 0) && (maptype == MAP_NOR_FLASH) && (memcmp(ath_partitions[5].name, "jffs2", 4) == 0)) {
                    /* JFFS2 vorhanden, auf 6810 prüfen ob verfügbarer Platz > 6 Blöcke und */
                    /* JFFS2 6 Blöcke groß. Dann haben wir eine Box mit fehlerhaft zu kleinem JFFS2. */
                    /* Dann neu anlegen mit min(JFFS2_MAX_SIZE, verfügb. Platz) */

                    u_int32_t   jffs2_size, jffs2_earliest_start;
                    struct squashfs_super_block squashfs_sb;

                    mtd->_read(mtd, (loff_t)pos, sizeof(struct squashfs_super_block), &readlen, (u_char*)&squashfs_sb);
                    jffs2_earliest_start = (u_int32_t)pos + (u_int32_t)squashfs_sb.bytes_used;
                    /*--- printk("squashfs pos: %x\n", (u_int32_t)pos); ---*/
                    /*--- printk("squashfs size: %x\n", (u_int32_t)squashfs_sb.bytes_used); ---*/
                    /*--- printk("jffs2_start (squashfs pos + len) = %x\n", (u_int32_t)jffs2_earliest_start); ---*/
                    if (jffs2_earliest_start & (mtd->erasesize-1)) {
                        /*--- printk("align jffs: start: %x\n", jffs2_earliest_start); ---*/
                        jffs2_earliest_start = (jffs2_earliest_start & ~(mtd->erasesize-1)) + mtd->erasesize;
                    }
                    /*--- printk("jffs2_earliest_start (aligned) = %x\n", jffs2_earliest_start); ---*/
                    jffs2_size = ((*p_mtd_pat)[0].offset + (*p_mtd_pat)[0].size - jffs2_earliest_start) >> 16;
                    /* jffs2_size in 64k Blöcken. Muss ggf. um 1 veringert werden für 128k Block Flash */
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/
                    jffs2_size = jffs2_size & ~((mtd->erasesize / 0x10000)-1);
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/

                    if (jffs2_size > JFFS2_MAX_SIZE) jffs2_size = JFFS2_MAX_SIZE;
                    if (jffs2_size >= (JFFS2_MIN_SIZE * (mtd->erasesize/0x10000))) {
                        u_int32_t current_jffs2_size = ath_partitions[5].size >> 16;
                        if ((current_jffs2_size == 6) && (jffs2_size > 6)) {
                            printk("resize jffs2\n");
                            ath_partitions[5].name = (char *)"reserved";
                        }
                    }
                }
                if ((maptype == MAP_NOR_FLASH) && (memcmp(ath_partitions[5].name, "jffs2", 4) != 0)) {
                    /* JFFS2 nicht gefunden: Wenn jffs2_size gesetzt ist, ggf. verkleinern */
                    /* sonst anlegen mit der verbleibenden Flash Grösse nach Filesystem % 64k */
                    u_int32_t   jffs2_size, jffs2_start, jffs2_earliest_start;
                    struct squashfs_super_block squashfs_sb;

                    mtd->_read(mtd, (loff_t)pos, sizeof(struct squashfs_super_block), &readlen, (u_char*)&squashfs_sb);
                    jffs2_earliest_start = (u_int32_t)pos + (u_int32_t)squashfs_sb.bytes_used;
                    /*--- printk("squashfs pos: %x\n", (u_int32_t)pos); ---*/
                    /*--- printk("squashfs size: %x\n", (u_int32_t)squashfs_sb.bytes_used); ---*/
                    /*--- printk("jffs2_start (squashfs pos + len) = %x\n", (u_int32_t)jffs2_earliest_start); ---*/
                    if (jffs2_earliest_start & (mtd->erasesize-1)) {
                        /*--- printk("align jffs: start: %x\n", jffs2_earliest_start); ---*/
                        jffs2_earliest_start = (jffs2_earliest_start & ~(mtd->erasesize-1)) + mtd->erasesize;
                    }
                    /*--- printk("jffs2_earliest_start (aligned) = %x\n", jffs2_earliest_start); ---*/
                    jffs2_size = ((*p_mtd_pat)[0].offset + (*p_mtd_pat)[0].size - jffs2_earliest_start) >> 16;
                    /* jffs2_size in 64k Blöcken. Muss ggf. um 1 veringert werden für 128k Block Flash */
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/
                    jffs2_size = jffs2_size & ~((mtd->erasesize / 0x10000)-1);
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/

                    if (jffs2_size < (JFFS2_MIN_SIZE * (mtd->erasesize/0x10000))) {
                        printk(KERN_WARNING "[%s]: not enough space for JFFS2!\n", __func__);
                    } else {

                        /*--- Größe auf JFFS2_MAX_SIZE begrenzen ---*/
                        if (jffs2_size > JFFS2_MAX_SIZE) {
                            jffs2_start = jffs2_earliest_start + (jffs2_size - JFFS2_MAX_SIZE) * 0x10000;
                            jffs2_size = JFFS2_MAX_SIZE;
                        } else {
                            jffs2_start = jffs2_earliest_start;
                        }

                        ath_partitions[5].offset = jffs2_start;
                        ath_partitions[5].size   = jffs2_size * 0x10000;
                        ath_partitions[5].name   = "jffs2";
                        printk("[%s] jffs2_start@%x size: %d\n", __func__, jffs2_start, jffs2_size); 
                        {
                            struct erase_info instr;
                            int ret;

                            /*--- printk("erasing jffs2\n"); ---*/
                            memset(&instr, 0, sizeof(instr));
                            instr.mtd = mtd;
                            instr.addr = jffs2_start;
                            instr.len = jffs2_size * 0x10000;
                            instr.callback = NULL;
                            instr.fail_addr = 0xffffffff;

                            ret = mtd->_erase(mtd, &instr);
                            if (ret) {
                                printk(KERN_ERR "jffs mtd erase failed %d\n", ret);
                            }
                        }
                    }
                }
                return maxcount;
            }
            pos += 256;
        }
    }
	return maxcount;
}

#define JFFS_NODES ( JFFS2_NODETYPE_DIRENT | JFFS2_NODETYPE_INODE | JFFS2_NODETYPE_CLEANMARKER | JFFS2_NODETYPE_PADDING | JFFS2_NODETYPE_SUMMARY | JFFS2_NODETYPE_XATTR | JFFS2_NODETYPE_XREF) 
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ath_jffs2_parser_function(struct mtd_info *mtd, struct mtd_partition **p_mtd_pat, struct mtd_part_parser_data *parse_data) {

    unsigned count = 1;
    
    DEBUG_MTD("mtd_info->name %s mtd_info->index %u p_mtd_pat=0x%p", mtd->name, mtd->index, p_mtd_pat);
    
    if (!strcmp(mtd->name, "ram-filesystem")) {
        return 0;   /* nicht im RAM suchen */
    } 
    if (strcmp(mtd->name, "ath-nor")) {
        printk(KERN_WARNING "[%s] with unknown mtd type %s\n", __func__, mtd->name);
        return 0;
    }

    if(p_mtd_pat) {
        unsigned int magic = 0, readlen = 0;
        loff_t pos;
        if(*p_mtd_pat) 
            printk("[%s] *p_mtd_pat->name %s\n",__func__ , (*p_mtd_pat)->name);
        else
            *p_mtd_pat = ath_partitions;

        DEBUG_MTD("try partition %s (offset 0x%lx len %lu)", (*p_mtd_pat)[count].name,
                                                           (unsigned long)((*p_mtd_pat)[count].offset), 
                                                           (unsigned long)((*p_mtd_pat)[count].size));

        pos = (*p_mtd_pat)[count].offset;
        while(pos < (*p_mtd_pat)[count].offset + (*p_mtd_pat)[count].size) {
            mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&magic);
            /*--- printk("[%s] read %u bytes, magic = 0x%08x index %u pos 0x%x\n", __func__, readlen, magic, mtd->index, pos); ---*/
#ifdef __LITTLE_ENDIAN
            if ((((magic >> 16) & ~JFFS_NODES) == 0) && ((magic & 0xFFFF) == JFFS2_MAGIC_BITMASK)) {
#else
            if (((magic >> 16) == JFFS2_MAGIC_BITMASK) && (((magic & 0xFFFF) & ~JFFS_NODES) == 0)) {
#endif
                (*p_mtd_pat)[5].size	 = (*p_mtd_pat)[1].offset + (*p_mtd_pat)[1].size - pos;
                (*p_mtd_pat)[5].offset   = pos;
                (*p_mtd_pat)[5].name	 = "jffs2";
                DEBUG_MTD("magic %04x found @pos 0x%x, size %ld\n", magic, (unsigned int)pos, 
                                        (unsigned long)((*p_mtd_pat)[5].size));
                return 0;
            }
            pos += mtd->erasesize;
        }
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int get_erase_block_size_on_ram_device(struct mtd_info *mtd) {
    unsigned int readlen = 0;
    unsigned int pos = 0;
    unsigned int value1, value2;

    mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value1);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[%s] name=%s pos=0x%x value=0x%x\n", __func__, mtd->name, pos, value1); ---*/

    pos += 0x10000;
    mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[%s] name=%s pos=0x%x value2=0x%x\n", __func__, mtd->name, pos, value2); ---*/

    if(value1 == value2) {
        pos += 0x10000;
        mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
        if(readlen != sizeof(unsigned int))
            return 0;
        /*--- printk("[%s] name=%s pos=0x%x value2=0x%x (check)\n", __func__, mtd->name, pos, value2); ---*/

        if(value1 == value2) {
            DEBUG_MTD("eraseblocksize=0x10000");
            return 0x10000;
        }
        return 0;
    }

    pos += 0x10000;
    mtd->_read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[%s] name=%s pos=0x%x value2=0x%x\n", __func__, mtd->name, pos, value2); ---*/

    if(value1 == value2) {
        DEBUG_MTD("eraseblocksize=0x20000");
        return 0x20000;
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct mtd_part_parser ath_squashfs_parser = {
	.name     = "find_squashfs",
	.parse_fn = ath_squashfs_parser_function
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct mtd_part_parser ath_jffs2_parser = {
	.name     = "find_jffs2",
	.parse_fn = ath_jffs2_parser_function
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
extern int tffs_mtd[2];
extern int tffs_mtd_offset[2];
static int found_rootfs_ram = 0;
char *str_rootfs[] = { "rootfs_ram", "rootfs", "filesystem" };
struct mtd_info * urlader_mtd;

void __init ath_mtd_add_notifier(struct mtd_info *mtd) {
    unsigned int i;

    if(!mtd->name) {
        DEBUG_MTD("Leeres MTD übergeben!");
        return;
    }
    DEBUG_MTD("name %s" , mtd->name);

    for (i = 0; i < sizeof(str_rootfs) / sizeof(char*) ; i++) {
        if (!strcmp(mtd->name, str_rootfs[i])) {
            static char root_device[64];

            DEBUG_MTD("found %s", mtd->name);
            if (found_rootfs_ram) {      /*--- we found a rootfs in RAM and use only this ---*/
				DEBUG_MTD("found %s but prefer rootfs from RAM", mtd->name);
                return;
			}
            if (!strcmp(mtd->name, str_rootfs[0]))
                found_rootfs_ram = 1;   /*--- signal that we found a rootfs in RAM ---*/

	        sprintf(root_device, "/dev/mtdblock%d", mtd->index);
	        DEBUG_MTD("mtd[%s] is root device: %s", mtd->name, root_device);
            root_dev_setup(root_device);
            return;
        }
    }

    if(!strcmp(mtd->name, "urlader")) {
        DEBUG_MTD("set urlader_mtd");
        urlader_mtd = mtd;
#if defined(CONFIG_TFFS)
    } else if(!strcmp(mtd->name, "tffs (1)")) {
        tffs_mtd[0] = mtd->index;
    } else if(!strcmp(mtd->name, "tffs (2)")) {
        tffs_mtd[1] = mtd->index;
#endif /*--- #if defined(CONFIG_TFFS) ---*/
    } else {
        DEBUG_MTD("skip %s" , mtd->name);
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init ath_mtd_rm_notifier(struct mtd_info *mtd) {
    printk("[ath_mtd_rm_notifier] ignore %s\n", mtd->name);
}

struct mtd_notifier __initdata ath_mtd_notifier = {
    add: ath_mtd_add_notifier,
    remove: ath_mtd_rm_notifier
};

/*------------------------------------------------------------------------------------------*\
 * Parst die erste Größe in einem Größenangaben String vom Urlader
 * Format der Größenangaben: xxx_size=<nn>{,KB,MB}
\*------------------------------------------------------------------------------------------*/
unsigned long long parse_mtd_size(char *p) {
    unsigned long long size;

    if((p[0] == '0') && (p[1] == 'x')) {
        size = simple_strtoul(p, NULL, 16);
    } else {
        size = simple_strtoul(p, NULL, 10);
    }
    
    p = strchr(p, 'B');
    if(p) {
        /*--- Die Größe enthält mindestens eine KB Angabe ---*/
        if(p[-1] == 'K')  {
            size *= 1024;
        } else if(p[-1] == 'M')  {
            size *= (1024 * 1024);
        }
    }
	DEBUG_MTD("[%s] %Ld\n", __func__,  size);

    return size; 
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void find_nmi_vector(void) {
    unsigned int len;
    if(strcmp(nmi_vector_location->vector_id, "NMI Boot Vector")) {
        printk(KERN_ERR "[%s] no nmi vector found\n",  __func__);
        return;
    }
    len  = (nmi_vector_location->firmware_length + flash_erase_block_size) & ~(flash_erase_block_size  - 1);
    printk(KERN_ERR "[%s] nmi vector found. Firmware length 0x%x bytes (erase block align 0x%x) vector gap size 0x%x bytes.\n", 
            __func__, nmi_vector_location->firmware_length, len, nmi_vector_location->vector_gap);
    len += ath_partitions[2].size;  /*--- urlader size ---*/
    printk(KERN_ERR "[%s] add '%s' size 0x%llx to length\n", __func__, ath_partitions[2].name, ath_partitions[2].size);
    
    set_nmi_vetor_gap(0xbfc00000, len, nmi_vector_location->vector_gap);
}


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void find_nmi_vector_gap(unsigned int base, unsigned int end) {
    unsigned int len;
    struct _nmi_vector_location *first_loc = (struct _nmi_vector_location *)(base + 0x40);
    struct _nmi_vector_location *last_loc = (struct _nmi_vector_location *)(base + 0xbe0040);
    while((unsigned long)first_loc <= (unsigned long)last_loc) {
        if((unsigned int)first_loc >= end)
            break;
        /*--- if(((unsigned long)first_loc > 0x87d95a00UL) && ((unsigned long)first_loc < 0x87d96000UL)) ---*/
            /*--- printk(KERN_ERR "[NMI] %p => %10pB\n", first_loc, first_loc); ---*/
        if(!strcmp(first_loc->vector_id, "NMI Boot Vector")) {

            len = end - ((unsigned int)first_loc - 0x40) + first_loc->vector_gap;

            printk(KERN_ERR "[%s] nmi vector found. Firmware length 0x%x bytes (move length 0x%x) vector gap size 0x%x bytes.\n", 
                 __func__, first_loc->firmware_length, len, first_loc->vector_gap);

            memmove((void *)((unsigned int)first_loc - 0x40), 
                    (void *)((unsigned int)first_loc + first_loc->vector_gap - 0x40), 
                    len);
            return;
        }
        first_loc = (struct _nmi_vector_location *)((unsigned int)first_loc + 256);
    }
    printk(KERN_ERR "[%s] no nmi vector found at base %#x\n",  __func__, base);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct platform_device *ath_platform_devices[12];
unsigned int ath_platform_devices_count = 0;

void add_to_platform_device_list(struct platform_device *device, char *name __attribute__ ((unused))) {
    DEBUG_MTD("[%s] %s: add %s to the platform device list (to be registered)\n", __func__, name, name);
    ath_platform_devices[ath_platform_devices_count++] = device;
}

/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
void ath_init_platform_devices(void) {
	DEBUG_MTD("[%s] registering %d platform device(s)\n", __func__, ath_platform_devices_count);
	platform_add_devices(ath_platform_devices, ath_platform_devices_count);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init mtdflash_setup(char *p);


int __init ath_mtd_init(void) {

    ath_init_platform_devices();
    register_mtd_parser(&ath_squashfs_parser);
    register_mtd_parser(&ath_jffs2_parser);
    register_mtd_user(&ath_mtd_notifier);
    find_nmi_vector();
    return 0;
}
subsys_initcall(ath_mtd_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init mtdflash_setup(char *p) {
    unsigned long flashsize;
    unsigned long mtd_start, mtd_end;
    unsigned long flashoffset = 0;

    if(!p)
        return 0;

    ath_partitions[0].name = (char *)"filesystem";
    ath_partitions[1].name = (char *)"kernel";
    ath_partitions[2].name = (char *)"urlader";
    ath_partitions[3].name = (char *)"tffs (1)";
    ath_partitions[4].name = (char *)"tffs (2)";
    ath_partitions[5].name = (char *)"reserved";

    flashsize = parse_mtd_size(p);

    /*--------------------------------------------------------------------------------------*\
     * Größen ermitteln
    \*--------------------------------------------------------------------------------------*/
    p = prom_getenv("mtd2");
    if(p) {
        DEBUG_MTD("mtd2 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16) - 1);
            flashoffset = mtd_start;
            ath_partitions[2].size       = mtd_end - mtd_start + 1;
            ath_partitions[2].offset     = mtd_start - flashoffset;
            ath_flash_resource[0].start = mtd_start;
            ath_flash_resource[0].end   = mtd_start + flashsize;
        }
    }
    p = prom_getenv("mtd0");
    if(p) {
        DEBUG_MTD("mtd0 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16) - 1);
            ath_partitions[0].size       = mtd_end - mtd_start + 1;
            ath_partitions[0].offset     = mtd_start - flashoffset;
        }
    }
    p = prom_getenv("mtd1");
    if(p) {
        DEBUG_MTD("mtd1 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16) - 1);
            ath_partitions[1].size       = mtd_end - mtd_start + 1;
            ath_partitions[1].offset     = mtd_start - flashoffset;
        }
    }
    p = prom_getenv("mtd3");
    if(p) {
        DEBUG_MTD("mtd3 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16) - 1);
            ath_partitions[3].size       = mtd_end - mtd_start + 1;
            ath_partitions[3].offset     = mtd_start - flashoffset;
        }
    }
    p = prom_getenv("mtd4");
    if(p) {
        DEBUG_MTD("mtd4 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16) - 1);
            ath_partitions[4].size       = mtd_end - mtd_start + 1;
            ath_partitions[4].offset     = mtd_start - flashoffset;
        }
    }
    p = prom_getenv("mtd5");
    if(p) {
        DEBUG_MTD("mtd5 = %s", p);
        mtd_start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        p = strchr(p, ',');
        if(p) {
            p++;
            mtd_end  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
            ath_partitions[5].size       = mtd_end - mtd_start + 1;
            ath_partitions[5].offset     = mtd_start - flashoffset;
        }
    }
    add_to_platform_device_list(&ath_flash_device, "ath-nor");
    return 0;
}
__setup("sflash_size=", mtdflash_setup);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init mtdram_setup(char *p) {
    char *start;
    DEBUG_MTD("str=\"%s\"", p);
    if(p) {
		start = prom_getenv("linux_fs_start");
	    if(start && !strcmp(start, "nfs")) {
		    DEBUG_MTD("dont use RAM filesystem, use NFS");
			return 0;
	    }

        DEBUG_MTD("mtdram1 %s", p);
        ath_ram_resource[0].start  = CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
        ath_ram_resource[0].flags  = IORESOURCE_MEM,
        p = strchr(p, ',');
        if(p) {
            p++;
            ath_ram_resource[0].end  =CPHYSADDR((unsigned int)simple_strtoul(p, NULL, 16));
            ath_ram_resource[0].end -= 1;
        } else {
            ath_ram_resource[0].start = 0;
        }
        DEBUG_MTD("mtdram1 0x%08x - 0x%08x", ath_flash_resource[0].start, ath_flash_resource[0].end );
        ath_ram_partitions[0].name		 = "rootfs_ram";
        ath_ram_partitions[0].offset	 = 0;
        ath_ram_partitions[0].size		 = ath_ram_resource[0].end - ath_ram_resource[0].start + 1;
        ath_ram_partitions[0].mask_flags = MTD_ROM;
        ath_ram_partitions[1].name		 = "kernel_ram";
        ath_ram_partitions[1].offset	 = 0;
        ath_ram_partitions[1].size		 = ath_ram_resource[0].end - ath_ram_resource[0].start + 1;
        ath_ram_partitions[1].mask_flags = MTD_ROM;
        ath_ram_data.nr_partitions = 2;
    }

    add_to_platform_device_list(&ath_ram_device, "mtd-ram");
    find_nmi_vector_gap(ath_ram_resource[0].start | 0x80000000, ath_ram_resource[0].end | 0x80000000);
    return 0;
}
__setup("mtdram1=", mtdram_setup);


