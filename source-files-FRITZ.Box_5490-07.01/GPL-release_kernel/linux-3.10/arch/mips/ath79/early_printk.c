/*
 *  Atheros AR7XXX/AR9XXX SoC early printk support
 *
 *  Copyright (C) 2008-2011 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2008 Imre Kaloz <kaloz@openwrt.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/io.h>
#include <linux/errno.h>
#include <linux/serial_reg.h>
#include <linux/clk.h>
#include <asm/addrspace.h>

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include <asm/mach-ath79/ar933x_uart.h>
#include <asm/mach-ath79/atheros.h>

static void (*_prom_putchar) (unsigned char);

static inline void prom_putchar_wait(void __iomem *reg, u32 mask, u32 val)
{
	u32 t;

	do {
		t = __raw_readl(reg);
		if ((t & mask) == val)
			break;
	} while (1);
}

#if (defined CONFIG_SOC_AR71XX || defined CONFIG_SOC_AR724X || defined CONFIG_SOC_AR913X || defined CONFIG_SOC_AR934X || defined CONFIG_SOC_QCA955X)
#define BOTH_EMPTY (UART_LSR_TEMT | UART_LSR_THRE)

static void prom_putchar_ar71xx(unsigned char ch)
{
	void __iomem *base = (void __iomem *)(KSEG1ADDR(ATH_UART_BASE));

	prom_putchar_wait(base + UART_LSR * 4, BOTH_EMPTY, BOTH_EMPTY);
	__raw_writel(ch, base + UART_TX * 4);
	prom_putchar_wait(base + UART_LSR * 4, BOTH_EMPTY, BOTH_EMPTY);
}
#endif

#if defined CONFIG_SOC_AR933X
static void prom_putchar_ar933x(unsigned char ch)
{
	void __iomem *base = (void __iomem *)(KSEG1ADDR(AR933X_UART_BASE));

	prom_putchar_wait(base + AR933X_UART_DATA_REG, AR933X_UART_DATA_TX_CSR,
			  AR933X_UART_DATA_TX_CSR);
	__raw_writel(AR933X_UART_DATA_TX_CSR | ch, base + AR933X_UART_DATA_REG);
	prom_putchar_wait(base + AR933X_UART_DATA_REG, AR933X_UART_DATA_TX_CSR,
			  AR933X_UART_DATA_TX_CSR);
}
#endif

static void prom_putchar_dummy(unsigned char ch)
{
	/* nothing to do */
}

static void prom_putchar_init(void)
{
	void __iomem *base;
	u32 id;

	base = (void __iomem *)(KSEG1ADDR(ATH_RESET_BASE));
	id = __raw_readl(base + ATH_RESET_REG_REV_ID);
	id &= ATH_RESET_REVISION_ID_MAJOR_MASK;

	switch (id) {
#if (defined CONFIG_SOC_AR71XX || defined CONFIG_SOC_AR724X || defined CONFIG_SOC_AR913X || defined CONFIG_SOC_AR934X || defined CONFIG_SOC_QCA955X)
	case REV_ID_MAJOR_AR71XX:
	case REV_ID_MAJOR_AR7240:
	case REV_ID_MAJOR_AR7241:
	case REV_ID_MAJOR_AR7242:
	case REV_ID_MAJOR_AR913X:
	case REV_ID_MAJOR_AR9341:
	case REV_ID_MAJOR_AR9342:
	case REV_ID_MAJOR_AR9344:
	case REV_ID_MAJOR_QCA9556:
	case REV_ID_MAJOR_QCA9558:
		_prom_putchar = prom_putchar_ar71xx;
		break;
#endif

#if defined CONFIG_SOC_AR933X
	case REV_ID_MAJOR_AR9330:
	case REV_ID_MAJOR_AR9331:
		_prom_putchar = prom_putchar_ar933x;
		break;
#endif        

	default:
		_prom_putchar = prom_putchar_dummy;
		break;
	}
}

void prom_putchar(unsigned char ch) {

	if (!_prom_putchar)
		prom_putchar_init();

	_prom_putchar(ch);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
extern int vsprintf(char *buf, const char *fmt, va_list args);
static char sprint_buf[1024];

void writeserial(char *str, int count) {

	int i;
	for (i = 0; i < count; i++)
		prom_putchar(str[i]);

	prom_putchar('\r');
	memset(str, '\0', 1024);
	return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void serial_print(const char *fmt, ...)
{
	va_list args;
	int n;

	va_start(args, fmt);
	n = vsprintf(sprint_buf, fmt, args);
	va_end(args);
	writeserial(sprint_buf, n);
}


