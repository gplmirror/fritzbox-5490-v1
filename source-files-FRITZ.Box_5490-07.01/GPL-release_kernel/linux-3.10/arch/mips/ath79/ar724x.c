/*
 * Qualcomm Atheros AP136 reference board support
 *
 * Copyright (c) 2012 Qualcomm Atheros
 * Copyright (c) 2012-2013 Gabor Juhos <juhosg@openwrt.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <linux/pci.h>
#include <linux/delay.h>

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>
#include <atheros.h>
#include <atheros_gpio.h>
#include <linux/avm_hw_config.h>

#include "dev-spi.h"
#include "pci.h"

static struct spi_board_info ar724x_spi_info[] = {
	{
		.bus_num	    = 0,
		.chip_select	= 0,
		.max_speed_hz	= 25000000,
		.modalias	    = "mx25l6405d",
	}
};

static struct ath79_spi_platform_data ar724x_spi_data = {
	.bus_num	= 0,
	.num_chipselect	= 1,
};

#ifdef CONFIG_PCI
static __init void ar724x_pci_pll_init(void) {

	/*----------------------------------------------------------------------*\
	 * Initialize PCIE PLL and get it out of RESET
	 * reg = 0x18050010
	\*----------------------------------------------------------------------*/
   	unsigned int pcie_pll_div = 5; /*--- MULT ---*/
	unsigned int pcie_pll_revdif = 2;

    ath_reg_wr(AR71XX_PLL_BASE + AR724X_PLL_PCIE_PLL_CONFIG, (0x02050000 | pcie_pll_div | (pcie_pll_revdif << 10)));
    mdelay(100);
    ath_reg_wr(AR71XX_PLL_BASE + AR724X_PLL_PCIE_PLL_CONFIG, (0x00050000 | pcie_pll_div | (pcie_pll_revdif << 10)));
    mdelay(100);

    ath_reg_wr(AR71XX_PLL_BASE + AR724X_PLL_PCIE_PLL_CONFIG, (0x00040000 | pcie_pll_div | (pcie_pll_revdif << 10)));
    mdelay(100);
}
#endif /* CONFIG_PCI */

void __init ar724x_setup(void) {

	ath79_register_spi(&ar724x_spi_data, ar724x_spi_info, ARRAY_SIZE(ar724x_spi_info));
	/*--- ath79_register_wmac(art + AP136_WMAC_CALDATA_OFFSET); ---*/
#ifdef CONFIG_PCI
	ar724x_pci_pll_init();
	ath79_register_pci();
#endif
}


