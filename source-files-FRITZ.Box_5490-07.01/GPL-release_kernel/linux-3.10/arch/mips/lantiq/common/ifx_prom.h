#ifndef _ifx_prom_h_
#define _ifx_prom_h_
/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 *
 *  Copyright (c) 2009 Infineon Technologies AG, Xu Liang
 *  Copyright (c) 2009-2013 AVM GmbH
 */

#include <board.h>

#ifdef CONFIG_IFX_ASC_CONSOLE_ASC0
#define IFX_CONSOLE_ASC_FSTAT     IFX_ASC0_FSTAT
#define IFX_CONSOLE_ASC_TBUF      IFX_ASC0_TBUF
#else /* !CONFIG_IFX_ASC_CONSOLE_ASC0 */
#define IFX_CONSOLE_ASC_FSTAT     IFX_ASC1_FSTAT
#define IFX_CONSOLE_ASC_TBUF      IFX_ASC1_TBUF
#endif /* CONFIG_IFX_ASC_CONSOLE_ASC0 */
#define ASCFSTAT_TXFFLMASK          0x3F00

#endif
