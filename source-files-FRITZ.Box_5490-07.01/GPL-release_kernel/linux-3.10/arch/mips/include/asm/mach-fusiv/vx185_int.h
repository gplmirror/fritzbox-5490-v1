/**
 * Copyright (C) 2010-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */

#ifndef _VX185_INT_H_
#define _VX185_INT_H_

#include <linux/irq.h>

/* If it is a dual core image */
#ifdef CONFIG_FUSIV_MIPS_DUALCORE
#ifdef CONFIG_FUSIV_MIPS_CMIPS_CORE
#define VX185_IPC_BASE_ADDR KSEG1ADDR(0xB9048000)
#else
#define VX185_IPC_BASE_ADDR KSEG1ADDR(0xB9040000)
#endif
#else	/* If it is a single core image */
#define VX185_IPC_BASE_ADDR KSEG1ADDR(0xB9040000)
#endif

typedef struct {
        unsigned long   src_ctrl[8];   		/* Source control/enable */
        unsigned long   dest_select[8];   	/* Destination select */
        unsigned long   status_low[16];   	/* Interrupt status - low */
        unsigned long   status_hi[16];   	/* Interrupt status - hi */
} ipc_t;

typedef struct ipc_src_map {
	unsigned char	reserved:1,		/* Reserved */
			trigger_type:1,		/* 0: Level, 1: Edge */
			sync_enable:1,		/* 0: Sync bypass, 1: Sync enable */
			polarity:1,		/* 0: Active low, 1: Active high */
			ipc_dst_line:4;		/* IPC destination line (0-6) */
} ipc_src_map_t;

/* MIPS HW interrupt line number used by periodic timer */
#define PERIODIC_TIMER_INT_PRIORITY 5

/* Priority for Mailbox interrupt, dst line num  = (priority + 1)*/
#define MAILBOX_INT_PRIORITY    3

#if defined (CONFIG_FUSIV_VX185_PALLADIUM)
#define MIPS_CORE_CLK_FREQ     CONFIG_VX185_PALLADIUM_CORE_CLOCK
#define UART_DIVISOR		1	
#endif

#if defined CONFIG_FUSIV_VX585_PALLADIUM
#define MIPS_CORE_CLK_FREQ     CONFIG_VX585_PALLADIUM_CORE_CLOCK
#define UART_DIVISOR		1	
#endif /* CONFIG_FUSIV_VX585_PALLADIUM */

//#if defined(CONFIG_FUSIV_VX185_FPGA) || defined(CONFIG_FUSIV_VX585_FPGA)
#define MIPS_CORE_CLK_FREQ      500*1000*1000
#define UART_DIVISOR		    108
#define UART_BAUD_RATE		    115200
#define BOARD_CRYSTAL_FREQ      25*1000*1000
//#endif

/* VX185 IPC bit defines */
#define IPC_SRC_ACT_HI_DETECT	0x1
#define IPC_SRC_SYNC_ENABLE	0x2
#define IPC_SRC_EDGE_DETECT 	0x4
#define IPC_SRC_ENABLE		0x8
#define IPC_SRC_CTRL_MASK	(IPC_SRC_ACT_HI_DETECT | IPC_SRC_SYNC_ENABLE | \
				IPC_SRC_EDGE_DETECT | IPC_SRC_ENABLE)

#define IPC_DEFAULT_DEST_LINE	0xF

/* IPC Interrupt numbers. */

/* 0 - 7 */
#define VPE0_SW_0_INT   	0
#define VPE0_SW_1_INT   	1
#define VPE0_PCI_0_INT  	2
#define VPE0_MIPS_TIMER_INT	3
#define VPE1_SW_0_INT   	4
#define VPE1_SW_1_INT   	5
#define VPE1_PCI_1_INT  	6
#define VPE1_MIPS_TIMER_INT	7

/* 8 - 15 */
#define MAILBOX_INT		8
#define WDT_INT			9
#define GPT0_INT        	10
#define GPT1_INT        	11
#define GPT2_INT        	12
#define GPT3_INT        	13
#define IDMA2_INT		14
#define IDMA1_INT		15

/* 16 - 23 */
#define DSP0_FL0_INT		16
#define DSP0_FL1_INT		17
#define DSP0_PF0_INT		18
#define DSP0_PF1_INT		19
#define DSP1_FL0_INT		20
#define DSP1_FL1_INT		21
#define DSP1_PF0_INT		22
#define DSP1_PF1_INT		23

/* 24 - 31 */
#define MIPS_SPORT_TX_INT	24
#define MIPS_SPORT_RX_INT	25
#define SPI0_INT		26
#define DMA0_INT		27
#define UART1_INT		28
#define UART2_INT		29
#define NAND_FLASH_INT		30
#define SATA_INT		31

/* 32 - 39 */
#ifdef CONFIG_FUSIV_VX585
#define PCIe0_INT		55
#define PCIe1_INT		56
#else
#define PCIe0_INT		32
#define PCIe1_INT		33
#endif

#define USBO_INT		34
#define USBE_INT		35
#define GIGE0_APU_INT		36
#define GIGE1_APU_INT		37
#define VAP_APU_INT		38
#define CLS_APU1_INT		39

/* 40 - 47 */
#define CLS_APU2_INT		40
#define BMU_APU_INT		41
#define SPA_APU_INT		42
#define PERIAPU_INT		43
#define HAPU_INT		44
#define KHZ_4_SYNC_SYMBOL_INT	45
#define KHZ_8_SYNC_SYMBOL_INT	46
#define KHZ_12_SYNC_SYMBOL_INT	47

/* 48 - 55 */
#define BME_GPT_INT		48
#define FMC_INT			49
#define RMC_INT			50
#define DMC_INT			51
#define BME_DMA_INT		52
#define PIO_TIMEOUT_INT		53
#define DMA1_INT		54
#define SPI1_INT		55

/* 56 - 63 */
#define GPIO24_INT		56
#define GPIO25_INT		57
#define GPIO26_INT		58
#define GPIO27_INT		59
#define GPIO28_INT		60
#define GPIO29_INT		61
#define GPIO30_INT		62
#define GPIO31_INT		63

#define VX185_MAX_INTS       	64

#define MIPS_TIMER_INT		VPE0_MIPS_TIMER_INT

/* MIPS CPU interrupt numbers */
#define MIPSCPU_INT_SW0		0	/* Software Interrupts 0-1 */
#define MIPSCPU_INT_SW1		1
#define MIPSCPU_INT_HW0		2	/* Hardware Interrupts 2-7 */
#define MIPSCPU_INT_HW1		3
#define MIPSCPU_INT_HW2		4
#define MIPSCPU_INT_HW3		5
#define MIPSCPU_INT_HW4		6
#define MIPSCPU_INT_HW5		7

#ifdef CONFIG_SMP
#define MIPS_VPE1_OFFSET        7
#define MAX_INTERRUPT_LINES_PERVPE  6
#else
#define MIPS_VPE1_OFFSET        0
#endif

/* MIPS interrupt line mapping */
#ifdef CONFIG_SMP
#define FUSIV_MIPS_INT_BMU       MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_MAILBOX	 MIPSCPU_INT_HW2
#define FUSIV_MIPS_INT_CLSAP1    MIPSCPU_INT_HW4
#define FUSIV_MIPS_INT_CLSAP2    MIPSCPU_INT_HW4
#define FUSIV_MIPS_INT_SATA      MIPSCPU_INT_HW2
#define FUSIV_MIPS_INT_PCIE1     MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_DSP0FL0   MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_DSP0FL1   MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_DSP1FL0   MIPSCPU_INT_HW4
#define FUSIV_MIPS_INT_DSP1FL1   MIPSCPU_INT_HW4
#define FUSIV_MIPS_INT_DSP0PF0   MIPSCPU_INT_HW1
#else
#define FUSIV_MIPS_INT_MAILBOX	 MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_BMU       MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_CLSAP1    MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_CLSAP2    MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_SATA      MIPSCPU_INT_HW3 
#define FUSIV_MIPS_INT_PCIE1     MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_DSP0FL0   MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_DSP0FL1   MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_DSP1FL0   MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_DSP1FL1   MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_DSP0PF0   MIPSCPU_INT_HW3
#endif
#define FUSIV_MIPS_INT_PIT       MIPSCPU_INT_HW5
#define	FUSIV_MIPS_INT_GPT1	     MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_SERIAL    MIPSCPU_INT_HW0
#define FUSIV_MIPS_INT_PCIE0     MIPSCPU_INT_HW2
#define FUSIV_MIPS_INT_USBO      MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_USBE      MIPSCPU_INT_HW1
#define FUSIV_MIPS_INT_GIGE0     MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_GIGE1     MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_VAP       MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_SPA       MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_PERIAP    MIPSCPU_INT_HW3
#define FUSIV_MIPS_INT_SPI1      MIPSCPU_INT_HW4

/* GPIO 29 is used as interrupt for CMIPS from classifier in fast path */
#ifdef CONFIG_FUSIV_MIPS_CMIPS_CORE
#define FUSIV_MIPS_INT_GPIO29    MIPSCPU_INT_HW3 
#endif

/* IPC destination line mapping */
#define FUSIV_IPC_DEST_PIT       (FUSIV_MIPS_INT_PIT - 1)
#define FUSIV_IPC_DEST_MAILBOX	 (FUSIV_MIPS_INT_MAILBOX - 1) + MIPS_VPE1_OFFSET
#define FUSIV_IPC_DEST_GPT1   	 (FUSIV_MIPS_INT_GPT1 - 1)
#define FUSIV_IPC_DEST_DSP0FL0   (FUSIV_MIPS_INT_DSP0FL0 - 1)  
#define FUSIV_IPC_DEST_DSP0FL1   (FUSIV_MIPS_INT_DSP0FL1 - 1)
#define FUSIV_IPC_DEST_DSP1FL0   (FUSIV_MIPS_INT_DSP1FL0 - 1)
#define FUSIV_IPC_DEST_DSP1FL1   (FUSIV_MIPS_INT_DSP1FL1 - 1)
#define FUSIV_IPC_DEST_DSP0PF0   (FUSIV_MIPS_INT_DSP0PF0 - 1)  
#define FUSIV_IPC_DEST_SERIAL    (FUSIV_MIPS_INT_SERIAL - 1)
#define FUSIV_IPC_DEST_SATA      (FUSIV_MIPS_INT_SATA - 1) + MIPS_VPE1_OFFSET

/* Make sure to route both PCIE0 & PCIE1 to the same VPE */
#define FUSIV_IPC_DEST_PCIE0     (FUSIV_MIPS_INT_PCIE0 - 1)
#define FUSIV_IPC_DEST_PCIE1     (FUSIV_MIPS_INT_PCIE1 - 1)

#define FUSIV_IPC_DEST_USBO      (FUSIV_MIPS_INT_USBO - 1) + MIPS_VPE1_OFFSET
#define FUSIV_IPC_DEST_USBE      (FUSIV_MIPS_INT_USBE - 1) + MIPS_VPE1_OFFSET
#define FUSIV_IPC_DEST_GIGE0     (FUSIV_MIPS_INT_GIGE0 - 1)
#define FUSIV_IPC_DEST_GIGE1     (FUSIV_MIPS_INT_GIGE1 - 1)
#define FUSIV_IPC_DEST_VAP       (FUSIV_MIPS_INT_VAP - 1) + MIPS_VPE1_OFFSET
#define FUSIV_IPC_DEST_CLSAP1    (FUSIV_MIPS_INT_CLSAP1 - 1)
#define FUSIV_IPC_DEST_CLSAP2    (FUSIV_MIPS_INT_CLSAP2 - 1)
#define FUSIV_IPC_DEST_BMU       (FUSIV_MIPS_INT_BMU - 1)
#define FUSIV_IPC_DEST_SPA       (FUSIV_MIPS_INT_SPA - 1) + MIPS_VPE1_OFFSET
#define FUSIV_IPC_DEST_PERIAP    (FUSIV_MIPS_INT_PERIAP - 1)
#ifdef CONFIG_SMP
#define FUSIV_IPC_DEST_PIT2      (FUSIV_MIPS_INT_PIT - 1) + MIPS_VPE1_OFFSET
#else
#define FUSIV_IPC_DEST_PIT2      (FUSIV_IPC_DEST_DEFAULT) 
#endif
#define FUSIV_IPC_DEST_SPI1      (FUSIV_MIPS_INT_SPI1 - 1)
/* GPIO 29 is used as interrupt for CMIPS from classifier in fast path */
#ifdef CONFIG_FUSIV_MIPS_CMIPS_CORE
#define FUSIV_IPC_DEST_GPIO29    (FUSIV_MIPS_INT_GPIO29 - 1)
#endif

/* Route all sources by default to IPC dest line 15 which is unconncted */
#define	FUSIV_IPC_DEST_DEFAULT	 0xF
/* Last IPC dest line routed to vpe0. Dest line 0 - 6 connected to vpe0 & 7 to 13 to vpe1. */
#define FUSIV_IPC_DEST_LAST_VPE0    6   

#if defined (CONFIG_CPU_MIPSR2_IRQ_VI)
/*
 * Following structure should be passed by all drivers at the time of
 * request_irq. The ipc_irq must have IPC source value. The priv can be used
 * to point to respective drivers structures.
 */
typedef struct fusiv_irq_params_s
{
	unsigned char	ipc_src;
	void		*priv;
}fusiv_irq_params_t;

extern unsigned int status_ipc_src(unsigned int irq_nr);
#endif //CONFIG_CPU_MIPSR2_IRQ_VI

extern unsigned int startup_ipc_src(struct irq_data *);
extern void shutdown_ipc_src(struct irq_data *);

#if (FUSIV_MIPS_INT_PCIE0 == FUSIV_MIPS_INT_PCIE1)
There will be a conflict at atheros driver to distiguish between wlan0 and wlan1
#endif

#ifdef CONFIG_SMP
/* Find the Current VPE ID */
#define get_current_vpe()   \
           ((read_c0_tcbind() >> TCBIND_CURVPE_SHIFT) & TCBIND_CURVPE)

/* Get the VPE ID on which an interrupt is routed.             *
 * 'x - IPC Dest line;y - output param representing the vpeid' */
#define GET_INTERRUPTAFFINED_VPEID(x,y) (x >= MAX_INTERRUPT_LINES_PERVPE)? (y = 1): (y = 0)
#endif

#endif /* _VX185_INT_H_ */
