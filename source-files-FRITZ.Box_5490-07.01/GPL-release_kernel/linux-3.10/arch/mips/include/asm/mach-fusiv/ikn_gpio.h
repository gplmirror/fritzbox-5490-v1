/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_GPIO_H__

#define __IKN_GPIO_H__
#include "ikn_types.h"
#include "ikn_gpio_regs.h"

eIKNReturn_t  iknGPIOSetMode(eGPIOPins_t eGPIOPin, eGPIOMode_t eGPIOMode);
eGPIOBitVal_t iknGPIOGetInputVal(eGPIOPins_t eGPIOPin);
eIKNReturn_t  iknGPIOSetOutput(eGPIOPins_t eGPIOPin,eGPIOBitVal_t eGPIOBitVal);  
//eIKNReturn_t  iknGPIOClearOutput(eGPIOPins_t eGPIOPin);  
eIKNReturn_t  iknGPIOAltFuncSel(eGPIOPins_t eGPIOPin,eGPIOBitVal_t eGPIOBitVal);
eIKNReturn_t  iknGPIODefAltFuncVal(eGPIOPins_t eGPIOPin,eGPIOBitVal_t eGPIOBitVal);

#endif   //__IKN_GPIO_H__
