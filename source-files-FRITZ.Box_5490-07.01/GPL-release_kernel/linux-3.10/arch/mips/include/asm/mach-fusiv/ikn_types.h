/**
 * Copyright (C) 2006-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 */
#ifndef __IKN_TYPES_H__

#define __IKN_TYPES_H__

typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;

typedef unsigned char UCHAR;
typedef char CHAR;
typedef int  INT32;
typedef short INT16;
typedef char INT8;

typedef void VOID;

typedef unsigned short volatile     reg16Bit;
typedef unsigned int   volatile     reg32Bit;

#ifndef NULL
#define NULL    ((void *)0)
#endif

typedef enum
{
    eRET_OK = 0,
    eRET_ERROR,
    eRET_INVALID_PARAM,
    eRET_TX_FULL,
    eRET_TX_SUCCESS,
    eRET_RX_EMPTY,
    eRET_RX_SUCCESS,
    eRET_MOD_DISABLED
}eIKNReturn_t; 
#endif //__IKN_TYPES_H__
