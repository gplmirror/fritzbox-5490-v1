/**
 * Copyright (C) 2013-2014 Ikanos Communications.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation. 
 * 
 * Info : This file implements simple HAL interface to 
 *        Fusiv Vx185 PCIE RC Core
 */
#ifndef _FUSIVVX185PCI_H_
#define _FUSIVVX185PCI_H_

#define FUSIV_PCIE_ADDR(port,reg)	(FUSIV_PCIE ## port ## _BASE + reg ## _OFFSET )

#define CFGADDR(busnum,devfn,where)	((busnum << 24) | (devfn << 16) | (where & ~3))

#define WRREG32(b, v) 			*(volatile unsigned int *)(b) = (v)
#define RDREG32(b) 			*(volatile unsigned int *)(b)

#define RDCFG32(port,addr,data) 	{ \
						WRREG32(FUSIV_PCIE_ADDR(port,CFG_ADDR),addr); \
						data = RDREG32(FUSIV_PCIE_ADDR(port,CFG_DATA_TYPE0)); \
					}

#define WRCFG32(port,addr,data)		{ \
						WRREG32(FUSIV_PCIE_ADDR(port,CFG_ADDR),addr); \
						WRREG32(FUSIV_PCIE_ADDR(port,CFG_DATA_TYPE0),data); \
					}

/* ____ Base Address for PCIe RC Port #0 _________ */
#define FUSIV_PCIE0_BASE		0xB9130000

/* ____ Base Address for PCIe RC Port #1 _________ */
#define FUSIV_PCIE1_BASE		0xB9140000


#define KSEG1_ADDR_LO                  0xA0000000
#define DRAM_ADDR_LO                   0xA0000000
#define PCIE_MEM_PA_LO_0               0x1A000000
#define PCIE_MEM_PA_LO_1               0x1B000000
#define PCIE_MEM_PA_HI_0               0x1AFFFFFF
#define PCIE_MEM_PA_HI_1               0x1BFFFFFF

#define PCIE_MEM_ADDR_LO_0             KSEG1_ADDR_LO + 0x1A000000
#define PCIE_MEM_ADDR_HI_0             KSEG1_ADDR_LO + 0x1AFFFFFF
#define PCIE_MEM_ADDR_LO_1             KSEG1_ADDR_LO + 0x1B000000
#define PCIE_MEM_ADDR_HI_1             KSEG1_ADDR_LO + 0x1BFFFFFF
#define PCIE_ADDR_LO_0                 KSEG1_ADDR_LO + 0x19130000
#define PCIE_ADDR_LO_1                 KSEG1_ADDR_LO + 0x19140000
#define PCIE_MEM_ADDR_LO_VC0_0         KSEG1_ADDR_LO + 0x1A000000
#define PCIE_MEM_ADDR_HI_VC0_0         KSEG1_ADDR_LO + 0x1A7FFFFF
#define PCIE_MEM_ADDR_LO_VC1_0         KSEG1_ADDR_LO + 0x1A800000
#define PCIE_MEM_ADDR_HI_VC1_0         KSEG1_ADDR_LO + 0x1AFFFFFF
#define PCIE_MEM_ADDR_LO_VC0_1         KSEG1_ADDR_LO + 0x1B000000
#define PCIE_MEM_ADDR_HI_VC0_1         KSEG1_ADDR_LO + 0x1B7FFFFF
#define PCIE_MEM_ADDR_LO_VC1_1         KSEG1_ADDR_LO + 0x1B800000
#define PCIE_MEM_ADDR_HI_VC1_1         KSEG1_ADDR_LO + 0x1BFFFFFF

/**********************************************************************************/
/* _____________________ Core Registers __________________________________________*/
/**********************************************************************************/
//dd #define CORE_REGS_BASE_PREFIX		0x0
#define CORE_REGS_BASE_PREFIX		0x2000 //dd for Catshark

/* Command and Status Register */
#define CMD_STAT_OFFSET			(CORE_REGS_BASE_PREFIX + 0x4)

/* Class Code Register */
#define CLASS_CODE_OFFSET		(CORE_REGS_BASE_PREFIX + 0x8)

/*Base Address Register 0 */
#define BAR0_OFFSET			(CORE_REGS_BASE_PREFIX + 0x10)

/* Link Width and Speed Change Control Register */
#define LINK_WIDTH_SPEED_CHANGE_OFFSET              (CORE_REGS_BASE_PREFIX + 0x80C)

/**********************************************************************************/
/* _____________________ Config Addr/Data Registers ______________________________*/
/**********************************************************************************/
#define CFG_ADDR_OFFSET			0x80C0
//dd #define CFG_DATA_TYPE0_OFFSET		0x803C
//dd #define CFG_DATA_TYPE1_OFFSET		0x807C
#define IO_ADDR_OFFSET			0x80C0
//dd #define IO_DATA_OFFSET			0x80BC

#define CFG_DATA_TYPE0_OFFSET		0x8000
#define CFG_DATA_TYPE1_OFFSET		0x8040
#define IO_DATA_OFFSET				0x8080

/**********************************************************************************/
/* _____________________ Wrapper Registers _______________________________________*/
/**********************************************************************************/
//dd #define WRAPPER_REGS_BASE_PREFIX        0xC000
#define WRAPPER_REGS_BASE_PREFIX        0x0000 //dd for Catshark

/* ______ Application and System Control _________ */
#define APP_SYS_CONTROL_OFFSET          (WRAPPER_REGS_BASE_PREFIX + 0x0)

/* Hot_Reset Message */
#define APP_SYS_CONTROL_APP_INIT_RST_SHIFT		0
#define APP_SYS_CONTROL_APP_INIT_RST_MASK		1
#define APP_SYS_CONTROL_APP_INIT_RST			(1 << APP_SYS_CONTROL_APP_INIT_RST_SHIFT)

/* System Electromechanical Interlock Engaged */
#define APP_SYS_CONTROL_SYS_EML_INTERLOCK_ENGAGED_SHIFT  1
#define APP_SYS_CONTROL_SYS_EML_INTERLOCK_ENGAGED_MASK   1
#define APP_SYS_CONTROL_SYS_EML_INTERLOCK_ENGAGED        (1 << APP_SYS_CONTROL_SYS_EML_INTERLOCK_ENGAGED_SHIFT)

/* Command Completed Interrupt */
#define APP_SYS_CONTROL_SYS_CMD_CPLED_INT_SHIFT          2
#define APP_SYS_CONTROL_SYS_CMD_CPLED_INT_MASK           1
#define APP_SYS_CONTROL_SYS_CMD_CPLED_INT                (1 << APP_SYS_CONTROL_SYS_CMD_CPLED_INT_SHIFT)

/* Presence Detect Changed */
#define APP_SYS_CONTROL_SYS_PRE_DET_CHGED_SHIFT          3
#define APP_SYS_CONTROL_SYS_PRE_DET_CHGED_MASK           1
#define APP_SYS_CONTROL_SYS_PRE_DET_CHGED                (1 << APP_SYS_CONTROL_SYS_PRE_DET_CHGED_SHIFT)

/* MRL Sensor Changed */
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_CHGED_SHIFT       4
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_CHGED_MASK        1
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_CHGED             (1 << APP_SYS_CONTROL_SYS_MRL_SENSOR_CHGED_SHIFT)

/* Power Fault Detected */
#define APP_SYS_CONTROL_SYS_PWR_FAULT_DET_SHIFT          5
#define APP_SYS_CONTROL_SYS_PWR_FAULT_DET_MASK           1
#define APP_SYS_CONTROL_SYS_PWR_FAULT_DET                (1 << APP_SYS_CONTROL_SYS_PWR_FAULT_DET_SHIFT)

/* MRL Sensor State */
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_STATE_SHIFT       6
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_STATE_MASK        1
#define APP_SYS_CONTROL_SYS_MRL_SENSOR_STATE             (1 << APP_SYS_CONTROL_SYS_MRL_SENSOR_STATE_SHIFT)

/* Presence Detect State */
#define APP_SYS_CONTROL_SYS_PRE_DET_STATE_SHIFT          7
#define APP_SYS_CONTROL_SYS_PRE_DET_STATE_MASK           1
#define APP_SYS_CONTROL_SYS_PRE_DET_STATE                (1 << APP_SYS_CONTROL_SYS_PRE_DET_STATE_SHIFT)

/* System Attention Button Pressed */
#define APP_SYS_CONTROL_SYS_ATTEN_BUTTON_PRESSED_SHIFT   8
#define APP_SYS_CONTROL_SYS_ATTEN_BUTTON_PRESSED_MASK    1
#define APP_SYS_CONTROL_SYS_ATTEN_BUTTON_PRESSED         (1 << APP_SYS_CONTROL_SYS_ATTEN_BUTTON_PRESSED_SHIFT)

/* Auxiliary Power Detected */
#define APP_SYS_CONTROL_SYS_AUX_PWR_DET_SHIFT            9
#define APP_SYS_CONTROL_SYS_AUX_PWR_DET_MASK             1
#define APP_SYS_CONTROL_SYS_AUX_PWR_DET                  (1 << APP_SYS_CONTROL_SYS_AUX_PWR_DET_SHIFT)

/* Generate a PM_Turn_Off Message */
#define APP_SYS_CONTROL_APPS_PM_XMT_TURNOFF_SHIFT        10
#define APP_SYS_CONTROL_APPS_PM_XMT_TURNOFF_MASK         1
#define APP_SYS_CONTROL_APPS_PM_XMT_TURNOFF              (1 << APP_SYS_CONTROL_APPS_PM_XMT_TURNOFF_SHIFT)

/* Enable Link establishment */
#define APP_SYS_CONTROL_APP_LTSSM_ENABLE_SHIFT           11
#define APP_SYS_CONTROL_APP_LTSSM_ENABLE_MASK            1
#define APP_SYS_CONTROL_APP_LTSSM_ENABLE                 (1 << APP_SYS_CONTROL_APP_LTSSM_ENABLE_SHIFT)

/* Generate an Unlock Message */
#define APP_SYS_CONTROL_APP_UNLOCK_MSG_SHIFT             12
#define APP_SYS_CONTROL_APP_UNLOCK_MSG_MASK              1
#define APP_SYS_CONTROL_APP_UNLOCK_MSG                   (1 << APP_SYS_CONTROL_APP_UNLOCK_MSG_SHIFT)

/* Enable Virtual Channel 1 */
#define APP_SYS_CONTROL_APP_USE_VC1_SHIFT                13
#define APP_SYS_CONTROL_APP_USE_VC1_MASK                 1
#define APP_SYS_CONTROL_APP_USE_VC1                      (1 << APP_SYS_CONTROL_APP_USE_VC1_SHIFT)

/* Device Alternate Type */
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SHIFT          14
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_MASK           0xf
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT(_X)            ((_X & APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_MASK) << APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SHIFT                       )

/* Override the strap values with Alternate Type */
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SEL_SHIFT      18
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SEL_MASK       1
#define APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SEL            (1 << APP_SYS_CONTROL_SYS_PORT_TYPE_ALT_SEL_SHIFT)

/* Generate a PCIe INTx */
#define APP_SYS_CONTROL_SYS_INTX_SHIFT                   19
#define APP_SYS_CONTROL_SYS_INTX_MASK                    1
#define APP_SYS_CONTROL_SYS_INTX                         (1 << APP_SYS_CONTROL_SYS_INTX_SHIFT)

/* AHB Master Xlate Address */
#define APP_SYS_CONTROL_MSTR_XLATE_ADRS_SHIFT            20
#define APP_SYS_CONTROL_MSTR_XLATE_ADRS_MASK             0x7f
#define APP_SYS_CONTROL_MSTR_XLATE_ADRS(_X)              ((_X & APP_SYS_CONTROL_MSTR_XLATE_ADRS_MASK) << APP_SYS_CONTROL_MSTR_XLATE_ADRS_SHIFT)

/* AHB Master Address Xlate Enable */
#define APP_SYS_CONTROL_MSTR_XLATE_EN_SHIFT              27
#define APP_SYS_CONTROL_MSTR_XLATE_EN_MASK               1
#define APP_SYS_CONTROL_MSTR_XLATE_EN                    (1 << APP_SYS_CONTROL_MSTR_XLATE_EN_SHIFT)

/* ______ PCIe Core Status  _________ */
#define CORE_STATUS_OFFSET                  		 (WRAPPER_REGS_BASE_PREFIX + 0x4)

/* XMLH LTSSM Link Up */
#define CORE_STATUS_XMLH_LINK_UP_SHIFT                   0
#define CORE_STATUS_XMLH_LINK_UP_MASK                    1
#define CORE_STATUS_XMLH_LINK_UP                         (1 << CORE_STATUS_XMLH_LINK_UP_SHIFT)

/* Current PCIe PHY Layer LTSSM State */
#define CORE_STATUS_XMLH_LTSSM_STATE_SHIFT               1
#define CORE_STATUS_XMLH_LTSSM_STATE_MASK                0x1f
#define CORE_STATUS_XMLH_LTSSM_STATE                     ((_X & CORE_STATUS_XMLH_LTSSM_STATE_MASK) << CORE_STATUS_XMLH_LTSSM_STATE_SHIFT)

/* PCIe PHY Link is up */
#define CORE_STATUS_RDLH_LINK_UP_SHIFT                   6
#define CORE_STATUS_RDLH_LINK_UP_MASK                    1
#define CORE_STATUS_RDLH_LINK_UP                         (1 << CORE_STATUS_RDLH_LINK_UP_SHIFT)

/* Current PCIe Power Management State */
#define CORE_STATUS_PM_CURNT_STATE_SHIFT                 7
#define CORE_STATUS_PM_CURNT_STATE_MASK                  0x7
#define CORE_STATUS_PM_CURNT_STATE(_X)                   ((_X & CORE_STATUS_PM_CURNT_STATE_MASK) << CORE_STATUS_PM_CURNT_STATE_SHIFT)

/* ______ PHY CR control Register  _________ */
#define PHY_CR_CONTROL_OFFSET                		     (WRAPPER_REGS_BASE_PREFIX + 0x8)

/* Address/Write Data for CR Port Accesses */
#define PHY_CR_CONTROL_SNPS_PHY_CR_DATA_IN_SHIFT         0
#define PHY_CR_CONTROL_SNPS_PHY_CR_DATA_IN_MASK          0xffff
#define PHY_CR_CONTROL_SNPS_PHY_CR_DATA_IN(_X)           ((_X & PHY_CR_CONTROL_SNPS_PHY_CR_DATA_IN_MASK) << PHY_CR_CONTROL_SNPS_PHY_CR_DATA_IN_SHIFT)

/* Read Control signal */
#define PHY_CR_CONTROL_SNPS_PHY_CR_READ_SHIFT            16
#define PHY_CR_CONTROL_SNPS_PHY_CR_READ_MASK             1
#define PHY_CR_CONTROL_SNPS_PHY_CR_READ                  (1 << PHY_CR_CONTROL_SNPS_PHY_CR_READ_SHIFT)

/* Write Control signal */
#define PHY_CR_CONTROL_SNPS_PHY_CR_WRITE_SHIFT           17
#define PHY_CR_CONTROL_SNPS_PHY_CR_WRITE_MASK            1
#define PHY_CR_CONTROL_SNPS_PHY_CR_WRITE                 (1 << PHY_CR_CONTROL_SNPS_PHY_CR_WRITE_SHIFT)

/* snps_phy_cr_data_in is write data */
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_DATA_SHIFT        18
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_DATA_MASK         1
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_DATA              (1 << PHY_CR_CONTROL_SNPS_PHY_CR_CAP_DATA_SHIFT)

/* snps_phy_cr_data_in is address */
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_ADDR_SHIFT        19
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_ADDR_MASK         1
#define PHY_CR_CONTROL_SNPS_PHY_CR_CAP_ADDR              (1 << PHY_CR_CONTROL_SNPS_PHY_CR_CAP_ADDR_SHIFT)

/* ______ PHY CR Status Register  _________ */
#define PHY_CR_STATUS_OFFSET                  		 (WRAPPER_REGS_BASE_PREFIX + 0xc)

/* CR port READ Data Transaction */
#define PHY_CR_STATUS_SNPS_PHY_CR_DATA_OUT_SHIFT         0
#define PHY_CR_STATUS_SNPS_PHY_CR_DATA_OUT_MASK          0xffff
#define PHY_CR_STATUS_SNPS_PHY_CR_DATA_OUT(_X)           ((_X & PHY_CR_STATUS_SNPS_PHY_CR_DATA_OUT_MASK) << PHY_CR_STATUS_SNPS_PHY_CR_DATA_OUT_SHIFT)

/* CR  Transaction Complete */
#define PHY_CR_STATUS_SNPS_PHY_CR_ACK_SHIFT              16
#define PHY_CR_STATUS_SNPS_PHY_CR_ACK_MASK               1
#define PHY_CR_STATUS_SNPS_PHY_CR_ACK                    (1 << PHY_CR_STATUS_SNPS_PHY_CR_ACK_SHIFT)

/* ______ PCIe PHY Control Register __________ */
#define PHY_CONTROL_OFFSET                     		 (WRAPPER_REGS_BASE_PREFIX + 0x10)

/* */
#define PHY_CONTROL_SNPS_PHY_TX_EDGERATE_SHIFT           0
#define PHY_CONTROL_SNPS_PHY_TX_EDGERATE_MASK            0x3
#define PHY_CONTROL_SNPS_PHY_TX_EDGERATE(_X)             (1 << PHY_CONTROL_SNPS_PHY_TX_EDGERATE_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_TX_BOOST_SHIFT              2
#define PHY_CONTROL_SNPS_PHY_TX_BOOST_MASK               0xf
#define PHY_CONTROL_SNPS_PHY_TX_BOOST(_X)                ((_X & PHY_CONTROL_SNPS_PHY_TX_BOOST_MASK) << PHY_CONTROL_SNPS_PHY_TX_BOOST_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_TX_ATTEN_SHIFT              6
#define PHY_CONTROL_SNPS_PHY_TX_ATTEN_MASK               0x7
#define PHY_CONTROL_SNPS_PHY_TX_ATTEN(_X)                ((_X & PHY_CONTROL_SNPS_PHY_TX_ATTEN_MASK) << PHY_CONTROL_SNPS_PHY_TX_ATTEN_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_EQ_VAL_SHIFT                9
#define PHY_CONTROL_SNPS_PHY_EQ_VAL_MASK                 0xf
#define PHY_CONTROL_SNPS_PHY_EQ_VAL(_X)                  ((_X & PHY_CONTROL_SNPS_PHY_EQ_VAL_MASK) << PHY_CONTROL_SNPS_PHY_EQ_VAL_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_TX_LVL_SHIFT                12
#define PHY_CONTROL_SNPS_PHY_TX_LVL_MASK                 0xf
#define PHY_CONTROL_SNPS_PHY_TX_LVL(_X)                  ((_X & PHY_CONTROL_SNPS_PHY_TX_LVL_MASK) << PHY_CONTROL_SNPS_PHY_TX_LVL_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_TX_LVL_BIT4_SHIFT           16
#define PHY_CONTROL_SNPS_PHY_TX_LVL_BIT4_MASK            1
#define PHY_CONTROL_SNPS_PHY_TX_LVL_BIT4                 (1 << PHY_CONTROL_SNPS_PHY_TX_LVL_BIT4_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_MPLL_PRESCALE_SHIFT         17
#define PHY_CONTROL_SNPS_PHY_MPLL_PRESCALE_MASK          0x3
#define PHY_CONTROL_SNPS_PHY_MPLL_PRESCALE(_X)           ((_X & PHY_CONTROL_SNPS_PHY_MPLL_PRESCALE_MASK) << PHY_CONTROL_SNPS_PHY_MPLL_PRESCALE_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY5_SHIFT             19
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY5_MASK              0x3
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY5(_X)               ((_X & PHY_CONTROL_SNPS_PHY_MPLL_NCY5_MASK) << PHY_CONTROL_SNPS_PHY_MPLL_NCY5_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY_SHIFT              21
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY_MASK               0x1f
#define PHY_CONTROL_SNPS_PHY_MPLL_NCY(_X)                ((_X & PHY_CONTROL_SNPS_PHY_MPLL_NCY_MASK) << PHY_CONTROL_SNPS_PHY_MPLL_NCY_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_LOS_LVL_SHIFT               26
#define PHY_CONTROL_SNPS_PHY_LOS_LVL_MASK                0x1f
#define PHY_CONTROL_SNPS_PHY_LOS_LVL(_X)                 ((_X & PHY_CONTROL_SNPS_PHY_LOS_LVL_MASK) << PHY_CONTROL_SNPS_PHY_LOS_LVL_SHIFT)

/* */
#define PHY_CONTROL_SNPS_PHY_COMMON_CLOCKS_SHIFT         31
#define PHY_CONTROL_SNPS_PHY_COMMON_CLOCKS_MASK          1
#define PHY_CONTROL_SNPS_PHY_COMMON_CLOCKS               (1 << PHY_CONTROL_SNPS_PHY_COMMON_CLOCKS_SHIFT)

/*_______ Target Timeout Information Register _______________*/
#define TRGT_TIMEOUT_INFO_ADDRESS              		 (WRAPPER_REGS_BASE_PREFIX + 0x14)

/* */
#define TRGT_TIMEOUT_INFO_INFO_SHIFT                     0
#define TRGT_TIMEOUT_INFO_INFO_MASK                      0xfffffff
#define TRGT_TIMEOUT_INFO_INFO(_X)                       ((_X & TRGT_TIMEOUT_INFO_INFO_MASK) << TRGT_TIMEOUT_INFO_INFO_SHIFT)

/*__________ RADM Timeout Info register __________________ */
#define RADM_TIMEOUT_INFO_OFFSET                	 (WRAPPER_REGS_BASE_PREFIX + 0x18)

/* */
#define RADM_TIMEOUT_INFO_INFO_SHIFT                     0
#define RADM_TIMEOUT_INFO_INFO_MASK                      0xfffffff
#define RADM_TIMEOUT_INFO_INFO(_X)                       ((_X & RADM_TIMEOUT_INFO_INFO_MASK) << RADM_TIMEOUT_INFO_INFO_SHIFT)

/*________ PCIe CAP Message Number Register______________*/
#define CFG_PCIE_CAP_INT_MSG_NUM_OFFSET         	 (WRAPPER_REGS_BASE_PREFIX +  0x1c)

/* */
#define CFG_PCIE_CAP_INT_MSG_NUM_MSG_NUM_SHIFT           0
#define CFG_PCIE_CAP_INT_MSG_NUM_MSG_NUM_MASK            0x1f
#define CFG_PCIE_CAP_INT_MSG_NUM_MSG_NUM(_X)             ((_X & CFG_PCIE_CAP_INT_MSG_NUM_MSG_NUM_MASK) << CFG_PCIE_CAP_INT_MSG_NUM_MSG_NUM_SHIFT)

/*________ AER Messgae Number Register______________ */
#define CFG_AER_INT_MSG_NUM_OFFSET              	 (WRAPPER_REGS_BASE_PREFIX + 0x20)

/* */
#define CFG_AER_INT_MSG_NUM_MSG_NUM_SHIFT                0
#define CFG_AER_INT_MSG_NUM_MSG_NUM_MASK                 0x1f
#define CFG_AER_INT_MSG_NUM_MSG_NUM(_X)                  ((_X & CFG_AER_INT_MSG_NUM_MSG_NUM_MASK) << CFG_AER_INT_MSG_NUM_MSG_NUM_SHIFT)

/*__________ RADM Message Payload  ______________ */
#define RADM_MSG_PAYLOAD_OFFSET                 	 (WRAPPER_REGS_BASE_PREFIX + 0x24)

/* */
#define RADM_MSG_PAYLOAD_PAYLOAD_SHIFT                   0
#define RADM_MSG_PAYLOAD_PAYLOAD_MASK                    0xffffffff
#define RADM_MSG_PAYLOAD_PAYLOAD(_X)                     ((_X & RADM_MSG_PAYLOAD_PAYLOAD_MASK) << RADM_MSG_PAYLOAD_PAYLOAD_SHIFT)

/*__________RADM Message Req. Id Register_____________ */
#define RADM_MSG_REQ_ID_OFFSET                 		 (WRAPPER_REGS_BASE_PREFIX + 0x28)

/* */
#define RADM_MSG_REQ_ID_REQ_ID_SHIFT                     0
#define RADM_MSG_REQ_ID_REQ_ID_MASK                      0xffff
#define RADM_MSG_REQ_ID_REQ_ID(_X)                       ((_X & RADM_MSG_REQ_ID_REQ_ID_MASK) << RADM_MSG_REQ_ID_REQ_ID_SHIFT)

/*__________PCIE Interrupt Register_____________ */
#define PCIE_INTERRUPT_OFFSET                   	 (WRAPPER_REGS_BASE_PREFIX + 0x2c)

/* */
#define PCIE_INTERRUPT_CFG_PME_MSI_SHIFT                 0
#define PCIE_INTERRUPT_CFG_PME_MSI_MASK                  1
#define PCIE_INTERRUPT_CFG_PME_MSI                       (1 << PCIE_INTERRUPT_CFG_PME_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_CFG_PME_INT_SHIFT                 1
#define PCIE_INTERRUPT_CFG_PME_INT_MASK                  1
#define PCIE_INTERRUPT_CFG_PME_INT                       (1 << PCIE_INTERRUPT_CFG_PME_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_CFG_SYS_ERR_RC_SHIFT              2
#define PCIE_INTERRUPT_CFG_SYS_ERR_RC_MASK               1
#define PCIE_INTERRUPT_CFG_SYS_ERR_RC                    (1 << PCIE_INTERRUPT_CFG_SYS_ERR_RC_SHIFT)

/* */
#define PCIE_INTERRUPT_CFG_AER_RC_MSI_SHIFT              3
#define PCIE_INTERRUPT_CFG_AER_RC_MSI_MASK               1
#define PCIE_INTERRUPT_CFG_AER_RC_MSI                    (1 << PCIE_INTERRUPT_CFG_AER_RC_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_CFG_AER_RC_INT_SHIFT              4
#define PCIE_INTERRUPT_CFG_AER_RC_INT_MASK               1
#define PCIE_INTERRUPT_CFG_AER_RC_INT                    (1 << PCIE_INTERRUPT_CFG_AER_RC_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_TRGT_CPL_TIMEOUT_SHIFT            5
#define PCIE_INTERRUPT_TRGT_CPL_TIMEOUT_MASK             1
#define PCIE_INTERRUPT_TRGT_CPL_TIMEOUT                  (1 << PCIE_INTERRUPT_TRGT_CPL_TIMEOUT_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_CPL_TIMEOUT_SHIFT            6
#define PCIE_INTERRUPT_RADM_CPL_TIMEOUT_MASK             1
#define PCIE_INTERRUPT_RADM_CPL_TIMEOUT                  (1 << PCIE_INTERRUPT_RADM_CPL_TIMEOUT_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_VENDOR_MSG_SHIFT             7
#define PCIE_INTERRUPT_RADM_VENDOR_MSG_MASK              1
#define PCIE_INTERRUPT_RADM_VENDOR_MSG                   (1 << PCIE_INTERRUPT_RADM_VENDOR_MSG_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_MSG_UNLOCK_SHIFT             8
#define PCIE_INTERRUPT_RADM_MSG_UNLOCK_MASK              1
#define PCIE_INTERRUPT_RADM_MSG_UNLOCK                   (1 << PCIE_INTERRUPT_RADM_MSG_UNLOCK_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTD_DEASSERTED_SHIFT        9
#define PCIE_INTERRUPT_RADM_INTD_DEASSERTED_MASK         1
#define PCIE_INTERRUPT_RADM_INTD_DEASSERTED              (1 << PCIE_INTERRUPT_RADM_INTD_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTD_ASSERTED_SHIFT          10
#define PCIE_INTERRUPT_RADM_INTD_ASSERTED_MASK           1
#define PCIE_INTERRUPT_RADM_INTD_ASSERTED                (1 << PCIE_INTERRUPT_RADM_INTD_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTC_DEASSERTED_SHIFT        11
#define PCIE_INTERRUPT_RADM_INTC_DEASSERTED_MASK         1
#define PCIE_INTERRUPT_RADM_INTC_DEASSERTED              (1 << PCIE_INTERRUPT_RADM_INTC_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTC_ASSERTED_SHIFT          12
#define PCIE_INTERRUPT_RADM_INTC_ASSERTED_MASK           1
#define PCIE_INTERRUPT_RADM_INTC_ASSERTED                (1 << PCIE_INTERRUPT_RADM_INTC_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTB_DEASSERTED_SHIFT        13
#define PCIE_INTERRUPT_RADM_INTB_DEASSERTED_MASK         1
#define PCIE_INTERRUPT_RADM_INTB_DEASSERTED              (1 << PCIE_INTERRUPT_RADM_INTB_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTB_ASSERTED_SHIFT          14
#define PCIE_INTERRUPT_RADM_INTB_ASSERTED_MASK           1
#define PCIE_INTERRUPT_RADM_INTB_ASSERTED                (1 << PCIE_INTERRUPT_RADM_INTB_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTA_DEASSERTED_SHIFT        15
#define PCIE_INTERRUPT_RADM_INTA_DEASSERTED_MASK         1
#define PCIE_INTERRUPT_RADM_INTA_DEASSERTED              (1 << PCIE_INTERRUPT_RADM_INTA_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_INTA_ASSERTED_SHIFT          16
#define PCIE_INTERRUPT_RADM_INTA_ASSERTED_MASK           1
#define PCIE_INTERRUPT_RADM_INTA_ASSERTED                (1 << PCIE_INTERRUPT_RADM_INTA_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_PM_TO_ACK_SHIFT              17
#define PCIE_INTERRUPT_RADM_PM_TO_ACK_MASK               1
#define PCIE_INTERRUPT_RADM_PM_TO_ACK                    (1 << PCIE_INTERRUPT_RADM_PM_TO_ACK_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_PM_PME_SHIFT                 18
#define PCIE_INTERRUPT_RADM_PM_PME_MASK                  1
#define PCIE_INTERRUPT_RADM_PM_PME                       (1 << PCIE_INTERRUPT_RADM_PM_PME_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_FATAL_ERR_SHIFT              19
#define PCIE_INTERRUPT_RADM_FATAL_ERR_MASK               1
#define PCIE_INTERRUPT_RADM_FATAL_ERR                    (1 << PCIE_INTERRUPT_RADM_FATAL_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_NONFATAL_ERR_SHIFT           20
#define PCIE_INTERRUPT_RADM_NONFATAL_ERR_MASK            1
#define PCIE_INTERRUPT_RADM_NONFATAL_ERR                 (1 << PCIE_INTERRUPT_RADM_NONFATAL_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_RADM_CORRECTABLE_ERR_SHIFT        21
#define PCIE_INTERRUPT_RADM_CORRECTABLE_ERR_MASK         1
#define PCIE_INTERRUPT_RADM_CORRECTABLE_ERR              (1 << PCIE_INTERRUPT_RADM_CORRECTABLE_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_HP_MSI_SHIFT                      22
#define PCIE_INTERRUPT_HP_MSI_MASK                       1
#define PCIE_INTERRUPT_HP_MSI                            (1 << PCIE_INTERRUPT_HP_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_HP_INT_SHIFT                      23
#define PCIE_INTERRUPT_HP_INT_MASK                       1
#define PCIE_INTERRUPT_HP_INT                            (1 << PCIE_INTERRUPT_HP_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_HP_PME_SHIFT                      24
#define PCIE_INTERRUPT_HP_PME_MASK                       1
#define PCIE_INTERRUPT_HP_PME                            (1 << PCIE_INTERRUPT_HP_PME_SHIFT)

/* */
#define PCIE_INTERRUPT_LINK_STATUS_SHIFT                 25
#define PCIE_INTERRUPT_LINK_STATUS_MASK                  1
#define PCIE_INTERRUPT_LINK_STATUS                       (1 << PCIE_INTERRUPT_LINK_STATUS_SHIFT)

/*___________ PCIE Interrupt Register Mask ______________________*/
#define PCIE_INTERRUPT_MASK_OFFSET             		 (WRAPPER_REGS_BASE_PREFIX + 0x30)

/* */
#define PCIE_INTERRUPT_MASK_CFG_PME_MSI_SHIFT            0
#define PCIE_INTERRUPT_MASK_CFG_PME_MSI_MASK             1
#define PCIE_INTERRUPT_MASK_CFG_PME_MSI                  (1 << PCIE_INTERRUPT_MASK_CFG_PME_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_CFG_PME_INT_SHIFT            1
#define PCIE_INTERRUPT_MASK_CFG_PME_INT_MASK             1
#define PCIE_INTERRUPT_MASK_CFG_PME_INT                  (1 << PCIE_INTERRUPT_MASK_CFG_PME_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_CFG_SYS_ERR_RC_SHIFT         2
#define PCIE_INTERRUPT_MASK_CFG_SYS_ERR_RC_MASK          1
#define PCIE_INTERRUPT_MASK_CFG_SYS_ERR_RC               (1 << PCIE_INTERRUPT_MASK_CFG_SYS_ERR_RC_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_MSI_SHIFT         3
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_MSI_MASK          1
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_MSI               (1 << PCIE_INTERRUPT_MASK_CFG_AER_RC_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_INT_SHIFT         4
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_INT_MASK          1
#define PCIE_INTERRUPT_MASK_CFG_AER_RC_INT               (1 << PCIE_INTERRUPT_MASK_CFG_AER_RC_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_TRGT_CPL_TIMEOUT_SHIFT       5
#define PCIE_INTERRUPT_MASK_TRGT_CPL_TIMEOUT_MASK        1
#define PCIE_INTERRUPT_MASK_TRGT_CPL_TIMEOUT             (1 << PCIE_INTERRUPT_MASK_TRGT_CPL_TIMEOUT_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_CPL_TIMEOUT_SHIFT       6
#define PCIE_INTERRUPT_MASK_RADM_CPL_TIMEOUT_MASK        1
#define PCIE_INTERRUPT_MASK_RADM_CPL_TIMEOUT             (1 << PCIE_INTERRUPT_MASK_RADM_CPL_TIMEOUT_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_VENDOR_MSG_SHIFT        7
#define PCIE_INTERRUPT_MASK_RADM_VENDOR_MSG_MASK         1
#define PCIE_INTERRUPT_MASK_RADM_VENDOR_MSG              (1 << PCIE_INTERRUPT_MASK_RADM_VENDOR_MSG_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_MSG_UNLOCK_SHIFT        8
#define PCIE_INTERRUPT_MASK_RADM_MSG_UNLOCK_MASK         1
#define PCIE_INTERRUPT_MASK_RADM_MSG_UNLOCK              (1 << PCIE_INTERRUPT_MASK_RADM_MSG_UNLOCK_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTD_DEASSERTED_SHIFT   9
#define PCIE_INTERRUPT_MASK_RADM_INTD_DEASSERTED_MASK    1
#define PCIE_INTERRUPT_MASK_RADM_INTD_DEASSERTED         (1 << PCIE_INTERRUPT_MASK_RADM_INTD_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTD_ASSERTED_SHIFT     10
#define PCIE_INTERRUPT_MASK_RADM_INTD_ASSERTED_MASK      1
#define PCIE_INTERRUPT_MASK_RADM_INTD_ASSERTED           (1 << PCIE_INTERRUPT_MASK_RADM_INTD_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTC_DEASSERTED_SHIFT   11
#define PCIE_INTERRUPT_MASK_RADM_INTC_DEASSERTED_MASK    1
#define PCIE_INTERRUPT_MASK_RADM_INTC_DEASSERTED         (1 << PCIE_INTERRUPT_MASK_RADM_INTC_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTC_ASSERTED_SHIFT     12
#define PCIE_INTERRUPT_MASK_RADM_INTC_ASSERTED_MASK      1
#define PCIE_INTERRUPT_MASK_RADM_INTC_ASSERTED           (1 << PCIE_INTERRUPT_MASK_RADM_INTC_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTB_DEASSERTED_SHIFT   13
#define PCIE_INTERRUPT_MASK_RADM_INTB_DEASSERTED_MASK    1
#define PCIE_INTERRUPT_MASK_RADM_INTB_DEASSERTED         (1 << PCIE_INTERRUPT_MASK_RADM_INTB_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTB_ASSERTED_SHIFT     14
#define PCIE_INTERRUPT_MASK_RADM_INTB_ASSERTED_MASK      1
#define PCIE_INTERRUPT_MASK_RADM_INTB_ASSERTED           (1 << PCIE_INTERRUPT_MASK_RADM_INTB_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTA_DEASSERTED_SHIFT   15
#define PCIE_INTERRUPT_MASK_RADM_INTA_DEASSERTED_MASK    1
#define PCIE_INTERRUPT_MASK_RADM_INTA_DEASSERTED         (1 << PCIE_INTERRUPT_MASK_RADM_INTA_DEASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_INTA_ASSERTED_SHIFT     16
#define PCIE_INTERRUPT_MASK_RADM_INTA_ASSERTED_MASK      1
#define PCIE_INTERRUPT_MASK_RADM_INTA_ASSERTED           (1 << PCIE_INTERRUPT_MASK_RADM_INTA_ASSERTED_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_PM_TO_ACK_SHIFT         17
#define PCIE_INTERRUPT_MASK_RADM_PM_TO_ACK_MASK          1
#define PCIE_INTERRUPT_MASK_RADM_PM_TO_ACK               (1 << PCIE_INTERRUPT_MASK_RADM_PM_TO_ACK_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_PM_PME_SHIFT            18
#define PCIE_INTERRUPT_MASK_RADM_PM_PME_MASK             1
#define PCIE_INTERRUPT_MASK_RADM_PM_PME                  (1 << PCIE_INTERRUPT_MASK_RADM_PM_PME_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_FATAL_ERR_SHIFT         19
#define PCIE_INTERRUPT_MASK_RADM_FATAL_ERR_MASK          1
#define PCIE_INTERRUPT_MASK_RADM_FATAL_ERR               (1 << PCIE_INTERRUPT_MASK_RADM_FATAL_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_NONFATAL_ERR_SHIFT      20
#define PCIE_INTERRUPT_MASK_RADM_NONFATAL_ERR_MASK       1
#define PCIE_INTERRUPT_MASK_RADM_NONFATAL_ERR            (1 << PCIE_INTERRUPT_MASK_RADM_NONFATAL_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_RADM_CORRECTABLE_ERR_SHIFT   21
#define PCIE_INTERRUPT_MASK_RADM_CORRECTABLE_ERR_MASK    1
#define PCIE_INTERRUPT_MASK_RADM_CORRECTABLE_ERR         (1 << PCIE_INTERRUPT_MASK_RADM_CORRECTABLE_ERR_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_HP_MSI_SHIFT                 22
#define PCIE_INTERRUPT_MASK_HP_MSI_MASK                  1
#define PCIE_INTERRUPT_MASK_HP_MSI                       (1 << PCIE_INTERRUPT_MASK_HP_MSI_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_HP_INT_SHIFT                 23
#define PCIE_INTERRUPT_MASK_HP_INT_MASK                  1
#define PCIE_INTERRUPT_MASK_HP_INT                       (1 << PCIE_INTERRUPT_MASK_HP_INT_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_HP_PME_SHIFT                 24
#define PCIE_INTERRUPT_MASK_HP_PME_MASK                  1
#define PCIE_INTERRUPT_MASK_HP_PME                       (1 << PCIE_INTERRUPT_MASK_HP_PME_SHIFT)

/* */
#define PCIE_INTERRUPT_MASK_LINK_STATUS_SHIFT            25
#define PCIE_INTERRUPT_MASK_LINK_STATUS_MASK             1
#define PCIE_INTERRUPT_MASK_LINK_STATUS                  (1 << PCIE_INTERRUPT_MASK_LINK_STATUS_SHIFT)

/*___________ Core CFG ______________________*/
#define PCIE_CORE_CFG_OFFSET                         (WRAPPER_REGS_BASE_PREFIX + 0x34)

/* Capabality Register */
#define CAP_REGS_BASE_PREFIX            0x2070 //for Catshark

/* Capabality Device Control Register */
#define DEV_CTRL_OFFSET                 (CAP_REGS_BASE_PREFIX + 0x8)

#endif /* #ifndef _FUSIVVX185PCI_H_ */

