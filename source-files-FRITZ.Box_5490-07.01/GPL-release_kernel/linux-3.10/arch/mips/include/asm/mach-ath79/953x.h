/*
 * vim: tabstop=8 : noexpandtab
 */
#ifndef _953x_H
#define _953x_H

// [GJu] Um tatsaechlich benoetigte defines aufzuspueren und zu kontrollieren und um redefines 
// auszuschliessen habe ich erstmal alles per #if 0 deaktiviert. Benoetigte defines koennen und 
// sollen bei Bedarf gern wieder reingenommen werden.
//
// Neben den allgemeinen Adressmaps, denen ich nur Klammern spendiert habe, sind Makros nach 
// folgender Konvention aufgebaut:
//
// Register Offset              QCA953X_REGOFS__<Registergruppe>__<Registername>
// Register Adresse             QCA953X_REG__<Registergruppe>__<Registername>
// Registerbit einzeln          QCA953X_SSB__<Registergruppe>__<Registername>__<Wertbezeichner>
// Registerbits MSB             QCA953X_MSB__<Registergruppe>__<Registername>__<Wertbezeichner>
// Registerbits LSB             QCA953X_LSB__<Registergruppe>__<Registername>__<Wertbezeichner>
//
// Bitmasken, get/set-Operationen usw. werden durch zweckdienliche Makros daraus abgeleitet.
//
// Grundsaetzlich werden die Bezeichner aus dem Datenblatt verwendet, um das Wiederfinden
// zu erleichtern.
//

/*
 * Address map
 */
#define ATH_PCI_MEM_BASE(x)		0x10000000	/* 128M */
#define ATH_APB_BASE			0x18000000	/* 384M */

#define ATH_GE0_BASE			0x19000000	/* 16M */
#define ATH_GE1_BASE			0x1a000000	/* 16M */

#define QCA953X_EHCI_BASE       0x1b000000
#define QCA953X_EHCI_SIZE       0x00000200

#define ATH_USB_OHCI_BASE		0x1b000000
#define ATH_USB_EHCI_BASE		QCA953X_EHCI_BASE

#define ATH_NAND_FLASH_BASE		0x1b000000u

#define ATH_SRAM_BASE			0x1d000000u

#define ATH_SPI_BASE			0x1f000000
#define ATH_SPI_SIZE            0x01000000

/*
 * APB block
 */
#define ATH_DDR_CTRL_BASE		    (ATH_APB_BASE + 0x00000000)
#define ATH_DDR_CTRL_SIZE		    0x00020000

#define ATH_UART_BASE			    (ATH_APB_BASE + 0x00020000)
#define ATH_UART_SIZE               0x00010000

#define ATH_USB_BASE		        (ATH_APB_BASE + 0x00030000)

#define ATH_GPIO_BASE			    (ATH_APB_BASE + 0x00040000)

#define ATH_PLL_CONTROL_BASE	    (ATH_APB_BASE + 0x00050000)
#define ATH_PLL_CONTROL_SIZE	    0x00010000

// for backward compatibiliy
#define ATH_PLL_BASE                ATH_PLL_CONTROL_BASE
#define ATH_PLL_SIZE                ATH_PLL_CONTROL_SIZE
//

#define ATH_RESET_BASE              (ATH_APB_BASE + 0x00060000)
#define ATH_RESET_SIZE              0x00010000

#define ATH_GMAC_INTERFACE	        (ATH_APB_BASE + 0x00070000)

#define ATH_WLAN_MAC_BASE			(ATH_APB_BASE + 0x00100000)
#define ATH_WLAN_MAC_SIZE			0x00007000

/********************************************************************************************************* 
 * Register definitions 
 */

// compatibility caused undefs

#undef ATH_PLL_SWITCH_CLOCK_CONTROL
#undef ATH_PLL_SWITCH_CLOCK_CONTROL_USB_REFCLK_FREQ_SEL_MASK
#undef ATH_PLL_SWITCH_CLOCK_CONTROL_MDIO_CLK_SEL1_1_SET
#undef ATH_PLL_SWITCH_CLOCK_CONTROL_OEN_CLK125M_SEL_SET
#undef ATH_PLL_SWITCH_CLOCK_CONTROL_EN_PLL_TOP_SET
#undef ATH_PLL_SWITCH_CLOCK_CONTROL_SWITCHCLK_SEL_SET
#undef ATH_PLL_ETH_XMII
#undef ATH_PLL_ETH_XMII_TX_INVERT_SET
#undef ATH_PLL_ETH_XMII_GIGE_SET
#undef ATH_PLL_ETH_XMII_PHASE1_COUNT_SET
#undef ATH_PLL_ETH_XMII_PHASE0_COUNT_SET
#undef ATH_PLL_ETH_SGMII
#undef ATH_PLL_ETH_SGMII_GIGE_SET
#undef ATH_PLL_ETH_SGMII_RX_DELAY_SET
#undef ATH_PLL_ETH_SGMII_TX_DELAY_SET
#undef ATH_PLL_ETH_SGMII_CLK_SEL_SET
#undef ATH_PLL_ETH_SGMII_PHASE1_COUNT_SET
#undef ATH_PLL_ETH_SGMII_PHASE0_COUNT_SET
#undef ATH_PLL_ETH_SGMII_SERDES
#undef ATH_PLL_ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK
#undef ATH_PLL_ETH_SGMII_SERDES_EN_LOCK_DETECT_SET
#undef ATH_PLL_ETH_SGMII_SERDES_PLL_EN_MASK
#undef ATH_PLL_ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET
#undef ATH_RESET_REVISION_ID_MAJOR_MASK
#undef ATH_RESET_GE0_PHY_MASK
#undef ATH_RESET_GE1_PHY_MASK
#undef ATH_RST_RESET
#undef ATH_RESET_ETH_SWITCH_ANALOG_RESET
#undef ATH_RESET_ETH_SWITCH_RESET
#undef ATH_GMAC_ETH_CFG
#undef ATH_GMAC_ETH_CFG_SW_PHY_SWAP_SET
#undef ATH_GMAC_ETH_CFG_SW_ONLY_MODE_SET
#undef ATH_GMAC_ETH_CFG_GE0_SGMII_SET
#undef ATH_GMAC_SGMII_SERDES
#undef ATH_GMAC_SGMII_SERDES_VCO_FAST_GET
#undef ATH_GMAC_SGMII_SERDES_VCO_SLOW_GET
#undef ATH_GMAC_SGMII_SERDES_RES_CALIBRATION_MASK
#undef ATH_GMAC_SGMII_SERDES_RES_CALIBRATION_SET
#undef ATH_GMAC_SGMII_SERDES_CDR_BW_SET            
#undef ATH_GMAC_SGMII_SERDES_TX_DR_CTRL_SET        
#undef ATH_GMAC_SGMII_SERDES_PLL_BW_SET           
#undef ATH_GMAC_SGMII_SERDES_EN_SIGNAL_DETECT_SET
#undef ATH_GMAC_SGMII_SERDES_FIBER_SDO_SET      
#undef ATH_GMAC_SGMII_SERDES_VCO_REG_SET       
#undef ATH_GMAC_SGMII_RESET
#undef ATH_GMAC_MR_AN_CONTROL
#undef ATH_GMAC_SGMII_CONFIG
#undef ATH_GMAC_SGMII_DEBUG            
#undef ATH_GMAC_SGMII_SERDES_LOCK_DETECT_STATUS_MASK
#undef ATH_GMAC_MR_AN_CONTROL_POWER_DOWN_SET          
#undef ATH_GMAC_MR_AN_CONTROL_PHY_RESET_SET          
#undef ATH_GMAC_MR_AN_CONTROL_DUPLEX_MODE_SET       
#undef ATH_GMAC_MR_AN_CONTROL_SPEED_SEL1_SET       
#undef ATH_GMAC_SGMII_CONFIG_MODE_CTRL_SET        
#undef ATH_GMAC_SGMII_CONFIG_FORCE_SPEED_SET     
#undef ATH_GMAC_SGMII_CONFIG_SPEED_SET          
#undef ATH_GMAC_SGMII_RESET_HW_RX_125M_N_SET   
#undef ATH_GMAC_SGMII_RESET_TX_125M_N_SET     
#undef ATH_GMAC_SGMII_RESET_RX_125M_N_SET    
#undef ATH_GMAC_SGMII_RESET_TX_CLK_N_SET    
#undef ATH_GMAC_SGMII_RESET_RX_CLK_N_SET   
#undef ATH_GMAC_SGMII_RESET_RX_CLK_N_RESET

// Bit operations

#define BIT_GET(from, bit)  (((from) >> (bit)) & 0x00000001)
#define BIT_SET(to  , bit)  (((to  ) & 0x00000001) << (bit))

#define BITS_GET(from, msb, lsb)  (((from) & GENMASK ((msb), (lsb))) >> (lsb))
#define BITS_SET(to  , msb, lsb)  (((to  ) << (lsb)) & GENMASK ((msb), (lsb)))

/* DDR
 */

/* -- specific -- */
 
// offsets
#define QCA953X_REGOFS__DDR__DDR_CONFIG                           0x0000
#define QCA953X_REGOFS__DDR__DDR_CONFIG2                          0x0004
#define QCA953X_REGOFS__DDR__DDR_MODE                             0x0008
#define QCA953X_REGOFS__DDR__DDR_EXTENDED_MODE_REGISTER           0x000c
#define QCA953X_REGOFS__DDR__DDR_CONTROL                          0x0010
#define QCA953X_REGOFS__DDR__DDR_REFRESH                          0x0014
#define QCA953X_REGOFS__DDR__DDR_RD_DATA_THIS_CYCLE               0x0018
#define QCA953X_REGOFS__DDR__TAP_CONTROL_0                        0x001c
#define QCA953X_REGOFS__DDR__TAP_CONTROL_1                        0x0020
#define QCA953X_REGOFS__DDR__DDR_WB_FLUSH_GE0                     0x009c
#define QCA953X_REGOFS__DDR__DDR_WB_FLUSH_GE1                     0x00a0
#define QCA953X_REGOFS__DDR__DDR_WB_FLUSH_USB	                  0x00a4
#define QCA953X_REGOFS__DDR__DDR_WB_FLUSH_PCIE                    0x00a8
#define QCA953X_REGOFS__DDR__DDR_WB_FLUSH_WMAC                    0x00ac
#define QCA953X_REGOFS__DDR__DDR_DDR2_CONFIG                      0x00b8
#define QCA953X_REGOFS__DDR__DDR_EMR2                             0x00bc
#define QCA953X_REGOFS__DDR__DDR_EMR3                             0x00c0
#define QCA953X_REGOFS__DDR__DDR_BURST                            0x00c4
#define QCA953X_REGOFS__DDR__DDR_BURST2                           0x00c8
#define QCA953X_REGOFS__DDR__AHB_MASTER_TIMEOUT_MAX               0x00cc
#define QCA953X_REGOFS__DDR__AHB_MASTER_TIMEOUT_CURNT             0x00d0
#define QCA953X_REGOFS__DDR__AHB_MASTER_TIMEOUT_SLAVE_ADDR        0x00d4
#define QCA953X_REGOFS__DDR__DDR_CTL_CONFIG                       0x0108
#define QCA953X_REGOFS__DDR__DDR_SF_CTL                           0x0110
#define QCA953X_REGOFS__DDR__SF_TIMER                             0x0114
#define QCA953X_REGOFS__DDR__WMAC_FLUSH                           0x0128
#define QCA953X_REGOFS__DDR__DDR3_CONFIG                          0x015c

/* GPIO
 */

/* -- generic -- */

// addresses
#define ATH_GPIO_OE			           (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_OE          )
#define ATH_GPIO_IN                    (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_IN          )
#define ATH_GPIO_OUT                   (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_OUT         )
#define ATH_GPIO_SET                   (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_SET         )
#define ATH_GPIO_CLEAR                 (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_CLEAR       )
#define ATH_GPIO_INT_ENABLE            (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_INT         )
#define ATH_GPIO_INT_TYPE              (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_INT_TYPE    )
#define ATH_GPIO_INT_POLARITY          (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_INT_POLARITY)
#define ATH_GPIO_INT_PENDING           (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_INT_PENDING )
#define ATH_GPIO_INT_MASK              (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_INT_MASK    )
#define ATH_GPIO_FUNCTION              (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_FUNCTION    )
#define ATH_GPIO_FUNCTIONS             ATH_GPIO_FUNCTION

// bit masks
#define ATH_GPIO_FUNCTION_DISABLE_JTAG  BIT (QCA953X_SSB__GPIO__GPIO_FUNCTION__DISABLE_JTAG)
#define ATH_GPIO_FUNCTION_JTAG_DISABLE  ATH_GPIO_FUNCTION_DISABLE_JTAG

// magic
#define ATH_GPIO_OUT_FUNCTION(i)       (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION0 + (i << 2)) 
#define ATH_GPIO_IN_ENABLE(i)          (ATH_GPIO_BASE + QCA953X_REGOFS__GPIO__GPIO_IN_ENABLE0 + (i << 2))

#define ATH_GPIO_IN_ENABLE_REG(func)          (ATH_GPIO_IN_ENABLE (0) + 0x00000004 * (func >> 8))
#define ATH_GPIO_IN_ENABLE_SET(val,func,pin)  ((pin << (func & 0xff)) | (val & ~(0xff << (func & 0xff))))
 
#define ATH_GPIO_OUT_FUNC_REG(pin)            (ATH_GPIO_OUT_FUNCTION (0) + (pin & ~0x00000003))
#define ATH_GPIO_OUT_FUNC_SET(val,func,pin)   ((func << (8 * (pin & 0x00000003))) | (val & ~(0x000000ff << (8 * (pin & 0x00000003)))))

/* -- specific -- */

// offsets
#define QCA953X_REGOFS__GPIO__GPIO_OE                             0x0000
#define QCA953X_REGOFS__GPIO__GPIO_IN                             0x0004
#define QCA953X_REGOFS__GPIO__GPIO_OUT                            0x0008
#define QCA953X_REGOFS__GPIO__GPIO_SET                            0x000c
#define QCA953X_REGOFS__GPIO__GPIO_CLEAR                          0x0010
#define QCA953X_REGOFS__GPIO__GPIO_INT                            0x0014
#define QCA953X_REGOFS__GPIO__GPIO_INT_TYPE                       0x0018
#define QCA953X_REGOFS__GPIO__GPIO_INT_POLARITY                   0x001c
#define QCA953X_REGOFS__GPIO__GPIO_INT_PENDING                    0x0020
#define QCA953X_REGOFS__GPIO__GPIO_INT_MASK                       0x0024
#define QCA953X_REGOFS__GPIO__GPIO_IN_ETH_SWITCH_LED              0x0028
#define QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION0                  0x002c
#define QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION1                  0x0030
#define QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION2                  0x0034
#define QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION3                  0x0038
#define QCA953X_REGOFS__GPIO__GPIO_OUT_FUNCTION4                  0x003c
#define QCA953X_REGOFS__GPIO__GPIO_IN_ENABLE0                     0x0044
#define QCA953X_REGOFS__GPIO__GPIO_FUNCTION                       0x006c

// bits
#define QCA953X_SSB__GPIO__GPIO_FUNCTION__DISABLE_JTAG                             1

// GPIO Input Select Values

#define ATH_GPIO_IN_ENABLE_REGNUM(num)        (num << 8)  // codierung als RegIdx:RegPos
#define ATH_GPIO_IN_ENABLE_SHIFT(bitno)       (bitno)

#define QCA953X__GPIO_IN__SPI_DATA_IN   (ATH_GPIO_IN_ENABLE_REGNUM(0) | ATH_GPIO_IN_ENABLE_SHIFT(0))   /*--- SPI data input ---*/
#define QCA953X__GPIO_IN__UART0_SIN     (ATH_GPIO_IN_ENABLE_REGNUM(0) | ATH_GPIO_IN_ENABLE_SHIFT(8))   /*--- Low speed UART0 serial data in ---*/

// GPIO Output Select Values

#define QCA953X__GPIO_OUT__SYS_RST_L            1 

#define QCA953X__GPIO_OUT__SPI_CLK              8
#define QCA953X__GPIO_OUT__SPI_CS_0             9
#define QCA953X__GPIO_OUT__SPI_CS_1            10
#define QCA953X__GPIO_OUT__SPI_CS_2            11
#define QCA953X__GPIO_OUT__SPI_MOSI            12

#define QCA953X__GPIO_OUT__UART0_SOUT          22
#define QCA953X__GPIO_OUT__SRIF_OUT            23

#define QCA953X__GPIO_OUT__LED_ACTN_0(x)      (26 + x)
#define QCA953X__GPIO_OUT__LED_COLN_0(x)      (31 + x)
#define QCA953X__GPIO_OUT__LED_DUPLEXN_0(x)   (36 + x)
#define QCA953X__GPIO_OUT__LED_LINK(x)        (41 + x)

#define QCA953X__GPIO_OUT__SMART_ANT_CTL_2     48
#define QCA953X__GPIO_OUT__SMART_ANT_CTL_3     49
#define QCA953X__GPIO_OUT__ATT_LED             50
#define QCA953X__GPIO_OUT__PWR_LED             51
#define QCA953X__GPIO_OUT__TX_FRAME            52
#define QCA953X__GPIO_OUT__RX_CLEAR_INTERNAL   53
#define QCA953X__GPIO_OUT__LED_NETWORK_EN      54
#define QCA953X__GPIO_OUT__LED_POWER_EN        55

#define QCA953X__GPIO_OUT__RX_CLEAR_EXTENSION  78

#define QCA953X__GPIO_OUT__USB_SUSPEND         86

#define QCA953X__GPIO_OUT__DDR_DQ_OE           88
#define QCA953X__GPIO_OUT__CLKREQ_N_RC         89
#define QCA953X__GPIO_OUT__CLK_OBS0            90
#define QCA953X__GPIO_OUT__CLK_OBS1            91
#define QCA953X__GPIO_OUT__CLK_OBS2            92
#define QCA953X__GPIO_OUT__CLK_OBS3            93
#define QCA953X__GPIO_OUT__CLK_OBS4            94
#define QCA953X__GPIO_OUT__CLK_OBS5            95
#define QCA953X__GPIO_OUT__CLK_OBS6            96

/* PLL
 */

/* -- generic -- */

// addresses
#define ATH_PLL_SWITCH_CLOCK_CONTROL  (ATH_PLL_BASE + QCA953X_REGOFS__PLL_CONTROL__SWITCH_CLOCK_CONTROL)
#define ATH_PLL_ETH_XMII              (ATH_PLL_BASE + QCA953X_REGOFS__PLL_CONTROL__ETH_XMII            )

// get/set
#define ATH_PLL_ETH_XMII_GIGE_SET(s)          BIT_SET (s,  \
            QCA953X_SSB__PLL_CONTROL__ETH_XMII__GIGE)
#define ATH_PLL_ETH_XMII_PHASE1_COUNT_SET(s)  BITS_SET (s,  \
            QCA953X_MSB__PLL_CONTROL__ETH_XMII__PHASE1_CNT,  \
            QCA953X_LSB__PLL_CONTROL__ETH_XMII__PHASE1_CNT)
#define ATH_PLL_ETH_XMII_PHASE0_COUNT_SET(s)  BITS_SET (s,  \
            QCA953X_MSB__PLL_CONTROL__ETH_XMII__PHASE0_CNT,  \
            QCA953X_LSB__PLL_CONTROL__ETH_XMII__PHASE0_CNT)

/* -- specific -- */

// offsets
#define QCA953X_REGOFS__PLL_CONTROL__CPU_PLL_CONFIG               0x0000
#define QCA953X_REGOFS__PLL_CONTROL__DDR_PLL_CONFIG               0x0004
#define QCA953X_REGOFS__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL        0x0008
#define QCA953X_REGOFS__PLL_CONTROL__CPU_SYNC                     0x000c
#define QCA953X_REGOFS__PLL_CONTROL__PCIE_PLL_CONFIG              0x0010
#define QCA953X_REGOFS__PLL_CONTROL__PCIE_PLL_DITHER_DIV_MAX      0x0014
#define QCA953X_REGOFS__PLL_CONTROL__PCIE_PLL_DITHER_DIV_MIN      0x0018
#define QCA953X_REGOFS__PLL_CONTROL__PCIE_PLL_DITHER_STEP         0x001c
#define QCA953X_REGOFS__PLL_CONTROL__LDO_POWER_CONTROL            0x0020
#define QCA953X_REGOFS__PLL_CONTROL__SWITCH_CLOCK_CONTROL         0x0024
#define QCA953X_REGOFS__PLL_CONTROL__CURRENT_PLL_DITHER           0x0028
#define QCA953X_REGOFS__PLL_CONTROL__ETH_XMII                     0x002c
#define QCA953X_REGOFS__PLL_CONTROL__BB_PLL_CONFIG                0x0040
#define QCA953X_REGOFS__PLL_CONTROL__DDR_PLL_DITHER               0x0044
#define QCA953X_REGOFS__PLL_CONTROL__CPU_DLL_DITHER               0x0048

// bits 
#define QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__OUTDIV                      21
#define QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__OUTDIV                      19
#define QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__REFDIV                      16
#define QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__REFDIV                      12
#define QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__NINT                        11
#define QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__NINT                         6
#define QCA953X_MSB__PLL_CONTROL__CPU_PLL_CONFIG__NFRAC                        5
#define QCA953X_LSB__PLL_CONTROL__CPU_PLL_CONFIG__NFRAC                        0

#define QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__OUTDIV                      25
#define QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__OUTDIV                      23
#define QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__REFDIV                      20
#define QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__REFDIV                      16
#define QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__NINT                        15
#define QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__NINT                        10
#define QCA953X_MSB__PLL_CONTROL__DDR_PLL_CONFIG__NFRAC                        9
#define QCA953X_LSB__PLL_CONTROL__DDR_PLL_CONFIG__NFRAC                        0

#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHBCLK_FROM_DDRPLL   24
#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDRCLK_FROM_DDRPLL   21
#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPUCLK_FROM_CPUPLL   20
#define QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_POST_DIV         19
#define QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_POST_DIV         15
#define QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_POST_DIV         14
#define QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_POST_DIV         10
#define QCA953X_MSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_POST_DIV          9
#define QCA953X_LSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_POST_DIV          5
#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__AHB_PLL_BYPASS        4
#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__DDR_PLL_BYPASS        3
#define QCA953X_SSB__PLL_CONTROL__CPU_DDR_CLOCK_CONTROL__CPU_PLL_BYPASS        2

#define QCA953X_SSB__PLL_CONTROL__ETH_XMII__GIGE                              25
#define QCA953X_MSB__PLL_CONTROL__ETH_XMII__PHASE1_CNT                        15
#define QCA953X_LSB__PLL_CONTROL__ETH_XMII__PHASE1_CNT                         8
#define QCA953X_MSB__PLL_CONTROL__ETH_XMII__PHASE0_CNT                         7
#define QCA953X_LSB__PLL_CONTROL__ETH_XMII__PHASE0_CNT                         0

/* Reset
 */

/* -- generic -- */

// addresses
#define ATH_RST_MISC_INTERRUPT_STATUS  (ATH_RESET_BASE + ATH_RESET_REG_MISC_INTERRUPT_STATUS  )
#define ATH_RST_MISC_INTERRUPT_MASK	   (ATH_RESET_BASE + ATH_RESET_REG_MISC_INTERRUPT_MASK    )
#define ATH_RST_RESET                  (ATH_RESET_BASE + ATH_RESET_REG_RESET)

// offsets
#define ATH_RESET_REG_WDOG_CTRL              QCA953X_REGOFS__RESET__RST_WATCHDOG_TIMER_CONTROL
#define ATH_RESET_REG_MISC_INTERRUPT_STATUS  QCA953X_REGOFS__RESET__RST_MISC_INTERUPT_STATUS
#define ATH_RESET_REG_MISC_INTERRUPT_MASK    QCA953X_REGOFS__RESET__RST_MISC_INTERUPT_MASK
#define ATH_RESET_REG_MISC_INTERRUPT_ENABLE  ATH_RESET_REG_MISC_INTERRUPT_MASK
#define ATH_RESET_REG_RESET                  QCA953X_REGOFS__RESET__RST_RESET
#define ATH_RESET_REG_RESET_MODULE           ATH_RESET_REG_RESET
#define ATH_RESET_REG_REVISION_ID            QCA953X_REGOFS__RESET__RST_REVISION_ID
#define ATH_RESET_REG_REV_ID                 ATH_RESET_REG_REVISION_ID
#define ATH_RESET_REG_BOOTSTRAP              QCA953X_REGOFS__RESET__RST_BOOTSTRAP
#define ATH_RESET_REG_STICKY                 QCA953X_REGOFS__RESET__SPARE_STKY_REG

// bit masks
#define ATH_RESET_FULL_CHIP                  BIT (QCA953X_SSB__RESET__RST_RESET__FULL_CHIP_RESET )
#define ATH_RESET_GE1_MDIO                   BIT (QCA953X_SSB__RESET__RST_RESET__RESET_GE1_MDIO   )
#define ATH_RESET_GE0_MDIO                   BIT (QCA953X_SSB__RESET__RST_RESET__RESET_GE0_MDIO   )
#define ATH_RESET_GE1_MAC                    BIT (QCA953X_SSB__RESET__RST_RESET__GE1_MAC_RESET    )
#define ATH_RESET_GE1_PHY                    BIT (QCA953X_SSB__RESET__RST_RESET__ETH_SWITCH_ARESET)
#define ATH_RESET_GE0_MAC                    BIT (QCA953X_SSB__RESET__RST_RESET__GE0_MAC_RESET    )
#define ATH_RESET_GE0_PHY                    BIT (QCA953X_SSB__RESET__RST_RESET__ETH_SWITCH_RESET )

#define ATH_RESET_REVISION_ID_MAJOR_MASK     0x0000fff0

#define ATH_BOOTSTRAP_REF_CLK_40             BIT (QCA953X_SSB__RESET__RST_BOOTSTRAP__REF_CLK)

// magic
#define ATH_QCA9531_1_0         0x0141u
#define ATH_QCA9531_1_x         0x0140u

#undef is_qca953x
#undef is_qca9531

#define is_qca953x()	(1)
#define is_qca9531()	(ATH_QCA9531_1_x == (ath_reg_rd (ATH_RST_REV_ID) & ATH_REV_ID_MASK_MAJOR))

/* -- specific -- */

// offsets
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER                  0x0000
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER_RELOADx          0x0004
#define QCA953X_REGOFS__RESET__RST_WATCHDOG_TIMER_CONTROL         0x0008
#define QCA953X_REGOFS__RESET__RST_WATHDOG_TIMER                  0x000c
#define QCA953X_REGOFS__RESET__RST_MISC_INTERUPT_STATUS           0x0010
#define QCA953X_REGOFS__RESET__RST_MISC_INTERUPT_MASK             0x0014
#define QCA953X_REGOFS__RESET__RST_GLOBAL_INTERRUPT_STATUS        0x0018
#define QCA953X_REGOFS__RESET__RST_RESET                          0x001c
#define QCA953X_REGOFS__RESET__RST_REVISION_ID                    0x0090
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER2                 0x0094
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER2_RELOAD          0x0098
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER3                 0x009c
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER3_RELOAD          0x00a0
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER4                 0x00a4
#define QCA953X_REGOFS__RESET__RST_GENERAL_TIMER4_RELOAD          0x00a8
#define QCA953X_REGOFS__RESET__PCIE_WMAC_INTERRUPT_STATUS         0x00ac
#define QCA953X_REGOFS__RESET__RST_BOOTSTRAP                      0x00b0
#define QCA953X_REGOFS__RESET__SPARE_STKY_REG                     0x00b8
#define QCA953X_REGOFS__RESET__RST_MISC2                          0x00bc
#define QCA953X_REGOFS__RESET__RST_CLKGAT_EN                      0x00c0

// bits
#define QCA953X_SSB__RESET__RST_RESET__FULL_CHIP_RESET                            24
#define QCA953X_SSB__RESET__RST_RESET__RESET_GE1_MDIO                             23
#define QCA953X_SSB__RESET__RST_RESET__RESET_GE0_MDIO                             22
#define QCA953X_SSB__RESET__RST_RESET__GE1_MAC_RESET                              13
#define QCA953X_SSB__RESET__RST_RESET__ETH_SWITCH_ARESET                          12
#define QCA953X_SSB__RESET__RST_RESET__USB_PHY_ARESET                             11
#define QCA953X_SSB__RESET__RST_RESET__GE0_MAC_RESET                               9
#define QCA953X_SSB__RESET__RST_RESET__ETH_SWITCH_RESET                            8
#define QCA953X_SSB__RESET__RST_RESET__USB_HOST_RESET                              5
#define QCA953X_SSB__RESET__RST_RESET__USB_PHY_RESET                               4
#define QCA953X_SSB__RESET__RST_RESET__USB_PHY_SUSPEND_OVERRIDE                    3

#define QCA953X_MASK__RESET__RST_REVISION_ID__VALUE                                0xffffffff

#define QCA953X_SSB__RESET__RST_BOOTSTRAP__REF_CLK                                 4

#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__PCIE_RC_INT3           8
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__PCIE_RC_INT2           7
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__PCIE_RC_INT1           6
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__PCIE_RC_INT0           5
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__PCIE_RC_IN             4
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__WMAC_RXHP_INT          3
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__WMAC_RXLP_INT          2
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__WMAC_TX_INT            1
#define QCA953X_SSB__RESET__RST_PCIE_WMAC_INTERRUPT_STATUS__WMAC_MISC_INT          0

// magic
#define QCA9531_REV_ID_MAJOR                 0x0140

/* SPI
 */

/* -- generic -- */

// addresses
#define ATH_SPI_FS                     (ATH_SPI_BASE + QCA953X_REGOFS__SERIAL_FLASH_SPI__FUNCTION_SELECT_ADDR)
#define ATH_SPI_CLOCK		           (ATH_SPI_BASE + QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_CONTROL_ADDR    )
#define ATH_SPI_WRITE		           (ATH_SPI_BASE + QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR )
#define ATH_SPI_RD_STATUS	           (ATH_SPI_BASE + QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_READ_DATA_ADDR  )

// bit masks
#define ATH_SPI_IOC_CS_ALL  \
        (BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0) | \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS1) | \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS2) ) 

#define ATH_SPI_IOC_CLK \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CLK)  
// magic
#define ATH_SPI_IOC_CS_ENABLE(i)  (ATH_SPI_IOC_CS_ALL & \
        ~BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0 + i))

/* -- specific -- */

// offsets
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__FUNCTION_SELECT_ADDR    0x0000
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_CONTROL_ADDR        0x0004
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR     0x0008
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_READ_DATA_ADDR      0x000c
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_SHIFT_DATAOUT_ADDR  0x0010
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_SHIFT_CNT_ADDR      0x0014
#define QCA953X_REGOFS__SERIAL_FLASH_SPI__SPI_SHIFT_DATAIN_ADDR   0x0018

// bits
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CLK                8
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0               16
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS1               17
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS2               18

#endif /* _953x_H */
