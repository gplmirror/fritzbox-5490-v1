/******************************************************************************
**
** FILE NAME    : ifx_gpio_def.h
** DATE         : 20 Okt 2014
** AUTHOR       : Hans Harte
** DESCRIPTION  : Enums containted in ifx_gpio.h are redefined as defines for device tree generation
** HISTORY
** $Date        $Author         $Comment
** 14 Okt 2014  Hans Harte        Init Version
*******************************************************************************/

#ifndef IFX_GPIO_DEF_H
#define IFX_GPIO_DEF_H

/*
 * ####################################
 *              Definition
 * ####################################
 */

#define IFX_GPIO_PIN_NUMBER_PER_PORT_BITS   4
#define IFX_GPIO_PIN_NUMBER_PER_PORT        (1 << IFX_GPIO_PIN_NUMBER_PER_PORT_BITS)
#define IFX_GPIO_PIN_ID(port, pin)          ((port) * IFX_GPIO_PIN_NUMBER_PER_PORT + (pin))
#define IFX_GPIO_PIN_ID_TO_PORT(pin_id)     (pin_id >> IFX_GPIO_PIN_NUMBER_PER_PORT_BITS)
#define IFX_GPIO_PIN_ID_TO_PIN(pin_id)      (pin_id & (IFX_GPIO_PIN_NUMBER_PER_PORT - 1))
#define IFX_GPIO_PIN_IS_SHIFT_PIN(pin_id)   (pin_id & (0x10 << IFX_GPIO_PIN_NUMBER_PER_PORT_BITS))

//#defines are a dublicate of the enums defined in ifx_gpio.h 
 #define    DEF_IFX_GPIO_PIN_AVAILABLE  0
 #define    DEF_IFX_GPIO_MODULE_MIN IFX_GPIO_PIN_AVAILABLE
 #define    DEF_IFX_GPIO_MODULE_TEST 1
 #define    DEF_IFX_GPIO_MODULE_MEI 2
 #define    DEF_IFX_GPIO_MODULE_DSL_NTR 3
 #define    DEF_IFX_GPIO_MODULE_SSC 4
 #define    DEF_IFX_GPIO_MODULE_ASC0 5
 #define    DEF_IFX_GPIO_MODULE_SDIO 6
 #define    DEF_IFX_GPIO_MODULE_LEDC 7
 #define    DEF_IFX_GPIO_MODULE_USB 8
 #define    DEF_IFX_GPIO_MODULE_INTERNAL_SWITCH 9
 #define    DEF_IFX_GPIO_MODULE_PCI 10
 #define    DEF_IFX_GPIO_MODULE_PCIE 11
 #define    DEF_IFX_GPIO_MODULE_NAND 12
 #define    DEF_IFX_GPIO_MODULE_PPA 13
 #define    DEF_IFX_GPIO_MODULE_TAPI_VMMC 14
 #define    DEF_IFX_GPIO_MODULE_TAPI_DEMO 15
 #define    DEF_IFX_GPIO_MODULE_TAPI_FXO 16
 #define    DEF_IFX_GPIO_MODULE_TAPI_DXT 17
 #define    DEF_IFX_GPIO_MODULE_TAPI_VCPE 18
 #define    DEF_IFX_GPIO_MODULE_VINAX 19
 #define    DEF_IFX_GPIO_MODULE_USIF_UART 20
 #define    DEF_IFX_GPIO_MODULE_USIF_SPI 21
 #define    DEF_IFX_GPIO_MODULE_SPI_FLASH 22
 #define    DEF_IFX_GPIO_MODULE_SPI_EEPROM 23
 #define    DEF_IFX_GPIO_MODULE_USIF_SPI_SFLASH 24
 #define    DEF_IFX_GPIO_MODULE_LED 25
 #define    DEF_IFX_GPIO_MODULE_EBU_LED 26
 #define    DEF_IFX_GPIO_MODULE_EXIN 27
 #define    DEF_IFX_GPIO_MODULE_PAGE 28
 #define    DEF_IFX_GPIO_MODULE_DECT 29
 #define    DEF_IFX_GPIO_MODULE_SI 30
 #define    DEF_IFX_GPIO_MODULE_SWRESET 31
 #define    DEF_IFX_GPIO_MODULE_PIGLET 32
 #define    DEF_IFX_GPIO_MODULE_PCMLINK 33
 #define    DEF_IFX_GPIO_MODULE_TDMCHECK 34
 #define    DEF_IFX_GPIO_MODULE_SYSTEM 35
 #define    DEF_IFX_GPIO_MODULE_EXTPHY_INT 36
 #define    DEF_IFX_GPIO_MODULE_EXTPHY_MDIO 37
 #define    DEF_IFX_GPIO_MODULE_EXTPHY_RESET 38
 #define    DEF_IFX_GPIO_MODULE_EXTPHY_25MHZ_CLOCK 39
 #define    DEF_IFX_GPIO_MODULE_FPGA 40
 #define    DEF_IFX_GPIO_MODULE_WLAN_OFFLOAD_WASP_RESET 41
 #define    DEF_IFX_GPIO_MODULE_WLAN_PEACOCK_RESET 42
 #define    DEF_IFX_GPIO_MODULE_PLL 43
 #define    DEF_IFX_GPIO_MODULE_SHIFT_REG 44
 #define    DEF_IFX_GPIO_MODULE_WLAN_OFFLOAD_SCRPN_UART 45
 #define    DEF_IFX_GPIO_MODULE_MAX 46
 #define    DEF_IFX_GPIO_MODULE_EARLY_REGISTER 0x08000000
//defines for the config field of the ifx_gpio_ioctl_pin_config struct in ifx_gpio.h
#define IFX_GPIO_IOCTL_PIN_CONFIG_OD_SET        (1 << 0)    /*!< config flag, enable Open Drain */
#define IFX_GPIO_IOCTL_PIN_CONFIG_OD_CLEAR      (1 << 1)    /*!< config flag, disable Open Drain */
#define IFX_GPIO_IOCTL_PIN_CONFIG_PUDSEL_SET    (1 << 2)    /*!< config flag, set Pull-Up */
#define IFX_GPIO_IOCTL_PIN_CONFIG_PUDSEL_CLEAR  (1 << 3)    /*!< config flag, set Pull-Down */
#define IFX_GPIO_IOCTL_PIN_CONFIG_PUDEN_SET     (1 << 4)    /*!< config flag, enable Pull-Up/Down */
#define IFX_GPIO_IOCTL_PIN_CONFIG_PUDEN_CLEAR   (1 << 5)    /*!< config flag, disable Pull-Up/Down */
#define IFX_GPIO_IOCTL_PIN_CONFIG_STOFF_SET     (1 << 6)    /*!< config flag, enable Schmitt Trigger */
#define IFX_GPIO_IOCTL_PIN_CONFIG_STOFF_CLEAR   (1 << 7)    /*!< config flag, disable Schmitt Trigger */
#define IFX_GPIO_IOCTL_PIN_CONFIG_DIR_OUT       (1 << 8)    /*!< config flag, configure GPIO pin as output */
#define IFX_GPIO_IOCTL_PIN_CONFIG_DIR_IN        (1 << 9)    /*!< config flag, configure GPIO pin as input */
#define IFX_GPIO_IOCTL_PIN_CONFIG_OUTPUT_SET    (1 << 10)   /*!< config flag, output 1 */
#define IFX_GPIO_IOCTL_PIN_CONFIG_OUTPUT_CLEAR  (1 << 11)   /*!< config flag, output 0 */
#define IFX_GPIO_IOCTL_PIN_CONFIG_ALTSEL0_SET   (1 << 12)   /*!< config flag, set Alternative Select 0 with value 1 */
#define IFX_GPIO_IOCTL_PIN_CONFIG_ALTSEL0_CLEAR (1 << 13)   /*!< config flag, set Alternative Select 0 with value 0 */
#define IFX_GPIO_IOCTL_PIN_CONFIG_ALTSEL1_SET   (1 << 14)   /*!< config flag, set Alternative Select 1 with value 1 */
#define IFX_GPIO_IOCTL_PIN_CONFIG_ALTSEL1_CLEAR (1 << 15)   /*!< config flag, set Alternative Select 1 with value 0 */


#endif // IFX_GPIO_DEF_H
