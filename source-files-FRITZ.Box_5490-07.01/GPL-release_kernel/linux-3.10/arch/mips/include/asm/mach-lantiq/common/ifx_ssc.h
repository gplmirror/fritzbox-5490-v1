/******************************************************************************
**
** FILE NAME    : ifx_ssc.h
** PROJECT      : IFX UEIP
** MODULES      : SSC
**
** DATE         : 03 July 2009
** AUTHOR       : Lei Chuanhua
** DESCRIPTION  : USIF for SPI Master/Slave
** COPYRIGHT    :       Copyright (c) 2009
**                      Infineon Technologies AG
**                      Am Campeon 1-12, 85579 Neubiberg, Germany
**
**    This program is free software; you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation; either version 2 of the License, or
**    (at your option) any later version.
** HISTORY
** $Date        $Author         $Comment
** 03 July,2009  Lei Chuanhua    Initial UEIP version
*******************************************************************************/
#ifndef IFX_SSC_H
#define IFX_SSC_H

/*!
 \defgroup IFX_SSC SSC bus driver module
 \brief UEIP Project - SSC bus driver module, support all CPEs.
*/

/*!
 \defgroup IFX_SSC_DEFINITIONS Definition and structions
 \ingroup IFX_SSC
 \brief definitions for ifx ssc driver
*/

/*!
  \defgroup IFX_SSC_FUNCTIONS external APIs
  \ingroup IFX_SSC
  \brief IFX ssc external driver functions
*/

/*!
  \defgroup IFX_SSC_INTERNAL Internal functions
  \ingroup IFX_SSC
  \brief IFX ssc internal driver functions
*/

/*!
 \file ifx_ssc.h
 \ingroup IFX_SSC
 \brief header file for SSC bus driver external interface
*/

/*!
 \addtogroup IFX_SSC_DEFINITIONS
*/
/* @{ */
/*! \enum  IFX_SSC_PRIO_t
    \brief Set communication priority of SSC connection

    Three priority levels are defined. Low-level priority queue used for applications like FLASH driver
    (ifx_ssc_prio_low). normal priority queue used for applications like display (ifx_ssc_prio_mid).
    High priority queue used for applications like RTP packet transfer (ifx_ssc_prio_high).
 */
typedef enum{
    IFX_SSC_PRIO_LOW = 0, /*!< Low Priority queue. For FLASH driver, etc. */
    IFX_SSC_PRIO_MID,     /*!< Normal Priority queue. For LCD, display data, etc. */
    IFX_SSC_PRIO_HIGH,    /*!< High priority queue. Ror RTP voice, etc. */
    IFX_SSC_PRIO_ASYNC,   /*!< Tasklet priority (This is the highest supported priority).
                               For this priority level only the asynchronous API set
                               of the SSC driver can be called. These funtions stay unblocked and a
                               callback function is called when the request is processed. This allows
                               that the APIs are called from tasklet level. The callback function is
                               always called on tasklet level */
}IFX_SSC_PRIO_t;

#define IFX_SSC_PRIO_MAX IFX_SSC_PRIO_ASYNC

/*! \enum  IFX_SSC_MODE_t
    \brief Defines the Ssc hardware mode settings supported

    Because there is no official specification, what exactly SPI is and what not, it is necessary
    to consult the data sheets of the components one wants to use. Important are the permitted clock
    frequencies and the type of valid transitions. There are no general rules for transitions where
    data should be latched. Although not specified by Motorola, in practice four modes are used.
    These four modes are the combinations of CPOL and CPHA. In table 1, the four modes are listed.If
    the phase of the clock is zero, i.e. CPHA = 0, data is latched at the rising edge of the clock with
    CPOL = 0, and at the falling edge of the clock with CPOL = 1. If CPHA = 1, the polarities are reversed.
    CPOL = 0 means falling edge, CPOL = 1 rising edge.The micro controllers from Motorola allow the polarity
    and the phase of the clock to be adjusted. A positive polarity results in latching data at the rising
    edge of the clock. However data is put on the data line already at the falling edge in order to stabilize.
    Most peripherals which can only be slaves, work with this configuration. If it should become necessary
    to use the other polarity, transitions are reversed.
  */
typedef enum {
    IFX_SSC_MODE_0 = 0,  /*!< CPOL=0,CPHA=0 */
    IFX_SSC_MODE_1,      /*!< CPOL=0,CPHA=1 */
    IFX_SSC_MODE_2,      /*!< CPOL=1,CPHA=0 */
    IFX_SSC_MODE_3,      /*!< CPOL=1,CPHA=1 */
    IFX_SSC_MODE_UNKNOWN,/*!< Unknown SPI mode */
} IFX_SSC_MODE_t;

/*! \enum  IFX_SSC_HANDL_TYPE_t
    \brief Defines the SPI handler type supported
 */
typedef enum {
   IFX_SSC_HANDL_TYPE_SYNC = 0, /*!< Only SYNC handler which be used by sync application */
   IFX_SSC_HANDL_TYPE_ASYNC,     /*!< Only ASYNC handler which be used by async application */
}IFX_SSC_HANDL_TYPE_t;

/*! \enum  IFX_SSC_DLX_t
    \brief Set communication duplex mode of SSC connection

    The duplex mode is used to notify SSC bus driver by SSC device driver about what kind of
    communication mode should be used. Which duplex mode will be used depends on the SSC device
    driver instead of SSC bus driver.
 */
typedef enum{
    IFX_SSC_HALF_DUPLEX = 0, /*!< Half Duplex. Interface is used in half duplex when
                                  calling \ref ifx_sscTxRx or \ref ifx_sscAsyncTxRx.
                                  The TX path is servered before the RX path. */
    IFX_SSC_FULL_DUPLEX,     /*!< Full Duplex. Interface is used in full duplex when
                                  calling \ref ifx_sscTxRx or \ref ifx_sscAsyncTxRx.
                                  The TX-and RX- path is servered simultaneously. */
}IFX_SSC_DLX_t;


/*!< \typedef IFX_CS_DATA
     \brief Definition of device specific data for chip select
 */
typedef int  IFX_CS_DATA;

enum {
    IFX_SSC_CS_ON = 0,
    IFX_SSC_CS_OFF,
};

#define IFX_SSC_WHBGPOSTAT_OUT0_POS  0
#define IFX_SSC_WHBGPOSTAT_OUT1_POS  1
#define IFX_SSC_WHBGPOSTAT_OUT2_POS  2
#define IFX_SSC_WHBGPOSTAT_OUT3_POS  3
#define IFX_SSC_WHBGPOSTAT_OUT4_POS  4
#define IFX_SSC_WHBGPOSTAT_OUT5_POS  5
#define IFX_SSC_WHBGPOSTAT_OUT6_POS  6
#define IFX_SSC_WHBGPOSTAT_OUT7_POS  7


/*! \typedef IFX_SSC_CS_CB_t
    \brief Chip Select Callback function type declaration

     csq csq=0, i.e. CS low (active). csq=1, i.e.CS high (idle).
     cs_data This is the device number in case if more than one device is using the same registered driver.
     E.g. two VINETICs, each with it�s own CS.
 */
typedef int  (*IFX_SSC_CS_CB_t)(u32 csq, IFX_CS_DATA cs_data);


/*!  \brief Parameter structure used to configure an SSC connection "ConnId".
 */
typedef struct {
    IFX_SSC_MODE_t  ssc_mode; /*!< Defines the hardware setup mode of the SSC */
    IFX_SSC_PRIO_t  ssc_prio; /*!< The communication priority of SSC connection.
                                   Three priority levels are defined. Low-level priority queue used for applications
                                   like FLASH driver (ifx_ssc_prio_low). Normal priority queue used for applications
                                   like display (ifx_ssc_prio_mid). High priority queue used for applications like RTP
                                   packet transfer (ifx_ssc_prio_high). The here defined SSC priority corresponds to
                                   the priority the SSC driver application and should be set just for information when
                                   the driver is registered by the application.
                               */
    int             baudrate; /*!< Baudrate used for the ConnId.
                                 This parameter can be later be modified by a call of ifx_sscSetBaud
                               */
    int             fragSize; /*!< All transmitted and received packets should be fragmented in this fragment
                                 size. Size given in Bytes. A maximum of 1024 Bytes is allowed. If the client
                                 uses a bigger values here, SSC returns with error. The client has to take
                                 care of the fragmentation
                               */
    int             maxFIFOSize; /*!< Maximum packet size in FIFO mode.
                                   All transmitted and received packets are transmitted in DMA mode if the packet
                                   size is greater than this value. A value of 148 is recommended at first.
                                   Size given in Bytes
                                  */
    IFX_SSC_CS_CB_t csset_cb;   /*!< Function Callback called by SSC driver when it starts/stops to receive or transmit */
    IFX_CS_DATA     cs_data;    /*!< Parameter used for the function call of "csSet_cb". */
    IFX_SSC_DLX_t   duplex_mode; /*!< Duplex Mode Selector. Connection used the SSC interface either in half- or full- duplex mode. */
} IFX_SSC_CONFIGURE_t;

/*! \typedef IFX_SSC_HANDLE
    \brief Definition of the connection handle

     as it is used by the client kernel module that use the SSC driver
     Inside of the SSC driver, this handle is mapped to an internal structure that contains the connection specific
     parameter (e.g. Baudrate, Chipselect Callbacks, etc.).
 */
typedef void * IFX_SSC_HANDLE;
/* @} */

struct chain_queue_head;

extern int ifx_ssc_cs_low(u32 pin);
extern int ifx_ssc_cs_high(u32 pin);
extern int ifx_sscSetBaud(IFX_SSC_HANDLE handler, unsigned int baud);

enum chain_queue_flags {
	CHAIN_QUEUE_FINISH,
	CHAIN_QUEUE_START,
	CHAIN_QUEUE_CONTINUE
};

extern int ifx_sscTxRx(IFX_SSC_HANDLE handler, char *tx_buf, u32 tx_len,
		       char *rx_buf, u32 rx_len, struct chain_queue_head **,
		       enum chain_queue_flags flag);
extern int ifx_sscRx(IFX_SSC_HANDLE handler, char *rx_buf, u32 rx_len,
		     struct chain_queue_head **, enum chain_queue_flags flag);
extern int ifx_sscTx(IFX_SSC_HANDLE handler, char *tx_buf, u32 tx_len,
		     struct chain_queue_head **, enum chain_queue_flags flag);
extern void ifx_ssc_chain_finish(IFX_SSC_HANDLE handler,
				 struct chain_queue_head **chain_queue);
extern IFX_SSC_HANDLE ifx_sscAllocConnection(char *dev_name, IFX_SSC_CONFIGURE_t *connid);
extern int ifx_sscFreeConnection(IFX_SSC_HANDLE handler);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
union _SPI_IRN {
    struct __SPI_IRN {
        volatile unsigned int Reserved1:27;
        volatile unsigned int TFI:1;       /*--- rw TX Finished Interrupt ---*/
        volatile unsigned int FEIR:1;      /*--- rw Frame Interrupt Request ---*/
        volatile unsigned int ERR:1;       /*--- rw Error Interrupt Request ---*/
        volatile unsigned int RIR:1;       /*--- rw Receive Interrupt Request ---*/
        volatile unsigned int TIR:1;       /*--- rw Transmit Interrupt Request ---*/
    } Bits;
    volatile unsigned int Register;
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _ifx_spi {
    union _SPI_CLC {
        struct __SPI_CLC {      
            volatile unsigned int Reserved:8;
            volatile unsigned int SMC:8;            /*--- rw Clock Divider for Sleep Mode ---*/
            volatile unsigned int RMC:8;            /*--- rw Clock Divider for normal Run Mode ---*/
            volatile unsigned int Reserved2:2;
            volatile unsigned int FSOE:1;           /*--- rw Fast Shut-Off Enable Bit 0: External fast shut off disabled ---*/
            volatile unsigned int SBWE:1;           /*---  w Suspend Write Enable Bit 0: write protection for SPEN is enabled ---*/
            volatile unsigned int EDIS:1;           /*--- rw External Request Disable 0: External clock disable request is enabled  ---*/
            volatile unsigned int SPEN:1;           /*--- rw Suspend Enable Bit for OCDS 0: Disable Module suspend disabled ---*/
            volatile unsigned int DISS:1;           /*--- rv Disable status Bit 0: Module enabled ---*/
            volatile unsigned int DISR:1;           /*--- rw Disable Request Bit 0: Not Requested Module disable is not requested ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_CLC;       /* SPI Clock control base + 0x0 */
    union _SPI_PISEL {
        struct __SPI_PISEL {
            volatile unsigned int Reserved:29;
            volatile unsigned int CIS:1;            /*--- rw Slave Mode Clock Input Select 0 PORTA Clock input is select 1: PORTB ... ---*/
            volatile unsigned int SIS:1;            /*--- rw Slave Mode Receiver Input Select 0 PORTA Receiver input is select 1: PORTB ... ---*/
            volatile unsigned int MIS:1;            /*--- rw Master Mode Receiver Input Select 0 PORTA Receiver input is select 1: PORTB ... ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_PISEL;      /* SPI Port Input Select base + 0x4 */
    union _SPI_ID {
        struct __SPI_ID {
            volatile unsigned int Reserved1:2;
            volatile unsigned int TXFS:6;           /*--- r Transmit FIFO Size  ---*/
            volatile unsigned int Reserved2:2;
            volatile unsigned int RXFS:6;           /*--- r Receive FIFO Size  ---*/
            volatile unsigned int ID:8;             /*--- r Module ID ---*/
            volatile unsigned int Reserved3:2;
            volatile unsigned int CFG:1;            /*--- r Configuration 1: DMA + is interface supported ---*/
            volatile unsigned int REV:5;            /*--- r Revision --*/
        } Bits;
        volatile unsigned int Register;
    } SPI_ID;       /* SPI Identification base + 0x8 */
    volatile unsigned int Reserved1;
    union _SPI_CON {
        struct __SPI_CON {
            volatile unsigned int Reserved1:5;
            volatile unsigned int RX_SWP:1; /*--- rw RX Byte Swap Enables byte swap in RX direction 0: SWP_OFF No byte swap ---*/
            volatile unsigned int TX_SWP:1; /*--- rw TX Byte Swap Enables byte swap in TX direction 0: SWP_OFF No byte swap ---*/
            volatile unsigned int EM:1;     /*--- rw Echo Mode 1: TXD mirrors the data on RXD ---*/
            volatile unsigned int IDLE:1;   /*--- rw Idle Bit Value ---*/
            volatile unsigned int ENBV:1;   /*--- rw Enable Byte Valid Control ---*/
            volatile unsigned int Reserved2:1;
            volatile unsigned int BM:5;     /*--- rw Data Width Selection ---*/
            volatile unsigned int TXENDI:1; /*--- rw Transmit Byte Swap Enable ---*/
            volatile unsigned int RXENDI:1; /*--- rw Receive Byte Swap Enable ---*/
            volatile unsigned int Reserved3:1;
            volatile unsigned int RUEN:1;   /*--- rw Receive Underflow Error Enable ---*/
            volatile unsigned int TUEN:1;   /*--- rw Transmit Underflow Error Enable ---*/
            volatile unsigned int AEN:1;    /*--- rw Abort Error Enable ---*/
            volatile unsigned int REN:1;    /*--- rw Receive Overflow Error Enable ---*/
            volatile unsigned int TEN:1;    /*--- rw Transmit Overflow Error Enable ---*/
            volatile unsigned int LB:1;	    /*--- rw Loopback Control ---*/
            volatile unsigned int PO:1;     /*--- rw Clock Polarity Control ---*/
            volatile unsigned int PH:1;	    /*--- rw Clock Phase Control ---*/
            volatile unsigned int HB:1;	    /*--- rw Heading Control ---*/
            volatile unsigned int CSBEN:1;	/*--- rw Chip Select Enable ---*/
            volatile unsigned int CSBINV:1;	/*--- rw Chip Select Invert ---*/
            volatile unsigned int RXOFF:1;	/*--- rw Receive Off ---*/
            volatile unsigned int TXOFF:1;	/*--- rw Transmit Off ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_CON;        /* SPI Control base + 0x10 */
    union _SPI_STAT {
        struct __SPI_STAT {
            volatile unsigned int RXOEM:1;      /*--- rh Receive End-of-Message ---*/
            volatile unsigned int RXBV:3;       /*--- rh Receive Byte Valid ---*/
            volatile unsigned int TXEOM:1;      /*--- rh Transmit End-of-Message ---*/
            volatile unsigned int TXBV:3;       /*--- rh Transmit Byte Valid ---*/
            volatile unsigned int Reserved1:3;
            volatile unsigned int BC:5;         /*--- rh Bit Count Field ---*/
            volatile unsigned int Reserved2:2;
            volatile unsigned int BSY:1;
            volatile unsigned int RUE:1;        /*--- rh Receive Underflow Error Flag ---*/
            volatile unsigned int TUE:1;        /*--- rh Transmit Underflow Error Flag ---*/
            volatile unsigned int AE:1;         /*--- rh Abort Error Flag ---*/
            volatile unsigned int RE:1;         /*--- rh Receive Error Flag ---*/
            volatile unsigned int TE:1;         /*--- rh Transmit Error Flag ---*/
            volatile unsigned int ME:1;         /*--- rh Mode Error Flag ---*/
            volatile unsigned int Reserved3:4;
            volatile unsigned int SSEL:1;       /*--- rh Slave Select ---*/
            volatile unsigned int MS:1;         /*--- rh Master/Slave Select Bit ---*/
            volatile unsigned int EN:1;         /*--- rh Enable Bit ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_STAT;        /* SPI Status base + 0x14 */
    union _SPI_WHBSTATE {
        struct __SPI_WHBSTATE {
            volatile unsigned int Reserved1:16;
            volatile unsigned int SETTUE:1;        /*--- w Set Transmit Underflow Error Flag ---*/
            volatile unsigned int SETAE:1;         /*--- w Set Abort Error Flag ---*/
            volatile unsigned int SETRE:1;         /*--- w Set Receive Error Flag ---*/
            volatile unsigned int SETTE:1;         /*--- w Set Transmit Error Flag ---*/
            volatile unsigned int CLRTUE:1;        /*--- w Clear Transmit Underflow Error Flag ---*/
            volatile unsigned int CLRAE:1;         /*--- w Clear Abort Error Flag ---*/
            volatile unsigned int CLRRE:1;         /*--- w Clear Receive Error Flag ---*/
            volatile unsigned int CLRTE:1;         /*--- w Clear Transmit Error Flag ---*/
            volatile unsigned int SETME:1;         /*--- w Set Mode Error Flag ---*/
            volatile unsigned int CLRME:1;         /*--- w Clear Mode Error Flag ---*/
            volatile unsigned int SETRUE:1;        /*--- w Set Underflow Error Flag ---*/
            volatile unsigned int CLRRUE:1;        /*--- w Clear Underflow Error Flag ---*/
            volatile unsigned int SETMS:1;         /*--- w Set Master/Slave Select Bit ---*/
            volatile unsigned int CLRMS:1;         /*--- w Clear Master/Slave Select Bit ---*/
            volatile unsigned int SETEN:1;         /*--- w Set Enable Bit ---*/
            volatile unsigned int CLREN:1;         /*--- w Clear Enable Bit ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_WHBSTATE;        /* SPI Write HW Modified State base + 0x18 */
    volatile unsigned int Reserved2;
    volatile unsigned int SPI_TB;   /*--- Transmit Data Register Value base + 0x20 ---*/
    volatile unsigned int SPI_RB;   /*--- Receive Data Register  Value base + 0x24 ---*/
    volatile unsigned int Reserved3[2];
    union _SPI_RXFCON {
        struct __SPI_RXFCON {
            volatile unsigned int Reserved1:18;
            volatile unsigned int RXFITL:6;     /*--- rw receive fifo interrupt trigger level ---*/
            volatile unsigned int Reserved2:6;
            volatile unsigned int RXFLU:1;      /*--- rw receive FIFO Flush ---*/
            volatile unsigned int RXFEN:1;      /*--- rw Receive FIFO Enable ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_RXFCON;        /* SPI Receive FIFO Control base + 0x30 */
    union _SPI_TXFCON {
        struct __SPI_TXFCON {
            volatile unsigned int Reserved1:18;
            volatile unsigned int TXFITL:6;     /*--- rw transmit fifo interrupt trigger level ---*/
            volatile unsigned int Reserved2:6;
            volatile unsigned int TXFLU:1;      /*--- rw receive FIFO Flush ---*/
            volatile unsigned int TXFEN:1;      /*--- rw Receive FIFO Enable ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_TXFCON;        /* SPI Transmit FIFO Control base + 0x34 */
    union _SPI_FSTAT {
        struct __SPI_FSTAT {
            volatile unsigned int Reserved3:18;
            volatile unsigned int TXFFL:6;      /*--- rv transmit FIFO Filling Level ---*/
            volatile unsigned int Reserved4:2;
            volatile unsigned int RXFFL:6;      /*--- rv Receive FIFO Filling Level ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_FSTAT;        /* SPI FIFO Status base + 0x38 */
    volatile unsigned int Reserved4;
    volatile unsigned int SPI_BRT;      /*--- rw Baudrate Timer Register base + 0x40 ---*/
    volatile unsigned int SPI_BRSTAT;   /*--- rv Baudrate Timer Status Register base + 0x44 ---*/
    volatile unsigned int Reserved5[6];
    union _SPI_SFCON {
        struct __SPI_SFCON {
            volatile unsigned int PLEN:10;         /*--- rw Pause Length in bits ---*/
            volatile unsigned int Reserved1:1;
            volatile unsigned int STOP:1;          /*--- rw stop after pause  ---*/
            volatile unsigned int ICLK:2;          /*--- rw idle clock configuration ---*/
            volatile unsigned int IDAT:2;          /*--- rw idle data configuration ---*/
            volatile unsigned int DLEN:12;         /*--- rw Data Length in bits ---*/
            volatile unsigned int IAEN:1;          /*--- rw enable interrupt after pause ---*/
            volatile unsigned int IBEN:1;          /*--- rw enable interrupt before pause ---*/
            volatile unsigned int Reserved2:1;
            volatile unsigned int SFEN:1;          /*--- rw serial frame enable ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_SFCON;                                   /*--- rw Serial Frame Control Register base + 0x60 ---*/
    union _SPI_SFSTAT {
        struct __SPI_SFSTAT {
            volatile unsigned int PCNT:10;         /*--- rh pause count ---*/
            volatile unsigned int Reserved1:6;
            volatile unsigned int DCNT:12;         /*--- rh pause count ---*/
            volatile unsigned int Reserved2:2;
            volatile unsigned int PBSY:1;          /*--- rh pause busy ---*/  
            volatile unsigned int DBSY:1;          /*--- rh data busy ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_SFSTAT;                                  /*--- rw Serial Frame Status Register base + 0x64 ---*/   
    volatile unsigned int Reserved6[2];
    union _SPI_GPOCON {
        struct __SPI_GPOCON {
            volatile unsigned int Reserved1:16;
            volatile unsigned int ISCSBn:8;        /*--- rw Output OUTn is Chip Select ---*/
            volatile unsigned int INVOUTn:8;       /*--- rw Invert Output OUTn ---*/
        } Bits;
        volatile unsigned int Register;
    }SPI_GPOCON;                                   /*--- SPI General Purpose Output Control Register base + 0x70 ---*/
    union _SPI_GPOSTAT {
        struct __SPI_GPOSTAT {
            volatile unsigned int Reserved1:24;
            volatile unsigned int OUTn:8;          /*--- r Output Register Bit n---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_GPOSTAT;                                 /*--- SPI General Purpose Status Register base + 0x74 ---*/
    union _SPI_FGPO {
        struct __SPI_FGPO {
            volatile unsigned int Reserved1:16;
            volatile unsigned int SETOUTn:8;       /*--- w Set Output Register Bit Bit n---*/
            volatile unsigned int CLROUTn:8;       /*--- w Clear Output Register Bit Bit n---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_FGPO;                                    /*--- SPI Force General Purpose Status Register base + 0x78 ---*/
    volatile unsigned int Reserved7;
    volatile unsigned int SPI_RXREQ;              /*--- SPI Receive Request Register base + 0x80 ---*/
    volatile unsigned int SPI_RXCNT;              /*--- SPI Receive Count Register base + 0x84 ---*/ 
    volatile unsigned int Reserved8[25];
    union _SPI_DMACON {
        struct __SPI_DMACON {
            volatile unsigned int Reserved1:28;
            volatile unsigned int RXCLS:2;      /*--- rw Receive Class ---*/
            volatile unsigned int TXON:1;       /*--- rw Transmit Path On ---*/
            volatile unsigned int RXON:1;       /*--- rw Receive Path On ---*/
        } Bits;
        volatile unsigned int Register;
    } SPI_DMACON;                               /*--- SPI DMA Control Register + 0xec ---*/
    volatile unsigned int Reserved9;
    union _SPI_IRN SPI_IRNEN;                   /*--- SPI Interrupt Node Enable Register + 0xf4 ---*/
    union _SPI_IRN SPI_IRNICR;                  /*--- SPI Interrupt Node Interrupt Capture Register + 0xf8 ---*/
    union _SPI_IRN SPI_IRNCR;                   /*--- SPI Interrupt Node Control Register + 0xfc ---*/
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void print_spi_register(const char *prefix, struct _ifx_spi *pspireg) {
    printk(KERN_INFO"%s\n", prefix);
    printk(KERN_INFO"%p:SPI_CLC         %08x\n", &pspireg->SPI_CLC,      pspireg->SPI_CLC.Register);
    printk(KERN_INFO"%p:SPI_PISEL       %08x CIS=%x SIS=%x MIS=%x\n", 
                                                 &pspireg->SPI_PISEL,    pspireg->SPI_PISEL.Register,
                                                  pspireg->SPI_PISEL.Bits.CIS, pspireg->SPI_PISEL.Bits.SIS, pspireg->SPI_PISEL.Bits.MIS);
    printk(KERN_INFO"%p:SPI_ID          %08x TXFS=%u RXFS=%u ID=0x%x CFG=%u REV=%u\n", 
                                                &pspireg->SPI_ID, pspireg->SPI_ID.Register,
                                                 pspireg->SPI_ID.Bits.TXFS, pspireg->SPI_ID.Bits.RXFS, pspireg->SPI_ID.Bits.ID,  
                                                 pspireg->SPI_ID.Bits.CFG, pspireg->SPI_ID.Bits.REV);
    printk(KERN_INFO"%p:SPI_CON         %08x RX/TX_SWP=%x/%x %s%s%sBM=%u TX/RXENDI=%x/%x %s%s%s%s%s%s PO=%x PH=%x %cSB first %s%s RX/TXOFF=%x/%x\n", 
                                               &pspireg->SPI_CON,      pspireg->SPI_CON.Register,
                                                pspireg->SPI_CON.Bits.RX_SWP,
                                                pspireg->SPI_CON.Bits.TX_SWP,
                                                pspireg->SPI_CON.Bits.EM   ? "EM " : "",
                                                pspireg->SPI_CON.Bits.IDLE ? "IDLEBIT " : "",
                                                pspireg->SPI_CON.Bits.ENBV ? "ENBV " : "",
                                                pspireg->SPI_CON.Bits.BM,
                                                pspireg->SPI_CON.Bits.TXENDI,
                                                pspireg->SPI_CON.Bits.RXENDI,
                                                pspireg->SPI_CON.Bits.RUEN ? "RUEN " : "",
                                                pspireg->SPI_CON.Bits.TUEN ? "TUEN " : "",
                                                pspireg->SPI_CON.Bits.AEN  ? "AEN " : "",
                                                pspireg->SPI_CON.Bits.REN  ? "REN " : "",
                                                pspireg->SPI_CON.Bits.TEN  ? "TEN " : "",
                                                pspireg->SPI_CON.Bits.LB   ? "LB " : "",
                                                pspireg->SPI_CON.Bits.PO,
                                                pspireg->SPI_CON.Bits.PH,
                                                pspireg->SPI_CON.Bits.HB     ? 'M' : 'L',
                                                pspireg->SPI_CON.Bits.CSBEN  ? "CSBEN " : "",    
                                                pspireg->SPI_CON.Bits.CSBINV ? "CSBINV " : "",
                                                pspireg->SPI_CON.Bits.RXOFF,
                                                pspireg->SPI_CON.Bits.TXOFF);

    printk(KERN_INFO"%p:SPI_STAT        %08x %sRXBV=%u %sTXBV=%u BC=%u %s%s%s%s%s%s%s%s%s%s\n", 
                                               &pspireg->SPI_STAT,     pspireg->SPI_STAT.Register,
                                                pspireg->SPI_STAT.Bits.RXOEM ? "RXOEM " : "",
                                                pspireg->SPI_STAT.Bits.RXBV, 
                                                pspireg->SPI_STAT.Bits.TXEOM ? "TXEOM " : "",
                                                pspireg->SPI_STAT.Bits.TXBV, 
                                                pspireg->SPI_STAT.Bits.BC,
                                                pspireg->SPI_STAT.Bits.BSY   ? "BSY " : "", 
                                                pspireg->SPI_STAT.Bits.RUE   ? "RUE " : "", 
                                                pspireg->SPI_STAT.Bits.TUE   ? "TUE " : "",
                                                pspireg->SPI_STAT.Bits.AE    ? "AE " : "",
                                                pspireg->SPI_STAT.Bits.RE    ? "RE " : "",
                                                pspireg->SPI_STAT.Bits.TE    ? "TE " : "",
                                                pspireg->SPI_STAT.Bits.ME    ? "ME " : "",
                                                pspireg->SPI_STAT.Bits.SSEL  ? "SSEL " : "",
                                                pspireg->SPI_STAT.Bits.MS    ? "MS " : "",
                                                pspireg->SPI_STAT.Bits.EN    ? "EN " : "");
    /*--- printk(KERN_INFO"%p:SPI_WHBSTATE   \n", &pspireg->SPI_WHBSTATE); ---*/
    printk(KERN_INFO"%p:SPI_RXFCON      %08x RXFITL=%u RXFEN=%x\n", 
                                               &pspireg->SPI_RXFCON,   pspireg->SPI_RXFCON.Register,
                                                pspireg->SPI_RXFCON.Bits.RXFITL,
                                                pspireg->SPI_RXFCON.Bits.RXFEN);
    printk(KERN_INFO"%p:SPI_TXFCON      %08x TXFITL=%u TXFEN=%x\n", 
                                               &pspireg->SPI_TXFCON, pspireg->SPI_TXFCON.Register,
                                                pspireg->SPI_TXFCON.Bits.TXFITL,
                                                pspireg->SPI_TXFCON.Bits.TXFEN);
    printk(KERN_INFO"%p:SPI_FSTAT       %08x TXFFL=%u RXFFL=%u\n", 
                                               &pspireg->SPI_FSTAT,    pspireg->SPI_FSTAT.Register,
                                                pspireg->SPI_FSTAT.Bits.TXFFL,
                                                pspireg->SPI_FSTAT.Bits.RXFFL);
    printk(KERN_INFO"%p:SPI_BRT         %08x\n", &pspireg->SPI_BRT,      pspireg->SPI_BRT);
    printk(KERN_INFO"%p:SPI_BRSTAT      %08x\n", &pspireg->SPI_BRSTAT,   pspireg->SPI_BRSTAT);
    printk(KERN_INFO"%p:SPI_SFCON       %08x PLEN=%u STOP=%x ICLK=%x IDAT=%x DLEN=%u IAEN=%x IBEN=%x SFEN=%x\n", 
                                               &pspireg->SPI_SFCON,    pspireg->SPI_SFCON.Register,
                                                pspireg->SPI_SFCON.Bits.PLEN,
                                                pspireg->SPI_SFCON.Bits.STOP,
                                                pspireg->SPI_SFCON.Bits.ICLK,
                                                pspireg->SPI_SFCON.Bits.IDAT,
                                                pspireg->SPI_SFCON.Bits.DLEN,
                                                pspireg->SPI_SFCON.Bits.IAEN,
                                                pspireg->SPI_SFCON.Bits.IBEN,
                                                pspireg->SPI_SFCON.Bits.SFEN);
    printk(KERN_INFO"%p:SPI_SFSTAT      %08x PCNT=%u DCNT=%u PBSY=%x DBSY=%x\n",
                                               &pspireg->SPI_SFSTAT,   pspireg->SPI_SFSTAT.Register,
                                                pspireg->SPI_SFSTAT.Bits.PCNT,
                                                pspireg->SPI_SFSTAT.Bits.DCNT,
                                                pspireg->SPI_SFSTAT.Bits.PBSY,
                                                pspireg->SPI_SFSTAT.Bits.DBSY);
    printk(KERN_INFO"%p:SPI_GPOCON      %08x ISCSBn=0x%x INVOUTn=0x%x\n", 
                                               &pspireg->SPI_GPOCON,   pspireg->SPI_GPOCON.Register,
                                                pspireg->SPI_GPOCON.Bits.ISCSBn,
                                                pspireg->SPI_GPOCON.Bits.INVOUTn);
    printk(KERN_INFO"%p:SPI_GPOSTAT     %08x OUTn=0x%x\n", 
                                               &pspireg->SPI_GPOSTAT,   pspireg->SPI_GPOSTAT.Register,
                                                pspireg->SPI_GPOSTAT.Bits.OUTn);
    /*--- printk(KERN_INFO"%p:SPI_FGPO        \n", &pspireg->SPI_FGPO); ---*/
    printk(KERN_INFO"%p:SPI_RXREQ       %08x\n", &pspireg->SPI_RXREQ, pspireg->SPI_RXREQ);
    printk(KERN_INFO"%p:SPI_RXCNT       %08x\n", &pspireg->SPI_RXCNT, pspireg->SPI_RXCNT);
    printk(KERN_INFO"%p:SPI_DMACON      %08x RXCLS=%u %s %s\n", &pspireg->SPI_DMACON, pspireg->SPI_DMACON.Register,
                                                        pspireg->SPI_DMACON.Bits.RXCLS,
                                                        pspireg->SPI_DMACON.Bits.TXON ? "TXON" : "TXOFF", 
                                                        pspireg->SPI_DMACON.Bits.RXON ? "RXON" : "RXOFF"
                                                        );
    printk(KERN_INFO"%p:SPI_IRNEN       %08x %s%s%s%s%s \n", &pspireg->SPI_IRNEN, pspireg->SPI_IRNEN.Register,
                                                    pspireg->SPI_IRNEN.Bits.TFI  ? "TFIEN "  : "",
                                                    pspireg->SPI_IRNEN.Bits.FEIR ? "FEIREN " : "",
                                                    pspireg->SPI_IRNEN.Bits.ERR  ? "ERREN "  : "",
                                                    pspireg->SPI_IRNEN.Bits.RIR  ? "RIREN "  : "",
                                                    pspireg->SPI_IRNEN.Bits.TIR  ? "TIREN "  : "");
    printk(KERN_INFO"%p:SPI_IRNICR      %08x %s%s%s%s%s \n", &pspireg->SPI_IRNICR, pspireg->SPI_IRNICR.Register,
                                                    pspireg->SPI_IRNICR.Bits.TFI  ? "TFI "  : "",
                                                    pspireg->SPI_IRNICR.Bits.FEIR ? "FEIR " : "",
                                                    pspireg->SPI_IRNICR.Bits.ERR  ? "ERR "  : "",
                                                    pspireg->SPI_IRNICR.Bits.RIR  ? "RIR "  : "",
                                                    pspireg->SPI_IRNICR.Bits.TIR  ? "TIR "  : "");
    printk(KERN_INFO"%p:SPI_IRNCR       %08x %s%s%s%s%s \n", &pspireg->SPI_IRNCR, pspireg->SPI_IRNCR.Register,
                                                    pspireg->SPI_IRNCR.Bits.TFI  ? "TFI "  : "",
                                                    pspireg->SPI_IRNCR.Bits.FEIR ? "FEIR " : "",
                                                    pspireg->SPI_IRNCR.Bits.ERR  ? "ERR "  : "",
                                                    pspireg->SPI_IRNCR.Bits.RIR  ? "RIR "  : "",
                                                    pspireg->SPI_IRNCR.Bits.TIR  ? "TIR "  : "");
}
#endif /* IFX_SSC_H */
