#include <linux/avm_power.h>

extern int ifx_ts_get_temp(int *p_temp);

static void *handle=0;

int Vr9TemperaturSensorReadTemperatureCallback(void * handle, void *context, int *value) {
    if( ifx_ts_get_temp(value) ) {
        return -1;
    }
    return 0;
}

static int __init register_sensor(void) {
    handle = TemperaturSensorRegister("CPU", Vr9TemperaturSensorReadTemperatureCallback, 0);
    if(!handle) {
        printk(KERN_ERR "Could not register CPU Temperature sensor to AVM Power driver.\n");
    }
    return 0;
}
static void __exit deregister_sensor(void) {
    if(handle) {
        TemperaturSensorDeregister(handle);
    }
}
__initcall(register_sensor);
__exitcall(deregister_sensor);
