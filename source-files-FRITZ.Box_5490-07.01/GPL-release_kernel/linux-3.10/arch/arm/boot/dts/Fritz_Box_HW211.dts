#include <linux/avm_hw_config_def.h>
#include <dt-bindings/interrupt-controller/arm-gic.h>
#include "qcom-ipq8064-v1.0.dtsi"

/ {
	model = "AVM4080 (IPQ8064; HW211)";
	compatible = "qcom,ipq8064-avm4080", "qcom,ipq8064", "avm,hw211", "avm,fb4080";
    memory {
      device_type = "memory";
      reg = <0x00000000 0x00000000>;
    };
	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		rsvd@41200000 {
			reg = <0x41200000 0x300000>;
			no-map;
		};
	};

	aliases {
		mdio-gpio0 = &mdio0;
	};
    avm-hw-revision{
        compatible = "avm,avm_hw_revision";
        revision = "211";
        //subrevision = "1";
    };
    avm-gpio{
      compatible = "avm,avm_gpio_generic";
        gpio_avm_led_power{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <31>;
        };
        gpio_avm_led_wan{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <32>;
        };
        gpio_avm_led_wlan{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <52>;
        };
        gpio_avm_led_internet{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <51>;
        };
        gpio_avm_led_info{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <60>;
        };
        gpio_avm_led_info_red{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <59>;
        };
        gpio_avm_button_dect{
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
            value = <55>;
        };
        gpio_avm_button_wps{
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
            value = <56>;
        };
        gpio_avm_button_wlan{
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
            value = <58>;
        };
        gpio_avm_dect_reset{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_LOW>;
            value = <25>;
        };
        gpio_avm_dect_uart_rx{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <23>;
        };
        gpio_avm_dect_uart_tx{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <22>;
        };
        gpio_avm_piglet_noemif_prog{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <6>;
        };
        gpio_avm_piglet_noemif_clk{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <9>;
        };
        gpio_avm_piglet_noemif_data{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <7>;
        };
        gpio_avm_piglet_noemif_done{
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
            value = <8>;
        };
        gpio_avm_tdm_fsc{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <16>;
        };
        gpio_avm_tdm_dcl{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <17>;
        };
        gpio_avm_pcmlink_fsc{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <16>;
        };
        gpio_avm_pcmlink_do{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_LOW>;
            value = <14>;
        };
        gpio_avm_pcmlink_di{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <15>;
        };
        gpio_avm_pcmlink_dcl{
            param = <AVM_DEF_HW_PARAM_NO_PARAM>;
            value = <17>;
        };
        gpio_avm_usb_power_enable{
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
            value = <54>;
        };
        gpio_avm_usb_fault_detect{
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
            value = <24>;
        };

    };

	soc {
		pinmux@800000 {
			pinctrl-0 = <&mdio0_pins_default>;
			pinctrl-names = "default";

			pcie1_pins: pcie1_pinmux {
				mux {
					pins = "gpio3";
					drive-strength = <2>;
					bias-disable;
				};
			};

			pcie2_pins: pcie2_pinmux {
				mux {
					pins = "gpio48";
					drive-strength = <2>;
					bias-disable;
				};
			};

			spi_pins: spi_pins {
				mux {
					pins = "gpio18", "gpio19", "gpio21";
					function = "gsbi5";
					drive-strength = <10>;
					bias-none;
				};
				cs {
					pins = "gpio20";
					drive-strength = <12>;
				};
			};

			dectuart_pins: uart1_pins {
				mux {
					pins = "gpio22", "gpio23";
					function = "gsbi2";
					drive-strength = <12>;
					bias-disable;
				};
			};
			nand_pins: nand_pins {
				mux {
					pins = "gpio34", "gpio35", "gpio36",
						"gpio37", "gpio38", "gpio39",
						"gpio40", "gpio41", "gpio42",
						"gpio43", "gpio44", "gpio45",
						"gpio46", "gpio47";
					function = "nand";
					drive-strength = <10>;
					bias-disable;
				};
				pullups {
					pins = "gpio39";
					bias-pull-up;
				};
				hold {
					pins = "gpio40", "gpio41", "gpio42",
						"gpio43", "gpio44", "gpio45",
						"gpio46", "gpio47";
					bias-bus-hold;
				};
			};

			mdio0_pins_default: mdio0_pins_default {
				mux {
					pins = "gpio0", "gpio1";
					function = "mdio";
					drive-strength = <8>;
					bias-disable;
				};

				clk {
					pins = "gpio1";
					input-disable;
				};
			};
		};
		gsbi@12480000 {
			qcom,mode = <GSBI_PROT_I2C_UART>;
			status = "ok";

			pinctrl-0 = <&dectuart_pins>;
			pinctrl-names = "default";

			serial@12490000 {
				status = "ok";
			};
		};

		gsbi@16300000 {
			qcom,mode = <GSBI_PROT_I2C_UART>;
			status = "ok";
			serial@16340000 {
				status = "ok";
			};
		};

		gsbi5: gsbi@1a200000 {
			qcom,mode = <GSBI_PROT_SPI>;
			status = "ok";

			spi4: spi@1a280000 {
				status = "ok";
				spi-max-frequency = <50000000>;

				pinctrl-0 = <&spi_pins>;
				pinctrl-names = "default";

				cs-gpios = <&qcom_pinmux 20 0>;

				dmas = <&adm_dma 6 9>,
					<&adm_dma 5 10>;
				dma-names = "rx", "tx";

				flash: m25p80@0 {
					compatible = "s25fl256s1";
					#address-cells = <1>;
					#size-cells = <1>;
					spi-max-frequency = <50000000>;
					reg = <0>;
					m25p,fast-read;

					partition@0 {
						label = "rootfs";
						reg = <0x0 0x1000000>;
					};

					partition@1 {
						label = "scratch";
						reg = <0x1000000 0x1000000>;
					};
				};
			};
		};

		pci@1b500000 {
			status = "ok";
			reset-gpio = <&qcom_pinmux 3 0>;
			pinctrl-0 = <&pcie1_pins>;
			pinctrl-names = "default";

			ranges = <0x00000000 0 0x00000000 0x0ff00000 0 0x00100000   /* configuration space */
				  0x81000000 0 0	  0x0fe00000 0 0x00100000   /* downstream I/O */
				  0x82000000 0 0x00000000 0x08000000 0 0x07e00000>; /* non-prefetchable memory */
		};

		pci@1b700000 {
			status = "ok";
			reset-gpio = <&qcom_pinmux 48 0>;
			pinctrl-0 = <&pcie2_pins>;
			pinctrl-names = "default";

			ranges = <0x00000000 0 0x00000000 0x31f00000 0 0x00100000   /* configuration space */
				  0x81000000 0 0	  0x31e00000 0 0x00100000   /* downstream I/O */
				  0x82000000 0 0x00000000 0x2e000000 0 0x03e00000>; /* non-prefetchable memory */

		};


		sata-phy@1b400000 {
			status = "ok";
		};

		sata@29000000 {
			status = "ok";
		};

		dma@18300000 {
			status = "ok";
		};

		nand@0x1ac00000 {
			status = "ok";

			pinctrl-0 = <&nand_pins>;
			pinctrl-names = "default";
		};

		tcsr@1a400000 {
			status = "ok";
			qcom,usb-ctrl-select = <TCSR_USB_SELECT_USB3_DUAL>;
		};

		phy@100f8800 {		/* USB3 port 1 HS phy */
			status = "ok";
		};

		phy@100f8830 {		/* USB3 port 1 SS phy */
			status = "ok";
		};

		phy@110f8800 {		/* USB3 port 0 HS phy */
			status = "ok";
		};

		phy@110f8830 {		/* USB3 port 0 SS phy */
			status = "ok";
		};

		usb30@0 {
			status = "ok";
		};

		usb30@1 {
			status = "ok";
		};

		gpio-leds {
			compatible = "gpio-leds";

			one {
				label = "avm:one";
				gpios = <&qcom_pinmux 31 0>;
			};
			two {
				label = "avm:two";
				gpios = <&qcom_pinmux 32 0>;
			};
			three {
				label = "avm:three";
				gpios = <&qcom_pinmux 51 0>;
			};
			four {
				label = "avm:four";
				gpios = <&qcom_pinmux 52 0>;
			};
			five {
				label = "avm:five";
				gpios = <&qcom_pinmux 59 0>;
			};
			six {
				label = "avm:six";
				gpios = <&qcom_pinmux 60 0>;
			};
		};

		mdio0: mdio {
			compatible = "virtual,mdio-gpio";
			#address-cells = <1>;
			#size-cells = <0>;
			gpios = <&qcom_pinmux 1 0 &qcom_pinmux 0 0>;

			phy0: ethernet-phy@0 {
				device_type = "ethernet-phy";
				reg = <0>;
				qca,ar8327-initvals = <
					0x00004 0x7600000   /* PAD0_MODE */
					0x00008 0x1000000   /* PAD5_MODE */
					0x0000c 0x80        /* PAD6_MODE */
					0x000e4 0xaa545     /* MAC_POWER_SEL */
					0x000e0 0xc74164de  /* SGMII_CTRL */
					0x0007c 0x4e        /* PORT0_STATUS */
					0x00094 0x4e        /* PORT6_STATUS */
				>;
			};

			phy4: ethernet-phy@4 {
				device_type = "ethernet-phy";
				reg = <4>;
			};
		};

		nss0: nss@40000000 {
			compatible = "qcom,nss0";
			interrupts = <GIC_SPI 213 IRQ_TYPE_LEVEL_HIGH
				      GIC_SPI 232 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x36000000 0x1000 0x39000000 0x20000>;
			reg-names = "nphys", "vphys";
			clocks = <&gcc NSS_CORE_CLK>, <&gcc NSSTCM_CLK_SRC>, <&gcc NSSTCM_CLK>;
			clock-names = "nss_core_clk", "nss_tcm_src", "nss_tcm_clk";
			resets = <&gcc UBI32_CORE1_CLKRST_CLAMP_RESET>,
				 <&gcc UBI32_CORE1_CLAMP_RESET>,
				 <&gcc UBI32_CORE1_AHB_RESET>,
				 <&gcc UBI32_CORE1_AXI_RESET>;
			reset-names = "clkrst_clamp", "clamp", "ahb", "axi";
			qcom,id = <0>;
			qcom,num_irq = <2>;
			qcom,rst_addr = <0x40000000>;
			qcom,load_addr = <0x40000000>;
			qcom,turbo_frequency = <0>;
			qcom,low_frequency = <110000000>;
			qcom,mid_frequency = <275000000>;
			qcom,max_frequency = <550000000>;
			qcom,ipv4_enabled = <1>;
			qcom,ipv6_enabled = <1>;
			qcom,l2switch_enabled = <1>;
			qcom,crypto_enabled = <0>;
			qcom,ipsec_enabled = <0>;
			qcom,wlan_enabled = <0>;
			qcom,tun6rd_enabled = <1>;
			qcom,tunipip6_enabled = <1>;
			qcom,shaping_enabled = <1>;
			qcom,gmac0_enabled = <1>;
			qcom,gmac1_enabled = <1>;
			qcom,gmac2_enabled = <1>;
			qcom,gmac3_enabled = <1>;
		};

		nss-gmac-common {
			reg = <0x03000000 0x0000FFFF 0x1bb00000 0x0000FFFF 0x00900000 0x00004000>;
			reg-names = "nss_reg_base" , "qsgmii_reg_base", "clk_ctl_base";
		};

		gmac1: ethernet@37000000 {
			device_type = "network";
			compatible = "qcom,nss-gmac";
			reg = <0x37000000 0x200000>;
			interrupts = <GIC_SPI 220 IRQ_TYPE_LEVEL_HIGH>;
			phy-mode = "rgmii";
			qcom,id = <0>;
			qcom,phy_mdio_addr = <4>;
			qcom,poll_required = <1>;
			qcom,rgmii_delay = <1>;
			qcom,emulation = <0>;
			qcom,forced_speed = <1000>;
			qcom,forced_duplex = <1>;
			qcom,socver = <0>;
			local-mac-address = [000000000000];
			mdiobus = <&mdio0>;
			avm,phy_mii_type = <0>;
			avm,dma_mask = <0xffffffff>;
		};

		gmac2: ethernet@37200000 {
			device_type = "network";
			compatible = "qcom,nss-gmac";
			reg = <0x37200000 0x200000>;
			interrupts = <GIC_SPI 223 IRQ_TYPE_LEVEL_HIGH>;
			phy-mode = "sgmii";
			qcom,id = <1>;
			qcom,phy_mdio_addr = <0>;
			qcom,poll_required = <0>;
			qcom,rgmii_delay = <0>;
			qcom,emulation = <0>;
			qcom,forced_speed = <1000>;
			qcom,forced_duplex = <1>;
			qcom,socver = <0>;
			local-mac-address = [000000000000];
			mdiobus = <&mdio0>;
			avm,phy_mii_type = <1>;
			avm,dma_mask = <0xffffffff>;
		};

	};
};
