#ifndef _mach_avm_h_
#define _mach_avm_h_

#include <linux/clk.h>
#include <linux/clkdev.h>
extern struct resource gpio_resource;

#ifdef CONFIG_ARCH_IPQ806X_DT

extern unsigned int msm_get_timer_count(void); 
extern unsigned long msm_get_timer_freq(void);
#if 0
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_cpu        = 0x0,
    avm_clock_id_timer	    = 0x1,
};
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int avm_get_clock(enum _avm_clock_id clock_id){
	printk(KERN_CRIT"%s ERROR: actually not implemented\n", __func__);
	if(clock_id == avm_clock_id_cpu) { 
	    return acpuclk_get_rate(smp_processor_id());
	} else if(clock_id == avm_clock_id_timer) {
	    return msm_get_timer_count();
	}
	return 0;
}
#endif
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _hw_gpio_direction {
    GPIO_OUTPUT_PIN = 0,
    GPIO_INPUT_PIN  = 1,
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
enum _hw_gpio_function {
    GPIO_PIN	    = 0,
	/*--- 	depends on GPIO ---*/
    FUNCTION_1_PIN  = 1,
    FUNCTION_2_PIN  = 2,
    FUNCTION_3_PIN  = 3,
};

#include <linux/pinctrl/pinconf-generic.h>

extern void msm_avmgpio_init(void);
extern int msm_avm_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir);
extern unsigned char *msm_avm_gpio_get_membase_and_bit(unsigned int gpio_pin, unsigned int *bit);
/*--------------------------------------------------------------------------------*\
 * bias: PIN_CONFIG_BIAS_...
 * drive: mA-Value
\*--------------------------------------------------------------------------------*/
extern int msm_avm_gpio_config(unsigned int gpio_pin, unsigned int bias, unsigned int drive);
extern int msm_avm_gpio_out_bit(unsigned int gpio_pin, int value);
extern int msm_avm_gpio_in_bit(unsigned int gpio_pin);


#define avm_gpio_init					msm_avm_gpio_init
#define avm_gpio_ctrl					msm_avm_gpio_ctrl
#define avm_gpio_out_bit				msm_avm_gpio_out_bit
#define avm_gpio_in_bit					msm_avm_gpio_in_bit
#define avm_gpio_config					msm_avm_gpio_config
#define avm_gpio_get_membase_and_bit			msm_avm_gpio_get_membase_and_bit
/*--- todo: ---*/
#define avm_gpio_request_irq				msm_avm_gpio_request_irq
#define avm_gpio_enable_irq				msm_avm_gpio_enable_irq
#define avm_gpio_disable_irq				msm_avm_gpio_disable_irq
#define avm_read_irq_gpio				msm_avm_read_irq_gpio
#define avm_gpio_set_delayed_irq_mode			msm_avm_gpio_set_delayed_irq_mode

#endif /*--- #ifdef CONFIG_ARCH_IPQ806X_DT ---*/

#endif/*--- #ifndef _mach_avm_h_ ---*/
