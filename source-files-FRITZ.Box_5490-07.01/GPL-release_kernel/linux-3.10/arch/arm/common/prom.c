#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <linux/avm_hw_config.h>
#include <linux/of_avm_dt.h>
#include <linux/errno.h>
#include <linux/of_fdt.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/bootmem.h>
#include <linux/memblock.h>
#include <linux/slab.h>
#include <linux/avm_hw_config_def.h>
#include <asm/mach_avm.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/env.h>

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int __init prom_init(void) {
    return set_wlan_dect_config_address(NULL);
}
late_initcall(prom_init); //initialisation via late initcall

