#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <linux/avm_hw_config.h>
#include <linux/env.h>

/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config für FRITZ!Box Fon 4080 
 *
 *
 *
 * Dummy bis durch Device-Tree-Generator ausgetauscht
\*------------------------------------------------------------------------------------------*/

#undef AVM_HARDWARE_CONFIG
#define AVM_HARDWARE_CONFIG  avm_hardware_config_hw211


struct _avm_hw_config AVM_HARDWARE_CONFIG[] = {

    /*--------------------------------------------------------------------------------------*\
     * DECT
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_dect_reset",
        .value  = 25,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_dect_uart_rx",
        .value  = 23,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_dect_uart_tx",
        .value  = 22,
        .param  = avm_hw_param_no_param,
    },


    /*--------------------------------------------------------------------------------------*\
     * FPGA
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_piglet_noemif_prog",
        .value  = 6,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_piglet_noemif_clk",
        .value  = 9,
        .param  = avm_hw_param_gpio_out_active_high,
    },
    {
        .name   = "gpio_avm_piglet_noemif_data",
        .value  = 7,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_piglet_noemif_done",
        .value  = 8,
        .param  = avm_hw_param_gpio_in_active_low,
    },
    /*--------------------------------------------------------------------------------*\
     * TDM-Check (Clockmessung)
    \*--------------------------------------------------------------------------------*/
    { 
        .name   = "gpio_avm_tdm_fsc",              
        .value  = 16,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_tdm_dcl",
        .value  = 17,
        .param  = avm_hw_param_no_param,
    },

    /*--------------------------------------------------------------------------------------*\
     * PCM-BUS
    \*--------------------------------------------------------------------------------------*/
    { 
        .name   = "gpio_avm_pcmlink_fsc",              
        .value  = 16,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_pcmlink_do",
        .value  = 14,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_pcmlink_di",
        .value  = 15,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_pcmlink_dcl",
        .value  = 17,
        .param  = avm_hw_param_no_param,
    },
    {   .name   = NULL }
};
EXPORT_SYMBOL(AVM_HARDWARE_CONFIG);



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config_table avm_hw_config_tables[] = {
    { .hwrev = 211, .hwsubrev = 0, .table = avm_hardware_config_hw211}, /*--- FRITZ!Box 4080 ---*/
};
EXPORT_SYMBOL(avm_hw_config_tables);



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_get_hw_config_table(void)
{
#if 0
    static struct _avm_hw_config *current_config_table = NULL;
    unsigned int hwrev, hwsubrev, i;
    char *s;
#endif
    if(avm_current_hw_config){
        return avm_current_hw_config;
    }
    printk("[%s] No Config Table found, generation from Device Tree must have failed!\n", __FUNCTION__); 
    BUG_ON(1);
    return NULL;
#if 0
    if(current_config_table) {
        return current_config_table;
    }

    s = prom_getenv("HWRevision");
    if (s) {
        hwrev = simple_strtoul(s, NULL, 10);
    } else {
        hwrev = 0;
    } 
    s = prom_getenv("HWSubRevision");
    if (s) {
        hwsubrev = simple_strtoul(s, NULL, 10);
    } else {
        hwsubrev = 1;
    }    
    if(hwrev == 0) {
        printk("[%s] error: no HWRevision detected in environment variables\n", __FUNCTION__);
        BUG_ON(1);
        return NULL;
    }
    for(i = 0; i < sizeof(avm_hw_config_tables)/sizeof(struct _avm_hw_config_table); i++) {
        if(avm_hw_config_tables[i].hwrev == hwrev) {
            if((avm_hw_config_tables[i].hwsubrev == 0) || (avm_hw_config_tables[i].hwsubrev == hwsubrev)) {
                current_config_table = avm_hw_config_tables[i].table;
                return current_config_table;
            }
        }
    }
    printk("[%s] error: No hardware configuration defined for HWRevision %d\n", __FUNCTION__, hwrev);
    BUG_ON(1);
    return NULL;
#endif
}
EXPORT_SYMBOL(avm_get_hw_config_table);


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int init_gpio_config(void) 
{
    int index;
    struct _avm_hw_config *config;
    printk("Initializing GPIO config\n"); 
    config = avm_current_hw_config;
    //config = avm_get_hw_config_table();
    if(!config) {
        printk("Initializing GPIO no config found\n"); 
     
        return -ENOENT; // error: no hardware config found!
    }
    index = 0;
    while(config->name) {
#if 0
        if(config->manufactor_hw_config.manufactor_lantiq_gpio_config.module_id > 0) {
            if(index >= GPIO_BOARD_CONFIG_SIZE - 1) {
                printk("[%s] error: Number of GPIOs in hardware config exceeds limit of %d\n", __FUNCTION__, GPIO_BOARD_CONFIG_SIZE);
                return;
            }
            g_board_gpio_pin_map[index].pin       = config->value;
            g_board_gpio_pin_map[index].module_id = config->manufactor_hw_config.manufactor_lantiq_gpio_config.module_id;
            g_board_gpio_pin_map[index].config    = config->manufactor_hw_config.manufactor_lantiq_gpio_config.config;
            index++;
            printk("[%s] %d: GPIO config for %s: pin=%d module_id=0x%x config=0x%x\n", __FUNCTION__, index, config->name, config->value,
                    /*--- g_board_gpio_pin_map[index].module_id,  g_board_gpio_pin_map[index].config); ---*/
        }
#endif
        config++;
    }

    /*--- writing the end-of-gpio-config marker ---*/
    /*--- g_board_gpio_pin_map[index].module_id = IFX_GPIO_PIN_AVAILABLE; ---*/
    return 0;
}
arch_initcall(init_gpio_config);

