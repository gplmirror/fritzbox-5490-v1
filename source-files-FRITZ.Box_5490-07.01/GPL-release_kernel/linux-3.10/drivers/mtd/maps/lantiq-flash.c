/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 *
 *  Copyright (C) 2004 Liu Peng Infineon IFAP DC COM CPE
 *  Copyright (C) 2010 John Crispin <blogic@openwrt.org>
 */

#include <linux/err.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/cfi.h>
#include <linux/platform_device.h>
#include <linux/mtd/physmap.h>
#include <linux/of.h>

#include <lantiq_soc.h>

#include <ifx_types.h>
#include <ifx_regs.h>

/*
 * The NOR flash is connected to the same external bus unit (EBU) as PCI.
 * To make PCI work we need to enable the endianness swapping for the address
 * written to the EBU. This endianness swapping works for PCI correctly but
 * fails for attached NOR devices. To workaround this we need to use a complex
 * map. The workaround involves swapping all addresses whilst probing the chip.
 * Once probing is complete we stop swapping the addresses but swizzle the
 * unlock addresses to ensure that access to the NOR device works correctly.
 */

enum {
	LTQ_NOR_PROBING,
	LTQ_NOR_NORMAL
};

struct ltq_mtd {
	struct resource *res;
	struct mtd_info *mtd;
	struct map_info *map;
};

static const char * const ltq_probe_types[] = { "cmdlinepart", "ofpart", NULL };

static map_word
ltq_read16(struct map_info *map, unsigned long adr)
{
	unsigned long flags;
	map_word temp;

	if (map->map_priv_1 == LTQ_NOR_PROBING)
		adr ^= 2;
	spin_lock_irqsave(&ebu_lock, flags);
	temp.x[0] = *(u16 *)(map->virt + adr);
	spin_unlock_irqrestore(&ebu_lock, flags);
	return temp;
}

static void
ltq_write16(struct map_info *map, map_word d, unsigned long adr)
{
	unsigned long flags;

	if (map->map_priv_1 == LTQ_NOR_PROBING)
		adr ^= 2;
	spin_lock_irqsave(&ebu_lock, flags);
	*(u16 *)(map->virt + adr) = d.x[0];
	spin_unlock_irqrestore(&ebu_lock, flags);
}

/*
 * The following 2 functions copy data between iomem and a cached memory
 * section. As memcpy() makes use of pre-fetching we cannot use it here.
 * The normal alternative of using memcpy_{to,from}io also makes use of
 * memcpy() on MIPS so it is not applicable either. We are therefore stuck
 * with having to use our own loop.
 */
static void
ltq_copy_from(struct map_info *map, void *to,
	unsigned long from, ssize_t len)
{
	unsigned char *f = (unsigned char *)map->virt + from;
	unsigned char *t = (unsigned char *)to;
	unsigned long flags;

	spin_lock_irqsave(&ebu_lock, flags);
	while (len--)
		*t++ = *f++;
	spin_unlock_irqrestore(&ebu_lock, flags);
}

static void
ltq_copy_to(struct map_info *map, unsigned long to,
	const void *from, ssize_t len)
{
#if 0
	unsigned char *f = (unsigned char *)from;
	unsigned char *t = (unsigned char *)map->virt + to;
	unsigned long flags;

	spin_lock_irqsave(&ebu_lock, flags);
	while (len--)
		*t++ = *f++;
	spin_unlock_irqrestore(&ebu_lock, flags);
#else
	unsigned short *dest;
	unsigned short *source;
	unsigned long flags;

	if((unsigned long)to & 0x01) {
		printk(KERN_ERR "[%s] unaligned write to 16 Bit device\n", __FUNCTION__);
		return;
	}

	to = (unsigned long) (to + map->virt);

	spin_lock_irqsave(&ebu_lock, flags);
	if((unsigned long)from & 0x01) {
		unsigned char *p = (unsigned char *)from;
		unsigned short s;
		dest = (unsigned short *)to;
		while(len) {
			s  = 0;
#ifdef __BIG_ENDIAN
			s |= *p++ << 8;
			s |= *p++ & 0xff;
#endif 
#ifdef __LITTLE_ENDIAN
			s |= *p++ & 0xff;
			s |= *p++ << 8;
#endif
			if(len == 1) {
				*dest = s | 0xff00;
				break;
			}
			*dest++ = s;
			len -= 2;
		}
	    spin_unlock_irqrestore(&ebu_lock, flags);
		return;
	}
	dest = (unsigned short *)to;
	source = (unsigned short *)from;

	while(len) {
		if(len == 1) {
			*dest = *source | 0xFF00;
			break;
		}
		*dest++ = *source++;
		len -= 2;
	}
	spin_unlock_irqrestore(&ebu_lock, flags);
#endif
}

/* Configure EBU */
static void ltq_mtd_hwinit(struct ltq_mtd *ltq_mtd) {

    unsigned int mask, base, reg;

    mask  = fls(ltq_mtd->map->size);
    mask -= 13;
    mask  = ~mask & 0xF;
    base  = ltq_mtd->res->start >> 12;

    reg   = IFX_EBU_ADDSEL0_BASE(base) | IFX_EBU_ADDSEL0_MASK(mask) | IFX_EBU_ADDSEL0_REGEN;

    *IFX_EBU_ADDSEL0 = reg;

    reg = SM(IFX_EBU_BUSCON0_XDM16, IFX_EBU_BUSCON0_XDM) |
#if 0
        IFX_EBU_BUSCON0_ADSWP |  /*--- Hardware-Swapping muss in diesem Treiber ausgeschaltet sein, da die Kommando-Adressen explitzit im 2.Bit verXORt werden ---*/
#endif
        SM(IFX_EBU_BUSCON0_ALEC3, IFX_EBU_BUSCON0_ALEC) |
        SM(IFX_EBU_BUSCON0_BCGEN_INTEL,IFX_EBU_BUSCON0_BCGEN) |
        SM(IFX_EBU_BUSCON0_WAITWRC7, IFX_EBU_BUSCON0_WAITWRC) |
        SM(IFX_EBU_BUSCON0_WAITRDC3, IFX_EBU_BUSCON0_WAITRDC) |
        SM(IFX_EBU_BUSCON0_HOLDC3, IFX_EBU_BUSCON0_HOLDC) |
        SM(IFX_EBU_BUSCON0_RECOVC3, IFX_EBU_BUSCON0_RECOVC);

    reg |= SM(IFX_EBU_BUSCON0_CMULT16, IFX_EBU_BUSCON0_CMULT);  

    IFX_REG_W32(reg, IFX_EBU_BUSCON0);

    /*--- pr_debug("{%s} phys 0x%x size 0x%lx virt 0x%p res %s\n", ---*/ 
            /*--- __func__, ltq_mtd->map->phys, ltq_mtd->map->size, ltq_mtd->map->virt, ltq_mtd->res->name); ---*/
}

static int
ltq_mtd_probe(struct platform_device *pdev)
{
	struct physmap_flash_data *pdata;
	struct mtd_part_parser_data ppdata;
	struct ltq_mtd *ltq_mtd;
	struct cfi_private *cfi;
	int err;

	if (of_machine_is_compatible("lantiq,falcon") && (ltq_boot_select() != BS_FLASH)) {
		dev_err(&pdev->dev, "invalid bootstrap options\n");
		return -ENODEV;
	}

	pdata = pdev->dev.platform_data;

	ltq_mtd = kzalloc(sizeof(struct ltq_mtd), GFP_KERNEL);
    if ( ! ltq_mtd) {
        dev_err(&pdev->dev, "no memory\n");
        return -ENOMEM;
    }

	platform_set_drvdata(pdev, ltq_mtd);

	ltq_mtd->res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!ltq_mtd->res) {
		dev_err(&pdev->dev, "failed to get memory resource\n");
		err = -ENOENT;
		goto err_out;
	}

	ltq_mtd->map = kzalloc(sizeof(struct map_info), GFP_KERNEL);
    if ( ! ltq_mtd->map) {
        dev_err(&pdev->dev, "map - no memory\n");
        err = -ENOMEM;
        goto err_out;
    }

	ltq_mtd->map->phys = ltq_mtd->res->start;
	ltq_mtd->map->size = resource_size(ltq_mtd->res);

	ltq_mtd->map->virt = devm_ioremap_resource(&pdev->dev, ltq_mtd->res);
	if (IS_ERR(ltq_mtd->map->virt)) {
		err = PTR_ERR(ltq_mtd->map->virt);
		goto err_free;
	}

    ltq_mtd_hwinit(ltq_mtd);

	ltq_mtd->map->name = pdev->name;
	ltq_mtd->map->bankwidth = pdata->width;

	ltq_mtd->map->read = ltq_read16;
	ltq_mtd->map->write = ltq_write16;
	ltq_mtd->map->copy_from = ltq_copy_from;
	ltq_mtd->map->copy_to = ltq_copy_to;

	ltq_mtd->map->map_priv_1 = LTQ_NOR_PROBING;
	ltq_mtd->mtd = do_map_probe("cfi_probe", ltq_mtd->map);
	ltq_mtd->map->map_priv_1 = LTQ_NOR_NORMAL;

	if (!ltq_mtd->mtd) {
		dev_err(&pdev->dev, "probing failed\n");
		err = -ENXIO;
		goto err_free;
	}

	ltq_mtd->mtd->owner = THIS_MODULE;

	cfi = ltq_mtd->map->fldrv_priv;
	cfi->addr_unlock1 ^= 1;
	cfi->addr_unlock2 ^= 1;

    if (pdata->part_probe_types) {
        err = mtd_device_parse_register(ltq_mtd->mtd, pdata->part_probe_types, NULL, NULL, 0);
    } else {
	    ppdata.of_node = pdev->dev.of_node;
	    err = mtd_device_parse_register(ltq_mtd->mtd, ltq_probe_types, &ppdata, NULL, 0);
    }
	if (err) {
		dev_err(&pdev->dev, "failed to add partitions\n");
		goto err_destroy;
	}

	return 0;

err_destroy:
	map_destroy(ltq_mtd->mtd);
err_free:
	kfree(ltq_mtd->map);
err_out:
	kfree(ltq_mtd);
	return err;
}

static int
ltq_mtd_remove(struct platform_device *pdev)
{
	struct ltq_mtd *ltq_mtd = platform_get_drvdata(pdev);

	if (ltq_mtd) {
		if (ltq_mtd->mtd) {
			mtd_device_unregister(ltq_mtd->mtd);
			map_destroy(ltq_mtd->mtd);
		}
		kfree(ltq_mtd->map);
		kfree(ltq_mtd);
	}
	return 0;
}

static const struct of_device_id ltq_mtd_match[] = {
	{ .compatible = "lantiq,nor" },
	{},
};
MODULE_DEVICE_TABLE(of, ltq_mtd_match);

static struct platform_driver ltq_mtd_driver = {
	.probe = ltq_mtd_probe,
	.remove = ltq_mtd_remove,
	.driver = {
		.name = "ltq-nor",
		.owner = THIS_MODULE,
		.of_match_table = ltq_mtd_match,
	},
};

module_platform_driver(ltq_mtd_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("John Crispin <blogic@openwrt.org>");
MODULE_DESCRIPTION("Lantiq SoC NOR");
