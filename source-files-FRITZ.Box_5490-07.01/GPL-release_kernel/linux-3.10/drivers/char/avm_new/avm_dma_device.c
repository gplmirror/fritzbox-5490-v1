  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#include <linux/module.h>
#include <linux/err.h>
#include <linux/timer.h>
#include <linux/jiffies.h>
#include <linux/rtc.h>
#include <linux/rtc-avm.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include <linux/dma-mapping.h>

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void dev_avm_dma_release(struct device *dev __maybe_unused) {
	return;
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static struct platform_device avm_dma_platform_device = {
	.name              =  "avm_dma",
	.id                = 0,
	.num_resources     = 0,
	.dev  = {
        .coherent_dma_mask = DMA_BIT_MASK(32),
        .release           = dev_avm_dma_release,
    }
};
struct device *avm_dma_device = &avm_dma_platform_device.dev;
EXPORT_SYMBOL(avm_dma_device);

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int __init avm_device_init(void) {

	if (platform_device_register(&avm_dma_platform_device)) {
		return -ENODEV;
	}
	return 0;
}
late_initcall(avm_device_init);
