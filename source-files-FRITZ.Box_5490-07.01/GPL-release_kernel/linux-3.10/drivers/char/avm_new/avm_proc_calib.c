#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/vmalloc.h>
#include <linux/simple_proc.h>
#include <linux/env.h>

#define CALIB_MAX_SIZE (1<<16) 

static struct proc_dir_entry *calibprocdir;

#define PROC_CALIBDIR   "avm/calib"


#define DBG_TRC(args...) 
/*--- #define DBG_TRC(args...) pr_info(args) ---*/
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static struct _calib_list {
    enum wlan_dect_type type;
    const char *proc_device;
}calib_list[] = {
    { .type = WLAN,   .proc_device = PROC_CALIBDIR"/wlan"   },
    { .type = WLAN2,  .proc_device = PROC_CALIBDIR"/wlan2"  },
#if defined(AVM_CALIB_WLAN3)
    { .type = WLAN3,  .proc_device = PROC_CALIBDIR"/wlan3"  },
#endif
    { .type = DECT,   .proc_device = PROC_CALIBDIR"/dect"   },
    { .type = DOCSIS, .proc_device = PROC_CALIBDIR"/docsis" },
    { .type = DSL,    .proc_device = PROC_CALIBDIR"/dsl"    },
};
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void lproc_calib_read(struct seq_file *seq, void *priv){
    struct wlan_dect_config  *pconfig;
    unsigned int i;
    struct _calib_list *pcalib_entry = (struct _calib_list *)priv;
    int result;
    char *testbuf;

    testbuf = vmalloc(CALIB_MAX_SIZE);
    if(testbuf == NULL) {
        pr_err("%s: out of memory\n", __func__);
        return;
    }
    result = get_wlan_dect_config(pcalib_entry->type, testbuf, CALIB_MAX_SIZE);
    if(result == 0) {
        pconfig = (struct wlan_dect_config *)testbuf;
        DBG_TRC("%s: Len=%u\n", __func__, pconfig->Len);
        for(i = sizeof(struct wlan_dect_config); i < (pconfig->Len + sizeof(struct wlan_dect_config)); i++) {
            seq_printf(seq, "%c", testbuf[i]);
        }
    }
    vfree(testbuf);
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int __init avm_calib_proc_init(void) {
    unsigned int i;
    int result;
    char *testbuf;

    testbuf = vmalloc(CALIB_MAX_SIZE);
    if(testbuf == NULL) {
        pr_err("%s: out of memory\n", __func__);
        return -1;
    }

    for(i = 0; i < ARRAY_SIZE(calib_list); i++) {
        result = get_wlan_dect_config(calib_list[i].type, testbuf, CALIB_MAX_SIZE);
        if(result == 0) {
            if(calibprocdir == NULL) {
                calibprocdir = proc_mkdir(PROC_CALIBDIR, NULL);
                if(calibprocdir == NULL) {
                    pr_err("%s: can't create %s\n", __func__, PROC_CALIBDIR);
                    break;
                }
            }
            DBG_TRC("%s: %s\n", __func__, calib_list[i].proc_device);
            add_simple_proc_file(calib_list[i].proc_device, NULL, lproc_calib_read, &calib_list[i]);
        } else {
            DBG_TRC("%s: type=%u %s not found result=%d\n", __func__, calib_list[i].type, 
                                                                      calib_list[i].proc_device, 
                                                                      result);
        }
    }
    vfree(testbuf);
    return 0;
}
late_initcall(avm_calib_proc_init);
