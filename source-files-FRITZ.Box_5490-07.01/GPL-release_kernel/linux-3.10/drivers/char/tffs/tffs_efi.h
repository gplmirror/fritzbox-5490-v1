/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2014 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifndef _tffs_efi_h_
#define _tffs_efi_h_

#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/zlib.h>
#include <linux/tffs.h>
#include <linux/nls.h>
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/

#define MAX_NAME_LEN    0x100UL
#define MAX_VAR_SIZE    0x100UL

struct TFFS_EFI_State {
    enum _tffs_id id;
    char readbuf[MAX_VAR_SIZE + 1];
    struct efivar_entry entry;
    loff_t start;
    size_t len;
    size_t offset;
};

#define PANIC_MODE_BIT   0
#define PANIC_MODE       (1 << PANIC_MODE_BIT)

struct tffs_efi_ctx {
    struct nls_table        *nls_tbl;
    void                    *notify_priv;
    tffs3_notify_fn         notify_cb;
};
#define EFI_TFFS_TOKENSPACE \
    EFI_GUID( 0x692cccd4, 0xd3b9, 0x5e94, 0xba, 0x7e, 0x50, 0xaa, 0xac, 0xd8, 0xa3, 0x8d )
//{ 0x692cccd4, 0xd3b9, 0x5e94, {0xba, 0x7e, 0x50, 0xaa, 0xac, 0xd8, 0xa3, 0x8d }}
#endif /*--- #ifndef _tffs_efi_h_ ---*/
