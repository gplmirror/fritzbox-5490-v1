#undef AVM_PA_NO_REPORT_FUNCTION
/*
 * Packet Accelerator Interface - Hardware Support definitions
 *
 * vim:set expandtab shiftwidth=3 softtabstop=3:
 *
 * Copyright (c) 2011-2015 AVM GmbH <info@avm.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions, and the following disclaimer,
 *    without modification.
 * 2. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * Alternatively, this software may be distributed and/or modified under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _LINUX_AVM_PA_HW_H
#define _LINUX_AVM_PA_HW_H

/* ------------------------------------------------------------------------ */

#include <linux/version.h>
#include <linux/list.h>

#define AVM_PA_HW_FEATURE_DSLITE    0x00000001
#define AVM_PA_HW_FEATURE_6TO4      0x00000002
#define AVM_PA_HW_FEATURE_L2TP      0x00000004
#define AVM_PA_HW_FEATURE_GRE       0x00000008

#define ALLOC_VIRT_DEV_FAILED         -1
#define FREE_VIRT_DEV_FAILED          -1

void avm_pa_rx_channel_suspend(avm_pid_handle pid_handle);
void avm_pa_rx_channel_resume(avm_pid_handle pid_handle);
void avm_pa_rx_channel_packet_not_accelerated(avm_pid_handle pid_handle, struct sk_buff *skb);
void avm_pa_tx_channel_accelerated_packet(avm_pid_handle pid_handle, avm_session_handle session_handle, struct sk_buff *skb);

struct avm_pa_virt_rx_dev {
   avm_pid_handle   pid_handle;
   /* 3 bytes hole */

   /* HW Accelerator private */
   unsigned long  hw_dev_id;
   unsigned long  hw_pkt_try_to_acc;
   unsigned long  hw_pkt_try_to_acc_dropped;
   unsigned long  hw_pkt_try_to_acc_virt_chan_not_ready;
   unsigned long  hw_pkt_slow_cnt;
};

struct avm_pa_virt_tx_dev {
   avm_pid_handle   pid_handle;
   /* 3 bytes hole */

   /* HW Accelerator private */
   unsigned long    hw_dev_id;
   unsigned long    hw_pkt_tx;
   unsigned long    hw_pkt_tx_session_lookup_failed;
};

struct avm_hardware_pa {
   unsigned long features;
   int (*add_session)( struct avm_pa_session *avm_session );
   int (*change_session)( struct avm_pa_session *avm_session );
   int (*remove_session)( struct avm_pa_session *avm_session );
   const char *(*session_state)( struct avm_pa_session *avm_session );

   /* virtual device handling */
   int  (*alloc_rx_channel)(avm_pid_handle pid_handle);
   int  (*alloc_tx_channel)(avm_pid_handle pid_handle);
   int  (*free_rx_channel)(avm_pid_handle pid_handle);
   int  (*free_tx_channel)(avm_pid_handle pid_handle);
   int  (*try_to_accelerate)(avm_pid_handle pid_handle, struct sk_buff *skb);

   /* telephon state */
   void (*telephony_state)(int active);

   /* hardware statistic, values since last call */
#define AVM_PA_HARDWARE_PA_HAS_SESSION_STATS
   int  (*session_stats)(struct avm_pa_session *avm_session,
                         struct avm_pa_session_stats *ingress);
};

void avm_pa_register_hardware_pa( struct avm_hardware_pa *pa_functions );

void avm_pa_hardware_session_report( avm_session_handle session_id,
                                     u32 pkts, u64 bytes );

static inline void *avm_pa_get_hw_session( struct avm_pa_session *session)
{
   smp_rmb();
   return session->hw_session;
}

static inline void avm_pa_set_hw_session( struct avm_pa_session *session,
                                    void *hw_session)
{
   session->hw_session = hw_session;
   smp_wmb();
}

/* ------------------------------------------------------------------------ */

#endif /* _LINUX_AVM_PA_HW_H */
