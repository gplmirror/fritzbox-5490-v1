#####################################################################################
#
#   vim: noexpandtab
#
#####################################################################################
# Quick&dirty libcapi20 Makefile
# (... until capi4k-utils can be cross-compiled)

# First, the environment...
#
CC		= $(CROSS_COMPILE)gcc
LD		= $(CROSS_COMPILE)ld
AR		= $(CROSS_COMPILE)ar
STRIP	= $(CROSS_COMPILE)strip
RANLIB	= $(CROSS_COMPILE)ranlib
RM      = rm

# Second: Definitions, options and the like...
#
ifeq ($(GDB_DEBUG),1)
DEBUG_FLAGS    := -g -ggdb -G 0 -DGDB -O1
endif

CAPI20VMAJOR   := 3
CAPI20VTOTAL   := $(CAPI20VMAJOR).0.4
CAPI20DEFS	   := -DPACKAGE=\"libcapi20\" -DVERSION=\"$(CAPI20VERS)\" -DHAVE_DLFCN_H=1 -DSTDC_HEADERS=1 \
				  -DHAVE_FCNTL_H=1 -DHAVE_SYS_IOCTL_H=1 -DHAVE_UNISTD_H=1 -DHAVE_LINUX_CAPI_H=1 \
				  -DHAVE_SELECT=1 
CAPI20OPTS	   := 
CAPI20INCS	   := -I. -I.
CAPI20NAME	   := libcapi20
CAPI20A		   := $(CAPI20NAME).a
CAPI20SO	   := $(CAPI20NAME).so
CAPI20SO_VM	   := $(CAPI20SO).$(CAPI20VMAJOR)
CAPI20SO_VT	   := $(CAPI20SO).$(CAPI20VTOTAL)

CFLAGS		   += $(CAPI20DEFS) $(CAPI20OPTS) $(CAPI20INCS) $(DEBUG_FLAGS) $(EXTRA_INCLUDE_FLAGS) -Wno-unused-parameter #-fdiagnostics-show-option

CAPI20SRCS	   := capi20.c capifunc.c convert.c
CAPI20OBJS	   := $(patsubst %.c,%.o,$(CAPI20SRCS))
CAPI20LOBJS	   := $(patsubst %.c,%.lo,$(CAPI20SRCS))

CAPI20_SOCK_NAME := libcapi20_remote
CAPI20_SOCK_A := $(CAPI20_SOCK_NAME).a
CAPI20_SOCK_SO := $(CAPI20_SOCK_NAME).so
CAPI20_SOCK_SO_VM := $(CAPI20_SOCK_SO).$(CAPI20VMAJOR)
CAPI20_SOCK_SO_VT := $(CAPI20_SOCK_SO).$(CAPI20VTOTAL)
CAPI20_SOCK_SRCS := capi20_sock.c capifunc.c convert.c
CAPI20_SOCK_OBJS := $(patsubst %.c,%.o,$(CAPI20_SOCK_SRCS))
CAPI20_SOCK_LOBJS := $(patsubst %.c,%.lo,$(CAPI20_SOCK_SRCS))

##################################################################################################
# The rules...
##################################################################################################
all:	$(CAPI20NAME)dyn.a $(CAPI20A) $(CAPI20SO) $(CAPI20_SOCK_A) $(CAPI20_SOCK_SO)
ifeq ($(GDB_DEBUG),1)
		echo "Erzeuge eine Debug Version der CapiLib !"
else
		$(STRIP) -s $(CAPI20SO_VT)
		$(STRIP) -s $(CAPI20_SOCK_SO_VT)
endif

##################################################################################################
$(CAPI20NAME)dyn.a:	capidyn.o
	rm -f $@
	$(AR) cru $@ $<
	$(RANLIB) $@

##################################################################################################
$(CAPI20A):	$(CAPI20OBJS)
	$(AR) cru $(CAPI20A) $(CAPI20OBJS) 
	$(RANLIB) $(CAPI20A)

##################################################################################################
$(CAPI20SO):	$(CAPI20LOBJS)
	$(CC) $(DEFAULT_LDFLAGS_LIB) $(DEBUG_FLAGS) -shared $(CAPI20LOBJS) -Wl,-soname -Wl,$(CAPI20SO_VM) -o $(CAPI20SO_VT)
	rm -f $(CAPI20SO_VM) && ln -s $(CAPI20SO_VT) $(CAPI20SO_VM)
	rm -f $(CAPI20SO)    && ln -s $(CAPI20SO_VT) $(CAPI20SO)

##################################################################################################
$(CAPI20_SOCK_A):	$(CAPI20_SOCK_OBJS)
	$(AR) cru $(CAPI20_SOCK_A) $(CAPI20_SOCK_OBJS) 
	$(RANLIB) $(CAPI20_SOCK_A)

##################################################################################################
$(CAPI20_SOCK_SO):	$(CAPI20_SOCK_LOBJS)
	$(CC) $(DEFAULT_LDFLAGS_LIB) $(DEBUG_FLAGS) -shared $(CAPI20_SOCK_LOBJS) -Wl,-soname -Wl,$(CAPI20SO_VM) -o $(CAPI20_SOCK_SO_VT)
	rm -f $(CAPI20_SOCK_SO_VM) && ln -s $(CAPI20_SOCK_SO_VT) $(CAPI20_SOCK_SO_VM)
	rm -f $(CAPI20_SOCK_SO)    && ln -s $(CAPI20_SOCK_SO_VT) $(CAPI20_SOCK_SO)

##################################################################################################
%.lo:		%.c
	$(CC) $(DEFAULT_CFLAGS_LIB) $(CFLAGS) -c $< -o $@ 

%.o:		%.c
	$(CC) $(DEFAULT_CFLAGS_SRC) $(CFLAGS) -c $< -o $@ 

##################################################################################################
depend: 
	cat /dev/null >depend.mak
	for i in $(CAPI20SRCS) ; do $(CC) $(CFLAGS) $(C_LOCAL) $(C_INCL) -M $$i >>depend.mak ; done

##################################################################################################
install:	$(CAPI20A) $(CAPI20SO) $(CAPI20_SOCK_A) $(CAPI20_SOCK_SO) $(CAPI20NAME)dyn.a
ifneq ($(GDB_DEBUG),1)
	$(STRIP) -s $(CAPI20SO_VT)
	$(STRIP) -s $(CAPI20_SOCK_SO_VT)
endif
	if [ ! -d $(FILESYSTEM)/lib ] ; then mkdir -p $(FILESYSTEM)/lib ; chmod 777 $(FILESYSTEM)/lib ; fi
	if [ ! -d $(FILESYSTEM)/include ] ; then mkdir -p $(FILESYSTEM)/include ; chmod 777 $(FILESYSTEM)/include ; fi
#	cp $(CAPI20SO) $(CAPI20SO_VM) $(CAPI20SO_VT) $(FILESYSTEM)/lib

	cp -f capi20.h capiutils.h capicmd.h $(FILESYSTEM)/include
	cp -f $(CAPI20SO_VT) $(FILESYSTEM)/lib
	cp -f $(CAPI20A) $(FILESYSTEM)/lib
	ln -fs $(CAPI20SO_VT) $(FILESYSTEM)/lib/$(CAPI20SO_VM)
	ln -fs $(CAPI20SO_VT) $(FILESYSTEM)/lib/$(CAPI20SO)
	cp -f $(CAPI20_SOCK_SO_VT) $(FILESYSTEM)/lib
	cp -f $(CAPI20_SOCK_A) $(FILESYSTEM)/lib
	ln -fs $(CAPI20_SOCK_SO_VT) $(FILESYSTEM)/lib/$(CAPI20_SOCK_SO_VM)
	ln -fs $(CAPI20_SOCK_SO_VT) $(FILESYSTEM)/lib/$(CAPI20_SOCK_SO)

##################################################################################################
clean:		
	$(RM) -f $(CAPI20OBJS) $(CAPI20LOBJS) capidyn.o $(CAPI20NAME)* 
	$(RM) -f $(CAPI20SO) $(CAPI20SO_VM) $(CAPI20SO_VT) $(CAPI20A) $(CAPI20_SOCK_A)
	$(RM) -f *.lo *.o

###################################################################################################################
-include depend.mak

###########################################################################################################
#
#   vim: noexpandtab
#
###########################################################################################################
